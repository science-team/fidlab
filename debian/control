Source: fidlab
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Apostol Faliagas <apostol.faliagas@gmail.com>
Build-Depends: debhelper (>=11), meson
Standards-Version: 4.2.1
Homepage: https://fidlab-numerics.bitbucket.io

Package: fidlab
Architecture: any
Multi-Arch: foreign
Depends: libfidlab2 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: Finite difference method for partial differential equations: binary package
 Fidlab is a library which can be used to generate parallel
 and nonparallel algorithms for the discretization and
 solution of partial differential equations, integrodifferential
 equations and other linear and nonlinear boundary value
 problems of local and nonlocal type.
 .
 This package provides utilities and examples.

Package: libfidlab2
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Finite difference method for partial differential equations: library package
 This package provides the shared libraries containing the
 C++ classes that a driver program must instantiate, and
 possibly customize, to generate code for the discretization
 and solution of a problem. To compile the generated code you
 also need GSL or PETSc.

Package: libfidlab-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libfidlab2 (= ${binary:Version}), ${misc:Depends}
Description: Finite difference method for partial differential equations: development package
 This package contains the development files, i.e., the
 header files, static libraries, scripts, and symbolic links
 that developers using fidlab will need.
