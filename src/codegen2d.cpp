#include <iostream>
#include <cstdlib>
#include <cmath>
// #include <ctime>
#include "pde2d.hpp"
#include "config_code.h"

using std::string;
using std::vector;
using fd::Boundary; using fd::NeumannBC;

namespace fd2 {

static inline
string code_elem_lhs(const string& f, const string& i, const string& j,
                     const string& c)
{ return f + "(" + i + "," + j + (c.empty() ? string("") : "," + c) + ")"; }

// =============================================================================
// PDE
// =============================================================================

// -----------------------------------------------------------------------------
// Code Generators
// -----------------------------------------------------------------------------

PDE::VertexSides PDE::get_vertex_bids(const PDE::VertexSides& s, int vertex,
                                      const vector<vector<int>>& bs)
{
  switch (vertex)
  {
    case Mesh::BL: return {
      (s.primary   == Mesh::B) ? bs[Mesh::B].front() : bs[Mesh::L].back(),
      (s.secondary == Mesh::B) ? bs[Mesh::B].front() : bs[Mesh::L].back() };
    case Mesh::BR: return {
      (s.primary   == Mesh::B) ? bs[Mesh::B].back() : bs[Mesh::R].front(),
      (s.secondary == Mesh::B) ? bs[Mesh::B].back() : bs[Mesh::R].front() };
    case Mesh::TR: return {
      (s.primary   == Mesh::T) ? bs[Mesh::T].front() : bs[Mesh::R].back(),
      (s.secondary == Mesh::T) ? bs[Mesh::T].front() : bs[Mesh::R].back() };
    case Mesh::TL: return {
      (s.primary   == Mesh::L) ? bs[Mesh::L].front() : bs[Mesh::T].back(),
      (s.secondary == Mesh::L) ? bs[Mesh::L].front() : bs[Mesh::T].back() };
    default: THROW("incorrect vertex.");
  }
  return {-1, -1};
}

void PDE::generate_code_function_body(int what)
{
  const vector<vector<int>> bs = mesh_->get_bids_by_side();
  const std::array<int,4> boundaries_on_side{
      (int) bs[Mesh::R].size(),
      (int) bs[Mesh::T].size(),
      (int) bs[Mesh::L].size(),
      (int) bs[Mesh::B].size()};

  // ---------------------------------------------------------------------------
  // Generate function body
  // ---------------------------------------------------------------------------

  code_ << R"(
{
	for (int i = 0; i < nx; ++i)
	{
		for (int j = 0; j < ny; ++j)
		{)";

  const int nc = components();
  const auto bc_selection_table = get_bc_selection_table();

  // ---------------------------------------------------------------------------
  auto generate_code_vertex = [this, what, nc, &bc_selection_table, &bs](
      int vertex, int ntabs)
  {
    for (int c = 0; c < nc; ++c)
    {
      const auto& sides = bc_selection_table[c][vertex];
      const auto bids = get_vertex_bids(sides, vertex, bs);
      generate_code_for_OLD(c, bids.primary, bids.secondary, what, ntabs);
    }
  }; //-------------------------------------------------------------------------
  auto generate_code_side_1 = [this, what, nc](int bid, int ntabs)
  {
    for (int c = 0; c < nc; ++c)
      if (boundary_discretization_method_ == SYM_DIFF2)
        generate_code_SYM_DIFF2_for(c, bid, what, ntabs, ALL_BC_CODES_2D, [this](
            const string& i, const string& j, const string& k, const string& c) {
                return format_elem_lhs("e", i, j, k, c);
          });
      else
        generate_code_for(c, bid, what, ntabs);
  }; //-------------------------------------------------------------------------
  auto generate_code_side = [this, &bs, generate_code_side_1](int sid, int ntabs)
  {
    const auto& boundaries = bs[sid];
    if (boundaries.size() == 1)
    { generate_code_side_1(boundaries[0], ntabs); return; }
    const string tabs(ntabs+1, '\t');
    for (const int bid : boundaries)
    {
      const Boundary *bd = mesh_->boundary(bid);
      const bool direc = (sid == Mesh::R || sid == Mesh::B);
      code_ << "if (" << (bd->type == Boundary::X ? "Y(j) " : "X(i) ")
            << (direc ? '<' : '>') << " boundary_["
            << bid << "][" << int(direc) << "])";
      code_ << R"(
				{)";
      generate_code_side_1(bid, ntabs);
      if (bid != boundaries.back()) code_ << R"(
				}
				else )";
    }
  }; //-------------------------------------------------------------------------

  if (!generate_code_function_body_replace_eqs(what, 2)) code_ << R"(
			)";

  code_ << R"(if (i == 0)
			{
				if (j == 0)
				{)";
  generate_code_vertex(Mesh::BL, 3);
  code_ << R"(
				}
				else if (j == ny-1)
				{)";
  generate_code_vertex(Mesh::TL, 3);
  code_ << R"(
				}
				else)"<<(boundaries_on_side[Mesh::L] == 1 ? R"(
				{)" : " ");
  generate_code_side(Mesh::L, 3);
  code_ << R"(
				}
			}
			else if (i == nx-1)
			{
				if (j == 0)
				{)";
  generate_code_vertex(Mesh::BR, 3);
  code_ << R"(
				}
				else if (j == ny-1)
				{)";
  generate_code_vertex(Mesh::TR, 3);
  code_ << R"(
				}
				else)"<<(boundaries_on_side[Mesh::R] == 1 ? R"(
				{)" : " ");
  generate_code_side(Mesh::R, 3);
  code_ << R"(
				}
			}
			else if (j == 0)
			{)";
  if (boundaries_on_side[Mesh::B] > 1) code_ << R"(
				)";
  generate_code_side(Mesh::B, 2+int(boundaries_on_side[Mesh::B] > 1));
  if (boundaries_on_side[Mesh::B] > 1) code_ << R"(
				})";
  code_ << R"(
			}
			else if (j == ny-1)
			{)";
  if (boundaries_on_side[Mesh::T] > 1) code_ << R"(
				)";
  generate_code_side(Mesh::T, 2+int(boundaries_on_side[Mesh::T] > 1));
  if (boundaries_on_side[Mesh::T] > 1) code_ << R"(
				})";
  code_ << R"(
			}
			else
			{)";

  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
				// Field component )" << c;
    const string findex = fd::to_string_empty_if(nc == 1, c);
    // const string eqno = code_elem_lhs("e", i_, j_, findex);
    const string eqno = format_elem_lhs(findex);
    if (what == GENERATE_FUNCTION_CODE)
      code_ << R"(
				F[)" << eqno << "] = "<< equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE)
      code_ << R"(
				J[)" << eqno << "] = "<< equation_codes_[c].text_j << ";";
    else
      code_ << R"(
				F[)" << eqno << "] = "<< equation_codes_[c].text_y << R"(;
				J[)" << eqno << "] = "<< equation_codes_[c].text_j << ";";
    // if (c < nc - 1)
      // code_ << std::endl;
  }

  code_ << R"(
			}
		}
	})";

  // Generate code for virtual corners -- active for SYM_DIFF2 code generation
  const int i_extr[2] = {-1, mesh_->nx + 1};
  const int j_extr[2] = {-1, mesh_->ny + 1};
  const string I[2] = {"-1", "nx"};
  const string J[2] = {"-1", "ny"};

  for (int c = 0; c < nc; ++c)
  {
    if (!(field_[c].i_start == i_extr[0] || field_[c].i_end == i_extr[1]) ||
        !(field_[c].j_start == j_extr[0] || field_[c].j_end == j_extr[1]))
      continue;
    string findex = fd::to_string_empty_if(nc == 1, c);
    if (nc > 1) code_ << R"(
	// Field component )" << c;
    for (int i = 0; i < 2; ++i)
    {
      const int i_start_end = (i == 0) ? field_[c].i_start : field_[c].i_end;
      if (i_start_end != i_extr[i]) continue;
      for (int j = 0; j < 2; ++j)
      {
        const int j_start_end = (j == 0) ? field_[c].j_start : field_[c].j_end;
        if (j_start_end != j_extr[j]) continue;
        const string eqnum = format_elem_lhs("e", I[i], J[j], "", findex);
        if (what == GENERATE_FUNCTION_CODE) code_ << R"(
	F[)"<<eqnum<<"]=u ["<<eqnum<<"];";
				else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
	J[)"<<eqnum<<"]=du["<<eqnum<<"];";
				else code_ << R"(
	F[)"<<eqnum<<"]=u ["<<eqnum<<R"(];
	J[)"<<eqnum<<"]=du["<<eqnum<<"];";
      }
    }
    if (c < nc - 1)
      code_ << std::endl;
  }

  generate_code_function_body_replace_eqs_ns(what);

  code_ << R"(
}
)";
}

// -------------------------------------------------------------------------- //
// ===========================  Main File Generator  ======================== //
// -------------------------------------------------------------------------- //

string PDE::format_constructor_comment() const
{
  return "// Discretize PDE " + _S(components() == 1 ? "" : "system ") +
  "on a "+fd::to_string(mesh_->nx)+" x "+fd::to_string(mesh_->ny)+R"( grid.
	// Replace default constructor with pde(m,n) for a m x n grid.)";
}

string PDE::format_solution_output_node(int c) const
{ return "i,j" + (components() == 1 ? _S"" : ","+fd::to_string(c)); }

string PDE::format_solution_output_coord(int c) const
{ return (components() == 1 ? _S"" : fd::to_string(c)+",")+"pde.X(i),pde.Y(j)"; }

string PDE::format_coord() const
{ return _S(components() == 1 ? "" : "c,")+"pde.X(i),pde.Y(j)"; }

// -------------------------------------------------------------------------- //
// =====================  Header (Interface) Generator  ===================== //
// -------------------------------------------------------------------------- //

void PDE::generate_header_constructors()
{
  code_h_ << R"(

	//! Default constructor.
	)"<<name<<R"(() :
		nx()" << mesh_->nx << "), ny(" << mesh_->ny << R"(),
		a()"  << mesh_->a  << "), b("  << mesh_->b  << R"(),
		c()"  << mesh_->c  << "), d("  << mesh_->d  << R"(),
		output_results(true) {init();}
	//! Construct mesh with specified number of nodes.
	)"<<name<<R"((int Nx, int Ny) :
		nx(Nx), ny(Ny),
		a()" << mesh_->a << "), b(" << mesh_->b << ")," << R"(
		c()" << mesh_->c << "), d(" << mesh_->d << R"(),
		output_results(true) {init();}
	//! Construct mesh with specified number of nodes and solution domain.
	)"<<name<<R"((int Nx, int Ny, double x0, double x1, double y0, double y1) :
		nx(Nx), ny(Ny), a(x0), b(x1), c(y0), d(y1), output_results(true) {init();}
)";
}

void PDE::generate_header_extra()
{
  if (!mesh_->simple())
  {
    const int sz = mesh_->num_boundaries();
    code_h_ << R"(

private:

	const double boundaries_[)" << sz << "][2]{";
    for (int i = 0; i < sz; ++i)
    {
      const auto interv = mesh_->get_boundary_extremities(i);
      code_h_ << R"(
		{)" << interv[0] << ", " << interv[1] << "}" << (i == sz - 1 ? "" : ",");
    }
    code_h_ << R"(
	};)";
  }
}

} // namespace fd2
