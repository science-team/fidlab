#include <iostream>
#include <cstdlib>
#include <regex>
#include <cmath>
#include <ctime>
#include <sstream>
#include "mesh.hpp"
#include "pde.hpp"
#include "config_code.h"
#include "global_config.h"

using std::string;
using std::vector;

namespace fd {

PDECommon::~PDECommon()
{
  for (BoundaryCondition *p : bc_) delete p;
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::  Utility functions  :::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

std::map<const std::string, int>& PDECommon::get_dict_for_source(int which)
{
  if (which == HEADER_FILE) return extra_headers_def_;
  if (which == IMP_FILE) return extra_headers_imp_;
  return extra_headers_main_;
}

string PDECommon::format_space_args(const string& i, const string& j,
                                    const string& k) const
{
  const int dim = dimension();
  string s = "X(" + i;
  if (dim > 1) s += "),Y(" + j;
  if (dim > 2) s += "),Z(" + k;
  return s + ")";
}

string PDECommon::format_function_call(const string& f, const string& i,
        const string& j, const string& k, const string& c, bool prepend) const
{
  const string fi = c.empty() ? _S"" : (prepend? (c+",") : (","+c));
  return prepend ? f + "(" + fi + format_space_args(i, j, k) + ")"
                 : f + "(" + format_space_args(i, j, k) + fi + ")";
}

string PDECommon::code_elem_func(const string& f, const string& i,
    const string& j, const string& k, const string& c) const
{
  const int dim = dimension();
  string s = f + "(" + i;
  if (dim > 1) s += "," + j;
  if (dim > 2) s += "," + k;
  return s + "$COMMA$" + c + ")";
}

string PDECommon::format_node_function_call(const string& f, const string& i,
    const string& j, const string& k, const string& c) const
{
  const int dim = dimension();
  string s = f + "(" + i;
  if (dim > 1) s += "," + j;
  if (dim > 2) s += "," + k;
  return s + (c.empty() ? string("") : "," + c) + ")";
}

string PDECommon::format_sid(Boundary *b) const
{
  switch (get_sid(b))
  {
    case MeshCommon::R: return "R";
    case MeshCommon::T: return "T";
    case MeshCommon::L: return "L";
    case MeshCommon::B: return "B";
    case MeshCommon::U: return "U";
    case MeshCommon::D: return "D";
  }
  return "?";
}

string PDECommon::code_field_index(const string& i, const string& j,
                                   const string& k) const
{ return code_elem_func("e", i, j, k, "$c$"); }

string PDECommon::format_mesh_params(int param) const
{
  static const string params[3][3]{
    {"nx", "a, b", "hx"},
    {"ny", "c, d", "hy"},
    {"nz", "p, q", "hz"}
  };
  const int dim = dimension();
  string s = params[0][param];
  if (dim > 1) s += ", " + params[1][param];
  if (dim > 2) s += ", " + params[2][param];
  return s;
}

void PDECommon::code_extra_headers(int which, int placement, bool endl_at_end)
{
  std::map<const string, int>& dict = get_dict_for_source(which);
  std::ofstream& o = (which == HEADER_FILE) ? code_h_ : code_;

  for (auto e : dict)
    if (e.second == placement)
    {
      const char *qm = e.first[0] == '<' ? "" : "\"";
      if (endl_at_end) o << "#include " << qm << e.first << qm << std::endl;
      else             o << std::endl << "#include " << qm << e.first << qm;
    }
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::  File/Directory Manipulation  :::::::::::::::::::::://
//----------------------------------------------------------------------------//

string PDECommon::strip_path(const string& fname)
{
  auto pos = fname.rfind('/');
  return pos == string::npos ? fname : fname.substr(pos + 1);
}

string PDECommon::get_path(const string& fname)
{
  auto pos = fname.rfind('/');
  return pos == string::npos ? string("") : fname.substr(0, pos + 1);
}

string PDECommon::strip_ext(const string& f)
{ return f.substr(0, f.rfind('.')); }

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::  PDE Properties  :::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

bool PDECommon::linear_BCs() const
{
  for (BoundaryCondition *bc : BCs())
    if (bc->type == BoundaryCondition::CUSTOM &&
    //   !static_cast<CustomBC *>(bc)->codeinfo.linear) return false;
      !((CustomBC *)bc)->expression->linear()) return false;
  return true;
}

bool PDECommon::linear_eqs() const
{
  for (const CodeFragment& cf : equation_codes_)
    if (!cf.linear) return false;
  return true;
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::::  Equations  ::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

PDECommon& PDECommon::Ref::operator=(const Expression& expr)
{
  if (eqname.empty())
  {
    if (eqnum < 0) throw _S"Equation numbers cannot be negative.";
    eqname = fd::to_string(eqnum);
  }
  pde->set_differential_expression(eqname, expr);
  return *pde;
}

PDECommon &PDECommon::set_differential_expression(const string& s, const Expression& expr)
{
  if (fd::find(equation_names_, s))
    throw "Equation " + s + " was already defined.";
  CodeFragment equation_code = expr.generate_code({this, i_, j_, k_});
  link(equation_code);
  equation_codes_.push_back(equation_code);
  equation_names_.push_back(s);
  delete &expr;
  return *this;
}

/*void PDECommon::preset_code(CustomBC *bc)
{
  const int bid = get_bid(bc->boundary());
  // If bid is a global boundary, we use the delayed code generation model.
  if (btype(bid) == Boundary::G) return;
  set_code_fragment(*(CustomBC *)bc, get_sid(bc->boundary()));
}

void PDECommon::set_code_fragment(CustomBC &bc, int sid)
{
  const int bid = get_bid(bc.boundary());
  const int bdm = boundary_discretization_method_;
  const BoundaryDiscretizationData bdd{this, bid, bdm, i_, j_, k_, sid};
  bc.codeinfo = bc.expression->generate_code_boundary(bdd);
//   bc.codeinfo.boundary_discretization_method = bdm;
  link(bc.codeinfo);
//   delete expr;
}*/

CodeFragment PDECommon::get_code_fragment(CustomBC &bc, int sid)
{
  const int bid = get_bid(bc.boundary());
  const int bdm = boundary_discretization_method_;
//   const BoundaryDiscretizationData bdd{this, bid, bdm, i_, j_, k_, sid};
//   return bc.generate_code_fragment(bdm);
  return bc.generate_code_fragment({this, bid, bdm, i_, j_, k_, sid});
}

const string& PDECommon::get_equation_name(int n) const
{
  if (n < 0 || n >= (int) equation_names_.size())
    throw "Incorrect equation number: " + fd::to_string(n) + ".";
  return equation_names_[n];
}

const string PDECommon::get_pde_system_name() const
{
  if (!name.empty()) return name;
  return "noname PDE" + _S(components() == 1 ? "" : " system");
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::  Field Manipulation  :::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::set_field(const Field &fc)
{ field_.set_component(fc); }

void PDECommon::set_fields(std::initializer_list<const Field> l)
{ for (const Field& fc : l) field_.set_component(fc); }

int PDECommon::get_component_index(const Field &fc) const
{ return field_.get_component_index(fc); }

int PDECommon::get_component_index(int cid) const
{ return field_.get_component_index(cid); }

int PDECommon::get_component(int index) const
{ return field_.get_component(index); }

bool PDECommon::find_component(const Field &fc) const
{ return field_.find_component(fc); }

int PDECommon::get_component_subindex(const Field &fc) const
{ return field_.get_component_subindex(fc); }

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::::::::  BCs  :::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::set_BC(const BoundaryCondition &bc, const Field& u)
{
  auto field_description = [this, &bc, &u]() -> string {
    return "field " + u.format_name() + " on boundary " +
        fd::to_string(get_bid(bc.boundary())) + ".";
  };
  const int index = get_component_index(u);
  if (index == -1) throw u.format_name() + " is not a field of " + name + ".";
  if (find_BC(bc, u))
    throw "A boundary condition was already defined for " + field_description();
  switch (bc.type)
  {
    case BoundaryCondition::DIRICHLET: set_Dirichlet_BC(bc, u); break;
    case BoundaryCondition::NEUMANN:   set_Neumann_BC(bc, u);   break;
    case BoundaryCondition::CUSTOM:    set_custom_BC(bc, u);    break;
    default: throw "Incorrect BC specification for " + field_description();
  }
}

void PDECommon::set_BC(const BoundaryCondition &bc)
{
  THROW_IF(field_.size() != 1, "PDE must have exactly one field.");
  set_BC(bc, field_[0]);
}

void PDECommon::set_BCs(std::initializer_list<const BoundaryCondition::Ref> l)
{ for (auto& bcref : l) set_BC(bcref.self); }

void PDECommon::set_BCs(std::initializer_list<const BoundaryCondition::Ref> l,
												const Field& u)
{ for (auto& bcref : l) set_BC(bcref.self, u); }

void PDECommon::set_Dirichlet_BC(const BoundaryCondition &bc, const Field& u)
{
  DirichletBC *dbc = new DirichletBC(static_cast<const DirichletBC&>(bc));
  dbc->set_component(u.id);
  bc_.push_back(dbc);
}

void PDECommon::set_Neumann_BC(const BoundaryCondition &bc, const Field& u)
{
  NeumannBC *nbc = new NeumannBC(static_cast<const NeumannBC&>(bc));
  nbc->set_component(u.id);
  bc_.push_back(nbc);
}

void PDECommon::set_custom_BC(const BoundaryCondition &bc, const Field& u)
{
  ASSERT_MSG(bc.type == BoundaryCondition::CUSTOM, "Expected custom BC.");
  if (static_cast<const CustomBC&>(bc).expression == nullptr)
    throw _S"Custom (user defined) boundary conditions which are undefined "
            "cannot be selected into a PDE.";

  CustomBC *cbc = new CustomBC(static_cast<const CustomBC&>(bc));
  cbc->set_component(u.id);
#if 0 // deprecated
  preset_code(cbc);
#endif // 0
  bc_.push_back(cbc);
}

bool PDECommon::find_BC(const BoundaryCondition& bc, const Field& u) const
{
  for (BoundaryCondition *p : bc_)
    if (p->boundary() == bc.boundary() && p->component() == u.id) return true;
  return false;
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::  Equation Replacement  ::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

PDECommon::EquationReplacement::EquationReplacement(int fi,
      int ic, int jc, int kc, double val,
      const std::string& rv, const std::string& jv, PDECommon *pde) :
    index	       (fi),
    i              (ic),
    j              (jc),
    k              (kc),
    value          (val),
    rhs            (rv),
    jac            (jv),
    pde_           (pde),
    initialized_   (false)
{}

void PDECommon::EquationReplacement::init()
{
  if (initialized_) return;
  THROW_IF(pde_ == nullptr, "null PDE object!");
  initialized_ = true;
  if (!rhs.empty()) return;
  const string fi = fd::to_string_empty_if(pde_->components() == 1, index);
  const string eq = pde_->format_elem_lhs("e",
      fd::to_string(i), fd::to_string(j), fd::to_string(k), fi);
  rhs = "u[" + eq + "]";
  if (!fd::is_zero(value))
  {
    rhs += "-";
    if (value > 0)
      rhs += fd::to_string(value);
    else
      rhs += "("+fd::to_string(value)+")";
  }
  jac = "du[" + eq + "]";
}

void PDECommon::replace_equation(const Field &u, int i, int j, int k,
                                 const string& rhs, const string& jac)
{
  const int index = get_component_index(u);
  THROW_IF(index == -1, u.format_name() + " is not a field of " + name + ".");
  replace_equation(index, i, j, k, 0, rhs, jac);
}

void PDECommon::replace_equation(const Field &u, int i, int j, int k, double rhs)
{
  const int index = get_component_index(u);
  THROW_IF(index == -1, u.format_name() + " is not a field of " + name + ".");
  replace_equation(index, i, j, k, rhs, "", "");
}

void PDECommon::replace_equation(int i, int j, int k,
                                 const string& rhs, const string& jac)
{
  THROW_IF(field_.empty(), "There is no field in the PDE.");
  THROW_IF(field_.size() > 1, "For multidimensional fields use "
      "the overload that accepts a Field as its first argument.");
  replace_equation(0, i, j, k, 0, rhs, jac);
}

void PDECommon::replace_equation(int i, int j, int k, double rhs)
{
  THROW_IF(field_.empty(), "There is no field in the PDE.");
  THROW_IF(field_.size() > 1, "For multidimensional fields use "
      "the overload that accepts a Field as its first argument.");
  replace_equation(0, i, j, k, rhs, "", "");
}

void PDECommon::replace_equation(int index, int i, int j, int k, double rhsval,
                                 const string& rhs, const string& rhs_jac)
{
  const EquationReplacement eqrepl(index, i, j, k, rhsval, rhs, rhs_jac, this);
  const bool present = [this, &eqrepl]() {
    for (const auto& r : replacements_)
      if (r.equation_is_identical_to(eqrepl)) return true;
    return false;
  }();
  if (present) throw _S"The equation for field "
    + field_[index].format_name() + " was already replaced.";
  replacements_.push_back(eqrepl);
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::  Function definition  ::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::set_function(Function& f, const std::string& imp)
{
  const string& fname = f.fname();

  // Check function name
  if (fname.empty())
    throw _S"Function has no name.";
  if (!fd::regex_match_funcname(fname))
    throw fname + " is not a valid function name.";

  // Check if function name is reserved
  if (fd::find(reserved_function_names_, fname))
    throw "The function name \'" + fname + "\' is reserved.";

  // Check if function is already defined:
  if (find_fname_in_cutoff_functions(fname) ||
      find_fname_in_field_functions(fname) ||
      find_fname_in_functions(fname))
    throw "Function " + fname + " is already selected in the PDE.";

  if (!imp.empty())
    f.set_implementation(imp);

  functions_.push_back(&f);
}

void PDECommon::set_function(FieldFunction& f, const vector<string>& imp)
{
  const string& fname = f.fname();

  // Check function name
  if (fname.empty())
    throw _S"Field function has no name.";
  if (!fd::regex_match_funcname(fname))
    throw fname + " is not a valid function name.";

  // Check if function name is reserved
  if (fd::find(reserved_function_names_, fname))
    throw "The function name \'" + fname + "\' is reserved.";

  // Check if function is already defined:
  if (find_fname_in_cutoff_functions(fname) ||
      find_fname_in_field_functions(fname) ||
      find_fname_in_functions(fname))
    throw "Function " + fname + " is already selected in the PDE.";

  if (!imp.empty())
    f.set_implementation(imp);

  field_functions_.push_back(&f);
}

void PDECommon::set_function(CutoffFunction& f, const string& imp)
{
  const string& fname = f.fname();

  // Check function name
  if (fname.empty())
    throw _S"Cutoff function has no name.";
  if (!fd::regex_match_funcname(fname))
    throw fname + " is not a valid function name.";

  // Check if function name is reserved
  if (fd::find(reserved_function_names_, fname))
    throw "The function name \'" + fname + "\' is reserved.";

  // Check if function is already defined:
  if (find_fname_in_cutoff_functions(fname) ||
      find_fname_in_field_functions(fname) ||
      find_fname_in_functions(fname))
    throw "Function " + fname + " is already selected in the PDE.";

  if (!imp.empty())
    f.set_implementation(imp);

  cutoff_functions_.push_back(&f);
}

void PDECommon::set_functions(std::initializer_list<Function::Ref> l)
{
  for (const Function::Ref& ref : l)
    set_function(ref.self);
}

void PDECommon::set_functions(std::initializer_list<FieldFunction::Ref> l)
{
  for (const FieldFunction::Ref& ref : l)
    set_function(ref.self);
}

void PDECommon::set_functions(std::initializer_list<CutoffFunction::Ref> l)
{
  for (const CutoffFunction::Ref& ref : l)
    set_function(ref.self);
}

Function& PDECommon::get_function(const string& func_name) const
{
  for (Function *f : functions_)
    if (f->fname() == func_name) return *f;
  throw "Unable to find function " + func_name + " in PDE " +
    (name.empty() ? _S"noname" : name) + ".";
}

FieldFunction& PDECommon::get_field_function(const string& func_name) const
{
  for (FieldFunction *f : field_functions_)
    if (f->fname() == func_name) return *f;
  throw "Unable to find field function " + func_name + " in PDE " +
    (name.empty() ? _S"noname" : name) + ".";
}

CutoffFunction& PDECommon::get_cutoff_function(const string& func_name) const
{
  for (CutoffFunction *f : cutoff_functions_)
    if (f->fname() == func_name) return *f;
  throw "Unable to find cutoff function " + func_name + " in PDE " +
    (name.empty() ? _S"noname" : name) + ".";
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::  Code generation utilities  :::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::link(string& code) const
{
  if (code.empty()) return;

  bool single_component = components() == 1;

  // Fix commas
  fd::replace_all(code, "$COMMA$", single_component ? "" : ",");

  // Build list if referenced fields ids
  std::vector<int> refs;
  std::smatch sm;
  const string rex("\\$c\\_[1-9][0-9]*\\$");
  auto it = code.cbegin();
  while (std::regex_search(it, code.cend(), sm, std::regex(rex)))
  {
    int id;
    try {id = std::stoi(sm.str(0).substr(3, sm.length(0) - 4));}
    catch (std::invalid_argument& err) {
      throw "Incorrectly formatted link: " + sm.str(0); }
    if (!fd::find(refs, id))
      refs.push_back(id);
    it += sm.position(0) + sm.length(0);
  }

  for (int id : refs)
  {
    // Check if ref is a valid field id selected in the PDE:
    const int cindex = get_component_index(id);
    if (cindex == -1)
    {
      const string global_field_name = Field::find_global_id(id);
      if (global_field_name.empty())
        throw _S"Internal error 0012. An unregistered field (#" +
          fd::to_string(id) + ") was encountered. Please report this error.";
      throw "The field " + global_field_name + " does not belong to the PDE " +
        _S(components() > 1 ? "system " : "") + name + ".";
    }
    // Fix reference:
    const string sym = "$c_" + fd::to_string(id) + "$";
    const string val = fd::to_string_empty_if(single_component, cindex);
    fd::replace_all(code, sym, val);
  }

  // Check linked code
  if (code.find('$') != string::npos) {
    DEBUG_LOG("Generated code is possibly incorrect!");
  } // Do not remove braces!
}

static constexpr int NBS = sizeof(CodeFragment::text_bx_y)/
                           sizeof(CodeFragment::text_bx_y[0]);
static constexpr int NVS = sizeof(CodeFragment::text_vx_y)/
                           sizeof(CodeFragment::text_vx_y[0]);

void PDECommon::link(CodeFragment& cf) const
{
  link(cf.text_y);
  link(cf.text_j);
  for (int i = 0; i < NBS; ++i)
  {
    link(cf.text_bx_y[i]);
    link(cf.text_bx_j[i]);
  }
  for (int i = 0; i < NVS; ++i)
  {
    link(cf.text_vx_y[i]);
    link(cf.text_vx_j[i]);
  }
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::::  Utilities  :::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

// TODO finalize PDE just before code generation
// TODO check that number of field components == number of eqs (in check() ?)

void PDECommon::finalize()
{
  finalized_ = true;
}

// TODO rewrite it!
void PDECommon::check() const
{
  if (components() != equations())
    throw _S"Number of equations does not match number of components.";
  if (!finalized_) throw _S"PDE system definition is not complete.";
}

void PDECommon::check_function_implementation(bool empty, const string& name)
{
  if (empty)
  {
    switch (fd::global_config().missing_function_impl)
    {
      case fd::NOT_AN_ERROR:
        break;
      case fd::NOTE: log_msg("NOTE: The function "+name+
        " has no implementation. Modify the generated code file(s)"
        " to add it manually.");
        break;
      case fd::WARNING: log_msg("WARNING: The function "+name+
        " has no implementation.");
        break;
      case fd::EXCEPTION: throw "The function "+name+
        " has no implementation.";
        break;
    }
  }
}

void PDECommon::check_function_implementations()
{
  for (const auto f: functions_)
    check_function_implementation(f->implementation.empty(), f->fname());
}

void PDECommon::check_field_function_implementations()
{
  for (const auto f: field_functions_)
    check_function_implementation(f->implementation.empty(), f->fname());
}

bool PDECommon::find_fname_in_functions(const std::string& fname) const
{
  for (auto f : functions_)
    if (fname == (f->fname())) return true;
  return false;
}

bool PDECommon::find_fname_in_field_functions(const std::string& fname) const
{
  for (auto f : field_functions_)
    if (fname == (f->fname())) return true;
  return false;
}

bool PDECommon::find_fname_in_cutoff_functions(const std::string& fname) const
{
  for (auto f : cutoff_functions_)
    if (fname == (f->fname())) return true;
  return false;
}

bool PDECommon::has_default_field_function_implementations() const
{
  for (FieldFunction *f : field_functions_)
    for (int n = 1; n < f->argc() + 1; ++n)
      if ((int) f->implementation.size() > n && f->implementation[n] == "default")
        return true;
  return false;
}

bool PDECommon::has_exact_solution() const
{
  for (auto f : functions_)
    if (f->fname() == "exact_solution") return true;
  return false;
};

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::  BC Utilities  :::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::finalize_BCs()
{
  if (BCs_finished_)
  {
//    fd::log(_S __PRETTY_FUNCTION__ + ": BCs already finilized.");
    return;
  }

  for (const int sid : supported_sids())
  {
    for (int index = 0; index < (int)field_.size(); ++index)
    {
      switch (sid)
      {
        case MeshCommon::R: field_[index].i_end   = nx(); break;
        case MeshCommon::L: field_[index].i_start = 0;    break;
        case MeshCommon::T: field_[index].j_end   = ny(); break;
        case MeshCommon::B: field_[index].j_start = 0;    break;
        case MeshCommon::U: field_[index].k_end   = nz(); break;
        case MeshCommon::D: field_[index].k_start = 0;    break;
      }
    }
  }

  BCs_finished_ = true;
}

void PDECommon::finalize_BCs_extdom()
{
  // TODO check if this restriction can be removed
  if (has_BCs_type(BoundaryCondition::CUSTOM))
    throw _S"Custom (user defined) boundary conditions cannot be "
            "discretized with the traditional symmetric differences "
            "method. Use an inward differences method.";

  if (BCs_finished_)
  {
//    fd::log(_S __PRETTY_FUNCTION__ + ": BCs already finilized.");
    return;
  }

  for (const int sid : supported_sids())
  {
    std::vector<int> bids = get_bids_on_side(sid);
    for (int index = 0; index < (int)field_.size(); ++index)
    {
      bool nbc = false;
      for (const auto& bid : bids)
        if (component_has_neumann_BC_on(index, bid))
        { nbc = true; break; }
      const int xtb = int(nbc);
      switch (sid)
      {
        case MeshCommon::R: field_[index].i_end   = nx() + xtb; break;
        case MeshCommon::L: field_[index].i_start =    0 - xtb; break;
        case MeshCommon::T: field_[index].j_end   = ny() + xtb; break;
        case MeshCommon::B: field_[index].j_start =    0 - xtb; break;
        case MeshCommon::U: field_[index].k_end   = nz() + xtb; break;
        case MeshCommon::D: field_[index].k_start =    0 - xtb; break;
      }
    }
  }

  BCs_finished_ = true;
}

bool PDECommon::mixed_BCs_on_boundary() const
{
  for (const int sid : supported_sids())
  {
    int other_bcs = 0, neumann_bcs = 0;
    for (const int bid : get_bids_on_side(sid))
    {
      for (const auto& field: field_)
      {
        BoundaryCondition *bc = get_BC_for_component(field.id, bid);
        if (bc == nullptr) continue;
        if (bc->type == BoundaryCondition::NEUMANN) ++neumann_bcs;
        else ++other_bcs;
      }
    }
    if (other_bcs > 0 && neumann_bcs > 0) return true;
  }
  return false;
}

bool PDECommon::has_neumann_BCs() const
{ return has_BCs_type(BoundaryCondition::NEUMANN); }

bool PDECommon::has_BCs_type(int type) const
{
  for (BoundaryCondition *bc : BCs())
    if (bc->type == type) return true;
  return false;
}

bool PDECommon::component_has_neumann_BC_on(int comp_index, int bid) const
{
  const auto bc = get_BC_for_component(get_component(comp_index), bid);
  THROW_IF(bc == nullptr, "no BC for component "+
      field_[comp_index].format_name()+" on boundary "+fd::to_string(bid)+".");
  return bc->type == BoundaryCondition::NEUMANN;
}

bool PDECommon::has_neumann_BC_on(int bid) const
{
  for (int comp_index = 0; comp_index < components(); ++comp_index)
    if (component_has_neumann_BC_on(comp_index, bid)) return true;
  return false;
}

bool PDECommon::boundary_has_all_BCs_type(const Boundary *b, int type) const
{
  int count = 0;
  for (const BoundaryCondition *bc : BCs())
  {
    if (bc->boundary() == b && bc->type == type) ++count;
    if (count == components()) return true;
  }
  return false;
}

bool PDECommon::unique_BCs(int cid, bool matching) const
{
  static vector<Boundary*>::size_type nboundaries[4]{0,2,4,6};
  vector<Boundary*> bs;
  for (auto bc : bc_)
    if (bc->component() == cid) bs.push_back(bc->boundary());
  std::sort(bs.begin(), bs.end());
  if (std::unique(bs.begin(), bs.end()) != bs.end())
    return false;
  return !matching || bs.size() == nboundaries[dimension()];
}

// -----------------------------------------------------------------------------
// Input/Output
// -----------------------------------------------------------------------------
std::string PDECommon::get_implem_filename() const {return code_file_;}
std::string PDECommon::get_header_filename() const {return header_file_;}
std::string PDECommon::get_main_filename() const {return program_;}

// -----------------------------------------------------------------------------
// Set output files
// -----------------------------------------------------------------------------
void PDECommon::set_output(const std::string& filename, const std::string& progname)
{
  auto make_filename = [](const std::string& name, bool header)->std::string {
    static const char *ext[] = {".cpp", ".cxx", ".c++", ".cc", ".C",
                                ".hpp", ".hxx", ".h++", ".hh", ".H"};
    static const int sz[] = {4, 4, 4, 3, 2};
    static const int n_ext = sizeof(ext)/(2*sizeof(ext[0]));
    int w = header ? n_ext : 0;
    if (name.rfind(ext[w + 0]) == name.size() - sz[0] ||
        name.rfind(ext[w + 1]) == name.size() - sz[1] ||
        name.rfind(ext[w + 2]) == name.size() - sz[2] ||
        name.rfind(ext[w + 3]) == name.size() - sz[3] ||
        name.rfind(ext[w + 4]) == name.size() - sz[4])
      return name;
    return name + ext[w];
  };
  // Open file makining necessary adjustments
  auto open_file = [make_filename](const string& filename, string& ofname,
                                   std::ofstream& of, bool hdr) {
    ofname = make_filename(filename, hdr);
    fd::backup_old_file(ofname);
    of.open(ofname);
    if (!of) throw "Could not open file " + ofname;
  };
  open_file(filename, code_file_, code_, false);
  open_file(filename, header_file_, code_h_, true);
  // Decide which file to use to output main function
  program_ = [this, progname, make_filename]() -> std::string {
    if (progname.empty())
      return code_file_.substr(0, code_file_.rfind('.')) + "_main.cpp";
    if (progname == "*") return code_file_;
    return make_filename(progname, false);
  }();
}

void PDECommon::switch_to_main()
{
  if (program_ == code_file_) return;
  code_.close();
  std::string fname = get_main_filename();
  fd::backup_old_file(fname);
  code_.open(fname);
  if (!code_) throw "Could not open file " + fname;
}

// -----------------------------------------------------------------------------
// Select methods
// -----------------------------------------------------------------------------
void PDECommon::set_library(const std::string& library)
{
  for (const auto& lib : libs_)
    if (lib.second == library)
    { library_ = lib.first; return; }
  throw "Unknown library: " + library;
}

void PDECommon::set_matrix_method(const std::string& matrix_method)
{
  for (const auto& mm : matrix_methods_)
    if (mm.second == matrix_method)
    { matrix_method_ = mm.first; return; }
  throw "Unknown matrix method: " + matrix_method;
}

void PDECommon::set_nonlinear_solver(const std::string& nonlinear_solver)
{
  for (const auto& nls : nonlinear_solvers_)
    if (nls.second == nonlinear_solver)
    { nonlinear_solver_ = nls.first; return; }
  throw "Unknown nonlinear solver: " + nonlinear_solver;
}

void PDECommon::set_boundary_discretization_method(int bdm)
{
  if (dimension() == 3) // TODO remove when new implementation is ready for 1 & 2d
  if (bdm == SYM_DIFF2) throw _S"The SYM_DIFF2 method can only be used "
      "with an extended domain PDE class.";
  boundary_discretization_method_ = bdm;
}

// -----------------------------------------------------------------------------
// Customization and options
// -----------------------------------------------------------------------------
void PDECommon::set_initial_estimate(const string& impl)
{
  init_estimate_imp_ = impl;
  fd::global_config().format_function_implementation(init_estimate_imp_);
}

bool PDECommon::has_initial_estimate() const
{ return !init_estimate_imp_.empty(); }

// -----------------------------------------------------------------------------
// Control output
// -----------------------------------------------------------------------------

void PDECommon::set_headers(const std::string& src, const std::string& pos,
                                                    const std::string& headers)
{
  const int which = [src] {
    if (src == "header" || src == "interface" || src == "int" || src == "h" )
      return HEADER_FILE;
    if (src == "implementation" || src == "imp" || src == "i") return IMP_FILE;
    if (src == "main" || src == "m" ) return MAIN_FILE;
    throw "Unknown output file type: " + src +".";
  }();

  const int where = [src, pos] {
    if (pos == "top" || pos == "begin" || pos == "start" || pos == "t" )
      return PLACEMENT_TOP;
    if (pos == "before library" || pos == "before interface" ||
        pos == "before header"  || pos == "b" || pos == "bh" )
      return PLACEMENT_BEFORE_LIB;
    if (pos == "bottom" || pos == "end" ||pos == "e" ) return PLACEMENT_LAST;
    throw "Unknown placement \"" + pos + "\" for header(s): " + src +".";
  }();

  /*std::map<const string, int>*/auto& d = get_dict_for_source(which);
  const char delim = (headers.find(':') == string::npos) ? ';' : ':';
  const string msg = " header specification.";
  std::stringstream ss(headers);
  string item;

  while (std::getline(ss, item, delim))
  {
    if (item.empty()) throw "Incorrect" + msg;
    if (d.find(item) != d.end()) throw "Duplicate" + msg;
    d[item] = where;
  }
}

void PDECommon::set_extra_includes(const std::string& s)
{
  const char delim = (s.find(':') == std::string::npos) ? ';' : ':';
  const std::string msg = " include file specification.";
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim))
  {
    if (item.empty()) throw "Incorrect" + msg;
    if (fd::find(extra_includes_, item)) throw "Duplicate" + msg;
    extra_includes_.push_back(item);
  }
}

} // namespace fd
