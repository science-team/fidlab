#include <ctime>
#include "mesh.hpp"
#include "pde.hpp"
#include "config_code.h"

using std::string;
using std::vector;
using std::array;

static string sFormatMsg(const string &fieldname, int bid)
{
  return "no BC for field "+fieldname+" on boundary with id "+
      fd::to_string(bid)+".";
}

namespace fd {

// -----------------------------------------------------------------------------
// Code Generation for BCs
// -----------------------------------------------------------------------------

void PDECommon::get_code_for_boundary(int bid, int c, CodeFragment& cfb)
{ get_code_for_boundary(bid, get_sid(bid), c, cfb); }

void PDECommon::get_code_for_boundary(int bid, int sid, int c, CodeFragment& cfb)
{
  BoundaryCondition *bc = get_BC_for_component(get_component(c), bid);
  THROW_IF(bc == nullptr, sFormatMsg(field_[c].format_name(), bid));
  const int bdm = boundary_discretization_method_;
  const BoundaryDiscretizationData bdd{this, bid, bdm, i_, j_, k_, sid};
  switch (bc->type)
  {
    case BoundaryCondition::DIRICHLET:
    {
      DirichletBCDiscretizer discr(static_cast<DirichletBC *>(bc));
      cfb = discr.discretize_on_boundary(bdd);
      const bool single = components() == 1;
      const string cindex = fd::to_string_empty_if(single, c);
      fd::replace_all(cfb.text_y, "$COMMA$", single ? "" : ",");
      fd::replace_all(cfb.text_j, "$COMMA$", single ? "" : ",");
      fd::replace_all(cfb.text_y, "$c$", cindex);
      fd::replace_all(cfb.text_j, "$c$", cindex);
      break;
    }
    case BoundaryCondition::NEUMANN:
    {
      CodeFragment dummy;
      get_code_for_neumann(bc, bdd, cfb, dummy);
      break;
    }
    case BoundaryCondition::CUSTOM:
    {
      // Set current code fragment
      cfb = get_code_fragment(*(CustomBC*)bc, sid);
      link(cfb);
      // QUESTION 1: Why don't we need here cfx when SYM_DIFF2
      //             is in effect (see line 80)?
      // ANSWER: Because SYM_DIFF2 works only with Neumann BCs.
      break;
    }
    default:
    {
      THROW("Incorrect BC type: " + fd::to_string(bc->type));
    }
  }
}

void PDECommon::get_code_for_neumann(BoundaryCondition *bc, int bid,
		CodeFragment& cfb, CodeFragment& cfx)
{
  const BoundaryDiscretizationData bdd{this, bid,
      boundary_discretization_method_, i_, j_, k_, get_sid(bid)};
  get_code_for_neumann(bc, bdd, cfb, cfx);
}

void PDECommon::get_code_for_neumann(BoundaryCondition *bc,
                                     const BoundaryDiscretizationData &bdd,
		                             CodeFragment& cfb,
                                     CodeFragment& cfx)
{
  const bool single = components() == 1;
  const string cindex = fd::to_string_empty_if(single,
      get_component_index(bc->component()));
  const string comma(single ? "" : ",");
  // Discretize BC
  NeumannBCDiscretizer discr(static_cast<NeumannBC *>(bc));
  cfb = discr.discretize_on_boundary(bdd);
  // The text_bx_{y,j} are used only by CUSTOM BCs.
  fd::replace_all(cfb.text_y,       "$COMMA$", comma);
  fd::replace_all(cfb.text_j,       "$COMMA$", comma);
/*fd::replace_all(cfb.text_bx_y[0], "$COMMA$", comma);
  fd::replace_all(cfb.text_bx_y[1], "$COMMA$", comma);
  fd::replace_all(cfb.text_bx_j[0], "$COMMA$", comma);
  fd::replace_all(cfb.text_bx_j[1], "$COMMA$", comma);*/
  fd::replace_all(cfb.text_y,       "$c$",     cindex);
  fd::replace_all(cfb.text_j,       "$c$",     cindex);
/*fd::replace_all(cfb.text_bx_y[0], "$c$",     cindex);
  fd::replace_all(cfb.text_bx_y[1], "$c$",     cindex);
  fd::replace_all(cfb.text_bx_j[0], "$c$",     cindex);
  fd::replace_all(cfb.text_bx_j[1], "$c$",     cindex);*/
}

// cfb Standard BC code.
// cfx Additional code, obtained from an equation of the PDE system
//     or other BCs. Required by the SYM_DIFF2 method for Neumann BCs.
void PDECommon::get_code_for_boundary_SYM_DIFF2(int bid, int c,
                                                CodeFragment& cfb,
                                                CodeFragment& cfx)
{
  ASSERT(boundary_discretization_method_ == SYM_DIFF2);

  BoundaryCondition *bc = get_BC_for_component(get_component(c), bid);
  THROW_IF(bc == nullptr, sFormatMsg(field_[c].format_name(), bid));
  if (bc->type == BoundaryCondition::NEUMANN)
  {
    get_code_for_neumann(bc, bid, cfb, cfx);
    // Discretized BC will be eq. no. e(-1, j, k, c) etc.
    // Discretized PDE will be eq. no. e(0, j, k, c) etc.
    const int eq = static_cast<NeumannBC *>(bc)->discretize_equation;
    cfx = equation_codes_[eq == -1 ? c : eq];
    // The code is already linked
  }
  else
  {
    get_code_for_boundary(bid, c, cfb);
  }
}

std::tuple<string, string, string>
PDECommon::extended_node_coordinates(int sid) const
{
  static const char *COORD[3][6]{{"nx",  "",  "-1",  "",   "",   ""},
                                 { "",  "ny",  "",  "-1",  "",   ""},
                                 { "",   "",   "",   "",  "nz", "-1"} };
  string I = COORD[Boundary::X][sid];
  string J = COORD[Boundary::Y][sid];
  string K = COORD[Boundary::Z][sid];
  return std::make_tuple(I.empty() ? i_ : I,
                         J.empty() ? j_ : J,
                         K.empty() ? k_ : K);
}

void PDECommon::generate_code_for(int c, int bid, int what, int ntabs)
{
  static const string f("e");
  generate_code_for(c, bid, what, ntabs, [this](
    const string& i, const string& j, const string& k, const string& c)->string {
      return format_elem_lhs(f, i, j, k, c);
    });
}

void PDECommon::generate_code_for(int c, int bid, int what, int ntabs,
                                  ElementFormatter ef)
{
  ASSERT(boundary_discretization_method_ != SYM_DIFF2);

  const string cidx = fd::to_string_empty_if(components() == 1, c);
  const string tabs(ntabs, '\t');
  CodeFragment cfb;
  get_code_for_boundary(bid, c, cfb);

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_y << ";";
  else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_j << ";";
  else code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_y << R"(;
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_j << ";";
}

void PDECommon::generate_code_for_global_boundary(int c, int sid, int what,
                                                  int ntabs)
{
  generate_code_for_global_boundary(c, sid, what, ntabs, [this](
      const string &i, const string &j, const string &k, const string &c) {
      return format_elem_lhs("e", i, j, k, c);
    }
  );
}

void PDECommon::generate_code_for_global_boundary(int c, int sid, int what,
                                                  int ntabs,
                                                  ElementFormatter ef)
{
  ASSERT(boundary_discretization_method_ != SYM_DIFF2);

  const string cidx = fd::to_string_empty_if(components() == 1, c);
  const string tabs(ntabs, '\t');
  CodeFragment cfb;
  get_code_for_boundary(get_global_bid(), sid, c, cfb);

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_y << ";";
  else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_j << ";";
  else code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_y << R"(;
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_j << ";";
}

void PDECommon::generate_code_SYM_DIFF2_for(int c, int bid, int what, int ntabs,
                                            int bc_code_flags, ElementFormatter ef)
{
  ASSERT(check_bc_code(bc_code_flags));
  ASSERT(boundary_discretization_method_ == SYM_DIFF2);

//   const int fcid = get_component(c);
  const string cidx = fd::to_string_empty_if(components() == 1, c);
  const string tabs(ntabs, '\t');
  const auto bc = get_BC_for_component(get_component(c), bid);
  THROW_IF(bc == nullptr, sFormatMsg(field_[c].format_name(), bid));
//   const bool neumann = bc->type == BoundaryCondition::NEUMANN;
  CodeFragment cfb, cfx;
  get_code_for_boundary_SYM_DIFF2(bid, c, cfb, cfx);

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  if (bc->type == BoundaryCondition::NEUMANN)
  {
    string I, J, K;
    std::tie(I,J,K) = extended_node_coordinates(get_sid(bid));
    if (what == GENERATE_FUNCTION_CODE)
    {
      if (ordinary_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cidx) << "] = " << cfb.text_y << ";";
      if (extra_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfx.text_y << ";";
    }
    else if (what == GENERATE_JACOBIAN_CODE)
    {
      if (ordinary_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"J[" << ef(I, J, K, cidx) << "] = " << cfb.text_j << ";";
      if (extra_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfx.text_j << ";";
    }
    else
    {
      if (ordinary_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cidx) << "] = " << cfb.text_y << R"(;
		)"<<tabs<<"J[" << ef(I, J, K, cidx) << "] = " << cfb.text_j << ";";
      if (extra_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfx.text_y << R"(;
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfx.text_j << ";";
    }
  }
  else
  {
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_j << ";";
    else code_ << R"(
		)"<<tabs<<"F[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_y << R"(;
		)"<<tabs<<"J[" << ef(i_,j_,k_,cidx) << "] = " << cfb.text_j << ";";
  }
}

void PDECommon::generate_code_for_OLD(int c, int bid, int bid2, int what, int ntabs)
{
  const string f("e");
  const int flags = dimension() == 3 ? ALL_BC_CODES : ALL_BC_CODES_2D;
  generate_code_for_OLD(c, bid, bid2,	what, ntabs, flags, [this, &f](
    const string& i, const string& j, const string& k, const string& c)->string {
      return format_elem_lhs(f, i, j, k, c);
    });
}

void PDECommon::generate_code_for_OLD(int c, int bid, int bid2, int what, int ntabs,
                                  int bc_code_flags, ElementFormatter ef)
{
  if (boundary_discretization_method_ == SYM_DIFF2)
  {
    generate_code_SYM_DIFF2_for_OLD(c, bid, bid2, what, ntabs, bc_code_flags, ef);
    return;
  }

  const string tabs(ntabs, '\t');
  const string cindex(fd::to_string_empty_if(components() == 1, c));
  const string &i = i_;
  const string &j = j_;
  const string &k = k_;
  const int  cid        = get_component(c);
  const auto bc         = get_BC_for_component(cid, bid );
  const auto bc2        = get_BC_for_component(cid, bid2);
  THROW_IF(bc  == nullptr, sFormatMsg(field_[c].format_name(), bid ));
  THROW_IF(bc2 == nullptr, sFormatMsg(field_[c].format_name(), bid2));
  const int  bctype     = bc->type;
  const int  bctype2    = bc2->type;
  const bool dirichlet  = bctype  == BoundaryCondition::DIRICHLET;
  const bool dirichlet2 = bctype2 == BoundaryCondition::DIRICHLET;
  const bool neumann    = bctype  == BoundaryCondition::NEUMANN;
  const bool neumann2   = bctype2 == BoundaryCondition::NEUMANN;
  CodeFragment cfb;
  const string& text_y  = cfb.text_y;
  const string& text_j  = cfb.text_j;

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;
  if (dirichlet || dirichlet2)
  {
    get_code_for_boundary(dirichlet ? bid : bid2, c, cfb);
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << text_j << ";";
    else code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << text_y << R"(;
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << text_j << ";";
  }
  else if (neumann || neumann2)
  {
    get_code_for_boundary(neumann ? bid : bid2, c, cfb);
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << text_j << ";";
    else code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << text_y << R"(;
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << text_j << ";";
  }
  else
  {
    get_code_for_boundary(bid, c, cfb);
    const int sid2 = get_sid(bid2);
    const int x = btype(bid) == Boundary::Y ?
        (sid2 == MeshCommon::L ? 0 : 1) : (sid2 == MeshCommon::B ? 0 : 1);
    const string& code_y = cfb.text_bx_y[x].empty() ? cfb.text_y :
                                                      cfb.text_bx_y[x];
    const string& code_j = cfb.text_bx_j[x].empty() ? cfb.text_j :
                                                      cfb.text_bx_j[x];
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << code_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << code_j << ";";
    else code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << code_y << R"(;
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << code_j << ";";
  }
}

void PDECommon::generate_code_SYM_DIFF2_for_OLD(int c, int bid, int bid2,
                    int what, int ntabs, int bc_code_flags, ElementFormatter ef)
{
  ASSERT(check_bc_code(bc_code_flags));
  ASSERT(btype(bid) != btype(bid2));

  const int sid  = get_sid(bid);
  const int sid2 = get_sid(bid2);
  const string tabs(ntabs, '\t');
  const string cindex(fd::to_string_empty_if(components() == 1, c));
  const string &i = i_;
  const string &j = j_;
  const string &k = k_;
  const int  cid        = get_component(c);
  const auto bc         = get_BC_for_component(cid, bid );
  const auto bc2        = get_BC_for_component(cid, bid2);
  THROW_IF(bc  == nullptr, sFormatMsg(field_[c].format_name(), bid ));
  THROW_IF(bc2 == nullptr, sFormatMsg(field_[c].format_name(), bid2));
  const int  bctype     = bc->type;
  const int  bctype2    = bc2->type;
  const bool dirichlet	= bctype  == BoundaryCondition::DIRICHLET;
  const bool dirichlet2	= bctype2 == BoundaryCondition::DIRICHLET;
  const bool neumann    = bctype  == BoundaryCondition::NEUMANN;
  const bool neumann2   = bctype2 == BoundaryCondition::NEUMANN;
  const bool general    = !(dirichlet  || neumann);
  const bool general2   = !(dirichlet2 || neumann2);
  CodeFragment cfb, cfx;
  const string& btext_y = cfb.text_y;
  const string& btext_j = cfb.text_j;
  const string& xtext_y = cfx.text_y;
  const string& xtext_j = cfx.text_j;

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  if (neumann && neumann2)
  {
    std::string I, J, K;
    // Generate code for the "primary" boundary
    get_code_for_boundary_SYM_DIFF2(bid, c, cfb, cfx);
    I = (sid == MeshCommon::L) ? "-1" : (sid == MeshCommon::R ? "nx" : i);
    J = (sid == MeshCommon::B) ? "-1" : (sid == MeshCommon::T ? "ny" : j);
    K = (sid == MeshCommon::D) ? "-1" : (sid == MeshCommon::U ? "nz" : k);
    if (what == GENERATE_FUNCTION_CODE)
    {
      if (ordinary_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = " << btext_y << ";";
      if (extra_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << xtext_y << ";";
    }
    else if (what == GENERATE_JACOBIAN_CODE)
    {
      if (ordinary_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = " << btext_j << ";";
      if (extra_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << xtext_j << ";";
    }
    else
    {
      if (ordinary_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = " << btext_y << R"(;
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = " << btext_j << ";";
      if (extra_bc_code(bc_code_flags)) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << xtext_y << R"(;
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << xtext_j << ";";
	}
    // Generate code for the "secondary" boundary
    if (secondary_bc_code(bc_code_flags))
    {
      get_code_for_boundary_SYM_DIFF2(bid2, c, cfb, cfx);
      I = (sid2 == MeshCommon::L) ? "-1" : (sid2 == MeshCommon::R ? "nx" : i);
      J = (sid2 == MeshCommon::B) ? "-1" : (sid2 == MeshCommon::T ? "ny" : j);
      K = (sid2 == MeshCommon::D) ? "-1" : (sid2 == MeshCommon::U ? "nz" : k);
      if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = " << btext_y << ";";
      else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = " << btext_j << ";";
      else code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = " << btext_y << R"(;
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = " << btext_j << ";";
    }
  }
  else if (neumann || neumann2)
  {
    if (ordinary_bc_code(bc_code_flags))
    {
      get_code_for_boundary_SYM_DIFF2(neumann ? bid : bid2, c, cfb, cfx);
      std::string I, J, K;
      if (neumann)
      {
        I = (sid == MeshCommon::L) ? "-1" : (sid == MeshCommon::R ? "nx" : i);
        J = (sid == MeshCommon::B) ? "-1" : (sid == MeshCommon::T ? "ny" : j);
        K = (sid == MeshCommon::D) ? "-1" : (sid == MeshCommon::U ? "nz" : k);
      }
      else
      {
        I = (sid2 == MeshCommon::L) ? "-1" : (sid2 == MeshCommon::R ? "nx" : i);
        J = (sid2 == MeshCommon::B) ? "-1" : (sid2 == MeshCommon::T ? "ny" : j);
        K = (sid2 == MeshCommon::D) ? "-1" : (sid2 == MeshCommon::U ? "nz" : k);
      }
      if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = " << btext_y << ";";
      else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = " << btext_j << ";";
      else code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = " << btext_y << R"(;
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = " << btext_j << ";";
	}
  }
  // If neumann or neumann2 is true an additional condition is necessary
  if (extra_bc_code(bc_code_flags))
  {
    if (dirichlet || dirichlet2)
    {
      get_code_for_boundary(dirichlet ? bid : bid2, c, cfb);
      if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << btext_y << ";";
      else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << btext_j << ";";
      else code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << btext_y << R"(;
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << btext_j << ";";
	}
    else if (general || general2)
    {
      ASSERT(!((general && neumann) || (general2 && neumann2)));
      get_code_for_boundary(neumann ? bid2 : bid, c, cfb);
      // QUESTION 2: Why boundary extremity codes cannot be null here? (!!)
      THROW_IF(cfb.text_bx_y[0].empty() || cfb.text_bx_y[1].empty() ||
               cfb.text_bx_j[0].empty() || cfb.text_bx_j[1].empty(),
               "expected boundary extremity code.");
      const int x = (sid == MeshCommon::B || sid == MeshCommon::T) ?
          (sid2 == MeshCommon::L ? 0 : 1) : (sid2 == MeshCommon::B ? 0 : 1);
      // QUESTION 3: Why do we use boundary extremity codes here instead of
      // normal boundary codes? (!!)
      const string& code_y = cfb.text_bx_y[x];
      const string& code_j = cfb.text_bx_j[x];
      if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << code_y << ";";
      else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << code_j << ";";
      else code_ << R"(
		)"<<tabs<<"F[" << ef(i, j, k, cindex) << "] = " << code_y << R"(;
		)"<<tabs<<"J[" << ef(i, j, k, cindex) << "] = " << code_j << ";";
    }
  }
}

//============================================================================//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//::::::::::::::::::::::::::   NEW IMPLEMENTATION   :::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//============================================================================//

int PDECommon::boundary_edge_extremity(int bid, int sid2) const
{
  return dimension() == 2 ? Boundary::edge_extremity(sid2, -1) :
                            Boundary::edge_extremity(get_sid(bid), sid2);
}

int PDECommon::boundary_vertex_extremity(const array<int,3> &bid) const
{
  return Boundary::vertex_extremity(get_sid(bid[1]), dimension() == 2 ? -1 :
                                    get_sid(bid[2]));
}

/*static*/
int PDECommon::boundary_vertex_extremity(const fd::VertexParts &vp)
{ return Boundary::vertex_extremity(vp.fids[1], vp.fids[2]); }

void PDECommon::generate_code_for(int c, int bid, int sid2, int what, int ntabs)
{
  static const string f("e");
  generate_code_for(c, bid,	sid2, what, ntabs, [this](const string& i, 
    const string& j, const string& k, const string& c) -> string { 
      return format_elem_lhs(f, i, j, k, c); });
}

void PDECommon::generate_code_for(int c, int bid, int sid2, int what, int ntabs, 
                                  ElementFormatter ef)
{
  ASSERT(boundary_discretization_method_ != SYM_DIFF2);

  const string tabs(ntabs, '\t');
  const string cindex(fd::to_string_empty_if(components() == 1, c));
  const int cid = get_component(c);

  const BoundaryCondition *bc = get_BC_for_component(cid, bid);

  THROW_IF(bc == nullptr, sFormatMsg(field_[c].format_name(), bid));

  CodeFragment cfb;
  get_code_for_boundary(bid, c, cfb);

  auto text_y = std::cref(cfb.text_y);
  auto text_j = std::cref(cfb.text_j);

  if (bc->type == BoundaryCondition::CUSTOM)
  {
    const int x = boundary_edge_extremity(bid, sid2);
    if (!cfb.text_bx_y[x].empty()) text_y = std::cref(cfb.text_bx_y[x]);
    if (!cfb.text_bx_j[x].empty()) text_j = std::cref(cfb.text_bx_j[x]);
  }

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<text_y.get()<<";";
  else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<text_j.get()<<";";
  else code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<text_y.get()<<R"(;
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<text_j.get()<<";";
}

void PDECommon::generate_code_for(int c, const fd::VertexParts &vertex,
                                  int what, int ntabs)
{
  static const string f("e");
  generate_code_for(c, vertex,	what, ntabs, [this/*, &f*/](
    const string& i, const string& j, const string& k, const string& c)->string
    { return format_elem_lhs(f, i, j, k, c); }
  );
}

void PDECommon::generate_code_for(int c, const fd::VertexParts &vertex,
                                  int what, int ntabs, ElementFormatter ef)
{
  ASSERT(boundary_discretization_method_ != SYM_DIFF2);

  const int D = dimension();
  const string tabs(ntabs, '\t');
  const auto bc = get_BC_for_component(get_component(c), vertex.bid);

  THROW_IF(bc == nullptr, sFormatMsg(field_[c].format_name(), vertex.bid));

  CodeFragment cfb;
  get_code_for_boundary(vertex.bid, c, cfb);

  auto text_y = std::cref(cfb.text_y);
  auto text_j = std::cref(cfb.text_j);

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  if (bc->type == BoundaryCondition::CUSTOM)
  {
    const int x = boundary_vertex_extremity(vertex);
    if (D == 2)
    {
      if (!cfb.text_bx_y[x].empty()) text_y = std::cref(cfb.text_bx_y[x]);
      if (!cfb.text_bx_j[x].empty()) text_j = std::cref(cfb.text_bx_j[x]);
    }
    else
    {
      if (!cfb.text_vx_y[x].empty()) text_y = std::cref(cfb.text_vx_y[x]);
      if (!cfb.text_vx_j[x].empty()) text_j = std::cref(cfb.text_vx_j[x]);
    }
  }

  const string cindex(fd::to_string_empty_if(components() == 1, c));
  if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<text_y.get()<<";";
  else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<text_j.get()<<";";
  else code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<text_y.get()<<R"(;
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<text_j.get()<<";";
}

int PDECommon::generate_code_SYM_DIFF2_for(int c, const array<int,3> &bid,
                                           int what, int ntabs)
{
  static const string f("e");
  const int flags = dimension() == 3 ? ALL_BC_CODES : ALL_BC_CODES_2D;
  return generate_code_SYM_DIFF2_for(c, bid, what, ntabs, flags, [this](
    const string& i, const string& j, const string& k, const string& c) {
      return format_elem_lhs(f, i, j, k, c);
    });
}

int PDECommon::generate_code_SYM_DIFF2_for(int c,
                                           const std::array<int,3>& bids,
                                           int what,
                                           int ntabs,
                                           int bc_code_flags,
                                           ElementFormatter ef)
{
  //:::::::::::::::::::::::::::::::::  NOTE  ::::::::::::::::::::::::::::::::://
  //                                                                          //
  // This method works also when no BC is Neumann and on both edges/vertices. //
  //                                                                          //
  //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://

  ASSERT(boundary_discretization_method_ == SYM_DIFF2);
  ASSERT(check_bc_code(bc_code_flags));

  const int D = dimension() == 3 ? (bids[2] == -1 ? 2 : 3) : 2;
  const string tabs(ntabs, '\t');
  const string cindex(fd::to_string_empty_if(components() == 1, c));
  const int cid = get_component(c);

  const BoundaryCondition *bcs[3]{
                          get_BC_for_component(cid, bids[0]),
                          get_BC_for_component(cid, bids[1]), D < 3 ? nullptr :
                          get_BC_for_component(cid, bids[2])};
  for (int i = 0; i < D; ++i)
  { THROW_IF(bcs[i] == nullptr, sFormatMsg(field_[c].format_name(), bids[i])); }
  const int bctypes[3]   {bcs[0]->type,
                          bcs[1]->type, D < 3 ? -1 : 
                          bcs[2]->type};
  const bool dirichlet[3]{bctypes[0] == BoundaryCondition::DIRICHLET,
                          bctypes[1] == BoundaryCondition::DIRICHLET,
                          bctypes[2] == BoundaryCondition::DIRICHLET};
  const bool neumann[3]  {bctypes[0] == BoundaryCondition::NEUMANN,
                          bctypes[1] == BoundaryCondition::NEUMANN,
                          bctypes[2] == BoundaryCondition::NEUMANN};

  CodeFragment cfb, cfx;
  auto btext_y = std::cref(cfb.text_y);
  auto btext_j = std::cref(cfb.text_j);
  auto xtext_y = std::cref(cfx.text_y);
  auto xtext_j = std::cref(cfx.text_j);

  if (components() > 1) code_ << R"(
		)"<<tabs<<"// Field component " << c;

  int count_neumann = 0;
  for (int i = 0; i < D; ++i)
  {
    if (!neumann[i]) continue;

    get_code_for_boundary_SYM_DIFF2(bids[i], c, cfb, cfx);
    if ((count_neumann == 0 && ordinary_bc_code(bc_code_flags)) ||
        (count_neumann == 1 && secondary_bc_code(bc_code_flags)) ||
        (count_neumann == 2 && tertiary_bc_code(bc_code_flags)))
    {
      string I, J, K;
      std::tie(I, J, K) = extended_node_coordinates(get_sid(bids[i]));
      if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = "<<btext_y.get()<<";";
      else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = "<<btext_j.get()<<";";
      else code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = "<<btext_y.get()<<R"(;
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = "<<btext_j.get()<<";";
    }
    ++count_neumann;
  }

  if (!extra_bc_code(bc_code_flags)) return count_neumann;

  /*================================|  NOTE  |================================*\ 
  |                                                                            |
  |       If all BCs are Neumann, the standard "BC" is the PDE on the          |
  |       edge/vertex. Otherwise it is the 1st available (highest              |
  |       priority) Dirichlet or custom BC.                                    |
  |                                                                            |
  \*==========================================================================*/

  if (/*all Neumann*/count_neumann == D)
  {
    get_code_for_boundary_SYM_DIFF2(bids[0], c, cfb, cfx);
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<xtext_y.get()<<";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<xtext_j.get()<<";";
    else code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<xtext_y.get()<<R"(;
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<xtext_j.get()<<";";
  }
  else /*not all Neumann*/
  {
    int i = 0;
    while (i < D && neumann[i]) ++i;

    THROW_IF(i == D, "Internal error 0015. Please report this error.");

    get_code_for_boundary_SYM_DIFF2(bids[i], c, cfb, cfx);

    if (!dirichlet[i]) // custom[i]
    {
      if (D == 2)
      {
        const int x = boundary_edge_extremity(bids[0], bids[1]);
        if (!cfb.text_bx_y[x].empty()) btext_y = std::cref(cfb.text_bx_y[x]);
        if (!cfb.text_bx_j[x].empty()) btext_j = std::cref(cfb.text_bx_j[x]);
      }
      else
      {
        const int x = boundary_vertex_extremity(bids);
        if (!cfb.text_vx_y[x].empty()) btext_y = std::cref(cfb.text_vx_y[x]);
        if (!cfb.text_vx_j[x].empty()) btext_j = std::cref(cfb.text_vx_j[x]);
      }
    }
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<btext_y.get()<<";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<btext_j.get()<<";";
    else code_ << R"(
		)"<<tabs<<"F[" << ef(i_, j_, k_, cindex) << "] = "<<btext_y.get()<<R"(;
		)"<<tabs<<"J[" << ef(i_, j_, k_, cindex) << "] = "<<btext_j.get()<<";";
  }

  return count_neumann + 1;
}

// -----------------------------------------------------------------------------
// Code Generation -- equation replacement
// -----------------------------------------------------------------------------

// TODO replace "i", "j", "k" with i_, j_, k_
bool PDECommon::generate_code_function_body_replace_eqs(int what, int ntabs,
                                                        const string& findex,
                                                        const string& n,
                                                        bool check_simple)
{
  if (!replace_equations()) return false;

  const int nc = components();
  const bool simple = (nc == 1 && boundary_discretization_method_ != SYM_DIFF2);

  if (check_simple && !simple) return false;

  const int dim = dimension();
  const string tabs(ntabs, '\t');
  const string field(nc == 1 ? _S"" : findex);
  const string eq = n.empty() ? format_elem_lhs("e", "i", "j", "k", field) : n;

  string else_kwd("");
  for (auto& r : replacements_)
  {
    const string cc = findex.empty() ? EMPTY :
        " && " + findex + " == " + fd::to_string(r.index);
    code_ << R"(
	)" << tabs << else_kwd << "if (i == " << r.i;
    if (dim > 1) code_ << " && j == " << r.j;
    if (dim > 2) code_ << " && k == " << r.k;
    code_ << cc << R"()
	)"<<tabs<<"{";
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F["<<eq<<"]="<<r.rhs_value()<<";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J["<<eq<<"]="<<r.rhs_jac()<<";";
    else code_ << R"(
		)"<<tabs<<"F["<<eq<<"]="<<r.rhs_value()<<R"(;
		)"<<tabs<<"J["<<eq<<"]="<<r.rhs_jac()<<";";
    code_ << R"(
	)"<<tabs<<"}";
    if (else_kwd.empty()) else_kwd = "else ";
  }
  code_ << R"(
	)"<<tabs<<"else ";
  return true;
}

bool PDECommon::generate_code_function_body_replace_eqs_ns(int what)
{
  const int nc = components();
  const bool simple = (nc == 1 && boundary_discretization_method_ != SYM_DIFF2);

  if (replacements_.empty() || simple) return false;

  code_ << R"(
	// User specified equation replacements)";

  for (auto& r : replacements_)
  {
    const string findex = fd::to_string_empty_if(components() == 1, r.index);
    const string eq = format_elem_lhs("e", fd::to_string(r.i),
        fd::to_string(r.j), fd::to_string(r.k), findex);
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
	)"<<"F["<<eq<<"] = "<<r.rhs_value()<<";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
	)"<<"J["<<eq<<"] = "<<r.rhs_jac()<<";";
    else code_ << R"(
	)"<<"F["<<eq<<"] = "<<r.rhs_value()<<R"(;
	)"<<"J["<<eq<<"] = "<<r.rhs_jac()<<";";
  }
  return true;
}

// -----------------------------------------------------------------------------
// Code Generation -- Utilities
// -----------------------------------------------------------------------------

void PDECommon::perform_consistency_checks()
{
  if (!code_ || !code_h_) throw _S"No output streams.";
  if (boundary_discretization_method_ == SYM_DIFF2 && mixed_BCs_on_boundary())
    throw _S"Mixed Neumann BCs on the same boundary cannot be discretized "
        "with the SYM_DIFF2 method. Use an INWARD_DIFFx method instead.";
  check_function_implementations();
  check_field_function_implementations();
}

// -----------------------------------------------------------------------------
// Code Generators
// -----------------------------------------------------------------------------

void PDECommon::generate_code_header()
{
  code_ << "/* -*- Mode: C++; indent-tabs-mode: t; "
        << "c-basic-offset: 2; tab-width: 2 -*-  */\n";

  std::time_t this_time = std::time(nullptr);
  string NOW(std::ctime(&this_time));
  string::size_type pos = NOW.rfind('\n');
  if (pos != string::npos) NOW = NOW.substr(0, pos);

  code_ << "// Generated by " APPNAME " on " << NOW << std::endl
        << "// *** Do not edit unless you know what you are doing! ***"
        << std::endl << std::endl;
}

void PDECommon::generate_code_includes()
{
  code_extra_headers(IMP_FILE, PLACEMENT_TOP);
  if (program_ == code_file_)
  {
    if (!linear() || library_ == GSL)
      code_ << "#include <iostream>\n";
    code_ << "#include <fstream>\n";
  }
  if (library_ == PETSC && !linear())
    code_ << "#include <memory>\n";
  code_extra_headers(IMP_FILE, PLACEMENT_BEFORE_HDR);
  code_ << "#include \"" << strip_path(get_header_filename()) << "\"\n";
  if (library_ == PETSC && !linear())
    code_ << "#include <petsc++/map.hpp>\n";
  code_extra_headers(IMP_FILE, PLACEMENT_LAST);

  if (library_ == GSL) code_ << R"(
using )"<<libs_[library_]<<R"(::vector;
)";
  else if (library_ == PETSC) code_ << R"(
using namespace )"<<libs_[library_]<<R"(;
)";
}

void PDECommon::generate_code()
{
  finalize_BCs();
  perform_consistency_checks();

  //--------------------------------------------------------------------------//
  //:::::::::::::::::::::::  Initialize code generation  ::::::::::::::::::::://
  //--------------------------------------------------------------------------//
  initialize_code_generator();

  //--------------------------------------------------------------------------//
  //:::::::::::::::::::::  Generate implementation (code)  ::::::::::::::::::://
  //--------------------------------------------------------------------------//
  generate_code_header();
  generate_code_includes();

  generate_code(GENERATE_CLASS_CODE);
  generate_code(GENERATE_FUNCTION_CODE);
  generate_code(GENERATE_JACOBIAN_CODE);
  generate_code(GENERATE_BOTH);

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::  Generate interface (header)  ::::::::::::::::::::://
  //--------------------------------------------------------------------------//
  // This must be after the generate_code(...) calls because they collect
  // information which is needed by the header generator.
  generate_header();

  //--------------------------------------------------------------------------//
  //:::::::::::::::::::::::::  Generate main function  ::::::::::::::::::::::://
  //--------------------------------------------------------------------------//
  switch_to_main();
  generate_main();

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::  Terminate code generation  ::::::::::::::::::::://
  //--------------------------------------------------------------------------//
  terminate_code_generator();
}

void PDECommon::generate_code(int what)
{
  switch (library_)
  {
    case GSL:
      generate_code_gsl(what);
      break;

    case PETSC:
      generate_code_petsc(what);
      break;

    default:
      code_generate_method();
  }
}

} // namespace fd
