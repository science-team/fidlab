#include <iostream>
#include <functional>
#include <cmath>
#include "geometry.hpp"
#include "global_config.h"

using std::fabs;

#define RESOL(i) global_config().space_resolution[i]

/** IO separator for geometry classes. */
static char sep = ' ';

namespace fd {

static const Axis AXIS [3][2]{{Y, Z}, {X, Z}, {X, Y}};
//                      ^  ^
//                      |  |
// Axis normal to plane-+  +--- first/second axis

Axis plane_coordinate_axis(const Axis n, int which)
{ return AXIS[n][which]; }

/*static*/
const char Geometry::set_io_separator(const char c)
{ const char oldval = sep; sep = c; return oldval; }

//============================================================================//
//::::::::::::::::::::::::::::::::  Interval  :::::::::::::::::::::::::::::::://
//============================================================================//
bool Interval::empty(Axis i) const noexcept
{ return b - a < RESOL(i); }

bool Interval::contains(const Interval &r, Axis i) const noexcept
{
  if (r.empty(i)) return true;
  return empty(i) ? false : (RESOL(i) > a-r.a && r.b-b < RESOL(i));
}

bool Interval::contains(double x, Axis i) const noexcept
{ return empty(i) ? false : (RESOL(i) > a-x && x-b < RESOL(i)); }

bool Interval::coincides(const Interval &r, Axis i) const noexcept
{ return empty(i)? r.empty(i) : (fabs(a-r.a)<RESOL(i) && fabs(b-r.b)<RESOL(i)); }

int Interval::extremity(double x, Axis i) const noexcept
{
  if (empty(i)) return -1;
  if (std::fabs(x - a) < RESOL(i)) return 0;
  if (std::fabs(b - x) < RESOL(i)) return 1;
  return -1;
}

Interval Interval::intersect(const Interval &r, Axis i) const noexcept
{
  if (empty(i) || r.empty(i)) return Interval{0,-1};
  return {std::max(a,r.a), std::min(b,r.b)};
}

Interval Interval::hull(const Interval &r, Axis i) const
{
  if (empty(i)) return r;
  if (r.empty(i)) return *this;
  return {std::min(a,r.a), std::max(b,r.b)};
}

/*static*/
void Interval::merge(std::vector<Interval> &l, Axis axis)
{
  auto end = l.end();
  for (auto i = l.begin(); i != end; ++i)
  {
    auto j = i + 1;
    while (j != end)
      if (!i->intersect(*j, axis).empty(axis))
      {
        i->merge(*j, axis);
        std::swap(*j, *--end);
      }
      else
      {
        ++j;
      }
  }
  l.erase(end, l.end());
  /*previously: for (auto i = l.end() - 1; i != end - 1; --i) l.erase(i);*/
}

//============================================================================//
//::::::::::::::::::::::::::::::::  Point2d  ::::::::::::::::::::::::::::::::://
//============================================================================//
bool Point2d::coincides(const Point2d &p, Axis i) const noexcept
{ return fabs(x-p.x) < RESOL(AXIS[i][0]) && fabs(y-p.y) < RESOL(AXIS[i][1]); }

Point Point2d::promote(const Axis n) const noexcept
{ Point pt{0, 0, 0}; pt.p[AXIS[n][0]] = x; pt.p[AXIS[n][1]] = y; return pt; }

//============================================================================//
//:::::::::::::::::::::::::::::::::  Point  :::::::::::::::::::::::::::::::::://
//============================================================================//
Point2d Point::project(const Axis axis) const noexcept
{
  static const int W[3][2]{{1,2},{0,2},{0,1}};
  return Point2d{p[W[axis][0]],p[W[axis][1]]};
}

Point2d Point::project_oriented(const Axis axis) const noexcept
{
  static const int W[3][2]{{1,2},{2,0},{0,1}};
  return Point2d{p[W[axis][0]],p[W[axis][1]]};
}

int Point::position_relative_to_plane_at(double pos, Axis n) const noexcept
{
  const double d = (*this)(n) - pos, res = RESOL(n);
  if (d < -res) return -1;
  if (d >  res) return  1;
  return 0;
}

//============================================================================//
//:::::::::::::::::::::::::::::::  Rectangle  :::::::::::::::::::::::::::::::://
//============================================================================//
bool Rectangle::empty(Axis n) const noexcept
{ return Interval{a,b}.empty(AXIS[n][0]) || Interval{c,d}.empty(AXIS[n][1]); }

double Rectangle::operator()(int i) const
{
#if 0
  switch (i) {
    case 0: return a;
    case 1: return b;
    case 2: return c;
    case 3: return d;
  }
  THROW("index out of range.");
  return -1;
#endif // 0
  static const double Rectangle::* ELEMENT[4]{&Rectangle::a, &Rectangle::b,
                                              &Rectangle::c, &Rectangle::d};
  return this->*(ELEMENT[i]);
}

bool Rectangle::coincides(const Rectangle &r, Axis n) const noexcept
{
  return empty(n) ? r.empty(n) : (
                    Point2d{a,c}.coincides(Point2d{r.a,r.c}, n) &&
                    Point2d{a,d}.coincides(Point2d{r.a,r.d}, n) &&
                    Point2d{b,c}.coincides(Point2d{r.b,r.c}, n) &&
                    Point2d{b,d}.coincides(Point2d{r.b,r.d}, n));
}

bool Rectangle::contains(const Point2d &pt, Axis n) const noexcept
{
  return empty(n) ? false : (Interval{a,b}.contains(pt.x, AXIS[n][0]) &&
                             Interval{c,d}.contains(pt.y, AXIS[n][1]));
}

bool Rectangle::contains(const Rectangle &r, Axis n) const noexcept
{
  return contains(Point2d{r.a,r.c}, n) && contains(Point2d{r.a,r.d}, n) &&
         contains(Point2d{r.b,r.c}, n) && contains(Point2d{r.b,r.d}, n);
}

bool Rectangle::contains_at(double d, const Point &pt, Axis n) const noexcept
{ return std::fabs(d - pt(n)) < RESOL(n) && contains(pt.project(n), n); }

Rectangle Rectangle::rect_hull(const Rectangle &r, Axis n) const
{
  if (empty(n)) return r;
  if (r.empty(n)) return *this;
  return {std::min(a,r.a), std::max(b,r.b), std::min(c,r.c), std::max(d,r.d)};
}

/*static*/
Rectangle Rectangle::rect_hull(const std::vector<Ref> &l, Axis n)
{
  if (l.empty()) return Rectangle{0,-1,0,-1};
  Rectangle r = *l.begin();
  for (auto i = l.begin()+1; i != l.end(); ++i) r = r.rect_hull(*i, n);
  return r;
}

Rectangle Rectangle::intersect(const Rectangle &r, Axis n) const noexcept
{
  if (empty(n) || r.empty(n)) return Rectangle{0,-1,0,-1};
  return {std::max(a,r.a), std::min(b,r.b), std::max(c,r.c), std::min(d,r.d)};
}

/*static*/
bool Rectangle::pairwise_disjoint(const std::vector<Ref> &l, Axis n) noexcept
{
  for (auto i = l.begin(); i != l.end(); ++i)
    for (auto j = i+1; j != l.end(); ++j)
      if (!i->get().intersect(*j, n).empty(n)) return false;
  return true;
}

std::istream& Point2d::restore(std::istream &is)
{ char s; is >> std::noskipws >> x >>s>> y >> std::skipws; return is; }

std::istream& Interval::restore(std::istream &is)
{ char s; is >> std::noskipws >> a >>s>> b >> std::skipws; return is; }

std::istream& Rectangle::restore(std::istream &is)
{ char s; is>>std::noskipws>> a >>s>> b >>s>> c >>s>> d>>std::skipws; return is; }

} // namespace fd

std::ostream& operator<<(std::ostream &os, const fd::Point2d &p)
{ return os << p.x << sep << p.y; }

std::ostream& operator<<(std::ostream &os, const fd::Interval &i)
{ return os << i.a << sep << i.b; }

std::ostream& operator<<(std::ostream &os, const fd::Rectangle &r)
{ return os << r.a << sep << r.b << sep << r.c << sep << r.d; }

std::istream& operator>>(std::istream &is, fd::Point2d &p)
{ is >> p.x >> p.y; return is; }

std::istream& operator>>(std::istream &is, fd::Interval &i)
{ is >> i.a >> i.b; return is; }

std::istream& operator>>(std::istream &is, fd::Rectangle &r)
{ is >> r.a >> r.b >> r.c >> r.d; return is; }
