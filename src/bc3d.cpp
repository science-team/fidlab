#include <iostream>
#include "bc3d.hpp"
#include "global_config.h"

namespace fd3 {


void Boundary3d::print() const
{
  fd::Boundary::print();
  if (type != G)
    std::cout << " (" << rect_ << ")";
}

void Boundary3d::check() const
{
  static const std::string s[3] {"x = ", "y = ", "z = "};
  THROW_IF(rect_.empty(static_cast<fd::Axis>(type)),
      "Empty boundary at "+s[type]+fd::to_string(pos()));
}

} // namespace fd3
