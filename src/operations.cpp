#include "operations.hpp"

using namespace fd;

static bool operations_use_brackets_hint = false;
static bool operations_omit_zero = false;
static bool operations_omit_one = false;

inline bool use_brackets_2nd(int hint)
{
  const bool b = (
      hint == CodeFragment::USE_BRACKETS ||
      hint == CodeFragment::BRACKETS_IF_LAST ||
      hint == CodeFragment::BRACKETS_IF_NOT_FIRST
  );
  return operations_use_brackets_hint ? b : true;
}

void fd::set_operations_options(const std::string& which, bool value)
{
  if (which == "optimize_brackets_use") operations_use_brackets_hint = value;
  else if (which == "omit_zero") operations_omit_zero = value;
  else if (which == "omit_one") operations_omit_one = value;
  else if (which == "all")
    for (auto opt : {"optimize_brackets_use", "omit_zero", "omit_one"})
      set_operations_options(opt, value);
}

// -----------------------------------------------------------------------------
// AddProcessor
// -----------------------------------------------------------------------------

bool AddProcessor::useby2() const
{ return b.terminal ? use_brackets_2nd(b.hint_y) : true; }

bool AddProcessor::usebj2() const
{ return b.terminal ? use_brackets_2nd(b.hint_j) : true; }

bool AddProcessor::usebxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_y[which]) : true; }

bool AddProcessor::usebxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_j[which]) : true; }

bool AddProcessor::usevxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_y[which]) : true; }

bool AddProcessor::usevxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_j[which]) : true; }

// -----------------------------------------------------------------------------
// SubProcessor
// -----------------------------------------------------------------------------

bool SubProcessor::useby2() const
{ return b.terminal ? use_brackets_2nd(b.hint_y) : true; }

bool SubProcessor::usebj2() const
{ return b.terminal ? use_brackets_2nd(b.hint_j) : true; }

bool SubProcessor::usebxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_y[which]) : true; }

bool SubProcessor::usebxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_j[which]) : true; }

bool SubProcessor::usevxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_y[which]) : true; }

bool SubProcessor::usevxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_j[which]) : true; }

// -----------------------------------------------------------------------------
// MulProcessor
// -----------------------------------------------------------------------------

bool MulProcessor::useby1() const
{ return a.terminal ? use_brackets_2nd(a.hint_y) : true; }

bool MulProcessor::usebj1() const
{ return a.terminal ? use_brackets_2nd(a.hint_j) : true; }

bool MulProcessor::usebxy1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_bx_y[which]) : true; }

bool MulProcessor::usebxj1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_bx_j[which]) : true; }

bool MulProcessor::usevxy1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_vx_y[which]) : true; }

bool MulProcessor::usevxj1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_vx_j[which]) : true; }

bool MulProcessor::useby2() const
{ return b.terminal ? use_brackets_2nd(b.hint_y) : true; }

bool MulProcessor::usebj2() const
{ return b.terminal ? use_brackets_2nd(b.hint_j) : true; }

bool MulProcessor::usebxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_y[which]) : true; }

bool MulProcessor::usebxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_j[which]) : true; }

bool MulProcessor::usevxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_y[which]) : true; }

bool MulProcessor::usevxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_j[which]) : true; }

// -----------------------------------------------------------------------------
// DivProcessor
// -----------------------------------------------------------------------------

bool DivProcessor::useby1() const
{ return a.terminal ? use_brackets_2nd(a.hint_y) : true; }

bool DivProcessor::usebj1() const
{ return a.terminal ? use_brackets_2nd(a.hint_j) : true; }

bool DivProcessor::usebxy1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_bx_y[which]) : true; }

bool DivProcessor::usebxj1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_bx_j[which]) : true; }

bool DivProcessor::usevxy1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_vx_y[which]) : true; }

bool DivProcessor::usevxj1(int which) const
{ return a.terminal ? use_brackets_2nd(a.hint_vx_j[which]) : true; }

bool DivProcessor::useby2() const
{ return b.terminal ? use_brackets_2nd(b.hint_y) : true; }

bool DivProcessor::usebj2() const
{ return b.terminal ? use_brackets_2nd(b.hint_j) : true; }

bool DivProcessor::usebxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_y[which]) : true; }

bool DivProcessor::usebxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_bx_j[which]) : true; }

bool DivProcessor::usevxy2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_y[which]) : true; }

bool DivProcessor::usevxj2(int which) const
{ return b.terminal ? use_brackets_2nd(b.hint_vx_j[which]) : true; }
