// #include <cstdlib>
// #include <cmath>
// #include <ctime>
#include "pdexd3d.hpp"

using std::string;
using std::vector;
using std::array;
using fd::Boundary;

namespace extdom {
namespace d3 {

//============================================================================//
//::::::::::::::::::::::::::::::::::  PDE  ::::::::::::::::::::::::::::::::::://
//============================================================================//

static const auto format_element =
    [](const string&,const string&,const string&,const string&) {return _S"eq";};

static const int fd::Field::* FEXTR[3][2] {
  {&fd::Field::i_start, &fd::Field::i_end},
  {&fd::Field::j_start, &fd::Field::j_end},
  {&fd::Field::k_start, &fd::Field::k_end}
};

static const int MISSING_AXIS[4] {-1, fd::Axis::Z, fd::Axis::Y, fd::Axis::X};
//                                /        /             /            /
//                              X+X      X+Y           X+Z          Y+Z

PDE::EdgeInfo PDE::get_edge_info(int eid, int c) const
{
  const int * const fids = get_edge_fids(eid);
  const int ft[2] { // face's boundary type or normal axis
    Mesh::face_btype(fids[0]),
    Mesh::face_btype(fids[1])
  };
  const int fx[2] {  // face extremity
    Mesh::face_extremity(fids[0]),
    Mesh::face_extremity(fids[1])
  };

  ASSERT(ft[0] > -1 && ft[1] > -1 && ft[0] + ft[1] < 4 && ft[0] != ft[1]);

  const int XEXTR[3][2] {
    {-1, mesh_->nx + 1},
    {-1, mesh_->ny + 1},
    {-1, mesh_->nz + 1}
  };
  const int xextr[2] {  // face's extended extremity (node coordinate)
    field_[c].*FEXTR[ft[0]][fx[0]],
    field_[c].*FEXTR[ft[1]][fx[1]],
  };
  const int axis = MISSING_AXIS[ft[0]+ft[1]];

  THROW_IF(axis == -1, "Internal error 0020. Please report this error.");

  return {
      fids,
      axis,
      {ft[0], ft[1]},
      {fx[0], fx[1]},
      {xextr[0], xextr[1]},
      {xextr[0] == XEXTR[ft[0]][fx[0]], xextr[1] == XEXTR[ft[1]][fx[1]]},
  };
}

bool PDE::has_extended_boundary(const PDE::EdgeInfo& ei, int which_side) const
{ return ei.extended_boundary[which_side]; }

bool PDE::has_extended_boundary(int eid, int c, int side) const
{ return has_extended_boundary(get_edge_info(eid, c), side); }

PDE::VertexInfo PDE::get_vertex_info(int vid, int c) const
{
  const int * const fids = get_vertex_fids(vid);
  const int ft[3] { // face's boundary type or normal axis
    Mesh::face_btype(fids[0]),
    Mesh::face_btype(fids[1]),
    Mesh::face_btype(fids[2]),
  };
  const int fx[3] {  // face extremity
    Mesh::face_extremity(fids[0]),
    Mesh::face_extremity(fids[1]),
    Mesh::face_extremity(fids[2]),
  };

  ASSERT(ft[0] > -1 && ft[1] > -1 && ft[2] > -1);
  ASSERT(ft[0] <  3 && ft[1] <  3 && ft[2] <  3);
  ASSERT(ft[0] != ft[1] && ft[1] != ft[2] && ft[2] != ft[0]);

  const int XEXTR[3][2] {
    {-1, mesh_->nx + 1},
    {-1, mesh_->ny + 1},
    {-1, mesh_->nz + 1}
  };
  const int xextr[3] {  // face's extended extremity (node coordinate)
    field_[c].*FEXTR[ft[0]][fx[0]],
    field_[c].*FEXTR[ft[1]][fx[1]],
    field_[c].*FEXTR[ft[2]][fx[2]],
  };

  return {
      fids,
      {ft[0], ft[1], ft[2]},
      {fx[0], fx[1], fx[2]},
      {xextr[0], xextr[1], xextr[2]},
      {xextr[0] == XEXTR[ft[0]][fx[0]], xextr[1] == XEXTR[ft[1]][fx[1]],
                                        xextr[2] == XEXTR[ft[2]][fx[2]]},
  };
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::  Code Generators  ::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

static const char *COORD_DELTA[3][6] {
    {"-1",  "",  "+1",  "",   "",   "" },
    { "",  "-1",  "",  "+1",  "",   "" },
    { "",   "",   "",   "",  "-1", "+1"}
};

static void sShiftVirtualNode(int sid, string &I, string &J, string &K)
{
  I += COORD_DELTA[Boundary::X][sid];
  J += COORD_DELTA[Boundary::Y][sid];
  K += COORD_DELTA[Boundary::Z][sid];
}

#if 0
int PDE::generate_code_strictly_NBC_for(int c,
                                        const array<int,3>& bids,
                                        const array<bool,3>& neumann,
                                        int what,
                                        int ntabs,
                                        int flags,
                                        ElementFormatter ef)
{
  ASSERT(boundary_discretization_method_ == SYM_DIFF2);
  ASSERT(check_bc_code(bc_code_flags));

  const int D = dimension() == 3 ? (bids[2] == -1 ? 2 : 3) : 2;
  const string tabs(ntabs, '\t');
  const string cindex(fd::to_string_empty_if(components() == 1, c));

  fd::CodeFragment cfb, cfx;
  auto btext_y = std::cref(cfb.text_y);
  auto btext_j = std::cref(cfb.text_j);

  int rv = 0;
  for (int i = 0; i < D; ++i)
  {
    get_code_for_boundary_SYM_DIFF2(bids[i], c, cfb, cfx);
    if ((i == 0 && neumann[i] && ordinary_bc_code(flags)) ||
        (i == 1 && neumann[i] && secondary_bc_code(flags)) ||
        (i == 2 && neumann[i] && tertiary_bc_code(flags)))
    {
      string I, J, K;
      std::tie(I, J, K) = extended_node_coordinates(get_sid(bids[i]));
      if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[" << ef(I, J, K, cindex) << "] = "<<btext_y.get()<<";";
      else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[" << ef(I, J, K, cindex) << "] = "<<btext_j.get()<<";";
      ++rv;
    }
  }

  return rv;
}
#endif // 0

void PDE::generate_code_dummy_BC(int what, int ntabs) noexcept
{
  const string tabs(ntabs, '\t');
  if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[eq] = u[eq];";
  else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[eq] = du[eq];";
}


void PDE::generate_code_for_extended_edge(
                                        const int eid,
                                        const int face_selector,
                                        const int bid,
                                        const int bid2,
                                        EdgeInfo  edge_info_field[],
                                        const int (&idx)[2],
                                        const int what,
                                        const int ntabs) {
//  constexpr int NBC = fd::BoundaryCondition::NEUMANN;
  static const int FLAGS[3]{ORDINARY_BC_CODE, SECONDARY_BC_CODE, TERTIARY_BC_CODE};
  const int nc = components();
  const bool exist_extended_edges = [nc, &edge_info_field](int which) {
    for (int c = 0; c < nc; ++c) {
      const auto& xb = edge_info_field[c].extended_boundary;
      if (xb[which]) return true;
    }
    return false;
  }(idx[face_selector]);

  if (!exist_extended_edges) return;

  const array<int,3> bids {bid, bid2, -1};
  const auto& edge_info = edge_info_field[0]; // common information
  const string tabs(ntabs, '\t');
  string I = i_, J = j_, K = k_;
  sShiftVirtualNode(edge_info.fids[idx[face_selector]], I, J, K);

  code_ << R"(
	else if (in_boundary_edge()"<<bid<<","<<eid<<","<<I<<","<<J<<","<<K<<R"())
	{)";
  if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";

  for (int c = 0; c < nc; ++c)
  {
#if 0
    const array<bool,3> neumann {
        get_BC_for_component(get_component(c), bid )->type == NBC,
        get_BC_for_component(get_component(c), bid2)->type == NBC,
        false,
    };
#endif // 0
    const auto& xb = edge_info_field[c].extended_boundary;

    if (!xb[idx[face_selector]]) /*this is not an extended boundary*/ continue;

    if (nc > 1) code_ << R"(
)"<<tabs<<R"(	case )"<<c<<":";
    const int written_eqs = generate_code_SYM_DIFF2_for(c, bids, what, ntabs,
        FLAGS[face_selector], format_element);
    if (written_eqs == 0) generate_code_dummy_BC(what, ntabs);
#if 0
    if (neumann[face_selector])
      generate_code_strictly_NBC_for(c, bids, neumann,
          what, ntabs, FLAGS[face_selector], format_element);
    else
      generate_code_dummy_BC(what, ntabs);
#endif // 0
    if (nc > 1) code_ << R"(
)"<<tabs<<R"(	break;)";
  }
}

void PDE::generate_code_function_body_petsc_nonlinear(int what)
{
  THROW_IF(what != GENERATE_FUNCTION_CODE && what != GENERATE_JACOBIAN_CODE,
           "Incorrect use of method.");

  enum BType {VERTEX, EDGE, FACE};

  const Mesh &mesh = *mesh_;
  const int nc = components();
  const string rv(what == GENERATE_FUNCTION_CODE ? "F" : "J");
  const bool collect_bs_info = boundary_sides_.empty();

  // ---------------------------------------------------------------------------
  auto generate_code_vertex_face = [this, what, nc](int vid, int bid, BType bt,
                                                    int ntabs)
  {
    ASSERT(bt == VERTEX || bt == FACE);

    const int flags = EXTRA_BC_CODE;
    array<int,3> bids;

    if (bt == VERTEX) bids = get_vertex_bids(vid, bid);
    if (nc > 1) ntabs += 2;

    const string tabs(ntabs, '\t');

    if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
	)"<<tabs<<R"(case )"<<c<<":";
      if (bt == VERTEX)
        generate_code_SYM_DIFF2_for(c, bids, what, ntabs, flags, format_element);
      else
        generate_code_SYM_DIFF2_for(c, bid, what, ntabs, flags, format_element);
      if (nc > 1 && c < nc-1) code_ << R"(
		)"<<tabs<<R"(break;)";
	}
    if (nc > 1) code_ << R"(
)"<<tabs<<"}";
  }; //-------------------------------------------------------------------------
  auto generate_code_edge = [this, what, nc](int eid, int bid, int fid2,
                                             int ntabs, int flags)
  {
    const int bid2 = std::get<0>(mesh_->get_meeting_bid(bid, eid));

    THROW_IF(bid2 == -1, "No matching boundary along edge "+fd::to_string(eid)
        +" for boundary "+fd::to_string(bid)+".");

    if (nc > 1) ntabs += 2;

    const array<int,3> bids{bid, bid2, -1};
    const string tabs(ntabs, '\t');

    if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
	)"<<tabs<<R"(case )"<<c<<":";
      generate_code_SYM_DIFF2_for(c, bids, what, ntabs, flags, format_element);
      if (nc > 1 && c < nc-1) code_ << R"(
		)"<<tabs<<R"(break;)";
	}
	if (nc > 1) code_ << R"(
)"<<tabs<<"}";
  }; //-------------------------------------------------------------------------

  //==========================================================================//
  //::::::::::::::::::::::::  Generate function body  :::::::::::::::::::::::://
  //==========================================================================//

  { const string decl_c(nc == 1 ? "" : "const int c = ");
    const string rt(what == GENERATE_FUNCTION_CODE ?
	R"(// Instruct compiler to handle F[eq] as if it were a double
	struct {
		double value;
		inline operator double() const noexcept {return value;}
		inline double &operator[](int) noexcept {return value;}
		inline void operator=(double x) noexcept {value=x;}
	})" : "SparseVector");
		code_ << R"(
{
	)"<<rt<<" "<<rv<<R"(;
	// End (one beyond last) of internal nodes
	const int il = nx-1, jl = ny-1, kl = nz-1;
	// Get node corresponding to field index eq
	int mesh_coord[3];
	)"<<decl_c<<R"(field_index_to_mesh_coordinates(eq, mesh_coord);
	const int i = mesh_coord[0];
	const int j = mesh_coord[1];
	const int k = mesh_coord[2];)"; } //***( End of block )***

  code_ << std::endl;

  if (!generate_code_function_body_replace_eqs(what, 0, "c", "eq", false))
    code_ << R"(
	)";

  //==========================================================================//
  //:::::::::::::::::::::::::                        ::::::::::::::::::::::::://
  //:::::::::::::::::::::::::   NEW IMPLEMENTATION   ::::::::::::::::::::::::://
  //:::::::::::::::::::::::::                        ::::::::::::::::::::::::://
  //==========================================================================//

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::  Generate code for the interior  :::::::::::::::::::://
  //--------------------------------------------------------------------------//

  code_ << R"(if (i > 0 && j > 0 && k > 0 && i < il && j < jl && k < kl)
	{)";
  if (nc > 1) code_ << R"(
		switch (c)
		{)";

  const string tabs(nc > 1 ? 2 : 0, '\t');

  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
			case )"<<c<<":";
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[eq] = "<< equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[eq] = "<< equation_codes_[c].text_j << ";";
    if (nc > 1 && c < nc-1) code_ << R"(
				break;)";
  }

  if (nc > 1) code_ << R"(
		})";
  code_ << R"(
	})";

  //--------------------------------------------------------------------------//
  //:::::::::::::::::::::  Generate code for boundaries  ::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  const int ntabs = 0 + (nc > 1 ? 2 : 0);
  vector<int> vertices_handled, edges_handled;
  array<vector<fd::Interval>,12> edge_intervals_handled;

  for (int bid = 0; bid < mesh.num_boundaries(); ++bid)
  {
    //------------------------------------------------------------------------//
    //:::::::::::::::::::::  Generate code for vertices  ::::::::::::::::::::://
    //------------------------------------------------------------------------//

    const vector<int> domain_vertices = mesh.get_domain_vertices(bid);
    for (const auto vid : domain_vertices)
    {
      if (fd::find(vertices_handled, vid)) continue;
      code_ << R"(
	else if (is_vertex()"<<vid<<R"(,i,j,k))
	{)";
      generate_code_vertex_face(vid, bid, VERTEX, 0);
      code_ << R"(
	})";
      vertices_handled.push_back(vid);
    }

    //------------------------------------------------------------------------//
    //::::::::::::::::::::::  Generate code for edges  ::::::::::::::::::::::://
    //------------------------------------------------------------------------//

    const vector<int> domain_edges = mesh.get_domain_edges(bid);
    for (const auto eid : domain_edges)
    {
      if (fd::find(edges_handled, eid)) continue;

      const int fid2 = mesh.get_meeting_face(bid, eid);
      fd::Axis axis;
      fd::Interval side;
      std::tie(side, axis) = mesh.get_boundary_side(bid, fid2);

      THROW_IF(side.empty(axis), "Incorrect side for boundary "+
                                 fd::to_string(bid)+": "+fd::to_string(fid2));

      edge_intervals_handled[eid].push_back(side);
      fd::Interval::merge(edge_intervals_handled[eid], axis);
      if (edge_intervals_handled[eid].size() == 1)
      {
        fd::Axis i;
        fd::Interval domain_side;
        std::tie(domain_side, i) = mesh.edge_interval(eid);

        ASSERT(i == axis);

        if (edge_intervals_handled[eid].front().coincides(domain_side, i))
          edges_handled.push_back(eid);
      }

      EdgeInfo edge_info_field[nc];
      for (int c = 0; c < nc; ++c)
        edge_info_field[c] = get_edge_info(eid, c);
      const auto& edge_info = edge_info_field[0];
      int idx[2] {0, 1};
      if (fid2 == edge_info.fids[0]) {idx[0] = 1; idx[1] = 0;}
      const int bid2 = std::get<0>(mesh.get_meeting_bid(bid, eid));

      //----------------------------------------------------------------------//
      //:::::::::::::::::::  Main (EXTRA) code for edges  :::::::::::::::::::://
      //----------------------------------------------------------------------//

      int bsindex;
      if (collect_bs_info)
      {
        bsindex = (int) boundary_sides_.size();
        boundary_sides_.push_back({bid,eid,axis,side});
      }
      else
      {
        bsindex = fd::index(boundary_sides_, {bid,eid,axis,side});
        THROW_IF(bsindex == -1,"Internal error 0005. Please report this error.");
      }
    
      code_ << R"(
	else if (in_boundary_edge()"<<bsindex<<R"(,i,j,k))
	{)";
      generate_code_edge(eid, bid, fid2, 0, EXTRA_BC_CODE);
      code_ << R"(
	})";

      //----------------------------------------------------------------------//
      //::::::::::::::::::::::  Primary extended edge  ::::::::::::::::::::::://
      //----------------------------------------------------------------------//
      const int (&IDX)[2] = idx;
      generate_code_for_extended_edge(eid, 0, bid, bid2, edge_info_field, IDX,
                                      what, ntabs);
#if 0
      string I = i_, J = j_, K = k_;
      sShiftVirtualNode(edge_info.fids[idx[0]], I, J, K);

      code_ << R"(
	else if (in_boundary_edge()"<<bid<<","<<eid<<","<<I<<","<<J<<","<<K<<R"())
	{)";
      if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";

      for (int c = 0; c < nc; ++c)
      {
        const bool neumann[2] {
            get_BC_for_component(get_component(c), bid )->type == NBC,
            get_BC_for_component(get_component(c), bid2)->type == NBC,
        };
        const auto& xb = edge_info_field[c].extended_boundary;

        if (!xb[idx[0]]) /*this is not an extended boundary*/ continue;

        if (nc > 1) code_ << R"(
)"<<tabs<<R"(	case )"<<c<<":";
        if (neumann[0])
          generate_code_strictly_NBC_for(c, {bid, bid2, -1},
              {neumann[0], neumann[1], false}, what, ntabs, ORDINARY_BC_CODE,
              format_element);
        else
          generate_code_dummy_BC(what, ntabs);
        if (nc > 1) code_ << R"(
)"<<tabs<<R"(	break;)";
#endif // 0

      //----------------------------------------------------------------------//
      //:::::::::::::::::::::  Secondary extended edge  :::::::::::::::::::::://
      //----------------------------------------------------------------------//
      generate_code_for_extended_edge(eid, 1, bid, bid2, edge_info_field, IDX,
                                      what, ntabs);
    }

    //------------------------------------------------------------------------//
    //::::::::::::::::::  Generate code for face interior  ::::::::::::::::::://
    //------------------------------------------------------------------------//

    code_ << R"(
	else if (in_boundary()"<<bid<<R"(,i,j,k))
	{)";
    generate_code_vertex_face(-1, bid, FACE, 0);
    code_ << R"(
	})";
  }

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::::::  Virtual edges  ::::::::::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  auto exist_virtual_edges = [nc](int eid, EdgeInfo edge_info_field[]) {
    for (int c = 0; c < nc; ++c)
    {
      const auto& xb = edge_info_field[c].extended_boundary;
      if (xb[0] && xb[1]) /*this is a virtual edge*/ return true;
    }
    return false;
  };

  for (int eid = 0; eid < 12; ++eid)
  {
    EdgeInfo edge_info_field[nc];
    for (int c = 0; c < nc; ++c)
      edge_info_field[c] = get_edge_info(eid, c);

    if (!exist_virtual_edges(eid, edge_info_field)) continue;

    const auto& edge_info = edge_info_field[0];
    string I = i_, J = j_, K = k_;
    sShiftVirtualNode(edge_info.fids[0], I, J, K);
    sShiftVirtualNode(edge_info.fids[1], I, J, K);

    code_ << R"(
	else if (in_edge()"<<eid<<","<<I<<","<<J<<","<<K<<R"())
	{)";
		if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";

    for (int c = 0; c < nc; ++c)
    {
      const auto& xb = edge_info_field[c].extended_boundary;

      if (!xb[0] || !xb[1]) /*this is not a virtual edge*/ continue;

      if (nc > 1) code_ << R"(
)"<<tabs<<R"(	case )"<<c<<":";
      generate_code_dummy_BC(what, ntabs);
      if (nc > 1) code_ << R"(
)"<<tabs<<R"(	break;)";
    }

    if (nc > 1) code_ << R"(
)"<<tabs<<"}";
    code_ << R"(
	})";
  }

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::::  Extended vertices  ::::::::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  auto exist_extended_vertices = [nc](int vid, VertexInfo vertex_info_field[]) {
    for (int c = 0; c < nc; ++c)
    {
      const auto& xb = vertex_info_field[c].extended_boundary;
      if (xb[0] && xb[1] && xb[2]) /*this is a virtual vertex*/ return true;
    }
    return false;
  };

  for (int vid = 0; vid < 8; ++vid)
  {
    const auto bids = get_vertex_bids(vid);
    VertexInfo vertex_info_field[nc];
    for (int c = 0; c < nc; ++c)
      vertex_info_field[c] = get_vertex_info(vid, c);

    if (!exist_extended_vertices(vid, vertex_info_field)) continue;

    const auto& vertex_info = vertex_info_field[0];
    // const array<int,3> bids{vertex_info.fids[0], vertex_info.fids[1], vertex_info.fids[2]};
    string I = i_, J = j_, K = k_;
    sShiftVirtualNode(vertex_info.fids[0], I, J, K);
    sShiftVirtualNode(vertex_info.fids[1], I, J, K);
    sShiftVirtualNode(vertex_info.fids[2], I, J, K);

    code_ << R"(
	else if (is_vertex()"<<vid<<","<<I<<","<<J<<","<<K<<R"())
	{)";
    if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";

    for (int c = 0; c < nc; ++c)
    {
      const auto& xb = vertex_info_field[c].extended_boundary;

      if (!xb[0] || !xb[1] || !xb[2]) continue;

      if (nc > 1) code_ << R"(
)"<<tabs<<R"(	case )"<<c<<":";
      const int written_eqs = generate_code_SYM_DIFF2_for(c, bids, what, ntabs,
          TERTIARY_BC_CODE, format_element);
      THROW_IF(written_eqs > 1,"Internal error 0023. Please report this error.");
      if (written_eqs == 0) generate_code_dummy_BC(what, ntabs);
      if (nc > 1) code_ << R"(
)"<<tabs<<R"(	break;)";
    }

    if (nc > 1) code_ << R"(
)"<<tabs<<"}";
    code_ << R"(
	})";
  }

  // Generate code for virtual corners -- active for SYM_DIFF2 code generation
  if (has_neumann_BCs())
    generate_code_virtual_points(what);

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::  End new implementation  :::::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  code_ << R"(
	return )"<<rv<<(what == GENERATE_FUNCTION_CODE ? ".value" : "[eq]")<<R"(;
}
)";
}

} // namespace d3
} // namespace extdom
