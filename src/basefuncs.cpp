#include "basedefs.h"
#include <fstream>
#include <regex>
#include <limits>
#include <cmath>
#include "global_config.h"

namespace fd {
  
std::vector<std::string> s_GlobalLog;

void replace_all(std::string& s, const std::string& from, const std::string& to)
{
  const std::size_t k = from.length(), l = to.length();
  for (std::size_t p = 0; (p = s.find(from, p)) != std::string::npos; p += l)
		s.replace(p, k, to);
}

std::string strip_front(const std::string& s, std::function<bool(const char)> f)
{
  auto pos = std::find_if_not(s.cbegin(), s.cend(), f);
  return std::string(pos, s.cend());
}

std::string strip_back(const std::string& s, std::function<bool(const char)> f)
{
  auto pos = std::find_if_not(s.crbegin(), s.crend(), f);
  return std::string(s.cbegin(), pos.base());
}

std::string strip_all(const std::string& s, std::function<bool(const char)> f)
{ return strip_back(strip_front(s, f), f); }

std::string strip_space_front(const std::string& s)
{	return strip_front(s, [](const char c){return isspace(c);}); }

std::string strip_space_back(const std::string& s)
{	return strip_back(s, [](const char c){return isspace(c);}); }

std::string strip_space(const std::string& s)
{	return strip_all(s, [](const char c){return isspace(c);}); }

#	if __cplusplus < 201103L
template<> std::string to_string<int>(int val)
{ char buf[256]; sprintf(buf, "%d", val); return std::string(buf); }
template<> std::string to_string<long>(long val)
{ char buf[256]; sprintf(buf, "%ld", val); return std::string(buf); }
template<> std::string to_string<unsigned>(unsigned val)
{ char buf[256]; sprintf(buf, "%u", val); return std::string(buf); }
template<> std::string to_string<unsigned long>(unsigned long val)
{ char buf[256]; sprintf(buf, "%lu", val); return std::string(buf); }
# endif // __cplusplus
template<> std::string to_string<double>(double val)
{ char buf[256]; sprintf(buf, "%g", val); return std::string(buf); }

int parse_int(const std::string& num, const std::string& prefix)
{	return std::stoi(num.substr(prefix.size())); }

bool regex_match_funcname(const std::string& fname)
{	return std::regex_match(fname, std::regex("[_a-zA-Z][_a-zA-Z0-9]*")); }

#if 0
int parse_int(const std::string& s,
							const std::string& prefix, const std::string& postfix,
							std::function<void(const std::string& trail)> on_error)
{
  const std::string num = s.substr(prefix.size());
	std::string::size_type endpos;
	int result;
	try	{result = std::stoi(num, &endpos);}
	catch (std::invalid_argument& err) {if (on_error != 0) on_error(s);}
  if (on_error == 0) return result;
  if (endpos == 0) on_error(num);
	else if (!postfix.empty() && num.substr(endpos) != postfix)
		on_error(num.substr(endpos));
	return result;
}

int parse_int(const std::string& num, const std::string& prefix,
              const std::string& postfix, const std::string& error_msg)
{
  const std::string msg(error_msg.empty() ? "Error parsing " +
      num.substr(0, 12) : error_msg);
	return parse_int(num, prefix, postfix, [](const std::string&) {throw msg;});
}
#endif // 0

bool file_exists(const std::string& name)
{	std::ifstream f(name.c_str()); return f.good(); }

void backup_old_file(const std::string& filename)
{
	if (!fd::file_exists(filename)) return;
  const int max_backup_files = fd::global_config().max_backup_files;
	std::string new_name;
	int i = 1;
	for (; i < max_backup_files; ++i)
	{
		const std::string tmpname = filename + "." + fd::to_string(i);
		if (fd::file_exists(tmpname)) continue;
		new_name = tmpname;
		break;
	}
	if (i == max_backup_files) throw _S"Too many backup files.";
	// Rename old file
	i = rename(filename.c_str(), new_name.c_str());
	if (i != 0 || fd::file_exists(filename))
		throw "Unable to backup file " + filename;
}

bool is_zero(double x)
{ return fabs(x) < std::numeric_limits<double>::min(); }

void log(const std::string& msg)
{ s_GlobalLog.push_back(msg); }

} // namespace fd
