#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include "pde1d.hpp"
#include "config_code.h"

using std::string;
using fd::NeumannBC;

namespace fd1 {

// static inline
// string code_elem_lhs(const string& f, const string& i, const string& c)
// { return f + "(" + i + (c.empty() ? _S("") : "," + c) + ")"; }

// =============================================================================
// PDE
// =============================================================================

// -----------------------------------------------------------------------------
// Code Generators
// -----------------------------------------------------------------------------

void PDE::generate_code_function_body(int what)
{
  // ---------------------------------------------------------------------------
  // Generate function body
  // ---------------------------------------------------------------------------

  code_ << R"(
{
	for (int i = 0; i < nx; ++i)
	{)";

  const int nc = components();

  // ---------------------------------------------------------------------------
  auto generate_code_at = [this, what, nc](int sid, int ntabs)
  {
    const int bid = (sid == Mesh::R ? 1 : 0);
    for (int c = 0; c < nc; ++c)
      if (boundary_discretization_method_ == SYM_DIFF2)
        generate_code_SYM_DIFF2_for(c, bid, what, ntabs, ALL_BC_CODES_2D, [this](
            const string& i, const string& j, const string& k, const string& c) {
                return format_elem_lhs("e", i, j, k, c);
          });
      else
        generate_code_for(c, bid, what, ntabs);
  }; //-------------------------------------------------------------------------

  if (!generate_code_function_body_replace_eqs(what, 1)) code_ << R"(
		)";

  code_ << R"(if (i == 0)
		{)";
  generate_code_at(Mesh::L, 1);
  code_ << R"(
		}
		else if (i == nx-1)
		{)";
  generate_code_at(Mesh::R, 1);
  code_ << R"(
		}
		else
		{)";

  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
			// Field component )" << c;
    const string findex = fd::to_string_empty_if(nc == 1, c);
    // const string eqno = code_elem_lhs("e", i_, findex);
    const string eqno = format_elem_lhs(findex);
    if (what == GENERATE_FUNCTION_CODE)
    	code_ << R"(
			F[)" << eqno << "] = "<< equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE)
      code_ << R"(
			J[)" << eqno << "] = "<< equation_codes_[c].text_j << ";";
    else
      code_ << R"(
			F[)" << eqno << "] = "<< equation_codes_[c].text_y << R"(;
			J[)" << eqno << "] = "<< equation_codes_[c].text_j << ";";
  }

  code_ << R"(
		}
	})";

  // No virtual corners in 1d

  generate_code_function_body_replace_eqs_ns(what);

  code_ << R"(
}
)";
}

// -------------------------------------------------------------------------- //
// ===========================  Main File Generator  ======================== //
// -------------------------------------------------------------------------- //

string PDE::format_constructor_comment() const
{
  return "// Discretize PDE " + _S(components() == 1 ? "" : "system ") +
  "on a " + fd::to_string(mesh_->nx) + _S"-grid." + R"(
	// Replace default constructor with pde(n) for a n-grid.)";
}

string PDE::format_solution_output_node(int c) const
{ return "i" + (components() == 1 ? _S"" : ","+fd::to_string(c)); }

string PDE::format_solution_output_coord(int c) const
{ return (components() == 1 ? _S"" : fd::to_string(c)+",")+"pde.X(i)"; }

string PDE::format_coord() const
{ return _S(components() == 1 ? "" : "c,")+"pde.X(i)"; }

// -------------------------------------------------------------------------- //
// =====================  Header (Interface) Generator  ===================== //
// -------------------------------------------------------------------------- //

void PDE::generate_header_constructors()
{
  code_h_ << R"(

	//! Default constructor.
	)"<<name<<R"(() :
		nx()" << mesh_->nx << R"(),
		a()"  << mesh_->a  << "), b("  << mesh_->b  << R"(),
		output_results(true) {init();}
	//! Construct mesh with specified number of nodes.
	)"<<name<<R"((int kx) :
		nx(kx),
		a()" << mesh_->a << "), b(" << mesh_->b << ")," << R"(
		output_results(true) {init();}
	//! Construct mesh with specified number of nodes and solution domain.
	)"<<name<<R"((int kx, double x0, double x1) :
		nx(kx), a(x0), b(x1), output_results(true) {init();}
)";
}

} // namespace fd1
