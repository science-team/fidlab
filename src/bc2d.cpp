#include <iostream>
#include "bc2d.hpp"
#include "global_config.h"

namespace fd2 {

void xBoundary::print() const
{
  Boundary::print();
  std::cout << " (" << y0() << "," << y1() << ")";
}

void yBoundary::print() const
{
  Boundary::print();
  std::cout << " (" << x0() << "," << x1() << ")";
}

void xBoundary::check() const
{
	if (fabs(y1_ - y0_) < fd::global_config().space_resolution[1])
		throw _S"Incorrect x-boundary at " + fd::to_string(pos());
}

void yBoundary::check() const
{
	if (fabs(x1_ - x0_) < fd::global_config().space_resolution[0])
		throw _S"Incorrect y-boundary at " + fd::to_string(pos());
}

} // namespace fd2
