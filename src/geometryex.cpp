#include <iostream>
#include <functional>
#include <cmath>
#include "geometryex.hpp"
#include "global_config.h"

#define RESOL(i) global_config().space_resolution[i]

using std::vector;

namespace fd {

static const Axis sAxis[3][2]{{Y, Z}, {X, Z}, {X, Y}};
//                      ^  ^
//                      |  |
// Axis normal to plane-+  +--- first/second
static const std::function<Point(const Line&)> FORIGIN[3] {
    [](const Line& l) {return Point{l.a,l.pos.x,l.pos.y};},
    [](const Line& l) {return Point{l.pos.x,l.a,l.pos.y};},
    [](const Line& l) {return Point{l.pos.x,l.pos.y,l.a};},
};

static vector<RectangleX> Partition(std::array<vector<double>,2>, Axis n);

//============================================================================//
//::::::::::::::::::::::::::::::::::  Line  :::::::::::::::::::::::::::::::::://
//============================================================================//
Point Line::origin() const
{ return FORIGIN[axis](*this); }

bool Line::contains(const Point &p) const noexcept
{ return empty()? false : p.project(axis).coincides(pos, axis); }

bool Line::contains(const Point &p, LineRestriction t) const noexcept
{
  if (!contains(p)) return false;
  if (t == LINE) return true;
  int pos_rtp = p.position_relative_to_plane_at(a, axis);
  if (t == RAY_POS) return pos_rtp >= 0;
  if (t == RAY_NEG) return pos_rtp <= 0;
  int pos_rtp_2 = p.position_relative_to_plane_at(b, axis);
  return pos_rtp >= 0 && pos_rtp_2 <= 0;
}

bool Line::intersects(const Rectangle &r, Axis n) const
{
  if (empty() || r.empty(n)) return false;
  const Point pt = origin();
  if (axis == X)
  {
    if (n == Y) /*xz-plane*/ return Interval{r.c, r.d}.contains(pt(Z), Z);
    if (n == Z) /*xy-plane*/ return Interval{r.c, r.d}.contains(pt(Y), Y);
  }
  else if (axis == Y)
  {
    if (n == X) /*yz-plane*/ return Interval{r.c, r.d}.contains(pt(Z), Z);
    if (n == Z) /*xy-plane*/ return Interval{r.a, r.b}.contains(pt(X), X);
  }
  else if (axis == Z)
  {
    if (n == X) /*yz-plane*/ return Interval{r.a, r.b}.contains(pt(Y), Y);
    if (n == Y) /*xz-plane*/ return Interval{r.a, r.b}.contains(pt(X), X);
  }
  return false;
}

bool Line::intersects(const Rectangle &r, Axis n, LineRestriction t) const
{
  if (!intersects(r, n)) return false;
  if (t == LINE) return true;

  double extremity[2]{0, -1};
  if (axis == X && n != X)
  {
    extremity[0] = t == RAY_NEG ? r.a : std::max(a, r.a);
    extremity[1] = t == RAY_POS ? r.b : std::min(b, r.b);
  }
  else if (axis == Y && n != Y)
  {
    const double beg = n == Z ? r.c : r.a;
    const double end = n == Z ? r.d : r.b;
    extremity[0] = t == RAY_NEG ? beg : std::max(a, beg);
    extremity[1] = t == RAY_POS ? end : std::min(b, end);
  }
  else if (axis == Z && n != Z)
  {
    extremity[0] = t == RAY_NEG ? r.c : std::max(a, r.c);
    extremity[1] = t == RAY_POS ? r.d : std::min(b, r.d);
  }
  return !Interval{extremity[0], extremity[1]}.empty(axis);
}

//============================================================================//
//:::::::::::::::::::::::::::::::  RectangleX  ::::::::::::::::::::::::::::::://
//============================================================================//
vector<Line> RectangleX::get_side_lines(Axis i) const
{
  if (empty(i)) return {};
  Line lc; *(Interval *)&lc = {a, b}; lc.axis = sAxis[i][0];
  Line ld; *(Interval *)&ld = {a, b}; ld.axis = sAxis[i][0];
  Line la; *(Interval *)&la = {c, d}; la.axis = sAxis[i][1];
  Line lb; *(Interval *)&lb = {c, d}; lb.axis = sAxis[i][1];
  lc.pos = Point2d{a, c}.promote(i).project(lc.axis);
  ld.pos = Point2d{a, d}.promote(i).project(ld.axis);
  la.pos = Point2d{a, c}.promote(i).project(la.axis);
  lb.pos = Point2d{b, c}.promote(i).project(lb.axis);
  return {lc, ld, la, lb};
}

vector<Line> RectangleX::select_intersecting(const vector<Line>& lines, Axis n) const
{
  vector<Line> result;
  for (const auto &line: lines)
    if (line.intersects(*this, n, Line::LINE_SEGMENT)) result.push_back(line);
  return result;
}

vector<RectangleX> RectangleX::partition(const vector<Line> &lines, Axis n) const
{
  static const int which[3][3]{{-1,1,0}, {1,-1,0}, {1,0,-1}};
  static const int coord[3][3]{{-1,1,1}, {1,-1,0}, {0,0,-1}};

  // Partition coordinate axes
  std::array<vector<double>,2> partition;
  partition[0].push_back(a);
  partition[1].push_back(c);
  for (const auto &line: lines)
  {
    const int w = which[n][line.axis];
    THROW_IF(w == -1, "incorrect line axis "+fd::to_string(line.axis)+
      " for plane normal to axis "+fd::to_string(n)+".");
    partition[w].push_back(line.pos(coord[n][line.axis]));
  }
  partition[0].push_back(b);
  partition[1].push_back(d);

  // Partition rectangle
  return Partition(partition, n);
}

vector<RectangleX> RectangleX::subtract(const RectangleX &r, Axis n) const
{
  Rectangle common = intersect(r, n);
  if (common.empty(n)) return vector<RectangleX>{*this};

  auto result = partition(select_intersecting(r.get_side_lines(n), n), n);

#ifdef __DEBUG__
  int removed = 0;
#endif // __DEBUG__
  auto true_end = result.end();
  for (auto it = result.begin(); it != true_end; ++it)
  {
    if (it->coincides(common, n))
    {
      std::swap(*it--, *--true_end);
#ifdef __DEBUG__
      ++removed;
#else
      break;
#endif // __DEBUG__
    }
  }
#ifdef __DEBUG__
  if (removed != 1) std::cout << "Failure: "+(removed == 0? _S"no rectangle was " :
      fd::to_string(removed)+" rectangles were ")+"removed!\n";
#endif // __DEBUG__
  ASSERT(removed == 1);
  result.erase(true_end, result.end());
  return result;
}

vector<RectangleX> RectangleX::subtract(const vector<RectangleX> &l, Axis n) const
{
  vector<RectangleX> tgt{*this};
  for (const auto &r: l)
    tgt = r.rsub(tgt, n);
  return tgt;
}

vector<RectangleX> RectangleX::rsub(const vector<RectangleX> &l, Axis n) const
{
  vector<RectangleX> result;
  for (const auto &r: l)
  {
    if (intersect(r, n).empty(n))
    {
      result.push_back(r);
    }
    else
    {
      auto ll = r.subtract(*this, n);
      result.insert(result.end(), ll.begin(), ll.end());
    }
  }
  return result;
}

bool RectangleX::merge_adjacent(const Rectangle &r, Axis n)
{
  const bool coincx = Interval{a,b}.coincides(Interval{r.a,r.b}, sAxis[n][0]);
  const bool coincy = Interval{c,d}.coincides(Interval{r.c,r.d}, sAxis[n][1]);

  if (coincx)
  {
    if      (std::fabs(c - r.d) < RESOL(sAxis[n][1])) c = r.c;
    else if (std::fabs(d - r.c) < RESOL(sAxis[n][1])) d = r.d;
    else return false;
  }
  else if (coincy)
  {
    if      (std::fabs(a - r.b) < RESOL(sAxis[n][0])) a = r.a;
    else if (std::fabs(b - r.a) < RESOL(sAxis[n][0])) b = r.b;
    else return false;
  }
  else
    return false;

  return true;
}

/*static*/
void RectangleX::merge(vector<RectangleX> &l, Axis n)
{
  std::sort(l.begin(), l.end(), [](const RectangleX &r, const RectangleX &s) {
    return std::max(r.b - r.a, r.d - r.c) > std::max(s.b - s.a, s.d - s.c);
  });
  bool changed;
  auto true_end = l.end();
  do {
    changed = false;
    for (auto i = l.begin(); i != true_end; ++i)
      for (auto j = i + 1; j != true_end; ++j)
        if (i->merge_adjacent(*j, n))
        {
          changed = true;
          std::swap(*j--, *--true_end);
        }
  } while (changed);
  l.erase(true_end, l.end());
}

/*
  vector<Rectangle> union(const RectangleX &r, Axis n) const;
vector<Rectangle> RectangleX::union(const RectangleX &r, Axis n) const
{
  if (contains(r, n)) return {*this};
  if (r.contains(*this, n)) return {r};
  RectangleX c = intersection(r, n);
  if (c.empty(n)) return {*this, r};
  return {subtract(c, n), r.subtract(c, n), c};
}*/

vector<RectangleX> RectangleX::partition(const vector<Point2d> &pts, Axis n) const
{
  if (empty(n)) return {};

  // Partition coordinate axes
  std::array<vector<double>,2> partition;
  partition[0].push_back(a);
  partition[1].push_back(c);
  for (const auto &pt: pts)
    if (contains(pt, n))
    {
      partition[0].push_back(pt.x);
      partition[1].push_back(pt.y);
    }
  partition[0].push_back(b);
  partition[1].push_back(d);

  // Partition rectangle
  return Partition(partition, n);
}

/*static*/
vector<RectangleX> Partition(std::array<vector<double>,2> axis_partition, Axis n)
{
  // Remove duplicates
  for (int i: {0, 1})
  {
    auto end_unique = axis_partition[i].end();
    for (auto iter = axis_partition[i].begin(); iter != end_unique; ++iter)
    {
      const auto r = *iter;
      const auto e = RESOL(sAxis[n][i]);
      end_unique = std::remove_if(iter+1, end_unique, [e, r](double x) {
        return std::fabs(x - r) < e;
      });
    }
    axis_partition[i].erase(end_unique, axis_partition[i].end()) ;
  }

  // Sort axis partitions
  for (int i: {0, 1})
    std::sort(axis_partition[i].begin(), axis_partition[i].end());

  // Partition rectangle
  vector<RectangleX> result;
  for (auto j = axis_partition[1].begin(); j != axis_partition[1].end()-1; ++j)
    for (auto i = axis_partition[0].begin(); i != axis_partition[0].end()-1; ++i)
    {
      RectangleX r; r.a = *i; r.b = i[1]; r.c = *j; r.d = j[1];
      result.push_back(r);
    }

  return result;
}

} // namespace fd

std::ostream& operator<<(std::ostream &os, const fd::Axis &i)
{ std::underlying_type<fd::Axis>::type t = i; return os << t; }

std::ostream& operator<<(std::ostream &os, const fd::Line &l)
{ return os << *(fd::Interval *)&l << ' ' << l.axis << ' ' << l.pos; }

std::istream& operator>>(std::istream &is, fd::Axis &i)
{ std::underlying_type<fd::Axis>::type t; is >> t; i = (fd::Axis)t; return is; }

std::istream& operator>>(std::istream &is, fd::Line &l)
{ return ((is >> *(fd::Interval *)&l) >> l.axis) >> l.pos; }
