#include <cmath>
#include "mesh.hpp"
#include "pde.hpp"
#include "expressions.hpp"
#include "discretizer.hpp"

using std::string;
using std::array;

#if 0
1d:
       2 +-------------+ 0
      [0]               [1]

2d:
                1
         +-------------+
         |             |
       2 |             | 0
         |             |
         +-------------+
                3
3d:
        y^  7    z [6]
         |  +---/---------+ 6
         | /|  /          |
         |/ | /           |[5]
       3 +--|/   [4]   +2 |
         |  +-------------+ 5
      [2]| /4       [0]| /
         |/            |/
         +-------------+---------> x
       0       [3]       1

  1562 {0} --> R
  2673 {1} --> T
  0473 {2} --> L
  0154 {3} --> B
  4567 {4} --> U
  0123 {5} --> D

Fig. The indicated numbers are the sid of the boundaries.
#endif

namespace fd {

// Global instances
DiscretizerDx  __attribute__((init_priority(200))) discretizer_x;
DiscretizerDy  __attribute__((init_priority(201))) discretizer_y;
DiscretizerDz  __attribute__((init_priority(202))) discretizer_z;
DiscretizerDxx __attribute__((init_priority(203))) discretizer_xx;
DiscretizerDyy __attribute__((init_priority(204))) discretizer_yy;
DiscretizerDzz __attribute__((init_priority(205))) discretizer_zz;
DiscretizerDxy __attribute__((init_priority(206))) discretizer_xy;
DiscretizerDxz __attribute__((init_priority(207))) discretizer_xz;
DiscretizerDyz __attribute__((init_priority(208))) discretizer_yz;
DiscretizerId  __attribute__((init_priority(209))) discretizer_id;

//============================================================================//
//::::::::::::::::::::::::::::::  Discretizer  ::::::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Boundaries from which to obtain reuse code
 *
 *  This array contains the boundaries from which code for boundary
 *  extremities will be obtained (reused).
 * 
 *  The first index is the axis of partial differentiation, and the second
 *  is a running index.
 *  This array is used by \sa Discretizer::discretize_on_boundary_extremities()
 *  and \sa Discretizer::discretize_on_boundary_vertices().
 */
static const int boundaries_reuse_code[3][2]{
    {MeshCommon::R, MeshCommon::L},
    {MeshCommon::T, MeshCommon::B},
    {MeshCommon::U, MeshCommon::D}
};

static constexpr CodeFragment::Format NB   = CodeFragment::NO_BRACKETS;
static constexpr CodeFragment::Format BINL = CodeFragment::BRACKETS_IF_NOT_LAST;

Discretizer *Discretizer::duplicate() const
{
  throw _S"Discretizer::duplicate() was used on a Discretizer that "
          "does not support it.";
  return const_cast<Discretizer *>(this);
}

IMPLEMENT_DISCRETIZER_DUP(DiscretizerDx);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDy);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDz);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDxx);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDyy);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDzz);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDxy);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDxz);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerDyz);
IMPLEMENT_DISCRETIZER_DUP(DiscretizerId);
IMPLEMENT_DISCRETIZER_DUP(FunctionDiscretizer);
IMPLEMENT_DISCRETIZER_DUP(ConstantDiscretizer);
IMPLEMENT_DISCRETIZER_DUP(FieldFunctionDiscretizer);
IMPLEMENT_DISCRETIZER_DUP(OperatorDiscretizer);
IMPLEMENT_DISCRETIZER_DUP(CutoffDiscretizer);

/*static*/
IntTriple Discretizer::order_from_pdo_type(int pdotype) noexcept
{
  IntTriple order;
  auto d = div(pdotype, 100);
  order[0] = d.rem;
  d = div(d.quot, 100);
  order[1] = d.rem;
  order[2] = d.quot;
  return order;
}

/*int Discretizer::get_diff_type() const noexcept
{
  static const int DT[5]{-1,Boundary::X,Boundary::Y,-1,Boundary::Z};
  if (!check_order()) return {-1,-1};
  const int to = total_order();
  if (to == order_[0]) return {Boundary::X,to};
  if (to == order_[1]) return {Boundary::Y,to};
  if (to == order_[2]) return {Boundary::Z,to};
  return {-1,to};
}*/

CodeFragment Discretizer::core_discretize(const DiscretizationData& d) const
{
  throw _S"Operator " + name() + " cannot be discretized.";
  return CodeFragment();
}

CodeFragment Discretizer::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  throw _S"Operator " + name() + " cannot be discretized on a boundary.";
  return CodeFragment();
}

CodeFragment Discretizer::discretize(const DiscretizationData& d) const
{ return core_discretize(d); }

CodeFragment Discretizer::discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{ return core_discretize_on_boundary(bdd); }

/******************************************************************************
 * bdm[X] : [b]oundary [d]iscretization [m]ethod [X]
 ******************************************************************************/

CodeFragment Discretizer::discretize_on_boundary(int pd_type,
    const BoundaryDiscretizationData &bdd) const
{
  if (bdd.dm == PDECommon::SYM_DIFF2) return discretize_on_boundary_bdm2(bdd);

  if (apply_std_discretization(bdd, pd_type))
  {
    CodeFragment cf = discretize_on_boundary_bdm2(bdd);
    discretize_on_boundary_extremities(pd_type, bdd, &cf);
    return cf;
  }

  if (bdd.sid != boundaries_reuse_code[pd_type][0] &&
      bdd.sid != boundaries_reuse_code[pd_type][1])
    THROW("incorrect side identifier: " + fd::to_string(bdd.sid) + ".");

  constexpr int ID3 = PDECommon::INWARD_DIFF3, ID4 = PDECommon::INWARD_DIFF4,
                                               ID5 = PDECommon::INWARD_DIFF5;
  switch (bdd.dm)
  {
    case ID3: return discretize_on_boundary_bdm3(pd_type, bdd);
    case ID4: return discretize_on_boundary_bdm4(pd_type, bdd);
    case ID5: return discretize_on_boundary_bdm5(pd_type, bdd);
  }
  THROW("unknown boundary discretization method: "+fd::to_string(bdd.dm)+".");
  return CodeFragment();
}

CodeFragment Discretizer::discretize_scalar_field(
    const DiscretizationData&) const
{
  throw _S"Operator " + name() + " does not support numerical calculation of "
          "functions.";
  return CodeFragment();
}

CodeFragment Discretizer::discretize_on_boundary_scalar_field(
    const BoundaryDiscretizationData& bdd) const
{ return discretize_scalar_field({bdd.pde, bdd.i, bdd.j, bdd.k}); }

bool Discretizer::apply_std_discretization(
    const BoundaryDiscretizationData &bdd, int pd_type) const
{
  static const int O[3][4]{
    {MeshCommon::B, MeshCommon::T, MeshCommon::D, MeshCommon::U},
    {MeshCommon::L, MeshCommon::R, MeshCommon::D, MeshCommon::U},
    {MeshCommon::L, MeshCommon::R, MeshCommon::B, MeshCommon::T}};
  const int DIM = bdd.pde->dimension();
  if (DIM == 1 || (DIM == 2 && pd_type == Boundary::Z)) return false;
  const bool bret = (bdd.sid == O[pd_type][0] || bdd.sid == O[pd_type][1]);
  return DIM == 2 ? bret :
      (bret || bdd.sid == O[pd_type][2] || bdd.sid == O[pd_type][3]);
}

void Discretizer::discretize_on_boundary_extremities(int pd_type,
    const BoundaryDiscretizationData &bdd, CodeFragment *cf) const
{
  const int DIM = bdd.pde->dimension();

  ASSERT(pd_type == Boundary::Z ? DIM == 3 : DIM > 1);
  ASSERT(!(pd_type == Boundary::X) || (DIM == 2 ?
    fd::find({MeshCommon::T,MeshCommon::B},bdd.sid) :  
    fd::find({MeshCommon::T,MeshCommon::B,MeshCommon::U,MeshCommon::D},bdd.sid)));
  ASSERT(!(pd_type == Boundary::Y) || (DIM == 2 ?
    fd::find({MeshCommon::L,MeshCommon::R},bdd.sid) :  
    fd::find({MeshCommon::L,MeshCommon::R,MeshCommon::U,MeshCommon::D},bdd.sid)));
  ASSERT(!(pd_type == Boundary::Z) || (fd::find({
    MeshCommon::L,MeshCommon::R,MeshCommon::T,MeshCommon::B},bdd.sid)));

  BoundaryDiscretizationData dd = bdd;
  for (const int sid_reuse : boundaries_reuse_code[pd_type])
  {
    dd.sid = sid_reuse;
    CodeFragment reuse = core_discretize_on_boundary(dd);
    // The primary face (edge in 2d) is bdd.sid and the secondary sid_reuse.
    const int w = DIM == 2 ? Boundary::vertex_extremity(sid_reuse, -1)
                           : Boundary::edge_extremity(bdd.sid, sid_reuse);
    cf->text_bx_y[w] = reuse.text_y;
    cf->text_bx_j[w] = reuse.text_j;
    cf->hint_bx_y[w] = reuse.hint_y;
    cf->hint_bx_j[w] = reuse.hint_j;
  }

  if (DIM == 3) discretize_on_boundary_vertices(pd_type, bdd, cf);
}

void Discretizer::discretize_on_boundary_vertices(int pd_type,
    const BoundaryDiscretizationData &bdd, CodeFragment *cf) const
{
  constexpr int _L = MeshCommon::L, _R = MeshCommon::R, _T = MeshCommon::T;
  constexpr int _B = MeshCommon::B, _D = MeshCommon::D, _U = MeshCommon::U;
  /**
   *  This array is used to determine the faces of each vertex.
   * 
   *  The primary boundary of the vertex is bdd.sid,
   *  the secondary is one of boundaries_reuse_code[], and
   *  the tertiary is one of boundaries_vertex[bdd.sid]
   *  The first index is the differentiation axis, the second the sid of
   *  the current face, and the last is an index running over possible
   *  tertiary face ids.
   */ 
  static const int boundaries_vertex[3][6][2]{
    {{-1, -1}, {_U, _D}, {-1, -1}, {_U, _D}, {_T, _B}, {_T, _B}},
    {{_U, _D}, {-1, -1}, {_U, _D}, {-1, -1}, {_R, _L}, {_R, _L}},
    {{_T, _B}, {_R, _L}, {_T, _B}, {_R, _L}, {-1, -1}, {-1, -1}}
  };
  BoundaryDiscretizationData dd = bdd;
  for (const int tertiary : boundaries_vertex[pd_type][bdd.sid])
    for (const int sid_reuse : boundaries_reuse_code[pd_type])
    {
      dd.sid = sid_reuse;
      CodeFragment reuse = core_discretize_on_boundary(dd);
      // The primary boundary of the vertex is bdd.sid.
      const int w = Boundary::vertex_extremity(sid_reuse, tertiary);
      cf->text_vx_y[w] = reuse.text_y;
      cf->text_vx_j[w] = reuse.text_j;
      cf->hint_vx_y[w] = reuse.hint_y;
      cf->hint_vx_j[w] = reuse.hint_j;
    }
}

CodeFragment Discretizer::discretize_on_boundary_bdm2(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = core_discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

static const string MESH_SPACING[3]{"hx","hy","hz"};
static const string SIGNUM[6]{"-","-","","","-",""};

CodeFragment Discretizer::discretize_on_boundary_bdm3(int pd_type,
    const BoundaryDiscretizationData &bdd) const
{
  const string s = SIGNUM[bdd.sid];
  const string h = MESH_SPACING[pd_type];
  const auto e = get_boundary_discretization_nodes(pd_type, 3, bdd);
  const string
  code_y = "(4.*u[" +e[1]+"]-u[" +e[2]+"]-3.*u[" +e[0]+"])/("+s+"2.*"+h+")",
  code_j = "(4.*du["+e[1]+"]-du["+e[2]+"]-3.*du["+e[0]+"])/("+s+"2.*"+h+")";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment Discretizer::discretize_on_boundary_bdm4(int pd_type,
    const BoundaryDiscretizationData &bdd) const
{
  const string s = SIGNUM[bdd.sid];
  const string h = MESH_SPACING[pd_type];
  const auto e = get_boundary_discretization_nodes(pd_type, 4, bdd);
  const string
  code_y = "(2.*u[" +e[3]+"]-9.*u[" +e[2]+"]+18.*u[" +e[1]+"]-11.*u["
                    +e[0]+"])/("+s+"6.*"+h+")",
  code_j = "(2.*du["+e[3]+"]-9.*du["+e[2]+"]+18.*du["+e[1]+"]-11.*du["
                    +e[0]+"])/("+s+"6.*"+h+")";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment Discretizer::discretize_on_boundary_bdm5(int pd_type,
    const BoundaryDiscretizationData &bdd) const
{
  const string s = SIGNUM[bdd.sid];
  const string h = MESH_SPACING[pd_type];
  const auto e = get_boundary_discretization_nodes(pd_type, 5, bdd);
  const string
  code_y = "(-3.*u[" +e[4]+"]+16.*u[" +e[3]+"]-36.*u[" +e[2]+"]+48.*u["
                     +e[1]+"]-25.*u[" +e[0]+"])/("+s+"12.*"+h+")",
  code_j = "(-3.*du["+e[4]+"]+16.*du["+e[3]+"]-36.*du["+e[2]+"]+48.*du["
                     +e[1]+"]-25.*du["+e[0]+"])/("+s+"12.*"+h+")";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

array<string,7> Discretizer::get_boundary_discretization_nodes(
    int pd_type, int dm, const BoundaryDiscretizationData &bdd) const
{
  static const string node[6][5]{
    {"nx-1", "nx-2", "nx-3", "nx-4", "nx-5"},
    {"ny-1", "ny-2", "ny-3", "ny-4", "ny-5"},
    {"0"   , "1"   , "2"   , "3"   , "4"   },
    {"0"   , "1"   , "2"   , "3"   , "4"   },
    {"nz-1", "nz-2", "nz-3", "nz-4", "nz-5"},
    {"0"   , "1"   , "2"   , "3"   , "4"   }
  };
  const string &i = bdd.i, &j = bdd.j, &k = bdd.k;
  const string E("e"), C("$c$");
  const auto &pde = *bdd.pde;

  array<string,7> e;

  switch (pd_type)
  {
    case Boundary::X:
      for (int w = 0; w < dm; ++w)
        e[w] = pde.code_elem_func(E, node[bdd.sid][w], j, k, C);
      break;
    case Boundary::Y:
      for (int w = 0; w < dm; ++w)
        e[w] = pde.code_elem_func(E, i, node[bdd.sid][w], k, C);
      break;
    case Boundary::Z:
      for (int w = 0; w < dm; ++w)
        e[w] = pde.code_elem_func(E, i, j, node[bdd.sid][w], C);
      break;
  }

  return e;
}

//============================================================================//
//::::::::::::::::::::::::::  Various Discretizers  :::::::::::::::::::::::::://
//============================================================================//
CodeFragment FunctionDiscretizer::discretize(
    const DiscretizationData& dd) const
{
  if (_basic_operator == nullptr)
  {
    const string code_y = dd.pde->code_func_call(fname, dd.i, dd.j, dd.k);
    const string code_j = "0";
    return CodeFragment(code_y, code_j);
  }

  Discretizer *d = _basic_operator->discretizer();
  THROW_IF(d == nullptr, "unexpected null discretizer.");
  CodeFragment cf = d->discretize_scalar_field(dd);

  // Replace placeholder $f$ with function name:
  fd::replace_all(cf.text_y, "$f$", fname);

  return cf;
}

static constexpr int NBS = sizeof(CodeFragment::text_bx_y)/
                           sizeof(CodeFragment::text_bx_y[0]);
static constexpr int NVS = sizeof(CodeFragment::text_vx_y)/
                           sizeof(CodeFragment::text_vx_y[0]);

CodeFragment FunctionDiscretizer::discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  if (_basic_operator == nullptr)
  {
    CodeFragment cf = discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
    cf.refers_to_boundary(bdd.bid);
    return cf;
  }

  Discretizer *d = _basic_operator->discretizer();
  THROW_IF(d == nullptr, "unexpected null discretizer.");
  CodeFragment cf = d->discretize_on_boundary_scalar_field(bdd);

  // Replace placeholder $f$ with function name:
  fd::replace_all(cf.text_y, "$f$", fname);
  for (int i = 0; i < NBS; ++i) fd::replace_all(cf.text_bx_y[i], "$f$", fname);
  for (int i = 0; i < NVS; ++i) fd::replace_all(cf.text_vx_y[i], "$f$", fname);

  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment ConstantDiscretizer::discretize(
    const DiscretizationData& d) const
{
  const string code_y = fd::to_string(_c);
  const string code_j = "0";
  return CodeFragment(code_y, code_j);
}

CodeFragment ConstantDiscretizer::discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerId::core_discretize(const DiscretizationData& d) const
{
  const string elem = d.pde->code_elem_func("e", d.i, d.j, d.k, "$c$");
  const string code_y = "u["  + elem + "]";
  const string code_j = "du[" + elem + "]";
  return CodeFragment(code_y, code_j);
}

CodeFragment DiscretizerId::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = core_discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerId::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string code_y = d.pde->code_func_call("$f$", d.i, d.j, d.k);
  const string code_j = "0";
  return CodeFragment(code_y, code_j);
}

//============================================================================//
//::::::::::::::::::::::::  FieldFunctionDiscretizer  :::::::::::::::::::::::://
//============================================================================//
string FieldFunctionDiscretizer::code_field_function_call(
    const string& func_name, const DiscretizationData& d) const
{
  string code = func_name + "(";
  if (space_args_) code += d.pde->format_space_args(d.i, d.j, d.k) + ",";
  for (int n = 0; n < argc_; ++n)
  {
//    const string c = "$c_" + fd::to_string(n) + "$";
    const string c = "$C_" + fd::to_string(n) + "$";
    code += "u[" + d.pde->code_elem_func("e", d.i, d.j, d.k, c) + "]";
    if (n != argc_-1) code += ",";
  }
  return code + ")";
}

CodeFragment
FieldFunctionDiscretizer::core_discretize(const DiscretizationData& d) const
{
  const string code_y = code_field_function_call(fname, d);
  string code_j;
  for (int n = 0; n < argc_; ++n)
  {
//    const string c = "$c_" + fd::to_string(n) + "$";
    const string c = "$C_" + fd::to_string(n) + "$";
    const string part_deriv = "D" + fd::to_string(n) + "_" + fname;
    code_j += code_field_function_call(part_deriv, d);
    code_j += "*du[" + d.pde->code_elem_func("e", d.i, d.j, d.k, c) + "]";
    if (n == argc_-1) continue;
    code_j += "+";
  }
  return CodeFragment(code_y, code_j, NB, CodeFragment::USE_BRACKETS);
}

CodeFragment FieldFunctionDiscretizer::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = core_discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  // TODO: brackets!
  return cf;
}

string FieldFunctionDiscretizer::code_call_with_expressions(
    const string& func_name, const DiscretizationData& d) const
{
  string code = func_name + "(";
  if (space_args_) code += d.pde->format_space_args(d.i, d.j, d.k) + ",";
  for (int n = 0; n < argc_; ++n)
  {
    const string u = "@x_" + fd::to_string(n) + "@";
    code += u;
    if (n != argc_-1) code += ",";
  }
  return code + ")";
}

CodeFragment FieldFunctionDiscretizer::discretize_with_expressions(
    const DiscretizationData& d) const
{
  const string code_y = code_call_with_expressions(fname, d);
  string code_j;
  for (int n = 0; n < argc_; ++n)
  {
    const string du = "@d_" + fd::to_string(n) + "@";
    const string part_deriv = "D" + fd::to_string(n) + "_" + fname;
    code_j += code_call_with_expressions(part_deriv, d);
    code_j += "*(" + du + ")";
    if (n != argc_-1) code_j += "+";
  }
  return CodeFragment(code_y, code_j,
      CodeFragment::NO_BRACKETS, CodeFragment::USE_BRACKETS);
}

CodeFragment FieldFunctionDiscretizer::discretize_on_boundary_with_expressions(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = discretize_with_expressions({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  // TODO: brackets!
  return cf;
}

//============================================================================//
//:::::::::::::::::::::::::::  CutoffDiscretizer  :::::::::::::::::::::::::::://
//============================================================================//
string CutoffDiscretizer::code_cutoff_call(const string &var,
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string code = var+"["+d.pde->code_elem_func("e", i, j, k, "$C_0$")+"]";
  return "(" + d.pde->code_func_call(fname, i, j, k) + " ? " + code + " : 0)";
}

CodeFragment CutoffDiscretizer::core_discretize(
    const DiscretizationData& d) const
{
  const string code_y = code_cutoff_call("u", d);
  const string code_j = code_cutoff_call("du", d);
  return CodeFragment(code_y, code_j, NB, NB);
}

CodeFragment CutoffDiscretizer::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = core_discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  // TODO: brackets!
  return cf;
}

string CutoffDiscretizer::code_call_with_expressions(const string &arg, 
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  return "(" + d.pde->code_func_call(fname, i, j, k) + " ? " + arg + " : 0)";
}

CodeFragment CutoffDiscretizer::discretize_with_expressions(
    const DiscretizationData& d) const
{
  const string code_y = code_call_with_expressions("@x_0@", d);
  const string code_j = code_call_with_expressions("@d_0@", d);
  return CodeFragment(code_y, code_j, NB, NB);
}

CodeFragment CutoffDiscretizer::discretize_on_boundary_with_expressions(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = discretize_with_expressions({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  // TODO: brackets!
  return cf;
}

//============================================================================//
//:::::::::::::::::::::::::::::::::::  Dx  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDx::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string ip(i+"+1"), im(i+"-1");
  const string code_y = "(u[" +d.pde->code_elem_func(e, ip, j, k, c)+"]-u["
                              +d.pde->code_elem_func(e, im, j, k, c)+"])/(2.*hx)";
  const string code_j = "(du["+d.pde->code_elem_func(e, ip, j, k, c)+"]-du["
                              +d.pde->code_elem_func(e, im, j, k, c)+"])/(2.*hx)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

// bdm[n] : [b]oundary [d]iscretization [m]ethod [n]
CodeFragment DiscretizerDx::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
#ifndef USE_UNIFIED_DISCRETIZER
  if (bdd.dm == PDECommon::SYM_DIFF2) return discretize_on_boundary_bdm2(bdd);

  if (apply_std_discretization(bdd, Boundary::X))
  {
    CodeFragment cf = discretize_on_boundary_bdm2(bdd);
    discretize_on_boundary_extremities(bdd, &cf);
    return cf;
  }

  if (bdd.sid != MeshCommon::L && bdd.sid != MeshCommon::R)
    THROW("incorrect side identifier: " + fd::to_string(bdd.sid) + ".");

  switch (bdd.dm)
  {
    case PDECommon::INWARD_DIFF3: return discretize_on_boundary_bdm3(bdd);
    case PDECommon::INWARD_DIFF4: return discretize_on_boundary_bdm4(bdd);
    case PDECommon::INWARD_DIFF5: return discretize_on_boundary_bdm5(bdd);
  }
  THROW("unknown boundary discretization method: "+fd::to_string(bdd.dm)+".");
  return CodeFragment();
#else
  return Discretizer::discretize_on_boundary(Boundary::X, bdd);
#endif // !USE_UNIFIED_DISCRETIZER
}

#ifndef USE_UNIFIED_DISCRETIZER
CodeFragment DiscretizerDx::discretize_on_boundary_bdm3(
    const BoundaryDiscretizationData &bdd) const
{
  const string /*&i = bdd.i, */&j = bdd.j, &k = bdd.k;
  const string e("e"), c("$c$");
  string i0, i1, i2, s;
  if (bdd.sid == MeshCommon::L) {i0="0"; i1="1"; i2="2";}
  else {i0="nx-1"; i1="nx-2"; i2="nx-3"; s="-";}

  const auto &pde = *bdd.pde; 
  string code_y, code_j;
  code_y = "(4.*u[" +pde.code_elem_func(e, i1, j, k, c)+"]-u["
                    +pde.code_elem_func(e, i2, j, k, c)+"]-3.*u["
                    +pde.code_elem_func(e, i0, j, k, c)+"])/("+s+"2.*hx)";
  code_j = "(4.*du["+pde.code_elem_func(e, i1, j, k, c)+"]-du["
                    +pde.code_elem_func(e, i2, j, k, c)+"]-3.*du["
                    +pde.code_elem_func(e, i0, j, k, c)+"])/("+s+"2.*hx)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerDx::discretize_on_boundary_bdm4(
    const BoundaryDiscretizationData &bdd) const
{
  const string /*&i = bdd.i, */&j = bdd.j, &k = bdd.k;
  const string e("e"), c("$c$");
  string i0, i1, i2, i3, s;
  if (bdd.sid == MeshCommon::L) {i0="0"; i1="1"; i2="2"; i3="3";}
  else {i0="nx-1"; i1="nx-2"; i2="nx-3"; i3="nx-4"; s="-";}

  const auto &pde = *bdd.pde; 
  string code_y, code_j;
  code_y = "(2.*u[" +pde.code_elem_func(e, i3, j, k, c)+"]-9.*u["
                    +pde.code_elem_func(e, i2, j, k, c)+"]+18.*u["
                    +pde.code_elem_func(e, i1, j, k, c)+"]-11.*u["
                    +pde.code_elem_func(e, i0, j, k, c)+"])/("+s+"6.*hx)";
  code_j = "(2.*du["+pde.code_elem_func(e, i3, j, k, c)+"]-9.*du["
                    +pde.code_elem_func(e, i2, j, k, c)+"]+18.*du["
                    +pde.code_elem_func(e, i1, j, k, c)+"]-11.*du["
                    +pde.code_elem_func(e, i0, j, k, c)+"])/("+s+"6.*hx)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerDx::discretize_on_boundary_bdm5(
    const BoundaryDiscretizationData &bdd) const
{
  const string /*&i = bdd.i, */&j = bdd.j, &k = bdd.k;
  const string e("e"), c("$c$");
  string i0, i1, i2, i3, i4, s;
  if (bdd.sid == MeshCommon::L) {i0="0"; i1="1"; i2="2"; i3="3"; i4="4";}
  else {i0="nx-1"; i1="nx-2"; i2="nx-3"; i3="nx-4"; i4="nx-5"; s="-";}

  const auto &pde = *bdd.pde; 
  string code_y, code_j;
  code_y = "(-3.*u[" +pde.code_elem_func(e, i4, j, k, c)+"]+16.*u["
                     +pde.code_elem_func(e, i3, j, k, c)+"]-36.*u["
                     +pde.code_elem_func(e, i2, j, k, c)+"]+48.*u["
                     +pde.code_elem_func(e, i1, j, k, c)+"]-25.*u["
                     +pde.code_elem_func(e, i0, j, k, c)+"])/("+s+"12.*hx)";
  code_j = "(-3.*du["+pde.code_elem_func(e, i4, j, k, c)+"]+16.*du["
                     +pde.code_elem_func(e, i3, j, k, c)+"]-36.*du["
                     +pde.code_elem_func(e, i2, j, k, c)+"]+48.*du["
                     +pde.code_elem_func(e, i1, j, k, c)+"]-25.*du["
                     +pde.code_elem_func(e, i0, j, k, c)+"])/("+s+"12.*hx)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}
#endif // !USE_UNIFIED_DISCRETIZER

void DiscretizerDx::discretize_at_boundary_extremities_OLD(
    const BoundaryDiscretizationData &bdd, CodeFragment *cf) const
{
  using fd::to_string;

  ASSERT(bdd.sid == MeshCommon::B || bdd.sid == MeshCommon::T);

  const string &j = bdd.j, &k = bdd.k;
  const int dm = bdd.dm;
  const string e("e"), c("$c$");
  string code_y, code_j;

  // "Lower" boundary extremity (L, B)
  string i0("0"), i1("1"), i2("2");
  if (dm == PDECommon::INWARD_DIFF3)
  {
    code_y  = "(4.*u["  +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-u["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]-3.*u["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(2.*hx)";
    code_j  = "(4.*du["  +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-du["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]-3.*du["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(2.*hx)";
  }
  else if (dm == PDECommon::INWARD_DIFF4)
  {
    const string i3("3");
    code_y  = "(2.*u["  +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-9.*u["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+18.*u["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-11.*u["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(6.*hx)";
    code_j  = "(2.*du["  +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-9.*du["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+18.*du["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-11.*du["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(6.*hx)";
  }
  else if (dm == PDECommon::INWARD_DIFF5)
  {
    const string i3("3"), i4("4");
    code_y  = "(-3.*u[" +bdd.pde->code_elem_func(e, i4, j, k, c)+"]+16.*u["
                        +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-36.*u["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+48.*u["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-25.*u["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(12.*hx)";
    code_j  = "(-3.*du["+bdd.pde->code_elem_func(e, i4, j, k, c)+"]+16.*du["
                        +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-36.*du["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+48.*du["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-25.*du["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(12.*hx)";
  }
  else
    THROW(_S"invalid boundary discretization method: " + to_string(dm)+".");
  cf->text_bx_y[0] = code_y;
  cf->text_bx_j[0] = code_j;
  cf->hint_bx_y[0] = cf->hint_bx_j[0] = BINL;

  // "Higher" boundary extremity (R, T)
  i0 = "nx-1"; i1 = "nx-2"; i2 = "nx-3";
  if (dm == PDECommon::INWARD_DIFF3)
  {
    code_y  = "(-4.*u["  +bdd.pde->code_elem_func(e, i1, j, k, c)+"]+u["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+3.*u["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(2.*hx)";
    code_j  = "(-4.*du["+bdd.pde->code_elem_func(e, i1, j, k, c)+"]+du["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+3.*du["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(2.*hx)";
  }
  else if (dm == PDECommon::INWARD_DIFF4)
  {
    const string i3("nx-4");
    code_y  = "(2.*u["  +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-9.*u["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+18.*u["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-11.*u["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(-6.*hx)";
    code_j  = "(2.*du["  +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-9.*du["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+18.*du["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-11.*du["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(-6.*hx)";
  }
  else if (dm == PDECommon::INWARD_DIFF5)
  {
    const string i3("nx-4"), i4("nx-5");
    code_y  = "(-3.*u["  +bdd.pde->code_elem_func(e, i4, j, k, c)+"]+16.*u["
                        +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-36.*u["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+48.*u["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-25.*u["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(-12.*hx)";
    code_j  = "(-3.*du["+bdd.pde->code_elem_func(e, i4, j, k, c)+"]+16.*du["
                        +bdd.pde->code_elem_func(e, i3, j, k, c)+"]-36.*du["
                        +bdd.pde->code_elem_func(e, i2, j, k, c)+"]+48.*du["
                        +bdd.pde->code_elem_func(e, i1, j, k, c)+"]-25.*du["
                        +bdd.pde->code_elem_func(e, i0, j, k, c)+"])/(-12.*hx)";
  }
  cf->text_bx_y[1] = code_y;
  cf->text_bx_j[1] = code_j;
  cf->hint_bx_y[1] = cf->hint_bx_j[1] = BINL;
}

CodeFragment DiscretizerDx::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string ip(i+"+1"), im(i+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", ip, j, k)+"-"
                           +d.pde->code_func_call("$f$", im, j, k)+")/(2.*hx)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//:::::::::::::::::::::::::::::::::::  Dy  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDy::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string jp = j + "+1";
  const string jm = j + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, i, jp, k, c)+"]-u["
                        +d.pde->code_elem_func(e, i, jm, k, c)+"])/(2.*hy)";
  string code_j = "(du["+d.pde->code_elem_func(e, i, jp, k, c)+"]-du["
                        +d.pde->code_elem_func(e, i, jm, k, c)+"])/(2.*hy)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment DiscretizerDy::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
#ifndef USE_UNIFIED_DISCRETIZER
  if (bdd.dm == PDECommon::SYM_DIFF2) return discretize_on_boundary_bdm2(bdd);

  if (apply_std_discretization(bdd, Boundary::Y))
  {
    CodeFragment cf = discretize_on_boundary_bdm2(bdd);
    discretize_on_boundary_extremities(bdd, &cf);
    return cf;
  }

  if (bdd.sid != MeshCommon::B && bdd.sid != MeshCommon::T)
    THROW("incorrect side identifier: " + fd::to_string(bdd.sid) + ".");

  switch (bdd.dm)
  {
    case PDECommon::INWARD_DIFF3: return discretize_on_boundary_bdm3(bdd);
    case PDECommon::INWARD_DIFF4: return discretize_on_boundary_bdm4(bdd);
    case PDECommon::INWARD_DIFF5: return discretize_on_boundary_bdm5(bdd);
  }
  THROW("unknown boundary discretization method: " + fd::to_string(bdd.dm)+".");
  return CodeFragment();
#else
  return Discretizer::discretize_on_boundary(Boundary::Y, bdd);
#endif // !USE_UNIFIED_DISCRETIZER
}

#ifndef USE_UNIFIED_DISCRETIZER
CodeFragment DiscretizerDy::discretize_on_boundary_bdm3(
    const BoundaryDiscretizationData &bdd) const
{
  const string &i = bdd.i, /*&j = bdd.j, */&k = bdd.k;
  const string e("e"), c("$c$");
  const auto &pde = *bdd.pde; 
  string j0, j1, j2, s;
  if (bdd.sid == MeshCommon::B) {j0="0"; j1="1"; j2="2";}
  else {j0="ny-1"; j1="ny-2"; j2="ny-3"; s="-";}

  string code_y, code_j;
  code_y = "(4.*u[" +pde.code_elem_func(e, i, j1, k, c)+"]-u["
                    +pde.code_elem_func(e, i, j2, k, c)+"]-3.*u["
                    +pde.code_elem_func(e, i, j0, k, c)+"])/("+s+"2.*hy)";
  code_j = "(4.*du["+pde.code_elem_func(e, i, j1, k, c)+"]-du["
                    +pde.code_elem_func(e, i, j2, k, c)+"]-3.*du["
                    +pde.code_elem_func(e, i, j0, k, c)+"])/("+s+"2.*hy)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerDy::discretize_on_boundary_bdm4(
    const BoundaryDiscretizationData &bdd) const
{
  const string &i = bdd.i, /*&j = bdd.j, */&k = bdd.k;
  const string e("e"), c("$c$");
  const auto &pde = *bdd.pde; 
  string j0, j1, j2, j3, s;
  if (bdd.sid == MeshCommon::B) {j0="0"; j1="1"; j2="2"; j3="3";}
  else {j0="ny-1"; j1="ny-2"; j2="ny-3"; j3="ny-4"; s="-";}

  string code_y, code_j;
  code_y = "(2.*u[" +pde.code_elem_func(e, i, j3, k, c)+"]-9.*u["
                    +pde.code_elem_func(e, i, j2, k, c)+"]+18.*u["
                    +pde.code_elem_func(e, i, j1, k, c)+"]-11.*u["
                    +pde.code_elem_func(e, i, j0, k, c)+"])/("+s+"6.*hy)";
  code_j = "(2.*du["+pde.code_elem_func(e, i, j3, k, c)+"]-9.*du["
                    +pde.code_elem_func(e, i, j2, k, c)+"]+18.*du["
                    +pde.code_elem_func(e, i, j1, k, c)+"]-11.*du["
                    +pde.code_elem_func(e, i, j0, k, c)+"])/("+s+"6.*hy)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerDy::discretize_on_boundary_bdm5(
    const BoundaryDiscretizationData &bdd) const
{
  const string& i = bdd.i, /*&j = bdd.j, */&k = bdd.k;
  const string e("e"), c("$c$");
  const auto &pde = *bdd.pde; 
  string j0, j1, j2, j3, j4, s;
  if (bdd.sid == MeshCommon::B) {j0="0"; j1="1"; j2="2"; j3="3"; j4="4";}
  else {j0="ny-1"; j1="ny-2"; j2="ny-3"; j3="ny-4"; j4="ny-5"; s="-";}

  string code_y, code_j;
  code_y = "(-3.*u[" +pde.code_elem_func(e, i, j4, k, c)+"]+16.*u["
                     +pde.code_elem_func(e, i, j3, k, c)+"]-36.*u["
                     +pde.code_elem_func(e, i, j2, k, c)+"]+48.*u["
                     +pde.code_elem_func(e, i, j1, k, c)+"]-25.*u["
                     +pde.code_elem_func(e, i, j0, k, c)+"])/("+s+"12.*hy)";
  code_j = "(-3.*du["+pde.code_elem_func(e, i, j4, k, c)+"]+16.*du["
                     +pde.code_elem_func(e, i, j3, k, c)+"]-36.*du["
                     +pde.code_elem_func(e, i, j2, k, c)+"]+48.*du["
                     +pde.code_elem_func(e, i, j1, k, c)+"]-25.*du["
                     +pde.code_elem_func(e, i, j0, k, c)+"])/("+s+"12.*hy)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}
#endif // !USE_UNIFIED_DISCRETIZER

void DiscretizerDy::discretize_at_boundary_extremities_OLD(
    const BoundaryDiscretizationData &bdd, CodeFragment *cf) const
{
  using fd::to_string;

  ASSERT(bdd.sid == MeshCommon::L || bdd.sid == MeshCommon::R);

  const string& i = bdd.i, &k = bdd.k;
  const int dm = bdd.dm;
  const string e("e"), c("$c$");
  string code_y, code_j;

  // "Lower" boundary extremity (L, B)
  string j0("0"), j1("1"), j2("2");
  if (dm == PDECommon::INWARD_DIFF3)
  {
    code_y  = "(4.*u["  +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-u["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]-3.*u["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(2.*hy)";
    code_j  = "(4.*du["  +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-du["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]-3.*du["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(2.*hy)";
  }
  else if (dm == PDECommon::INWARD_DIFF4)
  {
    const string j3("3");
    code_y  = "(2.*u["  +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-9.*u["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+18.*u["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-11.*u["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(6.*hy)";
    code_j  = "(2.*du["  +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-9.*du["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+18.*du["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-11.*du["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(6.*hy)";
  }
  else if (dm == PDECommon::INWARD_DIFF5)
  {
    const string j3("3"), j4("4");
    code_y  = "(-3.*u["  +bdd.pde->code_elem_func(e, i, j4, k, c)+"]+16.*u["
                        +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-36.*u["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+48.*u["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-25.*u["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(12.*hx)";
    code_j  = "(-3.*du["+bdd.pde->code_elem_func(e, i, j4, k, c)+"]+16.*du["
                        +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-36.*du["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+48.*du["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-25.*du["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(12.*hx)";
  }
  else
    THROW(_S"invalid boundary discretization method: " + to_string(dm)+".");
  cf->text_bx_y[0] = code_y;
  cf->text_bx_j[0] = code_j;
  cf->hint_bx_y[0] = cf->hint_bx_j[0] = BINL;

  // "Higher" boundary extremity (R, T)
  j0 = "ny-1"; j1 = "ny-2"; j2 = "ny-3";
  if (dm == PDECommon::INWARD_DIFF3)
  {
    code_y  = "(-4.*u["  +bdd.pde->code_elem_func(e, i, j1, k, c)+"]+u["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+3.*u["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(2.*hy)";
    code_j  = "(-4.*du["+bdd.pde->code_elem_func(e, i, j1, k, c)+"]+du["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+3.*du["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(2.*hy)";
  }
  else if (dm == PDECommon::INWARD_DIFF4)
  {
    const string j3("ny-4");
    code_y  = "(2.*u["  +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-9.*u["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+18.*u["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-11.*u["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(-6.*hy)";
    code_j  = "(2.*du["  +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-9.*du["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+18.*du["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-11.*du["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(-6.*hy)";
  }
  else if (dm == PDECommon::INWARD_DIFF5)
  {
    const string j3("ny-4"), j4("ny-5");
    code_y  = "(-3.*u["  +bdd.pde->code_elem_func(e, i, j4, k, c)+"]+16.*u["
                        +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-36.*u["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+48.*u["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-25.*u["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(-12.*hx)";
    code_j  = "(-3.*du["+bdd.pde->code_elem_func(e, i, j4, k, c)+"]+16.*du["
                        +bdd.pde->code_elem_func(e, i, j3, k, c)+"]-36.*du["
                        +bdd.pde->code_elem_func(e, i, j2, k, c)+"]+48.*du["
                        +bdd.pde->code_elem_func(e, i, j1, k, c)+"]-25.*du["
                        +bdd.pde->code_elem_func(e, i, j0, k, c)+"])/(-12.*hx)";
  }
  cf->text_bx_y[1] = code_y;
  cf->text_bx_j[1] = code_j;
  cf->hint_bx_y[1] = cf->hint_bx_j[1] = BINL;
}

CodeFragment DiscretizerDy::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string jp(j+"+1"), jm(j+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", i, jp, k)+"-"
                           +d.pde->code_func_call("$f$", i, jm, k)+")/(2.*hy)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//:::::::::::::::::::::::::::::::::::  Dz  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDz::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string kp(k+"+1"), km(k+"-1");
  const string code_y = "(u[" +d.pde->code_elem_func(e, i, j, kp, c)+"]-u["
                              +d.pde->code_elem_func(e, i, j, km, c)+"])/(2.*hz)";
  const string code_j = "(du["+d.pde->code_elem_func(e, i, j, kp, c)+"]-du["
                              +d.pde->code_elem_func(e, i, j, km, c)+"])/(2.*hz)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

// bdm[n] : [b]oundary [d]iscretization [m]ethod [n]
CodeFragment DiscretizerDz::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
#ifndef USE_UNIFIED_DISCRETIZER
  if (bdd.dm == PDECommon::SYM_DIFF2) return discretize_on_boundary_bdm2(bdd);

  if (apply_std_discretization(bdd, Boundary::Z))
  {
    CodeFragment cf = discretize_on_boundary_bdm2(bdd);
    discretize_on_boundary_extremities(bdd, &cf);
    return cf;
  }

  if (bdd.sid != MeshCommon::U && bdd.sid != MeshCommon::D)
    THROW("incorrect side identifier: " + fd::to_string(bdd.sid) + ".");

  switch (bdd.dm)
  {
    case PDECommon::INWARD_DIFF3: return discretize_on_boundary_bdm3(bdd);
    case PDECommon::INWARD_DIFF4: return discretize_on_boundary_bdm4(bdd);
    case PDECommon::INWARD_DIFF5: return discretize_on_boundary_bdm5(bdd);
  }
  THROW("unknown boundary discretization method: "+fd::to_string(bdd.dm)+".");
  return CodeFragment();
#else
  return Discretizer::discretize_on_boundary(Boundary::Z, bdd);
#endif // !USE_UNIFIED_DISCRETIZER
}

#ifndef USE_UNIFIED_DISCRETIZER
CodeFragment DiscretizerDz::discretize_on_boundary_bdm3(
    const BoundaryDiscretizationData &bdd) const
{
  const string &i = bdd.i, &j = bdd.j/*, &k = bdd.k*/;
  const string e("e"), c("$c$");
  string k0, k1, k2, s;
  if (bdd.sid == MeshCommon::D) {k0="0"; k1="1"; k2="2";}
  else {k0="nz-1"; k1="nz-2"; k2="nz-3"; s="-";}

  const auto &pde = *bdd.pde; 
  string code_y, code_j;
  code_y = "(4.*u[" +pde.code_elem_func(e, i, j, k1, c)+"]-u["
                    +pde.code_elem_func(e, i, j, k2, c)+"]-3.*u["
                    +pde.code_elem_func(e, i, j, k0, c)+"])/("+s+"2.*hz)";
  code_j = "(4.*du["+pde.code_elem_func(e, i, j, k1, c)+"]-du["
                    +pde.code_elem_func(e, i, j, k2, c)+"]-3.*du["
                    +pde.code_elem_func(e, i, j, k0, c)+"])/("+s+"2.*hz)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerDz::discretize_on_boundary_bdm4(
    const BoundaryDiscretizationData &bdd) const
{
  const string& i = bdd.i, &j = bdd.j/*, &k = bdd.k*/;
  const string e("e"), c("$c$");
  string k0, k1, k2, k3, s;
  if (bdd.sid == MeshCommon::D) {k0="0"; k1="1"; k2="2"; k3="3";}
  else {k0="nz-1"; k1="nz-2"; k2="nz-3"; k3="nz-4"; s="-";}

  const auto &pde = *bdd.pde; 
  string code_y, code_j;
  code_y = "(2.*u[" +pde.code_elem_func(e, i, j, k3, c)+"]-9.*u["
                    +pde.code_elem_func(e, i, j, k2, c)+"]+18.*u["
                    +pde.code_elem_func(e, i, j, k1, c)+"]-11.*u["
                    +pde.code_elem_func(e, i, j, k0, c)+"])/("+s+"6.*hz)";
  code_j = "(2.*du["+pde.code_elem_func(e, i, j, k3, c)+"]-9.*du["
                    +pde.code_elem_func(e, i, j, k2, c)+"]+18.*du["
                    +pde.code_elem_func(e, i, j, k1, c)+"]-11.*du["
                    +pde.code_elem_func(e, i, j, k0, c)+"])/("+s+"6.*hz)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

CodeFragment DiscretizerDz::discretize_on_boundary_bdm5(
    const BoundaryDiscretizationData &bdd) const
{
  const string &i = bdd.i, &j = bdd.j/*, &k = bdd.k*/;
  const string e("e"), c("$c$");
  string k0, k1, k2, k3, k4, s;
  if (bdd.sid == MeshCommon::D) {k0="0"; k1="1"; k2="2"; k3="3"; k4="4";}
  else {k0="nz-1"; k1="nz-2"; k2="nz-3"; k3="nz-4"; k4="nz-5"; s="-";}

  const auto &pde = *bdd.pde; 
  string code_y, code_j;
  code_y = "(-3.*u[" +pde.code_elem_func(e, i, j, k4, c)+"]+16.*u["
                     +pde.code_elem_func(e, i, j, k3, c)+"]-36.*u["
                     +pde.code_elem_func(e, i, j, k2, c)+"]+48.*u["
                     +pde.code_elem_func(e, i, j, k1, c)+"]-25.*u["
                     +pde.code_elem_func(e, i, j, k0, c)+"])/("+s+"12.*hz)";
  code_j = "(-3.*du["+pde.code_elem_func(e, i, j, k4, c)+"]+16.*du["
                     +pde.code_elem_func(e, i, j, k3, c)+"]-36.*du["
                     +pde.code_elem_func(e, i, j, k2, c)+"]+48.*du["
                     +pde.code_elem_func(e, i, j, k1, c)+"]-25.*du["
                     +pde.code_elem_func(e, i, j, k0, c)+"])/("+s+"12.*hz)";
  CodeFragment cf(code_y, code_j, BINL, BINL);
  cf.refers_to_boundary(bdd.bid);
  return cf;
}
#endif // !USE_UNIFIED_DISCRETIZER

CodeFragment DiscretizerDz::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string kp(k+"+1"), km(k+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", i, j, kp)+"-"
                           +d.pde->code_func_call("$f$", i, j, km)+")/(2.*hz)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//::::::::::::::::::::::::::::::::::  Dxx  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDxx::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string ip = i + "+1";
  const string im = i + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, ip, j, k, c)+"]-2.*u["
                        +d.pde->code_elem_func(e, i,  j, k, c)+"]+u["
                        +d.pde->code_elem_func(e, im, j, k, c)+"])/(hx*hx)";
  string code_j = "(du["+d.pde->code_elem_func(e, ip, j, k, c)+"]-2.*du["
                        +d.pde->code_elem_func(e, i,  j, k, c)+"]+du["
                        +d.pde->code_elem_func(e, im, j, k, c)+"])/(hx*hx)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment
DiscretizerDxx::discretize_scalar_field(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string ip(i+"+1"), im(i+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", ip, j, k)+"-2.*"
                           +d.pde->code_func_call("$f$", i,  j, k)+"+"
                           +d.pde->code_func_call("$f$", im, j, k)+")/(hx*hx)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//::::::::::::::::::::::::::::::::::  Dyy  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDyy::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string jp = j + "+1";
  const string jm = j + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, i, jp, k, c)+"]-2.*u["
                        +d.pde->code_elem_func(e, i, j,  k, c)+"]+u["
                        +d.pde->code_elem_func(e, i, jm, k, c)+"])/(hy*hy)";
  string code_j = "(du["+d.pde->code_elem_func(e, i, jp, k, c)+"]-2.*du["
                        +d.pde->code_elem_func(e, i, j , k, c)+"]+du["
                        +d.pde->code_elem_func(e, i, jm, k, c)+"])/(hy*hy)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment
DiscretizerDyy::discretize_scalar_field(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string jp(j+"+1"), jm(j+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", i, jp, k)+"-2.*"
                           +d.pde->code_func_call("$f$", i, j,  k)+"+"
                           +d.pde->code_func_call("$f$", i, jm, k)+")/(hy*hy)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//::::::::::::::::::::::::::::::::::  Dzz  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDzz::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string kp = k + "+1";
  const string km = k + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, i, j, kp, c)+"]-2.*u["
                        +d.pde->code_elem_func(e, i, j, k , c)+"]+u["
                        +d.pde->code_elem_func(e, i, j, km, c)+"])/(hz*hz)";
  string code_j = "(du["+d.pde->code_elem_func(e, i, j, kp, c)+"]-2.*du["
                        +d.pde->code_elem_func(e, i, j, k , c)+"]+du["
                        +d.pde->code_elem_func(e, i, j, km, c)+"])/(hz*hz)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment
DiscretizerDzz::discretize_scalar_field(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string kp(k+"+1"), km(k+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", i, j, kp)+"-2.*"
                           +d.pde->code_func_call("$f$", i, j, k )+"+"
                           +d.pde->code_func_call("$f$", i, j, km)+")/(hz*hz)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//::::::::::::::::::::::::::::::::::  Dxy  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDxy::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string ip = i + "+1";
  const string im = i + "-1";
  const string jp = j + "+1";
  const string jm = j + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, ip, jp, k, c)+"]-u["
                        +d.pde->code_elem_func(e, im, jp, k, c)+"]+u["
                        +d.pde->code_elem_func(e, im, jm, k, c)+"]-u["
                        +d.pde->code_elem_func(e, ip, jm, k, c)+"])/(4.*hx*hy)";
  string code_j = "(du["+d.pde->code_elem_func(e, ip, jp, k, c)+"]-du["
                        +d.pde->code_elem_func(e, im, jp, k, c)+"]+du["
                        +d.pde->code_elem_func(e, im, jm, k, c)+"]-du["
                        +d.pde->code_elem_func(e, ip, jm, k, c)+"])/(4.*hx*hy)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment DiscretizerDxy::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string ip(i+"+1"), im(i+"-1"), jp(j+"+1"), jm(j+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", ip, jp, k)+"-"
                           +d.pde->code_func_call("$f$", im, jp, k)+"+"
                           +d.pde->code_func_call("$f$", im, jm, k)+"-"
                           +d.pde->code_func_call("$f$", ip, jm, k)+")/(4.*hx*hy)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//::::::::::::::::::::::::::::::::::  Dxz  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDxz::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string ip = i + "+1";
  const string im = i + "-1";
  const string kp = k + "+1";
  const string km = k + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, ip, j, kp, c)+"]-u["
                        +d.pde->code_elem_func(e, im, j, kp, c)+"]+u["
                        +d.pde->code_elem_func(e, im, j, km, c)+"]-u["
                        +d.pde->code_elem_func(e, ip, j, km, c)+"])/(4.*hx*hz)";
  string code_j = "(du["+d.pde->code_elem_func(e, ip, j, kp, c)+"]-du["
                        +d.pde->code_elem_func(e, im, j, kp, c)+"]+du["
                        +d.pde->code_elem_func(e, im, j, km, c)+"]-du["
                        +d.pde->code_elem_func(e, ip, j, km, c)+"])/(4.*hx*hz)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment DiscretizerDxz::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string ip(i+"+1"), im(i+"-1"), kp(k+"+1"), km(k+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", ip, j, kp)+"-"
                           +d.pde->code_func_call("$f$", im, j, kp)+"+"
                           +d.pde->code_func_call("$f$", im, j, km)+"-"
                           +d.pde->code_func_call("$f$", ip, j, km)+")/(4.*hx*hz)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//::::::::::::::::::::::::::::::::::  Dyz  ::::::::::::::::::::::::::::::::::://
//============================================================================//
CodeFragment DiscretizerDyz::core_discretize(const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string e("e"), c("$c$");
  const string jp = j + "+1";
  const string jm = j + "-1";
  const string kp = k + "+1";
  const string km = k + "-1";
  string code_y = "(u[" +d.pde->code_elem_func(e, i, jp, kp, c)+"]-u["
                        +d.pde->code_elem_func(e, i, jm, kp, c)+"]+u["
                        +d.pde->code_elem_func(e, i, jm, km, c)+"]-u["
                        +d.pde->code_elem_func(e, i, jp, km, c)+"])/(4.*hy*hz)";
  string code_j = "(du["+d.pde->code_elem_func(e, i, jp, kp, c)+"]-du["
                        +d.pde->code_elem_func(e, i, jm, kp, c)+"]+du["
                        +d.pde->code_elem_func(e, i, jm, km, c)+"]-du["
                        +d.pde->code_elem_func(e, i, jp, km, c)+"])/(4.*hy*hz)";
  return CodeFragment(code_y, code_j, BINL, BINL);
}

CodeFragment DiscretizerDyz::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const string &i = d.i, &j = d.j, &k = d.k;
  const string jp(j+"+1"), jm(j+"-1"), kp(k+"+1"), km(k+"-1");
  const string code_y = "("+d.pde->code_func_call("$f$", i, jp, kp)+"-"
                           +d.pde->code_func_call("$f$", i, jm, kp)+"+"
                           +d.pde->code_func_call("$f$", i, jm, km)+"-"
                           +d.pde->code_func_call("$f$", i, jp, km)+")/(4.*hy*hz)";
  const string code_j = "0";
  return CodeFragment(code_y, code_j, BINL);
}

//============================================================================//
//:::::::::::::::::::::::::  Boundary discretizers  :::::::::::::::::::::::::://
//============================================================================//
CodeFragment DirichletBCDiscretizer::core_discretize(const DiscretizationData& d) const
{
  throw _S"DirichletBCDiscretizer::discretize() can only be used on a boundary.";
  return CodeFragment();
}

CodeFragment DirichletBCDiscretizer::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  const string &i = bdd.i, &j = bdd.j, &k = bdd.k;
  const string &s = _bc->fname;
  const string e = bdd.pde->code_elem_func("e", i, j, k, "$c$");
  const string f = fd::regex_match_funcname(s) ?
      "-" + bdd.pde->code_func_call(s, i, j, k) : (s.empty() ? _S"" : "-" + s);
  const string code_y = "u[" +e+"]"+f;
  const string code_j = "du["+e+"]";
  return CodeFragment(code_y, code_j);
}

CodeFragment NeumannBCDiscretizer::core_discretize(const DiscretizationData& d) const
{
  throw _S"NeumannBCDiscretizer::discretize() can only be used on a boundary.";
  return CodeFragment();
}

CodeFragment NeumannBCDiscretizer::core_discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  const int sid = bdd.sid;
  const string &i = bdd.i, &j = bdd.j, &k = bdd.k;
  const string e("e");
  const string cid("$c$");

  // Block B
  CodeFragment cf;
  if (sid == MeshCommon::L || sid == MeshCommon::R)
  {
    DiscretizerDx d;
    cf = d.discretize_on_boundary(bdd);
  }
  else if (sid == MeshCommon::T || sid == MeshCommon::B)
  {
    DiscretizerDy d;
    cf = d.discretize_on_boundary(bdd);
  }
  else if (sid == MeshCommon::U || sid == MeshCommon::D) /* 3d */
  {
    DiscretizerDz d;
    cf = d.discretize_on_boundary(bdd);
  }

  auto code_coef = [this, &bdd](int k, bool condition, const string& postfix)
  {
    string result = _bc->is_func(k) ?
      bdd.pde->code_func_call(_bc->coef[k], bdd.i, bdd.j, bdd.k) :
        (condition ? EMPTY : _bc->get_varname(k));
    return result.empty() ? result : result + postfix;
  };

  const string a = code_coef(2, _bc->no_coef(2), "*");
  const string b = code_coef(1, _bc->no_coef(1) || _bc->item_not_present(1), "");
  const string c = code_coef(0, _bc->is_zero(0), "");

  auto get_item = [this, &b](const string& u)
  {
    return _bc->item_not_present(1) ? EMPTY : string("+") +
        (_bc->no_coef(1) ? EMPTY : b + "*") + u;
  };

  // Constants are used through variables which are declared as class members.
  // So, there is no need to take care of the constant's sign.

  const string v = "[" + bdd.pde->code_elem_func(e, i, j, k, cid) + "]";
  const string u = get_item("u");
  const string du = get_item("du");
  const string item1 = u.empty() ? EMPTY : (u + v);
  const string ditem = du.empty() ? EMPTY : (du + v);
  const string item0 = c.empty() ? EMPTY : ("-" + c);
  cf.text_y = a + cf.text_y + item1 + item0;
  cf.text_j = a + cf.text_j + ditem;
  for (int i = 0; i < CodeFragment::sizeof_bx(); ++i)
    cf.text_bx_y[i] = cf.text_bx_j[i] = "";
  for (int i = 0; i < CodeFragment::sizeof_vx(); ++i)
    cf.text_vx_y[i] = cf.text_vx_j[i] = "";
  return cf;
}

} // namespace fd
