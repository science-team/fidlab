#include <iostream>
#include <cstdlib>
#include <cmath>
// #include <ctime>
#include "pdexd3d.hpp"
#include "config_code.h"
#include <tuple>

using std::string;  using std::vector;   using std::array;
using fd::Boundary; using fd::NeumannBC; using fd::Axis;

namespace extdom {
namespace d3 {

//============================================================================//
//::::::::::::::::::::::::::::::::::  PDE  ::::::::::::::::::::::::::::::::::://
//============================================================================//

//============================================================================//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//::::::::::::::::::::::::::   NEW IMPLEMENTATION   :::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//============================================================================//

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::  Generate function body  ::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

//::::::::::::::::::::::::  Boundary Code Generators  :::::::::::::::::::::::://

void PDE::generate_code_vertex(int what, int vid, int bid, int ntabs)
{
  const auto bids = get_vertex_bids(vid, bid);
  for (int c = 0; c < components(); ++c)
    generate_code_SYM_DIFF2_for(c, bids, what, ntabs);
}

void PDE::generate_code_edge(int what, int eid, int bid, int fid2, int ntabs)
{
  const int bid2 = std::get<0>(mesh_->get_meeting_bid(bid, eid));
  THROW_IF(bid2 == -1, "No matching boundary along edge "+fd::to_string(eid)
                     + " for boundary "+fd::to_string(bid)+".");
  const array<int,3> bids{bid, bid2, -1};
  for (int c = 0; c < components(); ++c)
    generate_code_SYM_DIFF2_for(c, bids, what, ntabs);
}

void PDE::generate_code_face(int what, int bid, int ntabs)
{
  for (int c = 0; c < components(); ++c)
    generate_code_SYM_DIFF2_for(c, bid, what, ntabs, ALL_BC_CODES, [this](
      const string &i, const string &j, const string &k, const string &c) {
      return format_elem_lhs("e", i, j, k, c);
    }
  );
}

//============================================================================//
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//::::::::::::::::::::::::::   NEW IMPLEMENTATION   :::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//============================================================================//

static const string XTC[3][2] = {
    {"-1", "nx"},
    {"-1", "ny"},
    {"-1", "nz"},
};
static const string (&I)[2] = XTC[0];
static const string (&J)[2] = XTC[1];
static const string (&K)[2] = XTC[2];

void PDE::generate_code_virtual_points(int what)
{
  static const string PDE::* VNAME[3] {&PDE::i_, &PDE::j_, &PDE::k_};

#if 0
  static const int fd::Field::* FEXTR[3][2] {
    {&fd::Field::i_start, &fd::Field::i_end},
    {&fd::Field::j_start, &fd::Field::j_end},
    {&fd::Field::k_start, &fd::Field::k_end}
  };

  static const int XEXTR[3][2] {
      {-1, mesh_->nx + 1},
      {-1, mesh_->ny + 1},
      {-1, mesh_->nz + 1}
  };

  static const int MISSING_AXIS[4] {-1, Axis::Z, Axis::Y, Axis::X};
  static const char VNAME[3] {'i','j','k'};
  static const string VARNAME[3] {"ie","je","ke"};
  static const string VAREND [3] {"nx","ny","nz"};
#endif // 0

  const int nc = components();

  bool header_written = false;
  for (int c = 0; c < nc; ++c)
  {
    const string findex = fd::to_string_empty_if(nc == 1, c);
    bool first_time = true;

    for (int eid = 0; eid < 12; ++eid)
    {
      auto edge_info = get_edge_info(eid, c);
      const auto& xb = edge_info.extended_boundary;

#if 0      //////////////////////////////////////
      const int * const fids = get_edge_fids(eid);
      const int ft[2] { // axis
        Mesh::face_btype(fids[0]),
        Mesh::face_btype(fids[1])
      };
      const int fx[2] {
        Mesh::face_extremity(fids[0]),
        Mesh::face_extremity(fids[1])
      };
      const int xextr[2] {
        field_[c].*FEXTR[ft[0]][fx[0]],
        field_[c].*FEXTR[ft[1]][fx[1]],
      };

      ASSERT(ft[0] > -1 && ft[1] > -1 && ft[0] + ft[1] < 4 && ft[0] != ft[1]);
#endif // 0      /////////////////////////////////////////////

      if (xb[0] && xb[1])
      {
        const string varnam = this->*VNAME[edge_info.edge_axis];//+_S"e";
        const string &varend = XTC[edge_info.edge_axis][1];
        if (!header_written)
        {
          header_written = true;
          code_ << R"(
	// Code for virtual edges and vertices)";
        }
        if (first_time)
        {
          first_time = false;
          if (nc > 1) code_ << R"(
	// Field )"<<c<<":";
        }
        code_ << R"(
	// Virtual edge )"<<eid<<R"(:
	for (int )"<<varnam<<" = 0; "<<varnam<<" < "<<varend<<"; ++"<<varnam<<R"()
	{)";

        const auto& ft = edge_info.face_type;
        const auto& fx = edge_info.face_extremity;

        array<int,3> t{-1,-1,-1}, x{-1,-1,-1};
        t[ft[0]] = ft[0]; x[ft[0]] = fx[0];
        t[ft[1]] = ft[1]; x[ft[1]] = fx[1];
        const string vi = (t[0] == -1) ? varnam : I[x[0]];
        const string vj = (t[1] == -1) ? varnam : J[x[1]];
        const string vk = (t[2] == -1) ? varnam : K[x[2]];
        const string eqnum = format_elem_lhs("e", vi, vj, vk, findex);

        if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		F[)"<<eqnum<<"]=u[" <<eqnum<<"];";
        else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		J[)"<<eqnum<<"]=du["<<eqnum<<"];";
        else code_ << R"(
		F[)"<<eqnum<<"]=u[" <<eqnum<<R"(];
		J[)"<<eqnum<<"]=du["<<eqnum<<"];";
        code_ << R"(
	})";
      }
    }

    for (int vid = 0; vid < 8; ++vid)
    {
      auto vertex_info = get_vertex_info(vid, c);
      const auto& ft = vertex_info.face_type;
      const auto& fx = vertex_info.face_extremity;
      const auto& xb = vertex_info.extended_boundary;

#if 0      
      const int * const fids = get_vertex_fids(vid);
      const int ft[3] { // axis
        Mesh::face_btype(fids[0]),
        Mesh::face_btype(fids[1]),
        Mesh::face_btype(fids[2]),
      };
      const int fx[3] {
        Mesh::face_extremity(fids[0]),
        Mesh::face_extremity(fids[1]),
        Mesh::face_extremity(fids[2]),
      };
      const int xextr[3] {
        field_[c].*FEXTR[ft[0]][fx[0]],
        field_[c].*FEXTR[ft[1]][fx[1]],
        field_[c].*FEXTR[ft[2]][fx[2]],
      };

      if (xextr[0] == XEXTR[ft[0]][fx[0]] &&
          xextr[1] == XEXTR[ft[1]][fx[1]] &&
          xextr[2] == XEXTR[ft[2]][fx[2]])
#endif // 0      
      if (xb[0] && xb[1] && xb[2])
      {
        array<int,3> x;
        x[ft[0]] = fx[0]; x[ft[1]] = fx[1]; x[ft[2]] = fx[2];
        const string &ii = I[x[0]];
        const string &jj = J[x[1]];
        const string &kk = K[x[2]];
        const string eqnum = format_elem_lhs("e", ii, jj, kk, findex);
        code_ << R"(
	// Virtual vertex )"<<vid<<":";
        if (what == GENERATE_FUNCTION_CODE) code_ << R"(
	F[)"<<eqnum<<"]=u[" <<eqnum<<"];";
        else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
	J[)"<<eqnum<<"]=du["<<eqnum<<"];";
        else code_ << R"(
	F[)"<<eqnum<<"]=u[" <<eqnum<<R"(];
	J[)"<<eqnum<<"]=du["<<eqnum<<"];";
      }
    }
  }
}

} // namespace d3
} // namespace extdom
