#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include "pde.hpp"
#include "config_code.h"
#include "global_config.h"

using std::string;
using std::vector;
using fd::Boundary; using fd::NeumannBC;

namespace fd {

// -------------------------------------------------------------------------- //
// :::::::::::::::::::::  Header (Interface) Generator  ::::::::::::::::::::: //
// -------------------------------------------------------------------------- //

string PDECommon::format_num_nodes() const
{
  static const string s[4]{"","nx","nx*ny","nx*ny*nz"};
  return s[dimension()];
}

string PDECommon::format_include_macro() const
{
  std::string incl_macro = get_header_filename();
  string::size_type pos = incl_macro.rfind('/');
  if (pos != string::npos) incl_macro = incl_macro.substr(pos + 1);
  std::transform(incl_macro.begin(), incl_macro.end(), incl_macro.begin(),
      toupper);
  fd::replace_all(incl_macro, ".", "_");
  incl_macro += "_INCLUDED";
  return incl_macro;
}

void PDECommon::generate_header_header(const string& incl_macro)
{
  std::time_t this_time = std::time(nullptr);
  string NOW(std::ctime(&this_time));
  string::size_type pos = NOW.rfind('\n');
  if (pos != string::npos) NOW = NOW.substr(0, pos);

  code_h_ << "/* -*- Mode: C++; indent-tabs-mode: t; "
          << "c-basic-offset: 2; tab-width: 2 -*-  */\n"
          << "// Generated by " APPNAME " on " << NOW << std::endl
          << "// *** Do not edit unless you know what you are doing! ***"
          << std::endl << std::endl
          << "#ifndef " << incl_macro << std::endl
          << "#define " << incl_macro << std::endl;
}

string PDECommon::format_start_end_indices() const
{
  const int dim = dimension();
  const string nc = fd::to_string(components());
  string s = R"(
	int is[)"+ nc + R"(];	//!< Natural start index in x-direction
	int ie[)"+ nc + R"(];	//!< Natural end index in x-direction)";
  if (dim > 1) s += R"(
	int js[)"+ nc + R"(];	//!< Natural start index in y-direction
	int je[)"+ nc + R"(];	//!< Natural end index in y-direction)";
  if (dim > 2) s += R"(
	int ks[)"+ nc + R"(];	//!< Natural start index in z-direction
	int ke[)"+ nc + R"(];	//!< Natural end index in z-direction)";
  return s;
}

string PDECommon::format_function_declaration(const string &f) const
{
  const int dim = dimension();
  const int nc = components();
  string s = f+"(int i";
  if (dim > 1) s += ", int j";
  if (dim > 2) s += ", int k";
  if (nc > 1) s += ", int c";
  return s+")";
}

string PDECommon::format_mesh_coordinates_to_field_index_1f() const
{
  const int dim = dimension();
  string s;
  if (dim > 2) s += "(ie[0] - is[0])*(je[0] - js[0])*(k - ks[0]) + ";
  if (dim > 1) s += "(ie[0] - is[0])*(j - js[0]) + ";
	return " return "+s+"i - is[0]; ";
}

string PDECommon::format_mesh_coordinates_to_field_index_mf() const
{
  const int dim = dimension();
  string s="", sz="";
  if (dim > 2)
  {
    s  = "(ie[f] - is[f])*(je[f] - js[f])*(k - ks[f]) + ";
    sz = "*(ke[f] - ks[f])";
  }
  if (dim > 1)
  {
    s += "(ie[f] - is[f])*(j - js[f]) + ";
    sz = "(ie[f] - is[f])*(je[f] - js[f])"+sz;
  }
  else /*dim == 1*/
  {
    sz = "ie[f] - is[f]";
  }
  s += "i - is[f]";
  return R"(
		for (int n = 0, f = 0; f < nc; ++f)
		{
			if (f == c)
				return n + )"+s+R"(;
			n += )"+sz+R"(;
		}
		throw ")"+name+R"(::mesh_coordinates_to_field_index(): component " +
			std::to_string(c) + " is out of range.";
		return -1; // prevent compiler complaints
	)";
}

string PDECommon::format_mesh_coordinates_to_field_index() const
{
  return components() == 1 ? format_mesh_coordinates_to_field_index_1f()
                           : format_mesh_coordinates_to_field_index_mf();
}

string PDECommon::format_field_index_to_mesh_coordinates_1f() const
{
  const int dim = dimension();
	if (dim == 1) return " mesh_coord[0] = is[0] + field_index; ";
  string s = R"(
		std::div_t division = std::div(field_index, ie[0] - is[0]);
		mesh_coord[0] = is[0] + division.rem;)";
  if (dim > 2) s += R"(
		division = std::div(division.quot, je[0] - js[0]);
		mesh_coord[1] = js[0] + division.rem;
		mesh_coord[2] = ks[0] + division.quot;
	)";
  else s += R"(
		mesh_coord[1] = js[0] + division.quot;
	)";
  return s;
}

string PDECommon::format_field_index_to_mesh_coordinates_mf() const
{
  const int dim = dimension();
  string s;
  if (dim == 1)
  {
    s = R"(
				mesh_coord[0] = is[c] + field_index;)";
  }
  else
  {
    s = R"(
				std::div_t division = std::div(e, ie[c] - is[c]);
				mesh_coord[0] = is[c] + division.rem;)";
    if (dim > 2) s += R"(
				division = std::div(division.quot, je[c] - js[c]);
				mesh_coord[1] = js[c] + division.rem;
				mesh_coord[2] = ks[c] + division.quot;)";
    else /*dim == 1*/ s += R"(
				mesh_coord[1] = js[c] + division.quot;)";
  }
  s += R"(
				return c;
			)";

  string sz = "ie[c] - is[c]";
  if (dim > 1) sz  = "("+sz+")"+_S"*(je[c] - js[c])";
  if (dim > 2) sz += "*(ke[c] - ks[c])";

  return R"(
		for (int e = field_index, c = 0; c < nc; ++c)
		{
			const int sz = )"+sz+R"(;
			if (e < sz)
			{)"+s+R"(}
			e -= sz;
		}
		throw ")"+name+R"(::field_index_to_mesh_coordinates(): component " +
				std::to_string(field_index) + " is out of range.";
		return -1;
	)";
}

string PDECommon::format_field_index_to_mesh_coordinates() const
{
  return components() == 1 ? format_field_index_to_mesh_coordinates_1f()
                           : format_field_index_to_mesh_coordinates_mf();
}

string PDECommon::format_space_function_declaration(const string &f, bool mf) const
{
  const int dim = dimension();
  string s = f+"(";
  if (mf) s += "int c, ";
  s += "double x";
  if (dim > 1) s += ", double y";
  if (dim > 2) s += ", double z";
  return s+")";
}

void PDECommon::generate_header_node_functions()
{
  const int nc = components();
	code_h_ << R"(
	//! Compute global field index from mesh coordinates)"<<(nc == 1 ?
        "." : " and field number.") << R"(
	int )"<<format_function_declaration("mesh_coordinates_to_field_index")
          << R"( const
	{)"<<format_mesh_coordinates_to_field_index()<<R"(}

	//! Alias for \c mesh_coordinates_to_field_index.
	inline int )"<<format_function_declaration("e")<<R"( const
	{ return )"<<format_node_function_call(
    "mesh_coordinates_to_field_index","i","j","k",nc==1? "" : "c")<<R"(; }

	//! Convert global field index to mesh coordinates)"<<(nc == 1 ?
        "." : " and return field number.") << R"(
	)"<<(nc == 1 ? "void " : "int ")
      <<R"(field_index_to_mesh_coordinates(int field_index, int mesh_coord[)"
      <<dimension()<<R"(]) const
	{)"<<format_field_index_to_mesh_coordinates()<<R"(}
)";
}

void PDECommon::generate_header_size_functions()
{
  static const char *zeros[] {
    "",
    "0,",
    "0,0,",
    "0,0,0,",
  };
  static const char *sz[] {
    "",
    "ie[c] - is[c]",
    "(ie[c] - is[c])*(je[c] - js[c])",
    "(ie[c] - is[c])*(je[c] - js[c])*(ke[c] - ks[c])",
  };

  const int nc = components();
  const int d = dimension();
  const string i{nc == 1 ? "0" : "nc-1"};
  const string c{nc == 1 ? ""  : "nc-1"};
  const string I{d < 2 ? "ie["+i+"]"  : "is["+i+"]"};
  const string J{d < 3 ? "je["+i+"]"  : "js["+i+"]"};

  code_h_ << R"(
	//! Dimension of the problem's matrix.
	int dim() const
	{ return )"<<format_node_function_call("mesh_coordinates_to_field_index",
                                           I, J, "ke["+i+"]", c)
          << R"(; }
	//! Alias for \n dim()
	inline int matdim() const {return dim();}
)";
  if (nc > 1) code_h_ << R"(
	//! Size of field \c c.
	inline int field_size(int c) const
	{ return )"<<sz[d]<<R"(; }

	//! Offset of field \c c.
	inline int field_offset(int c) const
	{ return mesh_coordinates_to_field_index()"<<zeros[d]<<R"(c); }
)";
  else code_h_ << R"(
	//! Size of field - alias for \n dim()
	inline int field_size() const {return dim();}
)";
}

void PDECommon::generate_header()
{
  const int nc = components();
  const int dim = dimension();
  const string incl_macro = format_include_macro();

  generate_header_header(incl_macro);
  generate_header_includes();
  if (library_ == PETSC)
    generate_header_classes_petsc();

  code_h_ << R"(

class )" << name << "\n{";

  // Generate code for Neumann BCs
  for (BoundaryCondition *bc : BCs())
  {
    if (bc->type != BoundaryCondition::NEUMANN) continue;
    NeumannBC *nbc = static_cast<NeumannBC *>(bc);
    bool first = true;
    for (int i = 0; i < 3; ++i)
    {
      if (nbc->coef_type[i] != NeumannBC::CONST_VALUE) continue;
      if (first)
      {
        code_h_ << R"(
	// Constant coefficients for Neumann BC, boundary )"
  	            << get_bid(bc->boundary());
  	    if (nc > 1)
  	    {
  	      int cindex = get_component_index(bc->component());
  	      const Field& fc = field_[cindex];
  	      code_h_ << ", field ";
  	      if (fc.name.empty()) code_h_ << "#" << cindex;
  	      else code_h_ << fc.name;
  	    }
  	    code_h_ << ":";
  	    first = false;
  	  }
  	  code_h_ << R"(
	double )" << nbc->coef[i] << ";";
  	  if (i == 2) code_h_ << std::endl;
    }
  }

  generate_header_extra_idefs();

  code_h_ << R"(
public:

	const int )"<<format_mesh_params(NODE_NUMBERS)<<R"(;		//!< Number of mesh nodes
	const int nc = )" << nc << ";	//!< Number of components" <<
  format_start_end_indices() << R"(
	double )"<<format_mesh_params(DOMAIN_EXTREMITIES)<<R"(;		//!< Geometric definition of domain.
	double )"<<format_mesh_params(MESH_SPACING)<<R"(;				//!< Mesh spacing.
	bool output_results;	//!< Output results.)";

  if (library_ == PETSC)
  {
    code_h_ << R"(
	bool norms_cache_valid = false;
	double norms_cache)"<<(nc==1 ? _S"" : _S"["+fd::to_string(nc)+"]")<<"[3];";
  }

  if (has_default_field_function_implementations())
  {
    const double h = global_config().num_diff_h;
    const double zero = global_config().num_diff_zero;
    code_h_ << R"(

	// Accuracy required in numerical differentiations (change as necessary):)";
    for (FieldFunction *f : field_functions_)
      for (int n = 1; n < f->argc() + 1; ++n)
        if ((int)f->implementation.size() > n && f->implementation[n] == "default")
        {
          code_h_ << R"(
	double )" << f->get_accuracy_var(n, 1) << " = " << h << ";" << R"(
	double )" << f->get_accuracy_var(n, 0) << " = " << zero << ";";
        }
  }

  generate_header_constructors();
  generate_header_node_functions();
  generate_header_size_functions();

  // Generate code for functions used in the PDE system
  if (!functions_.empty())
    code_h_ << R"(
	// PDE functions:)";
  for (fd::Function *f : functions_)
  {
    const bool incl_comp = (nc > 1 && f->fname() == "exact_solution");
    code_h_ << R"(
	double )" << format_space_function_declaration(f->fname(), incl_comp)
              << (f->implementation.empty() ? _S";" : f->implementation);
  }
  if (!functions_.empty())
    code_h_ << std::endl;

  // Generate code for field functions used in the PDE system
  if (!field_functions_.empty())
    code_h_ << R"(
	// PDE field functions:)";
  for (fd::FieldFunction *f : field_functions_)
    for (int n = 0; n < (int) f->argc() + 1; ++n)
      code_h_ << std::endl << f->code_implementation(n, dim);
  if (!field_functions_.empty())
    code_h_ << std::endl;

  // Generate code for cutoff functions used in the PDE system
  if (!cutoff_functions_.empty())
    code_h_ << R"(
	// PDE cutoff functions:)";
  for (fd::CutoffFunction *f : cutoff_functions_)
    code_h_ << std::endl << f->code_implementation(dim);
  if (!cutoff_functions_.empty())
    code_h_ << std::endl;

  code_h_ << R"(
	// Convert mesh coordinates to space coordinates.
	double X(int i) const {return a + double(i)*hx;})";
  if (dim > 1) code_h_ << R"(
	double Y(int j) const {return c + double(j)*hy;})";
  if (dim > 2) code_h_ << R"(
	double Z(int k) const {return p + double(k)*hz;})";
  code_h_ << std::endl;

  switch (library_)
  {
    case GSL:   generate_header_funcs_gsl();   break;
    case PETSC: generate_header_funcs_petsc(); break;
    default:    generate_header_funcs();
  }
  if (has_exact_solution())
  {
    switch (library_)
    {
      case GSL:   generate_header_compare_solution_gsl();   break;
      case PETSC: generate_header_compare_solution_petsc(); break;
      default:    generate_header_compare_solution();
    }
  }

  if (has_initial_estimate()) code_h_ << R"(
	double )"<<format_space_function_declaration("initial_estimate", nc > 1)<<";";

  if (use_field_names && nc > 1) code_h_ << R"(
	const std::string& field_name(int c) const;)";

  code_h_ << R"(

protected:

	void init()
	{
		// Natural (geometric) discretized start/end indices:)";
  for (int i = 0; i < nc; ++i)
  {
    const string fieldname = field_[i].name.empty() ? fd::to_string(i) :
      field_[i].name + " (#" + fd::to_string(i) + ")";
    if (nc > 1) code_h_ << R"(
		// Field component )" << fieldname << ":";
    code_h_ << R"(
		is[)" << i << "] = " << (field_[i].i_start == 0 ? "0" : "-1") << R"(;
		ie[)" << i << "] = " << (field_[i].i_end == nx() ? "nx" : "nx+1") << ";";
    if (dim > 1) code_h_ << R"(
		js[)" << i << "] = " << (field_[i].j_start == 0 ? "0" : "-1") << R"(;
		je[)" << i << "] = " << (field_[i].j_end == ny() ? "ny" : "ny+1") << ";";
    if (dim > 2) code_h_ << R"(
		ks[)" << i << "] = " << (field_[i].k_start == 0 ? "0" : "-1") << R"(;
		ke[)" << i << "] = " << (field_[i].k_end == nz() ? "nz" : "nz+1") << ";";
  }
  code_h_ << R"(

		hx = (b - a)/double(nx - 1);)";
  if (dim > 1) code_h_ << R"(
		hy = (d - c)/double(ny - 1);)";
  if (dim > 2) code_h_ << R"(
		hz = (q - p)/double(nz - 1);)";
  generate_header_init_extra();
  code_h_ << R"(
	})";

  generate_header_extra();

  code_h_ << R"(
};)";

  code_h_ << R"(

#endif // )" << incl_macro << std::endl << std::endl;
}

void PDECommon::generate_header_includes()
{
  code_extra_headers(HEADER_FILE, PLACEMENT_TOP, false);

  code_h_ << R"(
#include <string>)";
  if (library_ == PETSC)
  {
    // doublexz         --> limits, cmath
    // ElementCollector --> vector, algorithm
    // SparseVector     --> vector
    // PDE              --> cstdlib[NL] (sparse_vector.hpp[NL])
    code_h_ << R"(
#include <vector>
#include <algorithm>
#include <limits>
#include <cmath>)";
  }

  code_extra_headers(HEADER_FILE, PLACEMENT_BEFORE_LIB, false);

  switch (library_)
  {
    case GSL: code_h_ << (system_installed_gslpp ? R"(
#include <gsl++/gsl.hpp>)" : R"(
#include "gsl.hpp")");
      break;
  	case PETSC: code_h_ << R"(
#include <petsc++/petsc.hpp>)";
  	  if (!linear()) code_h_ << R"(
#include <petsc++/snes.hpp>)";
  	  break;
  }

  code_extra_headers(HEADER_FILE, PLACEMENT_LAST, false);
}

} // namespace fd
