#include <iostream>
#include <cstdlib>
#include <cmath>
#include "pde3d.hpp"
#include "config_code.h"
#include <tuple>
#include "global_config.h"
#include "range.hpp"

using std::string;  using std::vector;   using std::array;
using fd::Boundary; using fd::NeumannBC; using fd::Axis;

#define RESOL(i) fd::global_config().space_resolution[i]

namespace fd3 {

//                           _____________________   
//:::::::::::::::::::::::::::                     :::::::::::::::::::::::::::://
//:::::::::::::::::::::::::::      PDE Class      :::::::::::::::::::::::::::://
//:::::::::::::::::::::::::::_____________________:::::::::::::::::::::::::::://


//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::  Code Generators  ::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

//============================================================================//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//::::::::::::::::::::::::::   NEW IMPLEMENTATION   :::::::::::::::::::::::::://
//::::::::::::::::::::::::::                        :::::::::::::::::::::::::://
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//============================================================================//

inline static const array<int,3>& sGetVertexFids(int vid) noexcept
{
  static const array<int,3> FIDS[8] {
      /*DBL*/ {Mesh::L, Mesh::B, Mesh::D},
      /*DBR*/ {Mesh::R, Mesh::B, Mesh::D},
      /*DTR*/ {Mesh::R, Mesh::T, Mesh::D},
      /*DTL*/ {Mesh::L, Mesh::T, Mesh::D},
      /*UBL*/ {Mesh::L, Mesh::B, Mesh::U},
      /*UBR*/ {Mesh::R, Mesh::B, Mesh::U},
      /*UTR*/ {Mesh::R, Mesh::T, Mesh::U},
      /*UTL*/ {Mesh::L, Mesh::T, Mesh::U}
  };
  return FIDS[vid];
}

inline static const int *sGetEdgeFids(int eid) noexcept
{
  static const int EIDS[12][2]{
    /*BL*/ {Mesh::B, Mesh::L},
    /*BR*/ {Mesh::B, Mesh::R},
    /*TR*/ {Mesh::T, Mesh::R},
    /*TL*/ {Mesh::T, Mesh::L},
    /*UB*/ {Mesh::U, Mesh::B},
    /*UR*/ {Mesh::U, Mesh::R},
    /*UT*/ {Mesh::U, Mesh::T},
    /*UL*/ {Mesh::U, Mesh::L},
    /*DB*/ {Mesh::D, Mesh::B},
    /*DR*/ {Mesh::D, Mesh::R},
    /*DT*/ {Mesh::D, Mesh::T},
    /*DL*/ {Mesh::D, Mesh::L}
  };
  return EIDS[eid];
}

fd::Point PDE::vertex_point(int vid) const noexcept
{
  static const int C[8][3]{
      /*DBL*/ {0, 2, 4},
      /*DBR*/ {1, 2, 4},
      /*DTR*/ {1, 3, 4},
      /*DTL*/ {0, 3, 4},
      /*UBL*/ {0, 2, 5},
      /*UBR*/ {1, 2, 5},
      /*UTR*/ {1, 3, 5},
      /*UTL*/ {0, 3, 5}};
  const Mesh &mesh = *mesh_;
  return {mesh[C[vid][0]], mesh[C[vid][1]], mesh[C[vid][2]]};
}

/*static*/
const int *PDE::get_edge_fids(int eid) noexcept
{ return sGetEdgeFids(eid); }

/*static*/
const int *PDE::get_vertex_fids(int vid) noexcept
{ return sGetVertexFids(vid).data(); }

fd::VertexParts PDE::get_vertex_parts(int vid, int bid) const
{
  const Mesh &mesh = *mesh_;
  const auto &r = sGetVertexFids(vid);
  const int this_fid = mesh.get_sid(bid);

  // The supplied bid contains the vertex.
  ASSERT_MSG(std::find(r.cbegin(), r.cend(), this_fid) != r.cend() &&
      mesh.get_rectangle(mesh.boundary(bid)).contains(vertex_point(vid).
          project(static_cast<Axis>(mesh.boundary(bid)->type)),
          static_cast<Axis>(mesh.boundary(bid)->type)),
      __PRETTY_FUNCTION__ + _S": assertion failed: boundary "+
      fd::to_string(bid)+" does not contain vertex "+Mesh::vertex(vid)+".");

  vector<int> vertex_fids;
  for (const int i : r) if (i != this_fid) vertex_fids.push_back(i);
  THROW_IF(vertex_fids.size() != 2,
      "Internal error 0006. Please report this error.");

  return {bid, {this_fid, vertex_fids[0], vertex_fids[1]}};
}

array<int,3> PDE::get_vertex_bids(int vid, int bid) const
{
  const Mesh &mesh = *mesh_;
  const auto &r = sGetVertexFids(vid);
  const int this_fid = mesh.get_sid(bid);
  array<int,3> vertex_bids{bid};

  // The supplied bid contains the vertex.
  ASSERT_MSG(std::find(r.cbegin(), r.cend(), this_fid) != r.cend() &&
      mesh.get_rectangle(mesh.boundary(bid)).contains_at(
          mesh.boundary(bid)->pos(), vertex_point(vid),
          static_cast<Axis>(mesh.boundary(bid)->type)),
      __PRETTY_FUNCTION__ + _S": assertion failed: boundary "+
      fd::to_string(bid)+" does not contain vertex "+Mesh::vertex(vid)+".");

  vector<int> vertex_fids;
  for (const int i : r) if (i != this_fid) vertex_fids.push_back(i);
  THROW_IF(vertex_fids.size() != 2,
      "Internal error 0018. Please report this error.");

  // We start from the next bid because (1) bid is known to contain
  // the vertex, and (2) if i < bid and contains the vertex then the
  // vertex would have been taken previously. 
  int cur = 1;
  for (int i = bid + 1; i < mesh.num_boundaries() && !vertex_fids.empty(); ++i)
  {
    const int index = fd::find(vertex_fids, mesh.get_sid(i));
    if (index == -1) continue;
    const auto p = mesh.boundary(i);
    const Axis n = static_cast<Axis>(p->type);
    if (!mesh.get_rectangle(p).contains_at(p->pos(), vertex_point(vid), n))
      continue;
    vertex_bids[cur++] = i;
    std::swap(vertex_fids[index], vertex_fids.back());
    vertex_fids.pop_back();
  }

  // Exactly 3 boundaries must have been found because we know in advance
  // that bid is one of them and none of its predecessors contains the vertex.
  THROW_IF(cur != 3, "Internal error 0019. Please report this error.");

  return vertex_bids;
}

array<int,3> PDE::get_vertex_bids(int vid) const
{
  const Mesh &mesh = *mesh_;
  const auto &r = sGetVertexFids(vid);
  const int nb = mesh.num_boundaries();
  int bid = 0;

  while (bid < nb)
  {
    if (std::find(r.cbegin(), r.cend(), mesh.get_sid(bid)) != r.cend())
    {
      const auto p = mesh.boundary(bid);
      const Axis n = static_cast<Axis>(p->type);
      if (mesh.get_rectangle(p).contains_at(p->pos(), vertex_point(vid), n))
        break;
    }
    ++bid;
  }

  THROW_IF(bid == nb, "Internal error 0022. Please report this error.");

  return get_vertex_bids(vid, bid);
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::  Generate Function Body  ::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

//::::::::::::::::::::::::  Boundary Code Generators  :::::::::::::::::::::::://

void PDE::generate_code_vertex(int what, int vid, int bid, int ntabs)
{
  const auto vp = get_vertex_parts(vid, bid);
  for (int c = 0; c < components(); ++c)
    generate_code_for(c, vp, what, ntabs);
}

void PDE::generate_code_edge(int what, int eid, int bid, int fid2, int ntabs)
{
  for (int c = 0; c < components(); ++c)
    generate_code_for(c, bid, fid2, what, ntabs);
};

void PDE::generate_code_face(int what, int bid, int ntabs)
{
  for (int c = 0; c < components(); ++c)
    generate_code_for(c, bid, what, ntabs);
}

//::::::::::::::::::::::::::  Main Code Generator  ::::::::::::::::::::::::::://

void PDE::generate_code_function_body(int what)
{
  const Mesh &mesh = *mesh_;
  const int nc = components();
  const bool collect_bs_info = boundary_sides_.empty();

  code_ << R"(
{
	const int il = nx-1, jl = ny-1, kl = nz-1;
	for (int i = 0; i < nx; ++i)
	{
		for (int j = 0; j < ny; ++j)
		{
			for (int k = 0; k < nz; ++k)
			{)";

  if (!generate_code_function_body_replace_eqs(what, 3)) code_ << R"(
				)";

  //::::::::::::::::::::  Generate code for the interior  :::::::::::::::::::://

  code_ << R"(if (i > 0 && j > 0 && k > 0 && i < il && j < jl && k < kl)
				{)";

  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
					// Field component )" << c;
    const string findex = fd::to_string_empty_if(nc == 1, c);
    const string eqno = format_elem_lhs(findex);
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
					F[)" << eqno << "] = "<< equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
					J[)" << eqno << "] = "<< equation_codes_[c].text_j << ";";
    else code_ << R"(
					F[)" << eqno << "] = "<< equation_codes_[c].text_y << R"(;
					J[)" << eqno << "] = "<< equation_codes_[c].text_j << ";";
  }

  code_ << R"(
				})";

  //:::::::::::::::::::::  Generate code for boundaries  ::::::::::::::::::::://

  vector<int> vertices_handled, edges_handled;
  array<vector<fd::Interval>,12> edge_intervals_handled;

  for (int bid = 0; bid < mesh.num_boundaries(); ++bid)
  {
    //:::::::::::::::::::::  Generate code for vertices  ::::::::::::::::::::://

    const vector<int> domain_vertices = mesh.get_domain_vertices(bid);
    for (const auto vid : domain_vertices)
    {
      if (fd::find(vertices_handled, vid)) continue;
      code_ << R"(
				else if (is_vertex()"<<vid<<R"(,i,j,k))
				{)";
      generate_code_vertex(what, vid, bid, 3);
      code_ << R"(
				})";
      vertices_handled.push_back(vid);
    }

    //::::::::::::::::::::::  Generate code for edges  ::::::::::::::::::::::://

    const vector<int> domain_edges = mesh.get_domain_edges(bid);
    for (const auto eid : domain_edges)
    {
      if (fd::find(edges_handled, eid)) continue;

      const int fid2 = mesh.get_meeting_face(bid, eid);
      Axis axis;
      fd::Interval side;
      std::tie(side, axis) = mesh.get_boundary_side(bid, fid2);

      THROW_IF(side.empty(axis), "Incorrect side for boundary "+
                                 fd::to_string(bid)+": "+fd::to_string(fid2));

      edge_intervals_handled[eid].push_back(side);
      fd::Interval::merge(edge_intervals_handled[eid], axis);
      if (edge_intervals_handled[eid].size() == 1)
      {
        Axis i, ii;
        fd::Interval domain_side, ds;
        // TODO: remove next statement after testing
        std::tie(ds, ii) = mesh.get_face_side(mesh.get_sid(bid), fid2);
        std::tie(domain_side, i) = mesh.edge_interval(eid);

        // TODO: remove next statement after testing
        ASSERT(ii == i && ds.coincides(domain_side, i));
        ASSERT(i == axis);

        if (edge_intervals_handled[eid][0].coincides(domain_side, i))
          edges_handled.push_back(eid);
      }

      int bsindex;
      if (collect_bs_info)
      {
        bsindex = (int) boundary_sides_.size();
        boundary_sides_.push_back({bid,eid,axis,side});
      }
      else
      {
        bsindex = fd::index(boundary_sides_, {bid,eid,axis,side});
        THROW_IF(bsindex == -1,"Internal error 0003. Please report this error.");
      }
    
      code_ << R"(
				else if (in_boundary_edge()"<<bsindex<<R"(,i,j,k))
				{)";
      generate_code_edge(what, eid, bid, fid2, 3);
      code_ << R"(
				})";
    }

    //::::::::::::::::::  Generate code for face interior  ::::::::::::::::::://

    code_ << R"(
				else if (in_boundary()"<<bid<<R"(,i,j,k))
				{)";
    generate_code_face(what, bid, 3);
    code_ << R"(
				})";
  }

  // Handle global boundary
  if (mesh.has_global_boundary())
    generate_code_function_body_global_boundary(what, 3, [this, what](int fid,
        int ntabs) {
      for (int c = 0; c < components(); ++c)
        generate_code_for_global_boundary(c, fid, what, ntabs);
    });

  // Finish ijk for-loop:
  code_ << R"(
			}
		}
	})";

#if 0
  // Generate code for virtual corners -- active for SYM_DIFF2 code generation
  if (boundary_discretization_method == SYM_DIFF2 && has_neumann_BCs())
    generate_code_virtual_points(what);
#endif // 0

  generate_code_function_body_replace_eqs_ns(what);

  //::::::::::::::::::  Generate code for virtual corners  ::::::::::::::::::://
  generate_code_virtual_points(what);

  code_ << R"(
}
)";
}

void PDE::generate_code_function_body_global_boundary(int what, int ntabs,
    std::function<void(int,int)> gencode)
{
  const Mesh &mesh = *mesh_;
  const string tabs(ntabs, '\t');
  constexpr int DIRICHLET_BC = BoundaryCondition::DIRICHLET;

  if (boundary_has_all_BCs_type(&mesh.get_global_boundary(), DIRICHLET_BC))
  {
    code_ <<R"(
	)"<<tabs<<R"(else // Global Boundary
	)"<<tabs<<R"({)";
    gencode(-1, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(})";
  }
  else
  {
    code_ <<R"(
	)"<<tabs<<R"(else if (i == 0) // Global Boundary Begin
	)"<<tabs<<R"({)";
    gencode(Mesh::L, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(}
	)"<<tabs<<R"(else if (i == il)
	)"<<tabs<<R"({)";
    gencode(Mesh::R, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(}
	)"<<tabs<<R"(else if (j == 0)
	)"<<tabs<<R"({)";
    gencode(Mesh::B, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(}
	)"<<tabs<<R"(else if (j == jl)
	)"<<tabs<<R"({)";
    gencode(Mesh::T, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(}
	)"<<tabs<<R"(else if (k == 0)
	)"<<tabs<<R"({)";
    gencode(Mesh::D, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(}
	)"<<tabs<<R"(else if (k == kl)
	)"<<tabs<<R"({)";
    gencode(Mesh::U, ntabs);
    code_ <<R"(
	)"<<tabs<<R"(} // Global Boundary End)";
  }
}

void PDE::initialize_code_generator()
{
  THROW_IF(!boundary_sides_.empty(), "Improper use of method.");
}

void PDE::terminate_code_generator()
{
  boundary_sides_.clear();
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::  Class (Implementation) Generator  :::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDE::generate_code_class_extra()
{
  generate_code_class_set_bsi();
  generate_code_class_set_bri();
}

//:::::::::::::::::::::::::::  Βoundary Side Info  ::::::::::::::::::::::::::://

void PDE::generate_code_class_set_bsi() // bsi = Βoundary Side Info
{
  code_ << R"(
void )"<<name<<R"(::set_bside_info()
{
	const int EDGE_IVAL[12][3][2] {
		{{-1,   1}, {-1,   1}, {-3,  -3}},
		{{nx-2,nx}, {-1,   1}, {-3,  -3}},
		{{nx-2,nx}, {ny-2,ny}, {-3,  -3}},
		{{-1,   1}, {ny-2,ny}, {-3,  -3}},

		{{-3,  -3}, {-1,   1}, {nz-2,nz}},
		{{nx-2,nx}, {-3,  -3}, {nz-2,nz}},
		{{-3,  -3}, {ny-2,ny}, {nz-2,nz}},
		{{-1,   1}, {-3,  -3}, {nz-2,nz}},

		{{-3,  -3}, {-1,   1}, {-1,   1}},
		{{nx-2,nx}, {-3,  -3}, {-1,   1}},
		{{-3,  -3}, {ny-2,ny}, {-1,   1}},
		{{-1,   1}, {-3,  -3}, {-1,   1}},
	};

	int edge_ival[3][2];

	// Set boundary side information
	int count = 0;
	for (const auto &side_info : edge_info_)
	{  
		const auto t = side_info.axis;

		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 2; ++j)
				edge_ival[i][j] = EDGE_IVAL[side_info.eid][i][j];

		// Consistency check
		for (int i = 0; i < 3; ++i)
			if (edge_ival[i][0] == -3 && edge_ival[i][1] == -3) // to be filled in
			{
				if (t != i)
					throw std::string("Internal error, please report this error.");
				break;
			}

		// See Fdmeng Notes, NOTE 1
		const double hh = t == 0 ? hx : (t == 1 ? hy : hz);
		const double orig = t == 0 ? a : (t == 1 ? c : p);
		const double beta = (side_info.b - orig + RESOL[t])/hh;
		const int i1 = floor(beta);
		edge_ival[t][0] = floor((side_info.a - orig - RESOL[t])/hh);
		edge_ival[t][1] = beta <= i1 ? i1 : i1 + 1;

		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 2; ++j)
				bside_[count][i][j] = edge_ival[i][j];

		++count;
	}
}
)";
}

//::::::::::::::::::::::::   Βoundary Rectangle Info   ::::::::::::::::::::::://

void PDE::generate_code_class_set_bri() // bri = Βoundary Rectangle Info
{
  code_ << R"(
void )"<<name<<R"(::set_brect_info()
{
	for (int bid = 0; bid < int(sizeof(bside_info_)/sizeof(bside_info_[0])); ++bid)
	{
		const auto &bsi = bside_info_[bid];

		int face_ival[3][2] {{-3,-3},{-3,-3},{-3,-3}}; // to be filled in
		switch (bsi[0].eid /*  this is actually the fid  */) {
			case 0: face_ival[0][0] = nx-2; face_ival[0][1] = nx; break;
			case 1: face_ival[1][0] = ny-2; face_ival[1][1] = ny; break;
			case 2: face_ival[0][0] =   -1; face_ival[0][1] =  1; break;
			case 3: face_ival[1][0] =   -1; face_ival[1][1] =  1; break;
			case 4: face_ival[2][0] = nz-2; face_ival[2][1] = nz; break;
			case 5: face_ival[2][0] =   -1; face_ival[2][1] =  1; break;
		}

		// Consistency check
		int n_to_fillin = 0;
		int to_fillin[3];
		for (int i = 0; i < 3; ++i)
			if (face_ival[i][0] == -3 && face_ival[i][1] == -3) // to fill in
				to_fillin[n_to_fillin++] = i;
		if (n_to_fillin != 2 ||
				(bsi[0].axis != to_fillin[0] && bsi[0].axis != to_fillin[1]) ||
				(bsi[1].axis != to_fillin[0] && bsi[1].axis != to_fillin[1]))
			throw std::string("Internal error, please report this error.");

		for (int n = 0; n < 2; ++n)
		{
			const auto t = bsi[n].axis;
			// See FDMeng Notes, NOTE 1
			const double hh = t == 0 ? hx : (t == 1 ? hy : hz);
			const double orig = t == 0 ? a : (t == 1 ? c : p);
			const double beta = (bsi[n].b - orig + RESOL[t])/hh;
			const int i1 = floor(beta);
			face_ival[t][0] = floor((bsi[n].a - orig - RESOL[t])/hh);
			face_ival[t][1] = beta <= i1 ? i1 : i1 + 1;
		}

		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 2; ++j)
				brect_[bid][i][j] = face_ival[i][j];
	}
}
)";
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::  Main File Generator  :::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

string PDE::format_constructor_comment() const
{
  return "// Discretize PDE " + _S(components() == 1 ? "" : "system ") +
	"on a "+fd::to_string(mesh_->nx)+" x "+fd::to_string(mesh_->ny)+" x "
           +fd::to_string(mesh_->nz)+R"( grid.
	// Replace default constructor with pde(m,n,k) for a m x n x k grid.)";
}

string PDE::format_solution_output_node(int c) const
{ return "i,j,k" + (components() == 1 ? _S"" : ","+fd::to_string(c)); }

string PDE::format_solution_output_coord(int c) const
{ return (components() == 1 ? _S"" : fd::to_string(c)+",")+"pde.X(i),pde.Y(j),pde.Z(k)"; }

string PDE::format_coord() const
{ return _S(components() == 1 ? "" : "c,")+"pde.X(i),pde.Y(j),pde.Z(k)"; }

//----------------------------------------------------------------------------//
//::::::::::::::::::::::  Header (Interface) Generator  :::::::::::::::::::::://
//----------------------------------------------------------------------------//

// New code for 3-d
void PDE::generate_header_constructors()
{
	code_h_ << R"(

	//! Default constructor.
	)"<<name<<R"(() :
		nx()" << mesh_->nx << "), ny(" << mesh_->ny << "), nz(" << mesh_->nz << R"(),
		a()"  << mesh_->a  << "), b("  << mesh_->b  << R"(),
		c()"  << mesh_->c  << "), d("  << mesh_->d  << R"(),
		p()"  << mesh_->e  << "), q("  << mesh_->f  << R"(),
		output_results(true) {init();}
	//! Construct mesh with specified number of nodes.
	)"<<name<<R"((int Nx, int Ny, int Nz) :
		nx(Nx), ny(Ny), nz(Nz),
		a()" << mesh_->a << "), b(" << mesh_->b << R"(),
		c()" << mesh_->c << "), d(" << mesh_->d << R"(),
		p()" << mesh_->e << "), q(" << mesh_->f << R"(),
		output_results(true) {init();}
	//! Construct mesh with specified number of nodes and solution domain.
	)"<<name<<R"((int Nx, int Ny, int Nz,
			double x0, double x1, double y0, double y1, double z0, double z1) :
		nx(Nx), ny(Ny), nz(Nz),
		a(x0), b(x1), c(y0), d(y1), p(z0), q(z1),
		output_results(true) {init();}
)";
}

static const char BNAME[] {'?','x','y','z'};

void PDE::generate_header_extra_idefs()
{
  code_h_ << R"(
	struct BoundarySideInfo
	{
		int bid;     // Boundary id
		int eid;     // Edge (or face) id
		double a, b; // Endpoints of interval
		int axis;    // Coordinate axis parallel to interval
	};
)";
}

void PDE::generate_header_init_extra()
{
  // Domain Vertex Info
  code_h_ << R"(

		// Domain Vertices
		vertex_[0][0] =   0 ; vertex_[0][1] =   0 ; vertex_[0][2] =  0;
		vertex_[1][0] = nx-1; vertex_[1][1] =   0 ; vertex_[1][2] =  0;
		vertex_[2][0] = nx-1; vertex_[2][1] = ny-1; vertex_[2][2] =  0;
		vertex_[3][0] =   0 ; vertex_[3][1] = ny-1; vertex_[3][2] =  0;
		vertex_[4][0] =   0 ; vertex_[4][1] =   0 ; vertex_[4][2] = nz-1;
		vertex_[5][0] = nx-1; vertex_[5][1] =   0 ; vertex_[5][2] = nz-1;
		vertex_[6][0] = nx-1; vertex_[6][1] = ny-1; vertex_[6][2] = nz-1;
		vertex_[7][0] =   0 ; vertex_[7][1] = ny-1; vertex_[7][2] = nz-1;)";

  // Βoundary Side Info
  code_h_ << R"(

		// Set Boundary Side Information
		set_bside_info();)";

  // Βoundary Rectangle Info
  code_h_ << R"(

		// Set Boundary Rectangle Information
		set_brect_info();)";
}

array<PDE::BoundarySideInfo,2> PDE::get_boundary_side_info(int bid) const
{
  const Mesh &mesh = *mesh_;
  const int fid = get_sid(bid);
  const Axis n = static_cast<Axis>(Mesh::face_btype(fid));
  const fd::Rectangle &r = mesh.get_rectangle(bid);

  array<BoundarySideInfo,2> result;
  result[0].bid = result[1].bid = bid;
  result[0].eid = result[1].eid = fid;
  result[0].axis = plane_coordinate_axis(n,0);
  result[0].interval = {r.a,r.b};
  result[1].axis = plane_coordinate_axis(n,1);
  result[1].interval = {r.c,r.d};

  return result;
}

void PDE::generate_header_extra()
{
  const Mesh &mesh = *mesh_;
  const int sz = mesh.num_boundaries();

  const char sep = fd::Geometry::set_io_separator(',');

  code_h_ << R"(

protected:

	// Set Boundary Side Information
	void set_bside_info();

	// Set Boundary Rectangle Information
	void set_brect_info();

	bool in_boundary(int bid, int i, int j, int k) const noexcept
	{
		const int (&r)[3][2] = brect_[bid];
		return r[0][0] < i && i < r[0][1] &&
		       r[1][0] < j && j < r[1][1] &&
		       r[2][0] < k && k < r[2][1];
	}

	bool in_boundary_edge(int bsindex, int i, int j, int k) const noexcept
	{
		const int (&r)[3][2] = bside_[bsindex];
		return r[0][0] < i && i < r[0][1] &&
		       r[1][0] < j && j < r[1][1] &&
		       r[2][0] < k && k < r[2][1];
	}
    
	bool is_vertex(int vid, int i, int j, int k) const noexcept
	{ return i == vertex_[vid][0] && j == vertex_[vid][1] && k == vertex_[vid][2]; })";

  code_h_ << R"(

	const double RESOL[3] {)"<<RESOL(0)<<","<<RESOL(1)<<","<<RESOL(2)<<","<<"};";

  code_h_ << R"(

	int brect_[)"<<sz<<"][3][2];";

  code_h_ << R"(
	int bside_[)"<<boundary_sides_.size()<<"][3][2];";

  code_h_ << R"(
	int vertex_[8][3];)";

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::::  Βoundary Side Info  :::::::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  code_h_ << R"(
	BoundarySideInfo edge_info_[)"<<boundary_sides_.size()<<"] {";
  for (const auto &bsi : boundary_sides_)
  {
    code_h_ << R"(
		// Side of )" << BNAME[btype(bsi.bid)+1]
            << "-boundary #"  << bsi.bid << " on edge " 
            << Mesh::edge(bsi.eid)
            << ", interval (" << bsi.interval << R"():
		{)" <<bsi.bid<<","<<bsi.eid<<","<<bsi.interval<<","<<bsi.axis<<"},";
  }
  code_h_ << R"(
	};)";

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::  Βoundary Rectangle Info  ::::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  code_h_ << R"(
	BoundarySideInfo bside_info_[)"<<sz<<"][2] {";
  for (const int bid : fd::Range<int>(sz))
  {
    const auto bsi = get_boundary_side_info(bid);
    const int fid = bsi[0].eid; // This is actually the fid

    code_h_ << R"(
		// )" << BNAME[btype(bid)+1]
            << "-boundary #"  << bid << " on face " 
            << Mesh::face(fid)
            << ", rectangle [" << mesh.get_rectangle(bid) << R"(]:
		{{)"<<bid<<","<<fid<<","<<bsi[0].interval<<","<<bsi[0].axis<<"},{"
            <<bid<<","<<fid<<","<<bsi[1].interval<<","<<bsi[1].axis<<"}},";
  }
  code_h_ << R"(
	};)";

  fd::Geometry::set_io_separator(sep);

}

} // namespace fd3
