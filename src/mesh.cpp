#include <cstdlib>
#include <cmath>
#include <iostream>
#include "mesh.hpp"
#include "global_config.h"

using std::string;
using std::vector;
using std::array;

namespace fd {

//============================================================================//
//:::::::::::::::::::::::::::::::  MeshCommon  ::::::::::::::::::::::::::::::://
//============================================================================//

Boundary *MeshCommon::get_boundary(int bid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  THROW_IF(bid < 0 || bid >= (int)boundaries_.size(),
      "Boundary index is out of range.");
  return boundaries_[bid];
}

Boundary *MeshCommon::boundary(int bid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  return boundaries_[bid];
}

int MeshCommon::get_bid(Boundary *p) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  return index_of(p);
}

int MeshCommon::num_boundaries() const
{
  const int sz = (int)boundaries_.size();
  return has_global_boundary() ? sz - 1 : sz;
}

bool MeshCommon::has_global_boundary() const
{
  if (boundaries_.empty()) return false;
  return boundaries_.back()->type == Boundary::G;
}

const Boundary& MeshCommon::get_global_boundary() const
{
  THROW_IF(!has_global_boundary(), "Mesh has no global boundary.");
  return global_boundary_;
}

int MeshCommon::get_global_bid() const
{
  return has_global_boundary() ? (int)boundaries_.size() - 1 : -1;
}

BoundaryCondition *MeshCommon::get_BC_for_component(
		const vector<BoundaryCondition*> &bcs, int component, int bid) const
{
  Boundary *bd = get_boundary(bid);
  THROW_IF(bd == nullptr, "Null boundary for boundary id "+fd::to_string(bid));
  for (BoundaryCondition *bc : bcs)
    if (bc->boundary() == bd && bc->component() == component) return bc;
  return nullptr;
}

void MeshCommon::print_boundaries() const
{
  for (const auto p : boundaries_)
  { p->print(); std::cout << std::endl; }
}

} // namespace fd
