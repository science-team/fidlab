#include "field.hpp"
#include "expressions.hpp"

using std::string;

namespace fd {

// =============================================================================
// Field
// =============================================================================

IdGenerator Field::idgen_(1, false);
std::map<int, string> Field::global_field_table_;

string Field::format_name(bool include_id) const
{
	string r = name;
	if (name.empty())
	{
		r = "noname";
		include_id = true;
	}
	if (include_id)
		r += " (id " + fd::to_string(id) + ")";
	return r;
}

string Field::get_name() const
{
	return name.empty() ? ("#"+fd::to_string(id)) : name;
}

void Field::register_field() const
{
	const string field_name = get_name();
	for (auto& entry: global_field_table_)
		if (entry.second == field_name)
			throw "A field with the name " + field_name + " already exists.";
	auto result = global_field_table_.insert({id, field_name});
	if (!result.second)
		throw _S"Internal error 0011. Field " + format_name(true) +
			" already exists. Please report this error.";
}

string Field::find_global_id(int id)
{
	auto it = global_field_table_.find(id);
	return it == global_field_table_.end() ? _S"unregistered(!)" : it->second;
}

// Convert Field to Expression
#if !HAVE_STANDALONE_FIELDS
Field::operator Expression&() const
{	return Id[*this]; }
#endif // HAVE_STANDALONE_FIELDS

Expression *Field::create_expression() const
{
	// Expression& r = Id[*this];
	// return &r;
	return &Id[*this];
}

// -----------------------------------------------------------------------------
// Parse unary expressions
// -----------------------------------------------------------------------------
Expression& Field::generic_operator(const char op) const
{
	Constant *c = new Constant(0.);
	DEBUG_LOG("%s\t\t%c\n", name.c_str(), op);
	return *new Expression(new Expression(c), create_expression(), op);
}

Expression& Field::operator-() const
{	return generic_operator('-'); }

Expression& Field::operator+() const
{	return generic_operator('+'); }

// -----------------------------------------------------------------------------
// Parse binary expressions
// -----------------------------------------------------------------------------
Expression& Field::generic_operator(double a, const char op) const
{
	return generic_operator(*new Constant(a), op);
}

Expression& Field::generic_operator(const ExpressionItem& b, const char op) const
{	return generic_operator(*(new Expression(b.duplicate())), op); }
// {	return generic_operator(*(new Expression(&b)), op); }

Expression& Field::generic_operator(const Expression& b, const char op) const
{
	DEBUG_LOG("%s\t%s\t%c\n", name.c_str(), b.name().c_str(), op);
	return *new Expression(create_expression(), &b, op);
}

Expression& Field::generic_operator(const Field& b, const char op) const
{
	DEBUG_LOG("%s\t%s\t%c\n", name.c_str(), b.name.c_str(), op);
	return *new Expression(create_expression(), b.create_expression(), op);
}

Expression& Field::operator+(double a) const
{	return generic_operator(a, '+'); }

Expression& Field::operator+(const Field& b) const
{	return generic_operator(b, '+'); }

Expression& Field::operator+(const Function& b) const
{	return generic_operator(b, '+'); }

Expression& Field::operator+(const Expression& b) const
{	return generic_operator(b, '+'); }

Expression& Field::operator-(double a) const
{	return generic_operator(a, '-'); }

Expression& Field::operator-(const Field& b) const
{	return generic_operator(b, '-'); }

Expression& Field::operator-(const Function& b) const
{	return generic_operator(b, '-'); }

Expression& Field::operator-(const Expression& b) const
{	return generic_operator(b, '-'); }

Expression& Field::operator*(double a) const
{	return generic_operator(a, '*'); }

Expression& Field::operator*(const Field& b) const
{	return generic_operator(b, '*'); }

Expression& Field::operator*(const Function& b) const
{	return generic_operator(b, '*'); }

Expression& Field::operator*(const Expression& b) const
{	return generic_operator(b, '*'); }

Expression& Field::operator/(double a) const
{	return generic_operator(a, '/'); }

Expression& Field::operator/(const Field& b) const
{	return generic_operator(b, '/'); }

Expression& Field::operator/(const Function& b) const
{	return generic_operator(b, '/'); }

Expression& Field::operator/(const Expression& b) const
{	return generic_operator(b, '/'); }

#if HAVE_STANDALONE_FIELDS
static Expression& generic_operator(double a, const Field &f, const char op)
{
	DEBUG_LOG("%g\t%s\t%c\n", a, f.name.c_str(), op);
	Constant *c = new Constant(a);
	return *new Expression(new Expression(c), f.create_expression(), op);
}

Expression& operator+(double a, const Field &e)
{	return generic_operator(a, e, '+'); }

Expression& operator-(double a, const Field &e)
{	return generic_operator(a, e, '-'); }

Expression& operator*(double a, const Field &e)
{	return generic_operator(a, e, '*'); }

Expression& operator/(double a, const Field &e)
{	return generic_operator(a, e, '/'); }
#endif // HAVE_STANDALONE_FIELDS

// =============================================================================
// VectorField
// =============================================================================

void VectorField::set_component(const Field &f)
{
	DEBUG_LOG("Note: inserting field #%lu: %s.\n", size(), f.format_name().c_str());
	if (find_component(f))
		throw "The field " + f.format_name() + "is already in the PDE.";
	push_back(f);
}

int VectorField::get_component_index(const Field &fc) const
{
	return get_component_index(fc.id);
}

int VectorField::get_component_index(int cid) const
{
	for (int i = 0; i < (int) size(); ++i)
		if ((*this)[i].id == cid) return i;
	return -1;
}

int VectorField::get_component(int index) const
{
	THROW_IF(index < 0 || index >= (int)size(), "Invalid field component index.");
	return (*this)[index].id;
}

bool VectorField::find_component(const Field &fc) const
{
	return std::find(begin(), end(), fc) != end();
}

int VectorField::get_component_subindex(const Field &fc) const
{
	int i = -1;
	std::find_if(begin(), end(), [&i, fc](const Field& cur) {
		if (cur.name == fc.name) ++i;
		return fc.id == cur.id;
	});
	return i;
}

} // namespace fd
