#include <cstdlib>
#include <cmath>
#include <ctime>
#include "pde3d.hpp"

using std::string;
using std::vector;
using std::array;
using fd::Boundary;

namespace fd3 {

//============================================================================//
//::::::::::::::::::::::::::::::::::  PDE  ::::::::::::::::::::::::::::::::::://
//============================================================================//

static auto format_element = [](const string&, const string&, const string&,
    const string&) {return string("eq");};

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::  Code Generators  :::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDE::generate_code_function_body_petsc_nonlinear(int what)
{
  if (what != GENERATE_FUNCTION_CODE && what != GENERATE_JACOBIAN_CODE)
    THROW("Incorrect use of method.");

  enum BType {VERTEX, EDGE, FACE};

  const Mesh &mesh = *mesh_;
  const int nc = components();
  const string rv(what == GENERATE_FUNCTION_CODE ? "F" : "J");
  const bool collect_bs_info = boundary_sides_.empty();

  // ---------------------------------------------------------------------------
  auto generate_code_vertex_face = [this, what, nc](int vid, int bid,
                                                    BType bt, int ntabs) {
    ASSERT(bt == VERTEX || bt == FACE);

    fd::VertexParts vps;
    
    if (bt == VERTEX) vps = get_vertex_parts(vid, bid);
    if (nc > 1) ntabs += 2;

    const string tabs(ntabs, '\t');

    if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
	)"<<tabs<<R"(case )"<<c<<":";
      if (bt == VERTEX)
        generate_code_for(c, vps, what, ntabs, format_element);
      else
        generate_code_for(c, bid, what, ntabs, format_element);
      if (nc > 1 && c < nc-1) code_ << R"(
		)"<<tabs<<R"(break;)";
	}
	if (nc > 1) code_ << R"(
			})";
  }; //-------------------------------------------------------------------------
  auto generate_code_edge = [this, what, nc](int eid, int bid, int fid2,
                                                               int ntabs) {
    if (nc > 1) ntabs += 2;

    const string tabs(ntabs, '\t');

    if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
	)"<<tabs<<R"(case )"<<c<<":";
      generate_code_for(c, bid, fid2, what, ntabs);
      if (nc > 1 && c < nc-1) code_ << R"(
		)"<<tabs<<R"(break;)";
	}
	if (nc > 1) code_ << R"(
			})";
  }; //-------------------------------------------------------------------------

  //==========================================================================//
  //::::::::::::::::::::::::  Generate function body  :::::::::::::::::::::::://
  //==========================================================================//

  { //:::::::::::::::::::::::::::( Begin of Block):::::::::::::::::::::::::::://
    const string decl_c(nc == 1 ? "" : "const int c = ");
    const string rt(what == GENERATE_FUNCTION_CODE ?
	R"(// Instruct compiler to handle F[eq] as if it were a double
	struct {
		double value;
		inline operator double() const noexcept {return value;}
		inline double &operator[](int) noexcept {return value;}
		inline void operator=(double x) noexcept {value=x;}
	})" : "SparseVector");
		code_ << R"(
{
	)"<<rt<<" "<<rv<<R"(;
	// End (one beyond last) of internal nodes
	const int il = nx-1, jl = ny-1, kl = nz-1;
	// Get node corresponding to field index eq
	int mesh_coord[3];
	)"<<decl_c<<R"(field_index_to_mesh_coordinates(eq, mesh_coord);
	const int i = mesh_coord[0];
	const int j = mesh_coord[1];
	const int k = mesh_coord[2];)"; } //:::::::::::( End of Block )::::::::::://

  code_ << std::endl;

  if (!generate_code_function_body_replace_eqs(what, 0, "c", "eq", false))
    code_ << R"(
	)";

  //==========================================================================//
  //:::::::::::::::::::::::::                        ::::::::::::::::::::::::://
  //:::::::::::::::::::::::::   NEW IMPLEMENTATION   ::::::::::::::::::::::::://
  //:::::::::::::::::::::::::                        ::::::::::::::::::::::::://
  //==========================================================================//

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::  Generate code for the interior  :::::::::::::::::::://
  //--------------------------------------------------------------------------//

  code_ << R"(if (i > 0 && j > 0 && k > 0 && i < il && j < jl && k < kl)
	{)";
  if (nc > 1) code_ << R"(
		switch (c)
		{)";

  const string tabs(nc > 1 ? 2 : 0, '\t');

  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
			case )"<<c<<":";
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[eq] = "<< equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[eq] = "<< equation_codes_[c].text_j << ";";
    if (nc > 1 && c < nc-1) code_ << R"(
				break;)";
  }

  if (nc > 1) code_ << R"(
		})";
  code_ << R"(
	})";

  //--------------------------------------------------------------------------//
  //:::::::::::::::::::::  Generate code for boundaries  ::::::::::::::::::::://
  //--------------------------------------------------------------------------//

  vector<int> vertices_handled, edges_handled;
  array<vector<fd::Interval>,12> edge_intervals_handled;

  for (int bid = 0; bid < mesh.num_boundaries(); ++bid)
  {
    //------------------------------------------------------------------------//
    //:::::::::::::::::::::  Generate code for vertices  ::::::::::::::::::::://
    //------------------------------------------------------------------------//

    const vector<int> domain_vertices = mesh.get_domain_vertices(bid);
    for (const auto vid : domain_vertices)
    {
      if (fd::find(vertices_handled, vid)) continue;
      code_ << R"(
	else if (is_vertex()"<<vid<<R"(,i,j,k))
	{)";
      generate_code_vertex_face(vid, bid, VERTEX, 0);
      code_ << R"(
	})";
      vertices_handled.push_back(vid);
    }

    //------------------------------------------------------------------------//
    //::::::::::::::::::::::  Generate code for edges  ::::::::::::::::::::::://
    //------------------------------------------------------------------------//

    const vector<int> domain_edges = mesh.get_domain_edges(bid);
    for (const auto eid : domain_edges)
    {
      if (fd::find(edges_handled, eid)) continue;

      const int fid2 = mesh.get_meeting_face(bid, eid);
      fd::Axis axis;
      fd::Interval side;
      std::tie(side, axis) = mesh.get_boundary_side(bid, fid2);

      THROW_IF(side.empty(axis), "Incorrect side for boundary "+
                                 fd::to_string(bid)+": "+fd::to_string(fid2));

      edge_intervals_handled[eid].push_back(side);
      fd::Interval::merge(edge_intervals_handled[eid], axis);
      if (edge_intervals_handled[eid].size() == 1)
      {
        fd::Axis i;
        fd::Interval domain_side;
        std::tie(domain_side, i) = mesh.edge_interval(eid);

        ASSERT(i == axis);

        if (edge_intervals_handled[eid].front().coincides(domain_side, i))
          edges_handled.push_back(eid);
      }

      int bsindex;
      if (collect_bs_info)
      {
        bsindex = (int) boundary_sides_.size();
        boundary_sides_.push_back({bid,eid,axis,side});
      }
      else
      {
        bsindex = fd::index(boundary_sides_, {bid,eid,axis,side});
        THROW_IF(bsindex == -1,"Internal error 0004. Please report this error.");
      }
    
      code_ << R"(
	else if (in_boundary_edge()"<<bsindex<<R"(,i,j,k))
	{)";
      generate_code_edge(eid, bid, fid2, 0);
      code_ << R"(
	})";
    }

    //------------------------------------------------------------------------//
    //::::::::::::::::::  Generate code for face interior  ::::::::::::::::::://
    //------------------------------------------------------------------------//

    code_ << R"(
	else if (in_boundary()"<<bid<<R"(,i,j,k))
	{)";
    generate_code_vertex_face(-1, bid, FACE, 0);
    code_ << R"(
	})";
  }

  //::::::::::::::::::::::::  Handle global boundary  :::::::::::::::::::::::://

  if (mesh.has_global_boundary())
    generate_code_function_body_global_boundary(what, 0, //:::::  LAMBDA  :::://
        [this, what, nc](int fid, int ntabs) {
      if (nc > 1) ntabs += 2;
      const string tabs(ntabs, '\t');

      if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";
      for (int c = 0; c < nc; ++c)
      {
        if (nc > 1) code_ << R"(
	)"<<tabs<<R"(case )"<<c<<":";
        generate_code_for_global_boundary(c, fid, what, ntabs, format_element);
        if (nc > 1 && c < nc-1) code_ << R"(
		)"<<tabs<<R"(break;)";
	  }
	  if (nc > 1) code_ << R"(
			})";
    }/*::::::::::::::::::::::::::  END OF LAMBDA  ::::::::::::::::::::::::::*/);

  code_ << R"(
	return )"<<rv<<(what == GENERATE_FUNCTION_CODE ? ".value" : "[eq]")<<R"(;
}
)";
}

} // namespace fd3
