#include <cstdlib>
#include <cmath>
#include <ctime>
#include "pde2d.hpp"

using std::string;
using std::vector;
using fd::Boundary;

namespace fd2 {

// =============================================================================
// PDE
// =============================================================================

// -----------------------------------------------------------------------------
// Code Generators
// -----------------------------------------------------------------------------

void PDE::generate_code_function_body_petsc_nonlinear(int what)
{
  if (what != GENERATE_FUNCTION_CODE && what != GENERATE_JACOBIAN_CODE)
    THROW("Incorrect use of method.");

  enum BoundaryType {VERTEX, SIDE};

  const vector<vector<int>> bs = mesh_->get_bids_by_side();
  const std::array<int,4> boundaries_on_side{
      (int) bs[Mesh::R].size(),
      (int) bs[Mesh::T].size(),
      (int) bs[Mesh::L].size(),
      (int) bs[Mesh::B].size()};
  const auto bc_selection_table = get_bc_selection_table();
  const int nc = components();
  const string rv(what == GENERATE_FUNCTION_CODE ? "F" : "J");

  // ---------------------------------------------------------------------------
  // Generate function body
  // ---------------------------------------------------------------------------

  { const string decl_c(nc == 1 ? "" : "const int c = ");
    const string rt(what == GENERATE_FUNCTION_CODE ?
	R"(// Instruct compiler to handle F[eq] as if it were a double
	struct {
		double value;
		inline operator double() const noexcept {return value;}
		inline double &operator[](int) noexcept {return value;}
		inline void operator=(double x) noexcept {value=x;}
	})" : "SparseVector");
    code_ << R"(
{
	)"<<rt<<" "<<rv<<R"(;
	int mesh_coord[2];
	)"<<decl_c<<R"(field_index_to_mesh_coordinates(eq, mesh_coord);
	const int i = mesh_coord[0];
	const int j = mesh_coord[1];)"; } //***(End of block)***

  // ---------------------------------------------------------------------------
  auto generate_code_at_1 = [this, &bs, &bc_selection_table, what, nc]
                            (int bid_or_vertex, BoundaryType bt, int ntabs)
  {
    auto format_element = [](const string&,
        const string&, const string&, const string&) {return string("eq");};
    const auto& bid = bid_or_vertex;
    const auto& vertex = bid_or_vertex;
    const int flags = EXTRA_BC_CODE;

    if (nc > 1) ntabs += 2;
    const string tabs(ntabs, '\t');

    if (nc > 1) code_ << R"(
)"<<tabs<<R"(switch (c)
)"<<tabs<<R"({)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
	)"<<tabs<<R"(case )"<<c<<":";
      if (bt == VERTEX)
      {
        const auto& sides = bc_selection_table[c][vertex];
        const auto bids = get_vertex_bids(sides, vertex, bs);
        generate_code_for_OLD(c, bids.primary, bids.secondary, what, ntabs,
            flags, format_element);
      }
      else
      {
        if (boundary_discretization_method_ == SYM_DIFF2)
          generate_code_SYM_DIFF2_for(c, bid, what, ntabs, flags, format_element);
        else
          generate_code_for(c, bid, what, ntabs, format_element);
      }
      if (nc > 1 && c < nc-1) code_ << R"(
		)"<<tabs<<R"(break;)";
    }
    if (nc > 1) code_ << R"(
			})";
  }; //-------------------------------------------------------------------------
  auto generate_code_at = [this, &bs, generate_code_at_1](int sid_or_vertex,
                                                          BoundaryType bt,
                                                          int ntabs)
  {
    if (bt == VERTEX)
    { generate_code_at_1(sid_or_vertex, bt, ntabs); return; }
    const auto& sid = sid_or_vertex;
    const auto& bids = bs[sid];
    if (bids.size() == 1)
    { generate_code_at_1(bids[0], bt, ntabs); return; }
    const string tabs(ntabs+1, '\t');
    for (const int bid : bids)
    {
      const Boundary *bd = mesh_->boundary(bid);
      const bool direc = (sid == Mesh::R || sid == Mesh::B);
      code_ << "if (" << (bd->type == Boundary::X ? "Y(j) " : "X(i) ")
            << (direc ? '<' : '>') << " boundary_["
            << bid << "][" << int(direc) << "])";
      code_ << R"(
		{)";
      generate_code_at_1(bid, bt, ntabs);
      if (bid != bids.back()) code_ << R"(
		}
		else )";
    }
  }; //-------------------------------------------------------------------------

  code_ << std::endl;

  if (!generate_code_function_body_replace_eqs(what, 0, "c", "eq", false))
    code_ << R"(
	)";

  if (boundary_discretization_method_ == SYM_DIFF2 && has_neumann_BCs())
    generate_code_function_body_petsc_nonlinear_x(what);

  code_ << R"(if (i == 0)
	{
		if (j == 0)
		{)";
  generate_code_at(Mesh::BL, VERTEX, 1);
  code_ << R"(
		}
		else if (j == ny-1)
		{)";
  generate_code_at(Mesh::TL, VERTEX, 1);
  code_ << R"(
		}
		else)"<<(boundaries_on_side[Mesh::L] == 1 ? R"(
		{)" : " ");
  generate_code_at(Mesh::L, SIDE, 1);
  code_ << R"(
		}
	}
	else if (i == nx-1)
	{
		if (j == 0)
		{)";
  generate_code_at(Mesh::BR, VERTEX, 1);
  code_ << R"(
		}
		else if (j == ny-1)
		{)";
  generate_code_at(Mesh::TR, VERTEX, 1);
  code_ << R"(
		}
		else)"<<(boundaries_on_side[Mesh::R] == 1 ? R"(
		{)" : " ");
  generate_code_at(Mesh::R, SIDE, 1);
  code_ << R"(
		}
	}
	else if (j == 0)
	{)";
  if (boundaries_on_side[Mesh::B] > 1) code_ << R"(
		)";
  generate_code_at(Mesh::B, SIDE, 0+int(boundaries_on_side[Mesh::B] > 1));
  if (boundaries_on_side[Mesh::B] > 1) code_ << R"(
		})";
  code_ << R"(
	}
	else if (j == ny-1)
	{)";
  if (boundaries_on_side[Mesh::T] > 1) code_ << R"(
		)";
  generate_code_at(Mesh::T, SIDE, 0+int(boundaries_on_side[Mesh::T] > 1));
  if (boundaries_on_side[Mesh::T] > 1) code_ << R"(
		})";
  code_ << R"(
	}
	else
	{)";

  const string tabs(nc > 1 ? 2 : 0, '\t');
  if (nc > 1) code_ << R"(
		switch (c)
		{)";
  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
			case )"<<c<<":";
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)"<<tabs<<"F[eq] = "<< equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)"<<tabs<<"J[eq] = "<< equation_codes_[c].text_j << ";";
    if (nc > 1 && c < nc-1) code_ << R"(
				break;)";
  }
  if (nc > 1) code_ << R"(
		})";

  code_ << R"(
	})";

  code_ << R"(
	return )"<<rv<<(what == GENERATE_FUNCTION_CODE ? ".value" : "[eq]")<<R"(;
}
)";
}

void PDE::generate_code_function_body_petsc_nonlinear_x(int what)
{
  THROW_IF(!mesh_->simple(), "the SYM_DIFF2 boundary discretization method "
      "cannot be used in this context.");

  // ---------------------------------------------------------------------------
  // Generate code for extended boundary & virtual corners -- for SYM_DIFF2 only
  // ---------------------------------------------------------------------------

  const int nc = components();

  // ---------------------------------------------------------------------------
  auto generate_code_at = [this, what, nc](int bid, int ntabs)
  {
    auto format_element = [](const string&,
        const string&, const string&, const string&) {return string("eq");};
    const string ALIGN_BRACE_CINT{nc > 1 ? R"(
			{
				)" + string(ntabs, '\t') + "const int " : R"(
		)" + string(ntabs, '\t') + "const int "};

    if (nc > 1) ntabs += 2;

    if (nc > 1) code_ << R"(
			switch (c)
			{)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
				case )"<<c<<":";
      code_
          << ALIGN_BRACE_CINT
          << (bid == Mesh::L || bid == Mesh::R ? "iB = i" : "jB = j")
          << (bid == Mesh::T || bid == Mesh::R ? "-1; const int" : "+1; const int")
          << (bid == Mesh::L || bid == Mesh::R ? " i = iB;" : " j = jB;");

    //   generate_code_for(c, bid, what, ntabs, ORDINARY_BC_CODE, format_element);
      if (boundary_discretization_method_ == SYM_DIFF2)
        generate_code_SYM_DIFF2_for(c, bid, what, ntabs, ORDINARY_BC_CODE,
                                                         format_element);
      else
        generate_code_for(c, bid, what, ntabs, format_element);

      if (nc > 1) code_ << R"(
				})";
      if (nc > 1 && c < nc-1) code_ << R"(
					break;)";
    }
    if (nc > 1) code_ << R"(
			})";
  }; //-------------------------------------------------------------------------
  auto generate_code_at_sec = [this, what, nc](int vertex, int ntabs)
  {
    auto el = [](const string&,
      const string&, const string&, const string&) {return string("eq");};
    const int bid = (vertex==Mesh::BL || vertex==Mesh::TL) ? Mesh::L : Mesh::R;
    const string ALIGN_BRACE_CINT(nc > 1 ? R"(
				{
					const int )" : R"(
			const int )");

    if (nc > 1) ntabs += 2;

    if (nc > 1) code_ << R"(
			switch (c)
			{)";
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
				case )"<<c<<":";
      const auto bc_selection_table = get_bc_selection_table();
      const auto& sides = bc_selection_table[c][vertex];
      const int fl = sides.primary == bid ? ORDINARY_BC_CODE : SECONDARY_BC_CODE;
      if (sides.primary == bid) code_
        << ALIGN_BRACE_CINT
        << (bid == Mesh::L || bid == Mesh::R ? "iB = i" : "jB = j")
        << (bid == Mesh::T || bid == Mesh::R ? "-1; const int" : "+1; const int")
        << (bid == Mesh::L || bid == Mesh::R ? " i = iB;" : " j = jB;");

      generate_code_for_OLD(c, sides.primary, sides.secondary, what, ntabs, fl,
                            el);

      if (nc > 1 && c < nc-1) code_ << R"(
					break;)";
      if (sides.primary == bid && nc > 1) code_ << R"(
				})";
    }
    if (nc > 1) code_ << R"(
			})";
  }; //-------------------------------------------------------------------------
  auto generate_code_at_virtual_corner = [this, what]()
  {
    string F("???"), u("???");
    if (what == GENERATE_FUNCTION_CODE)      {F = "F"; u = "u"; }
    else if (what == GENERATE_JACOBIAN_CODE) {F = "J"; u = "du";}
    code_ << R"(
			)"<<F<<"[eq] = "<<u<<"[eq];";
  }; //-------------------------------------------------------------------------

  bool started = false;

  if (has_neumann_BC_on(Mesh::L))
  {
    started = true;
    code_ << R"(if (i == -1)
	{
		)";
    if (has_neumann_BC_on(Mesh::B) || has_neumann_BC_on(Mesh::T))
    {
      code_ << R"(if (j == -1 || j == ny)
		{)";
      generate_code_at_virtual_corner();
      code_ << R"(
		}
		else )";
    }
    code_ << R"(if (j == 0)
		{)";
    generate_code_at_sec(Mesh::BL, 1);
    code_ << R"(
		}
		else if (j == ny-1)
		{)";
    generate_code_at_sec(Mesh::TL, 1);
    code_ << R"(
		}
		else
		{)";
    generate_code_at(Mesh::L, 1);
    code_ << R"(
		}
	})";
  }
  if (has_neumann_BC_on(Mesh::R))
  {
    if (started) code_ << R"(
	else )";
    else started = true;
    code_ << R"(if (i == nx)
	{
		)";
    if (has_neumann_BC_on(Mesh::B) || has_neumann_BC_on(Mesh::T))
    {
      code_ << R"(if (j == -1 || j == ny)
		{)";
      generate_code_at_virtual_corner();
      code_ << R"(
		}
		else )";
    }
    code_ << R"(if (j == 0)
		{)";
    generate_code_at_sec(Mesh::BR, 1);
    code_ << R"(
		}
		else if (j == ny-1)
		{)";
    generate_code_at_sec(Mesh::TR, 1);
    code_ << R"(
		}
		else
		{)";
      generate_code_at(Mesh::R, 1);
      code_ << R"(
		}
	})";
  }
  if (has_neumann_BC_on(Mesh::B))
  {
    if (started) code_ << R"(
	else )";
    else started = true;
    code_ << R"(if (j == -1)
	{)";
    generate_code_at(Mesh::B, 0);
    code_ << R"(
	})";
  }
  if (has_neumann_BC_on(Mesh::T))
  {
    if (started) code_ << R"(
	else )";
    else started = true;
    code_ << R"(if (j == ny)
	{)";
    generate_code_at(Mesh::T, 0);
    code_ << R"(
	})";
  }
  if (started) code_ << R"(
	else )";
}

} // namespace fd2
