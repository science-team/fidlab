#include <cstdlib>
#include <cmath>
#include <vector>
#include "expressions.hpp"
#include "global_config.h"

using std::string;
using std::vector;

namespace fd
{

// -----------------------------------------------------------------------------
// Global instances
// -----------------------------------------------------------------------------
extern DiscretizerDx discretizer_x;
extern DiscretizerDy discretizer_y;
extern DiscretizerDz discretizer_z;
extern DiscretizerDxx discretizer_xx;
extern DiscretizerDyy discretizer_yy;
extern DiscretizerDzz discretizer_zz;
extern DiscretizerDxy discretizer_xy;
extern DiscretizerDxz discretizer_xz;
extern DiscretizerDyz discretizer_yz;
extern DiscretizerId discretizer_id;

BasicOperator __attribute__((init_priority(301))) Dx(discretizer_x);
BasicOperator __attribute__((init_priority(302))) Dy(discretizer_y);
BasicOperator __attribute__((init_priority(303))) Dz(discretizer_z);
BasicOperator __attribute__((init_priority(304))) Dxx(discretizer_xx);
BasicOperator __attribute__((init_priority(305))) Dyy(discretizer_yy);
BasicOperator __attribute__((init_priority(306))) Dzz(discretizer_zz);
BasicOperator __attribute__((init_priority(307))) Dxy(discretizer_xy);
BasicOperator __attribute__((init_priority(308))) Dyx(discretizer_xy);
BasicOperator __attribute__((init_priority(309))) Dxz(discretizer_xz);
BasicOperator __attribute__((init_priority(310))) Dzx(discretizer_xz);
BasicOperator __attribute__((init_priority(311))) Dyz(discretizer_yz);
BasicOperator __attribute__((init_priority(312))) Dzy(discretizer_yz);
BasicOperator __attribute__((init_priority(313))) Id(discretizer_id);

// =============================================================================
// ExpressionItem
// =============================================================================

#if 0
static string fixup_error(const string& what, int field_id, const string& pde)
{
  return what + " was applied to a field (id " + fd::to_string(field_id) +
                ") which does not belong to the PDE " +	pde + ".";
}
#endif // 0

IMPLEMENT_EXPRITEM_DUP(BasicOperator)
IMPLEMENT_EXPRITEM_DUP(Function)
IMPLEMENT_EXPRITEM_DUP(Constant)
IMPLEMENT_EXPRITEM_DUP(FieldFunction)
IMPLEMENT_EXPRITEM_DUP(CutoffFunction)

ExpressionItem& ExpressionItem::operator=(const ExpressionItem &r)
{
  if (discretizer_ != nullptr) delete discretizer_;
  discretizer_ = r.discretizer_->duplicate();
  return *this;
}

ExpressionItem *ExpressionItem::duplicate() const
{
  throw _S"ExpressionItem::duplicate() was used on an ExpressionItem (" +
        name() + ") that does not support it.";
  return const_cast<ExpressionItem *>(this);
}

// =============================================================================
// BasicOperator
// =============================================================================

// Constructor for operators
BasicOperator::BasicOperator(const Discretizer& discretizer) :
  ExpressionItem(discretizer.duplicate()), component_(-1) {} // TODO: 0 (?)

BasicOperator::BasicOperator(Discretizer *discretizer) :
  ExpressionItem(discretizer), component_(-1) {} // TODO: 0 (?)

BasicOperator& BasicOperator::operator=(const BasicOperator &r)
{
  *(ExpressionItem *)this = r;
  component_ = r.component_;
  return *this;
}

Expression& BasicOperator::operator[](int component) const
{
  ExpressionItem *o = duplicate();
  ((BasicOperator *) o)->component_ = component;
  return *new Expression(o);
}

Expression& BasicOperator::operator[](const Field &f) const
{ return (*this)[f.id]; }

Expression& BasicOperator::operator[](const Function &r)
{
  ExpressionItem *f = r.duplicate();
  ((FunctionDiscretizer *) f->discretizer())->associate_operator(this);
  return *new Expression(f);
}

CodeFragment BasicOperator::generate_code(const DiscretizationData& dd) const
{
  CodeFragment cf = ExpressionItem::generate_code(dd);
  const string c = "$c_" + fd::to_string(component_) + "$";
  fd::replace_all(cf.text_y, "$c$", c);
  fd::replace_all(cf.text_j, "$c$", c);
  return cf;
}

static constexpr int NBS = sizeof(CodeFragment::text_bx_y)/
                           sizeof(CodeFragment::text_bx_y[0]);
static constexpr int NVS = sizeof(CodeFragment::text_vx_y)/
                           sizeof(CodeFragment::text_vx_y[0]);

CodeFragment BasicOperator::generate_code_boundary(
			const BoundaryDiscretizationData& bdd) const
{
  CodeFragment cf = ExpressionItem::generate_code_boundary(bdd);
  const string c = "$c_" + fd::to_string(component_) + "$";
  fd::replace_all(cf.text_y, "$c$", c);
  fd::replace_all(cf.text_j, "$c$", c);
  for (int i = 0; i < NBS; ++i)
  {
    fd::replace_all(cf.text_bx_y[i], "$c$", c);
    fd::replace_all(cf.text_bx_j[i], "$c$", c);
  }
  for (int i = 0; i < NVS; ++i)
  {
    fd::replace_all(cf.text_vx_y[i], "$c$", c);
    fd::replace_all(cf.text_vx_j[i], "$c$", c);
  }
  return cf;
}

void BasicOperator::fixup_refs(const FixupInfo& fi, vector<UnresolvedRef>& l)
{
  int i = fd::index(fi.target, component_);
  if (i != -1)
    component_ = fi.result[i];
  else
    l.push_back({this, component_});
}

// =============================================================================
// Function
// =============================================================================

Function& Function::operator=(const Function &r) // TODO: not necessary!
{
  *(ExpressionItem *)this = r;
  implementation = r.implementation;
  return *this;
}

void Function::set_implementation(const string& spec)
{
  implementation = spec;
  fd::global_config().format_function_implementation(implementation);
  if (implementation.empty())
    throw "Empty implementation for function " + fname();
}

#ifdef __DEBUG__
const string Function::debug_name() const
{
  if (this_discretizer()->numerical_differentiation())
    return "BO[" + fname() + "]";
  return fname();
}
#endif // __DEBUG__

// -----------------------------------------------------------------------------
//		Function: Parse unary expressions
// -----------------------------------------------------------------------------
Expression& Function::generic_operator(const char op) const
{
  DEBUG_LOG("%s\t\t%c\n", name(), op);
  Expression *this_expr = new Expression(duplicate());
  return *new Expression(new Expression(new Constant(0.)), this_expr, op);
}

Expression& Function::operator-() const
{ return generic_operator('-'); }

Expression& Function::operator+() const
{ return generic_operator('+'); }

// -----------------------------------------------------------------------------
//		Function: Parse binary expressions
// -----------------------------------------------------------------------------
Expression& Function::generic_operator(double a, const char op) const
{ return generic_operator(*new Constant(a), op); }

Expression& Function::generic_operator(const ExpressionItem& b, const char op) const
{ return generic_operator(*new Expression(b.duplicate()), op); }

Expression& Function::generic_operator(const Expression& b, const char op) const
{
  DEBUG_LOG("%s\t%s\t%c\n", name(), b.name().c_str(), op);
  return *new Expression(new Expression(this->duplicate()), &b, op);
}

Expression& Function::generic_operator(const Field& b, const char op) const
{
  DEBUG_LOG("%s\t%s\t%c\n", name(), b.name.c_str(), op);
  return *new Expression(new Expression(this->duplicate()), b.create_expression(), op);
}

Expression& Function::operator+(double a) const
{ return generic_operator(a, '+'); }

Expression& Function::operator+(const Field& u) const
{ return generic_operator(u, '+'); }

Expression& Function::operator+(const Function& b) const
{ return generic_operator(b, '+'); }

Expression& Function::operator+(const Expression& b) const
{ return generic_operator(b, '+'); }

Expression& Function::operator-(double a) const
{ return generic_operator(a, '-'); }

Expression& Function::operator-(const Field& u) const
{ return generic_operator(u, '-'); }

Expression& Function::operator-(const Function& b) const
{ return generic_operator(b, '-'); }

Expression& Function::operator-(const Expression& b) const
{ return generic_operator(b, '-'); }

Expression& Function::operator*(double a) const
{ return generic_operator(a, '*'); }

Expression& Function::operator*(const Field& u) const
{ return generic_operator(u, '*'); }

Expression& Function::operator*(const Function& b) const
{ return generic_operator(b, '*'); }

Expression& Function::operator*(const Expression& b) const
{ return generic_operator(b, '*'); }

Expression& Function::operator/(double a) const
{ return generic_operator(a, '/'); }

Expression& Function::operator/(const Field& u) const
{ return generic_operator(u, '/'); }

Expression& Function::operator/(const Function& b) const
{ return generic_operator(b, '/'); }

Expression& Function::operator/(const Expression& b) const
{ return generic_operator(b, '/'); }

Expression& generic_operator(double a, const Function& f, const char op)
{
  Constant *c = new Constant(a);
  DEBUG_LOG("%s\t%s\t%c\n", c->name(), f.name(), op);
  return *new Expression(new Expression(c),	new Expression(f.duplicate()), op);
}

Expression& operator+(double a, const Function& f)
{ return generic_operator(a,f,'+'); }
Expression& operator-(double a, const Function& f)
{ return generic_operator(a,f,'-'); }
Expression& operator*(double a, const Function& f)
{ return generic_operator(a,f,'*'); }
Expression& operator/(double a, const Function& f)
{ return generic_operator(a,f,'/'); }

// =============================================================================
// FieldFunction
// =============================================================================

const string FieldFunction::s_std_field_names[3] = {"u","v","w"};

FieldFunction::FieldFunction(const string &fname, int argc, bool spacevars, bool lin) :
    ExpressionItem(new FieldFunctionDiscretizer(fname, argc, spacevars)),
    linear_(lin)
{
  if (argc < 1) throw _S"Field functions have at least one field argument";
  if (fname.empty()) throw _S"Field functions must have a valid name";
}

FieldFunction::FieldFunction(const FieldFunction &ff) : // TODO remove
    ExpressionItem(new FieldFunctionDiscretizer(ff.fname(), ff.argc(),
                                                ff.has_space_args())),
    linear_(ff.linear_) {
  // We do not dump this discretizer, it will be deleted together with the
  // FieldFunction, see ~FieldFunction().
  THROW_IF(!fcall_args_.empty(), ff.fname()+" is not a valid FieldFunction instance.");
  implementation = ff.implementation;
  // This instance must be dumped because it is being created in an expression.
}

void FieldFunction::set_implementation(const std::vector<string>& spec)
{
  if (spec.empty()) throw "Empty implementation for field function " + fname();
  implementation = spec;
  for (int i = 0; i < (int)implementation.size(); ++i)
    fd::global_config().format_function_implementation(implementation[i]);
}

#if 0
// GlobalConfig ================================================================
  std::string field_function_separator(DEF_IMPL_SEPARATOR);
  bool escape_field_function_separator = true;
  bool escape_long_field_function_separators = false;
// GlobalConfig end ============================================================

void FieldFunction::set_implementation(const string& spec)
{ set_implementation(parse_implementation(spec)); }

std::vector<string> FieldFunction::parse_implementation(const string& spec)
{
  const GlobalConfig& conf = fd::global_config();
  const string& sep = conf.field_function_separator;
  const int sz_sep = sep.length();
  const bool escape = conf.escape_field_function_separator &&
      (conf.escape_long_field_function_separators || sz_sep == 1);

  std::vector<string> items;
  string imp = fd::strip_space(spec);
  while (true)
  {
    bool escaped = false;
    auto pos = imp.find(sep);
    if (escape && pos != 0)
    {
      while (pos != string::npos && imp[pos-1] == '\\')
      {
        pos = imp.find(sep, pos + sz_sep);
        escaped = true;
      }
    }
    string item = strip_space(imp.substr(0, pos));
    if (escape && escaped)
      fd::replace_all(item, "\\" + sep, sep);
    items.push_back(item);
    if (pos == string::npos) break;
    imp = imp.substr(pos + sz_sep);
  }
  return items;
}
#endif // 0

// -----------------------------------------------------------------------------
// FieldFunction -- Code generation
// -----------------------------------------------------------------------------
string FieldFunction::field_name(int n, int argc)
{ return (argc<=3) ? s_std_field_names[n] : string("u")+fd::to_string(n); }

string FieldFunction::get_func_name(int n, const string& basename) const
{ return (n == 0 ? string("") : "D" + fd::to_string(n-1) + "_") + basename; }

string FieldFunction::get_func_name(int n) const
{ return get_func_name(n, fname()); }

string FieldFunction::get_accuracy_var(int n, int which) const
{
  static const char *const varname[2] = {"_zero_accuracy", "_diff_accuracy"};
  return get_func_name(n) + varname[which];
}

static string sFormatSpaceArgs(int dim, bool declaration)
{
  const string dbl(declaration ? "double " : "");
  string s = dbl+"x";
  if (dim > 1) s += ","+dbl+"y";
  if (dim > 2) s += ","+dbl+"z";
  return s;
}

string FieldFunction::code_implementation(int n, int dim) const
{
  FieldFunctionDiscretizer *ffd = this_discretizer();
  const int argc = ffd->argc();
  const bool spacevars = ffd->has_space_args();
  const string& fname = ffd->fname;
  // Function declarator
  string code = "\tdouble " + get_func_name(n, fname) + "(";
  if (spacevars) code += sFormatSpaceArgs(dim, true)+",";
  for (int i = 0; i < argc; ++i)
    code += "double " + field_name(i, argc) + (i != argc-1 ? "," : "");
  code += ")";
  const int impl_size = (int) implementation.size();
  // Function body
  if (impl_size < n+1 || implementation[n].empty()) return code + ";";
  if (implementation[n] != "default") return code + implementation[n];
  // Default implementation for function body:
  const string da = get_accuracy_var(n, 1);
  const string za = get_accuracy_var(n, 0);
  const string u  = field_name(n-1, argc);
  const string fp = code_function_call(fname, n-1, u+"_p", argc, spacevars, dim);
  const string fm = code_function_call(fname, n-1, u+"_m", argc, spacevars, dim);
  code += R"(
	{
		double )"+u+"_p, "+u+"_m, h;"+R"(
		if ()"+da+R"( > 0)
		{
			h = )"+da+"; "+u+"_p = "+u+"+h; "+u+"_m = "+u+R"(-h;
		}
		else if (fabs()"+u+") < "+za+R"()
		{
			h = -)"+da+"; "+u+"_p = h; "+u+R"(_m = -h;
		}
		else
		{
			h = fabs()"+da+"*"+u+"); "+u+"_p = "+u+"+h; "+u+"_m = "+u+R"(-h;
		}
		return ()"+fp+"-"+fm+R"()/(2.*h);
	})";
  return code;
}

string FieldFunction::code_function_call(const string& f, int which,
    const string& varname, int argc, bool spacevars, int dim) const
{
  string code = f + "(";
  if (spacevars) code += sFormatSpaceArgs(dim, false)+",";
  for (int n = 0; n < argc; ++n)
  {
    const string v = (n == which) ? varname : field_name(n, argc);
    code += v + (n != argc-1 ? "," : "");
  }
  return code + ")";
}

CodeFragment FieldFunction::generate_code(const DiscretizationData& dd) const
{
  CodeFragment cf;
  if (fcall_expr_args_.empty())
  {
    cf = ExpressionItem::generate_code(dd); // call discretizer
    for (int n = 0; n < argc(); ++n)
    {
      const string sym = "$C_" + fd::to_string(n) + "$";
      const string val = "$c_" + fd::to_string(fcall_args_[n]) + "$";
      fd::replace_all(cf.text_y, sym, val);
      fd::replace_all(cf.text_j, sym, val);
    }
  }
  else
  {
    cf = this_discretizer()->discretize_with_expressions(dd);
    // Discretize arguments
    for (int n = 0; n < argc(); ++n)
    {
      CodeFragment arg_code = fcall_expr_args_[n]->generate_code(dd);
      // Replace field arguments with their expressions:
      string sym = "@x_" + fd::to_string(n) + "@";
      fd::replace_all(cf.text_y, sym, arg_code.text_y);
      fd::replace_all(cf.text_j, sym, arg_code.text_y);
      // Replace field differential arguments with their expressions:
      sym = "@d_" + fd::to_string(n) + "@";
      fd::replace_all(cf.text_j, sym, arg_code.text_j);
    }
    // Note: there are no fields to do the translation $c$ --> $c_<ID>$
  }
  return cf;
}

CodeFragment FieldFunction::generate_code_boundary(
      const BoundaryDiscretizationData& bdd) const
{
  CodeFragment cf;
  if (fcall_expr_args_.empty())
  {
    cf = ExpressionItem::generate_code_boundary(bdd); // call discretizer
    // Discretize arguments
    for (int n = 0; n < argc(); ++n)
    {
      const string sym = "$C_" + fd::to_string(n) + "$";
      const string val = "$c_" + fd::to_string(fcall_args_[n]) + "$";
      fd::replace_all(cf.text_y, sym, val);
      fd::replace_all(cf.text_j, sym, val);
      fd::replace_all(cf.text_bx_y[0], sym, val);
      fd::replace_all(cf.text_bx_y[1], sym, val);
      fd::replace_all(cf.text_bx_j[0], sym, val);
      fd::replace_all(cf.text_bx_j[1], sym, val);
    }
  }
  else
  {
    cf = this_discretizer()->discretize_on_boundary_with_expressions(bdd);
    // Discretize arguments
    CodeFragment arg_code[argc()];
    // - generate code
    for (int n = 0; n < argc(); ++n)
    	arg_code[n] = fcall_expr_args_[n]->generate_code_boundary(bdd);
    // - check if boundary extremity code is needed
    bool uses_boundary_extremity_code = false;
    for (int n = 0; n < argc(); ++n)
      if (!arg_code[n].text_bx_y[0].empty())
      {
        uses_boundary_extremity_code = true;
        break;
      }
    if (uses_boundary_extremity_code) // Copy nonextremity code
    {
      cf.text_bx_y[0] = cf.text_bx_y[1] = cf.text_y;
      cf.text_bx_j[0] = cf.text_bx_j[1] = cf.text_j;
    }
    // - replace arguments with their expressions
    for (int n = 0; n < argc(); ++n)
    {
      CodeFragment& cf_arg = arg_code[n];
      // Replace field arguments:
      string sym = "@x_" + fd::to_string(n) + "@";
      fd::replace_all(cf.text_y, sym, cf_arg.text_y);
      fd::replace_all(cf.text_j, sym, cf_arg.text_y);
      // Replace field differential arguments:
      sym = "@d_" + fd::to_string(n) + "@";
      fd::replace_all(cf.text_j, sym, cf_arg.text_j);
      if (!uses_boundary_extremity_code) continue;
      // Fix boundary extremity code
      if (cf_arg.text_bx_y[0].empty()) // Copy nonextremity code
      {
        cf_arg.text_bx_y[0] = cf_arg.text_bx_y[1] = cf_arg.text_y;
        cf_arg.text_bx_j[0] = cf_arg.text_bx_j[1] = cf_arg.text_j;
      }
      // Replace field arguments:
      sym = "@x_" + fd::to_string(n) + "@";
      fd::replace_all(cf.text_bx_y[0], sym, cf_arg.text_bx_y[0]);
      fd::replace_all(cf.text_bx_y[1], sym, cf_arg.text_bx_y[1]);
      fd::replace_all(cf.text_bx_j[0], sym, cf_arg.text_bx_y[0]);
      fd::replace_all(cf.text_bx_j[1], sym, cf_arg.text_bx_y[1]);
      // Replace field differential arguments:
      sym = "@d_" + fd::to_string(n) + "@";
      fd::replace_all(cf.text_bx_j[0], sym, cf_arg.text_bx_j[0]);
      fd::replace_all(cf.text_bx_j[1], sym, cf_arg.text_bx_j[1]);
    }
    // Note: there are no fields to do the translation $c$ --> $c_<ID>$
  }
  return cf;
}

void FieldFunction::fixup_refs(const FixupInfo& fi, vector<UnresolvedRef>& l)
{
  if (fcall_args_.empty() && fcall_expr_args_.empty())
    throw "Invalid call to field function " + fname();

  for (int& component : fcall_args_)
  {
    int i = fd::index(fi.target, component);
    if (i != -1)
      component = fi.result[i];
    else
      l.push_back({this, component});
  }

  for (Expression *e : fcall_expr_args_)
    e->fixup_refs(fi, l);
}

// -----------------------------------------------------------------------------
// FieldFunction -- Linearity
// -----------------------------------------------------------------------------
int FieldFunction::degree_of_linearity() const
{
  if (fcall_expr_args_.empty()) // field arguments only
    return linear_ ? 1 : 2;
  // expression arguments
  int dol = 0;
  if (linear_)
    for (auto expr : fcall_expr_args_)
      dol = std::max(dol, expr->degree_of_linearity());
  else
    for (auto expr : fcall_expr_args_)
    {
      const int argdol = expr->degree_of_linearity();
      dol += (argdol == 0 ? 0 : std::max(argdol, 2));
    }
  return dol;
}

#if HAVE_STANDALONE_FIELDS
// -----------------------------------------------------------------------------
// FieldFunction -- Expression creation Part I: Field args
// -----------------------------------------------------------------------------
Expression& FieldFunction::generic_expression(
        std::initializer_list<const Field> args) const
{ return (*this)(args); }

Expression& FieldFunction::operator()(
				std::initializer_list<const Field> args) const
{
  if (argc() != (int) args.size())
    throw "Incorrect number of arguments calling field function "+fname()+".";
  FieldFunction *ff = (FieldFunction *) this->duplicate();
  for (const Field& f : args)
    ff->fcall_args_.push_back(f.id);
  return *new Expression(ff);
}

Expression& FieldFunction::operator()(const Field& u) const
{ return generic_expression({u}); }
Expression& FieldFunction::operator()(const Field& u, const Field& v) const
{ return generic_expression({u, v}); }
Expression& FieldFunction::operator()(const Field& u, const Field& v,
                                      const Field& w) const
{ return generic_expression({u, v, w}); }
Expression& FieldFunction::operator()(const Field& u, const Field& v,
                                      const Field& w, const Field& z) const
{ return generic_expression({u, v, w, z}); }
Expression& FieldFunction::operator()(const Field& u0, const Field& u1,
                                      const Field& u2, const Field& u3,
                                      const Field& u4) const
{ return generic_expression({u0, u1, u2, u3, u4}); }
Expression& FieldFunction::operator()(const Field& u0, const Field& u1,
                                      const Field& u2, const Field& u3,
                                      const Field& u4, const Field& u5) const
{ return generic_expression({u0, u1, u2, u3, u4, u5}); }
Expression& FieldFunction::operator()(const Field& u0, const Field& u1,
                                      const Field& u2, const Field& u3,
                                      const Field& u4, const Field& u5,
                                      const Field& u6) const
{ return generic_expression({u0, u1, u2, u3, u4, u5, u6}); }
Expression& FieldFunction::operator()(const Field& u0, const Field& u1,
                                      const Field& u2, const Field& u3,
                                      const Field& u4, const Field& u5,
                                      const Field& u6, const Field& u7) const
{ return generic_expression({u0, u1, u2, u3, u4, u5, u6, u7}); }
#endif // HAVE_STANDALONE_FIELDS

// -----------------------------------------------------------------------------
// FieldFunction -- Expression creation Part II: Expression args
// -----------------------------------------------------------------------------
Expression& FieldFunction::operator()(
				std::initializer_list<const ExpressionRef> args) const
{
  if (argc() != (int) args.size()) throw "Field function "+
      fname()+" was called with incorrect number of arguments.";
  FieldFunction *ff = (FieldFunction *) this->duplicate();
  for (const ExpressionRef& ref : args)
    ff->fcall_expr_args_.push_back(ref.ptr);
  return *new Expression(ff);
}

Expression& FieldFunction::generic_expression(
				std::initializer_list<Expression *> args) const
{
  if (argc() != (int) args.size()) throw "Field function "+
      fname()+" was called with incorrect number of arguments.";
  FieldFunction *ff = (FieldFunction *) this->duplicate();
  for (Expression *e : args)
    ff->fcall_expr_args_.push_back(e);
  return *new Expression(ff);
}

Expression& FieldFunction::operator()(Expression& u) const
{ return generic_expression({&u}); }
Expression& FieldFunction::operator()(Expression& u,
    Expression& v) const
{ return generic_expression({&u, &v}); }
Expression& FieldFunction::operator()(Expression& u,
    Expression& v, Expression& w) const
{ return generic_expression({&u, &v, &w}); }
Expression& FieldFunction::operator()(Expression& u,
    Expression& v, Expression& w, Expression& z) const
{ return generic_expression({&u, &v, &w, &z}); }
Expression& FieldFunction::operator()(Expression& u0,
    Expression& u1, Expression& u2, Expression& u3,
    Expression& u4) const
{ return generic_expression({&u0, &u1, &u2, &u3, &u4}); }
Expression& FieldFunction::operator()(Expression& u0,
    Expression& u1, Expression& u2, Expression& u3,
    Expression& u4, Expression& u5) const
{ return generic_expression({&u0, &u1, &u2, &u3, &u4, &u5}); }
Expression& FieldFunction::operator()(Expression& u0,
    Expression& u1, Expression& u2, Expression& u3,
    Expression& u4, Expression& u5, Expression& u6) const
{ return generic_expression({&u0, &u1, &u2, &u3, &u4, &u5, &u6}); }
Expression& FieldFunction::operator()(Expression& u0,
    Expression& u1, Expression& u2, Expression& u3,
    Expression& u4, Expression& u5, Expression& u6,
    Expression& u7) const
{ return generic_expression({&u0, &u1, &u2, &u3, &u4, &u5, &u6, &u7}); }

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// =============================================================================
// CutoffFunction
// =============================================================================

CutoffFunction::CutoffFunction(const string &fname) :
    ExpressionItem(new CutoffDiscretizer(fname))
{ if (fname.empty()) throw _S"A cut-off functions must have a valid name"; }

void CutoffFunction::set_implementation(const string& spec)
{
  if (spec.empty()) throw "Empty implementation for cut-off function "+fname();
  implementation = spec;
  fd::global_config().format_function_implementation(implementation);
}

// -----------------------------------------------------------------------------
// CutoffFunction -- Code generation
// -----------------------------------------------------------------------------
string CutoffFunction::code_implementation(int dim) const
{
  CutoffDiscretizer *ffd = this_discretizer();
  const string &fname = ffd->fname;
  // Function declarator
  string code = "\tbool " + fname + "(" + sFormatSpaceArgs(dim, true)+")";
  // Function body
  return code + (implementation.empty() ? string(";") : implementation);
}

CodeFragment CutoffFunction::generate_code(const DiscretizationData& dd) const
{
  CodeFragment cf;
  if (fcall_expr_arg_ == nullptr)
  {
    cf = ExpressionItem::generate_code(dd); // call discretizer
    const string sym = "$C_0$";
    const string val = "$c_" + fd::to_string(fcall_arg_) + "$";
    fd::replace_all(cf.text_y, sym, val);
    fd::replace_all(cf.text_j, sym, val);
  }
  else
  {
    cf = this_discretizer()->discretize_with_expressions(dd);
    // Discretize arguments
    CodeFragment arg_code = fcall_expr_arg_->generate_code(dd);
    // Replace field arguments with their expressions:
    string sym = "@x_0@";
    fd::replace_all(cf.text_y, sym, arg_code.text_y);
    fd::replace_all(cf.text_j, sym, arg_code.text_y);
    // Replace field differential arguments with their expressions:
    sym = "@d_0@";
    fd::replace_all(cf.text_j, sym, arg_code.text_j);
    // Note: there are no fields to do the translation $c$ --> $c_<ID>$
  }
  return cf;
}

CodeFragment CutoffFunction::generate_code_boundary(
      const BoundaryDiscretizationData& bdd) const
{
  CodeFragment cf;
  if (fcall_expr_arg_ == nullptr)
  {
    cf = ExpressionItem::generate_code_boundary(bdd); // call discretizer
    // Discretize arguments
    const string sym = "$C_0$";
    const string val = "$c_" + fd::to_string(fcall_arg_) + "$";
    fd::replace_all(cf.text_y, sym, val);
    fd::replace_all(cf.text_j, sym, val);
    fd::replace_all(cf.text_bx_y[0], sym, val);
    fd::replace_all(cf.text_bx_y[1], sym, val);
    fd::replace_all(cf.text_bx_j[0], sym, val);
    fd::replace_all(cf.text_bx_j[1], sym, val);
  }
  else
  {
    cf = this_discretizer()->discretize_on_boundary_with_expressions(bdd);
    // Discretize arguments
    CodeFragment arg_code;
    // - generate code
    arg_code = fcall_expr_arg_->generate_code_boundary(bdd);
    // - check if boundary extremity code is needed
    const bool uses_boundary_extremity_code = !arg_code.text_bx_y[0].empty();
    if (uses_boundary_extremity_code) // Copy nonextremity code
    {
      cf.text_bx_y[0] = cf.text_bx_y[1] = cf.text_y;
      cf.text_bx_j[0] = cf.text_bx_j[1] = cf.text_j;
    }
    // - replace arguments with their expressions
    //   * Replace field arguments:
    fd::replace_all(cf.text_y, "@x_0@", arg_code.text_y);
    fd::replace_all(cf.text_j, "@x_0@", arg_code.text_y);
    //   * Replace field differential arguments:
    fd::replace_all(cf.text_j, "@d_0@", arg_code.text_j);
    if (uses_boundary_extremity_code)
    {
      // Fix boundary extremity code
      if (arg_code.text_bx_y[0].empty()) // Copy nonextremity code
      {
        arg_code.text_bx_y[0] = arg_code.text_bx_y[1] = arg_code.text_y;
        arg_code.text_bx_j[0] = arg_code.text_bx_j[1] = arg_code.text_j;
      }
      // Replace field arguments:
      fd::replace_all(cf.text_bx_y[0], "@x_0@", arg_code.text_bx_y[0]);
      fd::replace_all(cf.text_bx_y[1], "@x_0@", arg_code.text_bx_y[1]);
      fd::replace_all(cf.text_bx_j[0], "@x_0@", arg_code.text_bx_y[0]);
      fd::replace_all(cf.text_bx_j[1], "@x_0@", arg_code.text_bx_y[1]);
      // Replace field differential arguments:
      fd::replace_all(cf.text_bx_j[0], "@d_0@", arg_code.text_bx_j[0]);
      fd::replace_all(cf.text_bx_j[1], "@d_0@", arg_code.text_bx_j[1]);
    }
    // Note: there are no fields to do the translation $c$ --> $c_<ID>$
  }
  return cf;
}

void CutoffFunction::fixup_refs(const FixupInfo& fi, vector<UnresolvedRef>& l)
{
  if (fcall_arg_ == -1 && fcall_expr_arg_ == nullptr)
    throw "Invalid call to cut-off function " + fname();

  if (fcall_expr_arg_ == nullptr)
  {
    int i = fd::index(fi.target, fcall_arg_);
    if (i != -1)
      fcall_arg_ = fi.result[i];
    else
      l.push_back({this, fcall_arg_});
  }
  else
  {
    fcall_expr_arg_->fixup_refs(fi, l);
  }
}

// -----------------------------------------------------------------------------
// CutoffFunction -- Linearity
// -----------------------------------------------------------------------------
int CutoffFunction::degree_of_linearity() const
{
  return fcall_expr_arg_ == nullptr? 1 : fcall_expr_arg_->degree_of_linearity();
}

#if HAVE_STANDALONE_FIELDS
// -----------------------------------------------------------------------------
// CutoffFunction -- Expression creation Part I: Field arg
// -----------------------------------------------------------------------------
Expression& CutoffFunction::operator()(const Field &u) const
{
  CutoffFunction *ff = (CutoffFunction *) this->duplicate();
  ff->fcall_arg_ = u.id;
  return *new Expression(ff);
}
#endif // HAVE_STANDALONE_FIELDS

// -----------------------------------------------------------------------------
// CutoffFunction -- Expression creation Part II: Expression arg
// -----------------------------------------------------------------------------
Expression& CutoffFunction::operator()(Expression& u) const
{
  CutoffFunction *ff = (CutoffFunction *) this->duplicate();
  ff->fcall_expr_arg_ = &u;
  return *new Expression(ff);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// =============================================================================
// Operator
// =============================================================================

Operator::~Operator()
{
  if (expression_ != nullptr) delete expression_;
  // The rest is done by ExpressionItem::~ExpressionItem()
}

Operator& Operator::operator=(const Operator& expr)
{
  throw _S"Operators cannot be copied.";
  return *this;
}

Operator& Operator::operator=(const Expression& expr)
{
  string prefix = "Y";
  if (!fname.empty())
    prefix = "Operator " + fname + ": y";
  throw prefix + "ou cannot define an operator by assigning an expression to "
    "its symbol. Use the syntax L(u,v) = <expression> instead.";
  return *this;
}

Operator& Operator::Ref::operator=(Expression& expr)
{
  return self.set_differential_expression(expr);
}

Operator::Ref Operator::operator()(std::initializer_list<const Field> args)
{
  if (!args_.empty())
    throw "Operator " + fname + (fname.empty() ? "" : " ") +
      "was previousy defined.";
  for (const Field& u : args)
  {
    if (fd::find(args_, u.id))
      throw _S"Duplicate operator argument" + (u.name.empty() ? "" : ": ") +
        u.name + ".";
    args_.push_back(u.id);
  }
  return Ref(*this);
}

Operator& Operator::set_differential_expression(Expression &expr)
{
  if (expression_ != nullptr)
    throw "Operator " + fname + (fname.empty() ? "" : " ") +
      "was previously defined.";
  expression_ = &expr;
  return *this;
}

Expression& Operator::operator[](std::initializer_list<const Field> args) const
{
  if (args.size() != args_.size())
    throw "Operator " + fname + (fname.empty() ? "" : " ") +
      "was declared with " + fd::to_string(args_.size()) + " arguments "
      "but called with " + fd::to_string(args.size()) + ".";
  if (expression_ == nullptr)
    throw std::string("Operator") + (fname.empty() ? "" : " ") + fname +
      " was declared but not defined.";

  Expression *replica = expression_->duplicate();

  std::vector<int> result;
  for (auto& u : args) result.push_back(u.id);
  FixupInfo fi(args_, result);
  vector<UnresolvedRef> unresolved_refs;

  replica->fixup_refs(fi, unresolved_refs);

  return *replica;
}

CodeFragment Operator::generate_code(const DiscretizationData& dd) const
{
  throw _S"Internal error 0009: attempt to generate code for operator" +
    (fname.empty() ? "" : " ") + fname + ". Please report this error.";
  return CodeFragment();
}

CodeFragment Operator::generate_code_boundary(
			const BoundaryDiscretizationData& bdd) const
{
  throw _S"Internal error 0010: attempt to generate boundary code for operator" +
    (fname.empty() ? "" : " ") + fname + ". Please report this error.";
  return CodeFragment();
}

void Operator::fixup_refs(const FixupInfo&, vector<UnresolvedRef>&)
{
  throw "Internal error 0008: operator " + fname + (fname.empty() ? "" : " ") +
        "was used as expression item. Please report this error.";
}

// =============================================================================
// Expression
// =============================================================================

Expression::Expression(const Expression& r) :
    left_(nullptr),
    right_(nullptr),
    term_(nullptr),
    operation_(r.operation_),
    type_(r.type_),
    linear_(r.linear_)
{
  if (r.term_ != nullptr)
  {
    ASSERT(r.left_ == nullptr && r.right_ == nullptr);
    term_ = r.term_->duplicate();
  }
  else if (r.left_ != nullptr)
  {
    ASSERT(r.term_ == nullptr && r.right_ != nullptr);
    left_ = r.left_->duplicate();
    right_ = r.right_->duplicate();
  }
}

Expression::~Expression()
{
  if (term_ != nullptr)
  {
    delete term_;
    term_ = nullptr;
  }
  else if (left_ != nullptr)
  {
    delete left_;
    delete right_;
  }
}

#ifdef __DEBUG__
std::string Expression::name() const
{
  if (term_ == nullptr) return "result";
  switch (term_->type())
  {
    case DE::CONST_FIELD:     return fd::to_string(((Constant*)term_)->value());
    case DE::FUNCTION:        return ((Function *) term_)->debug_name();
    case DE::FIELD_FUNCTION:  return ((FieldFunction *) term_)->fname();
    case DE::CUTOFF_FUNCTION: return "cutoff-"+((CutoffFunction*)term_)->fname();
    case DE::BASIC_OPERATOR:
    case DE::DIFF_EXPR:       return term_->name(); break;
    default: return "unknown term (type " + fd::to_string(term_->type()) + ")";
  }
  return "?";
}
#endif

int Expression::get_type() const
{
  if (left_ == nullptr) return /*In this case it is already defined*/type_;

  ASSERT(right_ != nullptr);

  const int left_type = left_->type_;
  const int right_type = right_->type_;
  bool unknown_type = false;

  if (left_type == DE::DIFF_EXPR)
    switch (right_type)
    {
      case DE::DIFF_EXPR:
      case DE::CUTOFF_FUNCTION:
      case DE::FIELD_FUNCTION:
      case DE::FUNCTION:
      case DE::CONST_FIELD: return DE::DIFF_EXPR;
      default: unknown_type = true;
    }
  else if (left_type == DE::FIELD_FUNCTION)
    switch (right_type)
    {
      case DE::DIFF_EXPR:
      case DE::FIELD_FUNCTION: return right_type;
      case DE::CUTOFF_FUNCTION:
      case DE::FUNCTION:
      case DE::CONST_FIELD: return DE::FIELD_FUNCTION;
      default: unknown_type = true;
    }
  else if (left_type == DE::CUTOFF_FUNCTION)
    switch (right_type)
    {
      case DE::DIFF_EXPR:
      case DE::CUTOFF_FUNCTION:
      case DE::FIELD_FUNCTION: return right_type;
      case DE::FUNCTION:
      case DE::CONST_FIELD: return DE::FIELD_FUNCTION;
      default: unknown_type = true;
    }
  else if (left_type == DE::FUNCTION)
    switch (right_type)
    {
      case DE::DIFF_EXPR:
      case DE::FIELD_FUNCTION:;
      case DE::FUNCTION: return right_type;
      case DE::CUTOFF_FUNCTION: return DE::FIELD_FUNCTION;
      case DE::CONST_FIELD: return DE::FUNCTION;
      default: unknown_type = true;
    }
  else if (left_type == DE::CONST_FIELD)
    switch (right_type)
    {
      case DE::DIFF_EXPR:
      case DE::FIELD_FUNCTION:
      case DE::CUTOFF_FUNCTION:
      case DE::FUNCTION:
      case DE::CONST_FIELD: return right_type;
      default: unknown_type = true;
    }
  if (unknown_type)
    throw "Right operand has unknown type " + fd::to_string(right_type) + ".";
  throw "Left operand has unknown type " + fd::to_string(left_type) + ".";
  return -1;
}

bool Expression::get_linearity() const
{
  const bool l_is_linear = left_->linear();
  const bool r_is_linear = right_->linear();

  if (operation_ == '+' || operation_ == '-')
    return l_is_linear && r_is_linear;

  if (!l_is_linear || !r_is_linear) return false;

  switch (right_->type())
  {
    case DE::FUNCTION:
    case DE::CONST_FIELD: return true;
  }

  if (operation_ == '*')
    switch (left_->type())
    {
      case DE::FUNCTION:
      case DE::CONST_FIELD: return true;
    }

  return false;
}

int Expression::degree_of_linearity() const
{
  if (term_ != nullptr)
    return term_->degree_of_linearity();

  int left_dol = left_->degree_of_linearity();
  int right_dol = right_->degree_of_linearity();
  switch (operation_)
  {
    case '+':
    case '-': return std::max(left_dol, right_dol);
    case '*': return left_dol + right_dol;
    case '/': return left_dol + 2*right_dol;
    default: throw _S"Internal error 0002. Please report this error.";
  }
  return 2;
}

void Expression::fixup_refs(const FixupInfo& fi, vector<UnresolvedRef>& l)
{
  if (term_ != nullptr)
    term_->fixup_refs(fi, l);
  else
  {
    left_->fixup_refs(fi, l);
    right_->fixup_refs(fi, l);
  }
}

// -----------------------------------------------------------------------------
// Code generation
// -----------------------------------------------------------------------------
CodeFragment Expression::generate_code(const DiscretizationData& dd) const
{
  if (term_ != nullptr)
  {
    CodeFragment result = term_->generate_code(dd);
    result.linear = linear_;
    return result;
  }

  DEBUG_LOG("%s (obtained by applying %c on \n", name().c_str(), operation_);

  const CodeFragment left  = left_->generate_code(dd);
  const CodeFragment right = right_->generate_code(dd);
  CodeFragment result = apply_operation(left, right);
  result.linear = linear_;

  DEBUG_LOG(")\n");

  return result;
}

CodeFragment Expression::generate_code_boundary(
      const BoundaryDiscretizationData& bdd) const
{
  CodeFragment result;
  if (term_ != nullptr)
  {
    result = term_->generate_code_boundary(bdd);
  }
  else
  {
    DEBUG_LOG("%s (obtained by applying %c on \n", name().c_str(), operation_);

    CodeFragment left  = left_->generate_code_boundary(bdd);
    CodeFragment right = right_->generate_code_boundary(bdd);
    result = apply_operation(left, right);

    DEBUG_LOG(")\n");
  }
  result.linear = linear_;
  return result;
}

// -----------------------------------------------------------------------------
// Parse unary expressions
// -----------------------------------------------------------------------------
Expression& Expression::operator-() const
{
  Constant *c = new Constant(0.);
  DEBUG_LOG("%s\t\t%s\n", name().c_str(), "-");
  return *new Expression(new Expression(c), this, '-');
}

Expression& Expression::operator+() const
{
  Constant *c = new Constant(0.);
  DEBUG_LOG("%s\t\t%s\n", name().c_str(), "+");
  return *new Expression(new Expression(c), this, '+');
}

// -----------------------------------------------------------------------------
// Parse binary expressions
// -----------------------------------------------------------------------------
Expression& Expression::generic_operator(double a, const char op) const
{
  return generic_operator(*new Constant(a), op);
}

Expression& Expression::generic_operator(const ExpressionItem& b, const char op) const
{ return generic_operator(*(new Expression(b.duplicate())), op); }

Expression& Expression::generic_operator(const Expression& b, const char op) const
{
  DEBUG_LOG("%s\t%s\t%c\n", name().c_str(), b.name().c_str(), op);
  return *new Expression(this, &b, op);
}

Expression& Expression::generic_operator(const Field& b, const char op) const
{
  DEBUG_LOG("%s\t%s\t%c\n", name().c_str(), b.name.c_str(), op);
  return *new Expression(this, b.create_expression(), op);
}

Expression& Expression::operator+(double a) const
{ return generic_operator(a, '+'); }

Expression& Expression::operator+(const Field& b) const
{ return generic_operator(b, '+'); }

Expression& Expression::operator+(const Function& b) const
{ return generic_operator(b, '+'); }

Expression& Expression::operator+(const Expression& b) const
{ return generic_operator(b, '+'); }

Expression& Expression::operator-(double a) const
{ return generic_operator(a, '-'); }

Expression& Expression::operator-(const Field& b) const
{ return generic_operator(b, '-'); }

Expression& Expression::operator-(const Function& b) const
{ return generic_operator(b, '-'); }

Expression& Expression::operator-(const Expression& b) const
{ return generic_operator(b, '-'); }

Expression& Expression::operator*(double a) const
{ return generic_operator(a, '*'); }

Expression& Expression::operator*(const Field& b) const
{ return generic_operator(b, '*'); }

Expression& Expression::operator*(const Function& b) const
{ return generic_operator(b, '*'); }

Expression& Expression::operator*(const Expression& b) const
{ return generic_operator(b, '*'); }

Expression& Expression::operator/(double a) const
{ return generic_operator(a, '/'); }

Expression& Expression::operator/(const Field& b) const
{ return generic_operator(b, '/'); }

Expression& Expression::operator/(const Function& b) const
{ return generic_operator(b, '/'); }

Expression& Expression::operator/(const Expression& b) const
{ return generic_operator(b, '/'); }

static
Expression& generic_operator(double a, const Expression &e, const char op)
{
  DEBUG_LOG("%g\t%s\t%c\n", a, e.name().c_str(), op);
  Constant *c = new Constant(a);
  return *new Expression(new Expression(c), &e, op);
}

Expression& operator+(double a, const Expression &e)
{ return generic_operator(a, e, '+'); }

Expression& operator-(double a, const Expression &e)
{ return generic_operator(a, e, '-'); }

Expression& operator*(double a, const Expression &e)
{ return generic_operator(a, e, '*'); }

Expression& operator/(double a, const Expression &e)
{ return generic_operator(a, e, '/'); }

} // namespace fd
