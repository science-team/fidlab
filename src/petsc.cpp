#include <iostream>
#include "pde.hpp"
#include "config_code.h"

using std::string;

namespace fd {

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::::::  Code  :::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::generate_code_petsc(int what)
{
  if (linear()) generate_code_petsc_linear(what);
  else generate_code_petsc_nonlinear(what);
}

void PDECommon::generate_code_petsc_linear(int what)
{
  // ---------------------------------------------------------------------------
  // Generate implementation
  // ---------------------------------------------------------------------------

  string funcname, outvars, invars;
  switch (what)
  {
    case GENERATE_CLASS_CODE:
      generate_code_class_petsc();
      return;
    case GENERATE_FUNCTION_CODE:
      funcname = "func";
      outvars = "SparseVector& F, ";
      invars = "const SparseVector& u";
      break;
    case GENERATE_JACOBIAN_CODE:
      funcname = "jac" ;
      outvars = "SparseVector& J, ";
      invars = "const SparseVector& du";
      break;
    case GENERATE_BOTH:
      return;
    default: THROW("bad parameter");
  }

  // Write function code

  code_ << R"(
void )"<<name<<"::"<<funcname<<"("<<outvars<<invars<<")";

  // Write function body
  generate_code_function_body(what);
}

void PDECommon::generate_code_petsc_nonlinear(int what)
{
  // ---------------------------------------------------------------------------
  // Generate implementation
  // ---------------------------------------------------------------------------

  string funcname, args, ret;
  switch (what)
  {
    case GENERATE_CLASS_CODE:
      generate_code_class_petsc();
      return;
    case GENERATE_FUNCTION_CODE:
      funcname = "func";
      args = "int eq, T& u";
      ret = "double";
      generate_code_petsc_snes_mp();
      break;
    case GENERATE_JACOBIAN_CODE:
      funcname = "jac" ;
      args = "int eq, T& u, const SparseVector& du";
      ret = "doublexz";
      break;
    case GENERATE_BOTH:
      return;
    default: THROW("bad parameter");
  }

  // Write function code

  // 1. Template
  code_ << R"(
template<typename T>
)"<<ret<<" "<<name<<"::"<<funcname<<"("<<args<<")";

  // 2. Function body
  generate_code_function_body_petsc_nonlinear(what);
}

void PDECommon::generate_code_class_petsc()
{
  const int nc = components();
  const int dim = dimension();

  code_ << R"(
// =============================================================================
// Class SparseVector
// =============================================================================

const doublexz& SparseVector::Reference::operator=(const doublexz& r)
{
	*(doublexz *)this = r;
	if (self.not_in_range(element)) return *this;
	int ofs = self.find(element);
	if (r.is_zero())
	{
		if (ofs != -1) self.erase(ofs);
		return *this;
	}
	if (ofs == -1)
	{
		self.append(element);
		ofs = self.size() - 1;
	}
	self.set_value_at(ofs, r);
	return *this;
}

void SparseVector::clear_zeros()
{
	auto i = cols.begin();
	auto j = values.begin();
	while (i != cols.end())
	{
		if (j->is_zero())
		{
			i = cols.erase(i);
			j = values.erase(j);
		}
		else { ++i; ++j; }
	}
}
)";

  const string ie_imp = has_initial_estimate() ? R"(
double )"+format_space_function_declaration(name+"::initial_estimate", nc > 1)+
          init_estimate_imp_+R"(
)" : "";

  string fn_imp;
  if (use_field_names && nc > 1)
  {
    string field_names;
    const string sep = (nc <= 3) ? ", " : ",\n\t\t";
    for (int i = 0; i < nc; ++i)
    {
      if (i == 0) field_names += (nc <= 3 ? "" : "\n\t\t");
      else field_names += sep;
      field_names += "\""+field_[i].name+"\"";
    }
    if (nc > 3) field_names += "\n\t";

    fn_imp = R"(
const std::string& )"+name+R"(::field_name(int c) const
{
	static const std::string s_field_names[]{)"+field_names+R"(};
	if (c < 0 || c > )"+fd::to_string(nc-1)+R"()
		throw std::string(__PRETTY_FUNCTION__)+": field index out of bounds.";
	return s_field_names[c];
}
)";
  }

  code_ << R"(
// =============================================================================
// Class )"<<name<<R"(
// =============================================================================
)"<<ie_imp<<fn_imp;

  if (has_exact_solution())
  {
    const string f = (nc == 1) ? "" : "c";
    code_ << R"(
void )"<<name<<R"(::dnorms(const Vector& v)
{
	if (norms_cache_valid) return;

	Vector dv;
	v.Duplicate(dv);
	const Scalar *av = v.GetArrayRead();
	Scalar *ad = dv.GetArray();
	Int start, end;
	dv.GetOwnershipRange(&start, &end);
	for (int e = start; e < end; ++e)
	{
		int mesh_coord[)"<<dim<<R"(];
		)"<<(nc == 1 ? "" : "int c = ")<<R"(field_index_to_mesh_coordinates(e,mesh_coord);
		const int &i = mesh_coord[0];)";
    if (dim > 1) code_ << R"(
		const int &j = mesh_coord[1];)";
    if (dim > 2) code_ << R"(
    const int &k = mesh_coord[2];)";
    code_ << R"(
		const bool vp = (i < 0 || i > nx-1)";
    if (dim > 1) code_ << " || j < 0 || j > ny-1";
    if (dim > 2) code_ << " || k < 0 || k > nz-1";
    code_ << R"();
		ad[e-start] = vp ? 0. : av[e-start] - )" <<
            format_function_call("exact_solution","i","j","k",f)<<R"(;
	}
	dv.RestoreArray(&ad);
	v.RestoreArrayRead(&av);)";
		if (nc == 1) code_ << R"(
	dv.Norm12(&norms_cache[1]);
	norms_cache[0] = dv.NormSup();
	norms_cache[1] /= )"<<format_num_nodes()<<R"(;
	norms_cache[2] /= sqrt()"<<format_num_nodes()<<R"();)";
		else code_ << R"(
	for (int c = 0; c < )"<<nc<<R"(; ++c)
	{
		Vector field;
		IndexSet is_field;
		is_field.CreateStride(field_size(c), field_offset(c), 1, MPI(PETSC_COMM_WORLD));
		dv.GetSubVector(is_field, field);
		field.Norm12(&norms_cache[c][1]);
		norms_cache[c][0] = field.NormSup();
		norms_cache[c][1] /= )"<<format_num_nodes()<<R"(;
		norms_cache[c][2] /= sqrt()"<<format_num_nodes()<<R"();
	})";
    code_ << R"(
	norms_cache_valid = true;
}
)";
  }

  generate_code_class_extra();
}

void PDECommon::generate_code_petsc_snes_mp()
{
  string funcname, outvars, invars;

  //------------------------- Write function code ----------------------------//

  funcname = "Function";
  invars  = "Vector& u, ";
  outvars = "Vector& F";

  code_ << R"(
Petsc::ErrorCode )"<<name<<"::"<<funcname<<"("<<invars<<outvars<<R"()
{
	try
	{
		Scalar *F_local_data = F.GetArray();
		Int start, end;
		F.GetOwnershipRange(&start, &end);
		int max_block_size = F.GetMaxBlockSize();
		for (int i = start; i < end; ++i)
		{
			// Track down the elements in use
			ElementCollector used;
			func(i, used);
			// Download them
			std::unique_ptr<Scalar[]> values(new Scalar[used.size()]);
			u.GetValuesP(used.size(), used.data(), values.get());
			// Compute element
			const Map map(used.size(), used.data(), values.get());
			F_local_data[i-start] = func(i, map);
		}
		for (int i = end; i < max_block_size; ++i)
		{
			// Download empty set of items
			u.GetValuesP(0, nullptr, nullptr);
			// There is no need to compute element when the system is not non-local
		}
		F.RestoreArray(&F_local_data);
	}
	catch (Petsc::Exception ex)
	{
		ex.handle();
		std::cout << ex.description << std::endl;
		return ex.error_code;
	}
	return 0;
}
)";

  // ------------------------ Write Jacobian code ------------------------------

  funcname = "Jacobian";
  invars  = "Vector& u, ";
  outvars = "Matrix& J, Matrix& P";

  code_ << R"(
Petsc::ErrorCode )"<<name<<"::"<<funcname<<"("<<invars<<outvars<<R"()
{
	try
	{
		Int start, end;
		J.GetOwnershipRange(&start, &end);
		int max_block_size = J.GetMaxBlockSize();
		for (int i = start; i < end; ++i)
		{
			// Track down the elements that will be used
			ElementCollector used;
			for (int j = 0; j < dim(); ++j)
			{
				SparseVector du(j);
				jac(i, used, du);
			}
			// Download them
			std::unique_ptr<Scalar[]> values(new Scalar[used.size()]);
			u.GetValuesP(used.size(), used.data(), values.get());
			// Compute elements of row i
			SparseVector Jval;
			const Map map(used.size(), used.data(), values.get());
			for (int j = 0; j < dim(); ++j)
			{
				SparseVector du(j);
				Jval[j] = jac(i, map, du);
			}
			std::vector<double> Jvec = Jval.export_vector();
			J.SetValues(1, &i, Jval.size(), Jval.cols.data(), Jvec.data());
		}
		for (int i = end; i < max_block_size; ++i)
		{
			// Download empty set of items
			u.GetValuesP(0, nullptr, nullptr);
			// There is no need to compute element when the system is not non-local
#if 0
			// Compute elements of row i
			SparseVector Jval;
			Map map(0, nullptr, nullptr);
			for (int j = 0; j < dim(); ++j)
			{
				SparseVector du(j);
				/* Discard return value */ jac(i, map, du);
			}
#endif // 0
		}
		J.Assembly();
	}
	catch (Petsc::Exception ex)
	{
		ex.handle();
		std::cout << ex.description << std::endl;
		return ex.error_code;
	}
	return 0;
}
)";
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::::::  Main  :::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::generate_main_petsc()
{
  code_ << "using namespace Petsc;" << std::endl;
  if (linear()) generate_main_petsc_linear();
  else generate_main_petsc_nonlinear();
}

void PDECommon::generate_main_petsc_linear()
{
  code_ << R"(
int Petsc::Main()
{
	)" << format_constructor_comment() << R"(
	)"<<name<<R"( pde;
	const int N = pde.dim();
	// Multiprocessor system
	MPI mps(PETSC_COMM_WORLD);
	// The std::cout of the multiprocessor system
	uniproc_ostream mpout(std::cout, mps);

	try
	{
		Matrix A(N, N);
		Vector x(N);)";
	code_ << R"(

		int start, end;
		// Build matrix
		A.GetOwnershipRange(&start, &end);
		for (int i = start; i < end; i++)
		{
			SparseVector row, basis;
			basis[i] = 1;
			pde.jac(row, basis);
			A.SetValues(1, &i, row.size(), row.cols.data(), row.export_vector().data());
		}
		A.Assembly();
		A.Transpose();
		// Build RHS
		x.GetOwnershipRange(&start, &end);
		{
			SparseVector rhs, zero;
			rhs.set_range(start, end-1);
			pde.func(rhs, zero);
			Scalar *x_local_data = x.GetArray();
			for (int i = start; i < end; i++)
				x_local_data[i-start] = -rhs[i].value();
			x.RestoreArray(&x_local_data);
		})";

  // See example ex1.c:
  // www.mcs.anl.gov/petsc/petsc-current/src/ksp/pc/examples/tutorials/ex1.c.html
  code_ << R"(

		// Solve the system.
		Kspace ksp(A);
		ksp.Solve(x);

		// Display diagnostics.
		std::string divergence_reason;
		switch (ksp.GetConvergedReason())
		{
			case KSP_DIVERGED_ITS:
				divergence_reason = "Number of iterations was exceeded before any "
				                    "convergence criteria were reached.";
				break;
			case KSP_DIVERGED_DTOL:
				divergence_reason = "Residual norm increased by a factor of <divtol>.";
				break;
			case KSP_DIVERGED_NANORINF:
				divergence_reason = "Residual norm became Not-A-Number or Infinity, "
				                    "possibly due to division by zero.";
				break;
			case KSP_DIVERGED_BREAKDOWN:
				divergence_reason = "General breakdown in Krylov-space method. "
				                    "Possible reason a singlular matrix or "
				                    "preconditioner.";
				break;
			case KSP_DIVERGED_BREAKDOWN_BICG:
				divergence_reason = "A breakdown in the KSPBICG method was detected. "
				                    "Initial residual is orthogonal to preconditioned "
				                    "initial residual. Try a different preconditioner, "
				                    "or a different initial level.";
				break;
			default:
				mpout << "Number of iterations: " << ksp.GetIterationNumber() + 1 << '\n';
		}
		if (!divergence_reason.empty())
			throw divergence_reason;
)";

  generate_solution_output();

  code_ << R"(
	}
	catch (Exception ex)
	{
		ex.handle();
		if (!ex.description.empty())
			std::cout << "Note from processor " << mps.Comm_rank()
			          << " (re. previous message): " << ex.description << '\n';
		return ex.error_code;
	}
	catch (std::string& msg)
	{
		std::cout << "Processor " << mps.Comm_rank() << ": " << msg << '\n';
		return 1;
	}

	return 0;
}

)";
}

void PDECommon::generate_main_petsc_nonlinear()
{
  const int nc = components();
  const string locvar[4]{
      "",
      "const int &i = mc[0];",
      "const int &i = mc[0], &j = mc[1];",
      "const int &i = mc[0], &j = mc[1], &k = mc[2];"
  };
  const string initial_estimate = has_initial_estimate() ? R"(

		// Set initial estimate for the solution
		{
			Int start, end;
			x.GetOwnershipRange(&start, &end);
			Scalar *x_local_data = x.GetArray();
			for (int fi = start; fi < end; fi++)
			{
				int mc[)"+fd::to_string(dimension())+R"(];
				)"+_S(nc == 1 ? "" : "int c = ")+
				R"(pde.field_index_to_mesh_coordinates(fi, mc);
				)"+locvar[dimension()]+R"(
				x_local_data[fi-start] = pde.initial_estimate()"+format_coord()+R"();
			}
			x.RestoreArray(&x_local_data);
		})" : "";

	code_ << R"(
bool )"<<name<<R"(::check_mpi_compatibility(const Petsc::MPI& mpi) const
{
	const bool compat = (dim() % mpi.Comm_size() == 0);
	if (!compat && mpi.Comm_rank() == 0)
		std::cout << "The number of processors is not compatible with the grid "
		             "size. The total number of knots ("<<dim()<<") must be "
		             "divisible by the number of processors.\n";
	return compat;
}

int Petsc::Main()
{
	)" << format_constructor_comment() << R"(
	)"<<name<<R"( pde;
	const int N = pde.dim();
	// Multiprocessor system
	MPI mps(PETSC_COMM_WORLD);
	// The std::cout of the multiprocessor system
	uniproc_ostream mpout(std::cout, mps);

	if (!pde.check_mpi_compatibility(mps))
		return 1;

	try
	{
		Matrix J(N, N);
		Vector x(N), f;)";
  code_ << R"(
		x.Set(0.0).Duplicate(f);)"<<initial_estimate<<R"(

		// Create nonlinear solver object
		Petsc::NonlinearSolver solver;
		solver.SetFunction(pde, f).SetJacobian(pde, J);

		// Solve the system
		solver.Solve(x);

		// Display diagnostics; alternatively, use the command-line option
		// -snes_converged_reason [ascii]
		const bool show_converged_reason = Petsc::OptionsGetBool(
				"-show_converged_reason", PETSC_FALSE) == PETSC_TRUE;
		const int converged_reason = solver.GetConvergedReason();
		const int niter = show_converged_reason ? solver.GetIterationNumber() : -1;
		const bool diverged = (converged_reason < 0);
		std::string divergence_reason;
		switch (converged_reason)
		{
			// Converged
			// =========
			case SNES_CONVERGED_FNORM_ABS     : divergence_reason =
				"The norm of the function was less than the absolute tolerance, atol.";
				break;
			case SNES_CONVERGED_FNORM_RELATIVE: divergence_reason =
				"The relative norm of the function was less than rtol.";
				break;
			case SNES_CONVERGED_SNORM_RELATIVE: divergence_reason = "Newton "
				"step was small; the relative norm of the step was less than stol.";
				break;
			case SNES_CONVERGED_ITS           : divergence_reason = "The maximum "
				"number of iterations was reached; convergence criteria were satisfied.";
				break;
			case SNES_CONVERGED_TR_DELTA      : divergence_reason =
				"SNES_CONVERGED_TR_DELTA";
				break;
			// Diverged
			// ========
			case SNES_DIVERGED_FUNCTION_DOMAIN: divergence_reason =
				"The new x location is not in the domain of the function.";
				break;
			case SNES_DIVERGED_FUNCTION_COUNT : divergence_reason = "The number of "
				"function calls exceeded the specified value; "
				"see SNESSetTolerances(), last argument.";
				break;
			case SNES_DIVERGED_LINEAR_SOLVE   : divergence_reason =
				"The linear solver failed.";
				break;
			case SNES_DIVERGED_FNORM_NAN      : divergence_reason =
				"The norm of the function was NAN (SNES_DIVERGED_FNORM_NAN).";
				break;
			case SNES_DIVERGED_MAX_IT         : divergence_reason =
				"The solver reached the maximum number of iterations "
				"without satisfying any convergence criteria.";
				break;
			case SNES_DIVERGED_LINE_SEARCH    : divergence_reason =
				"The line search process failed.";
				break;
			case SNES_DIVERGED_INNER          : divergence_reason =
				"SNES_DIVERGED_INNER";
				break;
			case SNES_DIVERGED_LOCAL_MIN      : divergence_reason =
				"The algorithm seems to have stagnated at a local minimum that "
				"is not a zero. This failure affects the line search algorithm.";
				break;
			case SNES_DIVERGED_DTOL           : divergence_reason =
				"The relative norm of the function is too large, exceeding divtol.";
				break;
			// Other
			// =====
			case SNES_CONVERGED_ITERATING     : divergence_reason =
				"SNES_CONVERGED_ITERATING";
				break;
			default:
				divergence_reason = diverged ? "Unknown divergence reason." :
				                               "Unknown convergence reason.";
		}
		if (diverged)
		{
			mpout << "Failure: " << divergence_reason << '\n';
			return 1;
		}
		if (show_converged_reason && converged_reason != SNES_CONVERGED_ITERATING)
			mpout << "Success: " << divergence_reason << '\n'
			      << "Number of iterations: " << niter << '\n';
)";

  generate_solution_output();

  code_ << R"(
	}
	catch (Exception ex)
	{
		ex.handle();
		if (!ex.description.empty())
			std::cout << "Note from processor " << mps.Comm_rank()
			          << " (re. previous message): " << ex.description << '\n';
		return ex.error_code;
	}
	catch (std::string& msg) // <-- TODO! is this necessary? Check also linear case!
	{
		std::cout << "Processor " << mps.Comm_rank() << ": " << msg << '\n';
		return 1;
	}

	return 0;
}

)";
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::::::  Header  ::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::generate_header_funcs_petsc()
{
  if (linear()) generate_header_funcs_petsc_linear();
  else generate_header_funcs_petsc_nonlinear();
}

void PDECommon::generate_header_funcs_petsc_linear()
{
  const string vec = "SparseVector";
  code_h_ << R"(
	// Functions generating problem discretization.
	void func()"<<vec<<"& F, const "<<vec<<R"(& u);
	void jac ()"<<vec<<"& J, const "<<vec<<  "& du);";
}

void PDECommon::generate_header_funcs_petsc_nonlinear()
{
  code_h_ << R"(
	// Functions generating the discretization of the problem.
	template<typename T> // T = ElementCollector or Map
	double func(int equation_number, T& u);
	template<typename T> // T = ElementCollector or Map
	doublexz jac(int equation_number, T& u, const SparseVector& du);

	// SNES C++ interface; see snes.hpp, struct SolverFunctionInterface.
	Petsc::ErrorCode Function(Petsc::Vector& x, Petsc::Vector& f);
	Petsc::ErrorCode Jacobian(Petsc::Vector& x, Petsc::Matrix& A, Petsc::Matrix& P);

	bool check_mpi_compatibility(const Petsc::MPI& mpi) const;)";
}

void PDECommon::generate_header_compare_solution_petsc()
{
  const int nc = components();
  const string nsp = libs_[library_]; // namespace
  const string fi = (nc == 1) ? "" : "int field_number,";
  code_h_ << R"(

	// Comparison with exact solution
	double dnorm1()"<<fi<<"const "<<nsp<<R"(::Vector& v)
	{ dnorms(v); return norms_cache)"<<(nc == 1 ? "" : "[field_number]")<<R"([1]; }
	double dnorm2()"<<fi<<"const "<<nsp<<R"(::Vector& v)
	{ dnorms(v); return norms_cache)"<<(nc == 1 ? "" : "[field_number]")<<R"([2]; }
	double dnormsup()"<<fi<<"const "<<nsp<<R"(::Vector& v)
	{ dnorms(v); return norms_cache)"<<(nc == 1 ? "" : "[field_number]")<<R"([0]; }
	void dnorms(const )"<<nsp<<R"(::Vector& v);
	inline void invalidate_norms_cache() {norms_cache_valid = false;})";
}

void PDECommon::generate_header_classes_petsc()
{
  // const string nsp = libs_[library_];
  const string element_collector = linear() ? _S"" : R"(
//! \class ElementCollector A class pretending to be a vector
//! but collecting used vector elements instead.
struct ElementCollector : public std::vector<int>
{
	inline double& operator[](int i) noexcept {insert(i); return dummy_ref();}
	inline void insert(int i) noexcept {if (has_no(i)) push_back(i);}

private:

	inline static double& dummy_ref() noexcept
	{ static double dummy_; dummy_ = 1; return dummy_; }
	inline bool has_no(int i) const noexcept
	{ return std::find(begin(), end(), i) == end(); }
}; // struct ElementCollector
)";

  code_h_ << R"(

//! \class doublexz FP with exactly known zero.
/*! Floating point numbers, double precision, with exactly known zero.
    This class is used by sparse vector and matrix classes.
    Maintainer's note: Do not use \c value_ directly; use value()
    instead. If \c value_ has to be used, check first if it is \c zero_. */
class doublexz
{
	double value_;
	bool zero_;

public:

	doublexz() noexcept : value_(0), zero_(true) {}
	doublexz(int i) noexcept : value_(i), zero_(i == 0) {}
	doublexz(double x) noexcept : value_(x), zero_(is_zero(x)) {}

	inline bool is_zero() const noexcept {return zero_;}
	inline double value() const noexcept {return zero_ ? (double)0 : value_;}
	// Unary operators
	inline doublexz operator+() const noexcept {return *this;}
	inline doublexz operator-() const noexcept {return zero_ ? doublexz() : doublexz(-value_);}
	inline doublexz& operator++() noexcept {return *this += 1;}
	inline doublexz& operator--() noexcept {return *this -= 1;}
	inline doublexz operator++(int) noexcept {doublexz r = *this; ++*this; return r;}
	inline doublexz operator--(int) noexcept {doublexz r = *this; --*this; return r;}
	// Binary operators
	doublexz operator+(const doublexz& r) const noexcept
	{ return zero_ ? r : doublexz(value_+r.value()); }
	doublexz operator-(const doublexz& r) const noexcept
	{ return r.zero_ ? *this : doublexz(value()-r.value_);}
	doublexz operator*(const doublexz& r) const noexcept
	{ return (zero_ || r.zero_) ? doublexz() : doublexz(value_*r.value_); }
	doublexz operator/(const doublexz& r) const
	{ return zero_ ? doublexz() : doublexz(value_/r.value()); }
	// Binary operators with FP
	doublexz operator+(double x) const noexcept {return zero_ ? x : value_+x;}
	doublexz operator-(double x) const noexcept {return zero_ ? -x : value_-x;}
	doublexz operator*(double x) const noexcept {return zero_ ? doublexz() : doublexz(value_*x);}
	doublexz operator/(double x) const {return zero_ ? doublexz() : doublexz(value_/x);}
	// Binary operators with integers
	doublexz operator+(int k) const noexcept {return zero_ ? doublexz(k) : doublexz(value_+k);}
	doublexz operator-(int k) const noexcept {return zero_ ? doublexz(-k) : doublexz(value_-k);}
	doublexz operator*(int k) const noexcept {return (zero_ || k==0) ? doublexz() : doublexz(value_*k);}
	doublexz operator/(int k) const {return zero_ ? doublexz() : doublexz(value_ / k);}
	// Operational copy operators
	inline doublexz& operator+=(const doublexz& r) noexcept {return *this = *this+r;}
	inline doublexz& operator-=(const doublexz& r) noexcept {return *this = *this-r;}
	inline doublexz& operator*=(const doublexz& r) noexcept {return *this = *this*r;}
	inline doublexz& operator/=(const doublexz& r)          {return *this = *this/r;}
	// -- with FPs
	inline doublexz& operator+=(double x) noexcept {return *this = *this+x;}
	inline doublexz& operator-=(double x) noexcept {return *this = *this-x;}
	inline doublexz& operator*=(double x) noexcept {return *this = *this*x;}
	inline doublexz& operator/=(double x) {return *this = *this/x;}
	// -- with integers
	inline doublexz& operator+=(int k) noexcept {return *this = *this+k;}
	inline doublexz& operator-=(int k) noexcept {return *this = *this-k;}
	inline doublexz& operator*=(int k) noexcept {return *this = *this*k;}
	inline doublexz& operator/=(int k) {return *this = *this/k;}

protected:

	static inline bool is_zero(double x) noexcept
	{ return fabs(x) < std::numeric_limits<double>::min(); }
}; // class doublexz

inline doublexz operator+(double x, const doublexz& r) noexcept {return doublexz(x)+r;}
inline doublexz operator-(double x, const doublexz& r) noexcept {return doublexz(x)-r;}
inline doublexz operator*(double x, const doublexz& r) noexcept {return doublexz(x)*r;}
inline doublexz operator/(double x, const doublexz& r) {return doublexz(x)/r;}

inline doublexz operator+(int k, const doublexz& r) noexcept {return doublexz(k)+r;}
inline doublexz operator-(int k, const doublexz& r) noexcept {return doublexz(k)-r;}
inline doublexz operator*(int k, const doublexz& r) noexcept {return doublexz(k)*r;}
inline doublexz operator/(int k, const doublexz& r) {return doublexz(k)/r;}
)"<<element_collector<<R"(
//! Matrix elements must be set in groups, each group consisting of the
//!	nonzero elements of a SINGLE matrix row owned by the processor. We
//!	have to set one row at a time because of a limitation of MatSetValues
//!	accepting only block element data; recall that boundary knots may
//!	have different number of nonzero elements.

//! \class SparseVector Encapsulation of a PETSC matrix row data.
/*! The data encapsulated by SparseVector are in the form expected by the
    PETSC function \c MatSetValues(). Use
    MatSetValues(1, &i, sv.cols.size(), sv.cols.data(), sv.values.data());
    to set the matrix elements of row \c i. */
class SparseVector
{
	int first_, last_;

public:

	std::vector<int> cols;
	std::vector<doublexz> values;

	SparseVector() : first_(0), last_(std::numeric_limits<int>::max()) {}
	SparseVector(int col) : SparseVector() {append(col, 1);}

	void set_range(int first, int last) {first_ = first; last_ = last;}
	inline bool not_in_range(int i) const {return i < first_ || i > last_;}
	inline bool in_range(int i) const {return !not_in_range(i);}
	inline bool has_element(int i) const noexcept {return find(i) != -1;}
	inline int size() const noexcept {return (int)cols.size();}
	double value_at_offset(int i) const noexcept {return values[i].value();}

	std::vector<double> export_vector() const
	{
		std::vector<double> r;
		for (const doublexz& fp : values) r.push_back(fp.value());
		return r;
	}

	struct Reference : public doublexz
	{
		SparseVector& self;
		int element;
		Reference(SparseVector& sv, int i) :
				doublexz(sv.value_at(i)), self(sv), element(i) {}
		const doublexz& operator=(const doublexz& r);
		const doublexz& operator=(double x)
		{ return operator=(doublexz(x)); }
		const doublexz& operator=(int i)
		{ return operator=(doublexz(i)); }
		const doublexz& operator=(const Reference& r)
		{ return operator=(static_cast<const doublexz&>(r)); }
	};

	doublexz value_at(int e) const noexcept
	{ int ofs = find(e); return ofs == -1 ? doublexz() : values[ofs]; }

	Reference operator[](int i) noexcept {return Reference(*this, i);}

	inline const doublexz operator[](int i) const noexcept
	{ return value_at(i); }

	void clear_zeros();

protected:

	inline void set_value_at(int ofs, const doublexz& x) {values[ofs] = x;}

	int find(int n) const noexcept
	{
		for (int i = 0; i < (int) cols.size(); ++i)
			if (cols[i] == n) return i;
		return -1;
	}

	inline void append(int element)
	{ cols.push_back(element); values.push_back(doublexz(0)); }
	inline void append(int element, int value)
	{ cols.push_back(element); values.push_back(doublexz(value)); }

	void erase(int ofs)
	{
		cols.erase(cols.cbegin() + ofs);
		values.erase(values.cbegin() + ofs);
	}
}; // class SparseVector)";
}

} // namespace fd
