#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <sstream>
#include "pde.hpp"
#include "config_code.h"
#include "global_config.h"

using std::string;
using std::vector;

namespace fd {

//----------------------------------------------------------------------------//
//====================[  MODULE 1: Makefile generation  ]=====================//
//----------------------------------------------------------------------------//

void PDECommon::generate_makefile()
{
  MakefileGenerationInfo mgi{
    nullptr,
    get_implem_filename(),
    get_main_filename(),
    executable.empty() ? [](string s) {
        auto f = [](unsigned char c) {return std::tolower(c);};
        std::transform(s.begin(), s.end(), s.begin(), f);
        return s;
      }(name) : executable,
    _S""
  };
  const string implem_filename = mgi.implem;

  std::ofstream of;
  {
    string makefile = get_path(implem_filename) + "makefile";
    of.open(makefile);
    if (!of) throw _S"Could not create makefile.";
  }

  mgi.pof = &of;

  mgi.implem = strip_path(implem_filename);
  mgi.mainfn = (mgi.mainfn == implem_filename)? "" : strip_path(mgi.mainfn);
  mgi.objects = strip_ext(mgi.mainfn);
  if (!mgi.objects.empty()) mgi.objects = " " + mgi.objects + ".o";
  mgi.objects = strip_ext(mgi.implem)+".o"+mgi.objects;

  //----------------------( Makefile Generation Method )----------------------//
  switch (library_)
  {
    case GSL:   generate_makefile_gsl   (mgi); break;
    case PETSC: generate_makefile_petsc (mgi); break;
    default:    makefile_generate_method(mgi);
  }
}

void PDECommon::generate_makefile_header(MakefileGenerationInfo& mgi)
{
  std::ofstream& of = *mgi.pof;

  std::time_t this_time = std::time(nullptr);
  string NOW(std::ctime(&this_time));
  auto pos = NOW.rfind('\n');

  if (pos != string::npos) NOW = NOW.substr(0, pos);

  of  << "# Generated by " APPNAME " on " << NOW << std::endl
      << "# *** Do not edit unless you know what you are doing! ***\n"
      << "#\n"
      << "# Makefile for project " << name << std::endl;

  of << extra_make_defs_;
}

static const string SYS_CONFIG_GSL[]{
	R"(
ifeq ($(shell which dpkg),)
$(error dpkg is required for automatic configuration. For manual configuration\
				use the auto_system_config and system_installed_XXX flags)
endif

ifneq ($(shell dpkg -l gsl>/dev/null 2>&1; echo $$?),0)
HAVE_SYS_GSL = false
else
HAVE_SYS_GSL = true
endif

ifneq ($(shell dpkg -l libgsl++-dev>/dev/null 2>&1; echo $$?),0)
HAVE_SYS_GSLPP = false
else
HAVE_SYS_GSLPP = true
endif

SYS_INST_LIBS = $(and $(findstring true,$(HAVE_SYS_GSL)),\
                      $(findstring true,$(HAVE_SYS_GSLPP)))
)",
	R"(
HAVE_SYS_GSL = false
HAVE_SYS_GSLPP = false
SYS_INST_LIBS = false
)",
	R"(
HAVE_SYS_GSL = false
HAVE_SYS_GSLPP = true
SYS_INST_LIBS = false
)",
	R"(
HAVE_SYS_GSL = true
HAVE_SYS_GSLPP = false
SYS_INST_LIBS = false
)",
	R"(
HAVE_SYS_GSL = true
HAVE_SYS_GSLPP = true
SYS_INST_LIBS = true
)"
};

int PDECommon::get_gsl_config() const noexcept
{
  return auto_system_config ? 0 : 2*int(system_installed_gsl) +
                                  int(system_installed_gslpp) + 1;
}

void PDECommon::generate_makefile_gsl(MakefileGenerationInfo& mgi)
{
  std::ofstream& of = *mgi.pof;
  const string  header = strip_path(get_header_filename());
  const string  cxx = compiler.empty() ? _S"c++" : compiler;
  const string &implem = mgi.implem;
  const string &mainfn = mgi.mainfn;
  const string &pgm_name = mgi.pgm_name;
  const string &objects = mgi.objects;
  const std::array<string,2> cppflags = [this](){
    std::array<string,2> fl{"CPPFLAGS +=",
                            "CPPFLAGS += $(shell pkg-config --cflags gsl++)"};
    for (const auto& i : extra_includes_)
    {
    	fl[0] += " -I" + i;
    	fl[1] += " -I" + i;
    }
    return fl;
  }();

  generate_makefile_header(mgi);

  of << R"(
# Check system configuration)"<<SYS_CONFIG_GSL[get_gsl_config()]<<R"(
ifeq ($(shell which pkg-config),)
HAVE_PKG_CONFIG = false
else
HAVE_PKG_CONFIG = true
endif

# If the libraries are not installed in system folders pkg-config is required
ifeq ($(SYS_INST_LIBS),)
ifeq ($(HAVE_PKG_CONFIG),false)
$(error $(MSG_0))
endif
endif

# Set RPATH_FLAG if gsl is not installed in system folders
RPATH_FLAG =
ifeq ($(HAVE_SYS_GSL),false)
RPATH_FLAG += -Wl,--rpath=$(subst -L,,$(shell pkg-config --libs-only-L gsl))
endif

ifeq ($(HAVE_SYS_GSL),true)
)"<<cppflags[0]<<R"( # Add here more preprocessor flags
else
)"<<cppflags[1]<<R"( # Add here more preprocessor flags
endif

CXX = )"<<cxx<<R"(
# To debug replace -O3 with -g in next 2 lines and remove -s
CXXFLAGS += --std=c++11 -O3 #-Wall
LDFLAGS += -O3 -s $(RPATH_FLAG)
HDRS = )"<<header<<R"(
SRCS = )"<<implem+" "+mainfn<<R"(
OBJS = )"<<objects<<R"( # $(SRCS:.cpp=.o)
ifeq ($(HAVE_PKG_CONFIG),false)
GSL_LIBS = -lgsl -lgslcblas -lm
else
GSL_LIBS = $(shell pkg-config --libs gsl)
endif

# Error messages
MSG_0=pkg-config is required because gsl or gsl++ are not\
installed in system folders
ifeq ($(HAVE_PKG_CONFIG),true)
MSG_1=The libgsl-dev package is required to compile project $(TGT).\
Install it or add the package\'s pkgconfig path to PKG_CONFIG_PATH.
MSG_2=The package libgsl-dev is not valid.
MSG_3=The libgsl++-dev package is required to compile project $(TGT).\
Install it or add the package\'s pkgconfig path to PKG_CONFIG_PATH.
MSG_4=The package libgsl++-dev is not valid.
endif

TGT = )"<<pgm_name<<R"(

all: check_shell $(TGT)

$(TGT) : $(OBJS)
	$(CXX) $(LDFLAGS) -o )"<<pgm_name<<R"( $(OBJS) $(GSL_LIBS)

$(OBJS) : $(HDRS)

.PHONY: clean clear check_shell distclean

check_shell:
	@if ! $(HAVE_SYS_GSL); then\
		if ! $$(pkg-config --exists gsl); then echo $(MSG_1); exit 1; fi;\
		if ! $$(pkg-config --validate --silence-errors gsl);then echo $(MSG_2);exit 2;fi;\
	fi
	@if ! $(HAVE_SYS_GSLPP); then\
		if ! $$(pkg-config --exists gsl++); then echo $(MSG_3); exit 3; fi;\
		if ! $$(pkg-config --validate --silence-errors gsl++);then echo $(MSG_4);exit 4;fi;\
	fi

clear: clean

clean:
	rm -f *.o

distclean: clean
	rm -f )"<<pgm_name<<R"(
)";
}

static const string SYS_CONFIG_PETSC[]{
	R"(
ifeq ($(shell which dpkg),)
$(error dpkg is required for automatic configuration. For manual configuration\
				use the auto_system_config and system_installed_XXX flags)
endif

ifneq ($(shell dpkg -l libpetsc++-dev>/dev/null 2>&1; echo $$?),0)
HAVE_SYS_PETSCPP = false
else
HAVE_SYS_PETSCPP = true
endif

SYS_INST_LIBS = $(findstring true,$(HAVE_SYS_PETSCPP))
)",
	R"(
HAVE_SYS_PETSCPP = false
SYS_INST_LIBS = false
)",
	R"(
HAVE_SYS_PETSCPP = true
SYS_INST_LIBS = true
)"
};

int PDECommon::get_petsc_config() const noexcept
{
  return auto_system_config ? 0 : int(system_installed_petscpp) + 1;
}

void PDECommon::generate_makefile_petsc(MakefileGenerationInfo& mgi)
{
  std::ofstream& of = *mgi.pof;
  const string  header = strip_path(get_header_filename());
  const string &implem = mgi.implem;
  const string &mainfn = mgi.mainfn;
  const string &pgm_name = mgi.pgm_name;
  const string &objects = mgi.objects;
  const string  EXTRA_INCLUDES = [this](){
    string fl = extra_includes_.empty() ? "" : "PETSC_CC_INCLUDES +=";
    for (const auto& i : extra_includes_)
      fl += " -I" + i;
    return fl;
  }();

  generate_makefile_header(mgi);

  of << R"(
# Check system configuration)"<<SYS_CONFIG_PETSC[get_petsc_config()]<<R"(

TGT = )"<<pgm_name<<R"(

all: check_shell $(TGT)

-include $(PETSC_DIR)/lib/petsc/conf/variables
-include $(PETSC_DIR)/lib/petsc/conf/rules

PETSCPP_OBJECTS = main.o print.o vector.o
ifeq ($(HAVE_SYS_PETSCPP),true)
PETSCPP_SRC_DIR = /usr/src/petsc++
else
PETSCPP_SRC_DIR = $(PETSCPP_DIR)/src/petsc++
PETSCPP_INC_DIR = $(PETSCPP_DIR)/include
PETSC_CC_INCLUDES += -I$(PETSCPP_INC_DIR)
endif
)"<<EXTRA_INCLUDES<<R"(
CXXFLAGS += --std=c++11 #-Wall
HDRS = )"<<header<<R"(
SRCS = )"<<implem+" "+mainfn<<R"(
OBJS = $(PETSCPP_OBJECTS) )"<<objects<<R"__( # $(SRCS:.cpp=.o)
X_PATH = $(PETSC_DIR)/$(PETSC_ARCH)/bin

# Error messages
TIT_0 = Incorrect PETSc installation
TIT_1 = Incorrect Petsc++ installation
MSG_1 = PETSc directory $(PETSC_DIR) does not exist.
MSG_2 = $(TIT_0): path $(PETSC_DIR)/lib/petsc/conf/variables does not exist.
MSG_3 = $(TIT_0): path $(PETSC_DIR)/lib/petsc/conf/rules does not exist.
MSG_4 = Petsc++ source directory $(PETSCPP_SRC_DIR) does not exist.
MSG_5 = $(TIT_0): path $(X_PATH) does not exist.
MSG_6 = $(TIT_1): path $(PETSCPP_SRC_DIR) is not a valid Petsc++ source path.
MSG_7 = $(TIT_1): path $(PETSCPP_INC_DIR) is not a valid Petsc++ include path.
WRN_1 = Warning: it appears that $(X_PATH) is not in the path.

.PHONY: check_shell clear distclean

check_shell:
	@if [ -z "$(PETSC_DIR)" -o -z "$(PETSC_ARCH)" ]; then\
		echo Undefined PETSc directory. On the command line use;\
		echo export PETSC_DIR=/my/petsc/path;\
		echo export PETSC_ARCH=my_arch;\
		exit 8;\
	fi
	@if ! $(HAVE_SYS_PETSCPP); then\
		if [ -z "$(PETSCPP_DIR)" -o ! -d "$(PETSCPP_DIR)" ]; then\
			echo Undefined Petsc++ directory. On the command line issue;\
			echo export PETSCPP_DIR=/my/petsc++/path;\
			exit 9;\
		fi;\
		if [ ! -f "$(PETSCPP_INC_DIR)/petsc.hpp" ]; then echo $(MSG_7); exit 7; fi;\
	fi
	@if [ ! -d "$(PETSC_DIR)" ]; then echo $(MSG_1); exit 1; fi
	@if [ ! -f "$(PETSC_DIR)/lib/petsc/conf/variables" ];then echo $(MSG_2);exit 2;fi
	@if [ ! -f "$(PETSC_DIR)/lib/petsc/conf/rules" ];then echo $(MSG_3);exit 3;fi
	@if [ ! -d "$(PETSCPP_SRC_DIR)" ]; then echo $(MSG_4); exit 4; fi
	@if [ ! -f "$(PETSCPP_SRC_DIR)/main.cpp" ]; then echo $(MSG_6); exit 6; fi
	@if [ ! -d "$(X_PATH)" ]; then echo $(MSG_5); exit 5; fi
	@if ! echo ":$(PATH):" | grep -q -e ":$(X_PATH):" -e ":$(X_PATH)/:"; then\
		echo $(WRN_1);\
	fi

$(TGT) : $(OBJS) chkopts
	-$(CLINKER) -o $@ $(OBJS) $(PETSC_LIB)

$(OBJS) : $(HDRS)

vector.o: $(PETSCPP_SRC_DIR)/vector.cpp
	$(PETSC_CXXCOMPILE_SINGLE) $(PETSCPP_SRC_DIR)/vector.cpp

print.o: $(PETSCPP_SRC_DIR)/print.cpp
	$(PETSC_CXXCOMPILE_SINGLE) $(PETSCPP_SRC_DIR)/print.cpp

main.o: $(PETSCPP_SRC_DIR)/main.cpp
	$(PETSC_CXXCOMPILE_SINGLE) $(PETSCPP_SRC_DIR)/main.cpp

clear:
	rm -f *.o

distclean: clear
	rm -f )__"<<pgm_name<<R"(
)";
}

} // namespace fd
