#include <cstdlib>
#include <cmath>
#include <utility>
#include "mesh1d.hpp"
#include "global_config.h"

using std::string;
using std::vector;

static inline int sGetSid(int bid)
{
  ASSERT(bid == 0 || bid == 1 || bid == -1);
  static constexpr int SID[3]{-1, fd1::Mesh::L, fd1::Mesh::R};
  return SID[bid + 1];
}

namespace fd1 {

//============================================================================//
//:::::::::::::::::::::::::::::::  fd1::Mesh  :::::::::::::::::::::::::::::::://
//============================================================================//

Mesh &Mesh::set_boundary(Boundary &bc)
{
  THROW_IF(finalized(), "Too many boundaries.");
  boundaries_.push_back(&bc);
  if (boundaries_.size() == 2) finalize();
  return *this;
}

void Mesh::set_boundaries(std::initializer_list<Boundary::Ref> l)
{ for (auto& ref : l) set_boundary(ref.self); }

void Mesh::finalize()
{
  THROW_IF(!order_boundaries(), "Incorrect specification of boundaries.");
  a = boundaries_[LEFT]->pos();
  b = boundaries_[RIGHT]->pos();
  hx = (b - a)/double(nx - 1);
  finalized_ = true;
}

bool Mesh::order_boundaries()
{
  if (!check_boundaries()) return false;
  if (boundaries_[RIGHT]->pos() < boundaries_[LEFT]->pos())
    std::swap(boundaries_[LEFT], boundaries_[RIGHT]);
  return true;
}

bool Mesh::check_boundaries() const
{
  if (boundaries_.size() != 2) return false;
  const double accur = fd::global_config().space_resolution[0];
  return fabs(boundaries_[1]->pos() - boundaries_[0]->pos()) > accur;
}

int Mesh::get_sid(Boundary *p) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.")
  return sGetSid(index_of(p));
}

int Mesh::get_sid(int bid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.")
  return sGetSid(bid);
}

int Mesh::extremity(Boundary *p) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.")
  return index_of(p);
}

} // namespace fd1
