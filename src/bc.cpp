// #include <cstdlib>
#include <iostream>
#include "global_config.h"
#include "bc.hpp"
#include "expressions.hpp"

using std::string;

namespace fd {

//==============================================================================
//:::::::::::::::::::::::::::::::::  Boundary  :::::::::::::::::::::::::::::::::
//==============================================================================
void Boundary::print() const
{
  static const char AXIS_NAME[3] {'x','y','z'};
  if (type == G)
    std::cout << "Boundary (global)";
  else
    std::cout << "Boundary @ " << AXIS_NAME[type] << " = " << pos();
}

/*static*/
int Boundary::edge_extremity(int sec2d_pri3d, int pri2d_sec3d)
{
  static const int EXTREMITY[6][6]{{-1, 1,-1, 0, 3, 2}, { 1,-1, 0,-1, 3, 2},
                                   {-1, 1,-1, 0, 3, 2}, { 1,-1, 0,-1, 3, 2},
                                   { 1, 3, 0, 2,-1,-1}, { 1, 3, 0, 2,-1,-1}};
  // 2d:
  if (pri2d_sec3d == -1) return Boundary::vertex_extremity(sec2d_pri3d, -1);
  // 3d:
  const int extremity = EXTREMITY[sec2d_pri3d][pri2d_sec3d];
  THROW_IF(extremity == -1, "inconsistent boundaries.");
  return extremity;
}

/*static*/
int Boundary::vertex_extremity(int secondary, int tertiary)
{
  static const int EXTREMITY[6]{1,1,0,0,1,0};
  static const int BTYPE[6]{X, Y, X, Y, Z, Z};
  if (tertiary == -1) /*2d*/ return EXTREMITY[secondary];
  // 3d: The first (primary) bid determines the face of the boundary while the
  // other two determine the boundary's extremities (boundary's boundaries).
  if (BTYPE[secondary] > BTYPE[tertiary]) std::swap(secondary, tertiary);
  return EXTREMITY[secondary] + 2*EXTREMITY[tertiary];
}

//==============================================================================
//::::::::::::::::::::::::::::::::::::  BC  ::::::::::::::::::::::::::::::::::::
//==============================================================================
int NeumannBC::count_ = 0;

void NeumannBC::build_coef(const string& specval, int which)
{
  const auto npos = string::npos;
  THROW_IF(which < 0 || which > 2, "argument \'which\' is out of range.");
  if (specval.empty())
    switch (which)
    {
      case 0: coef_type[0] = ZERO; return;
      case 1: coef_type[1] = ITEM_NOT_PRESENT; return;
      case 2: coef_type[2] = NO_COEF; return;
    }
  if (specval.find(' ') != npos || specval.find('\t') != npos)
  	throw _S"Space in constant or function name in Neumann BC.";
  if (specval == "0")
    switch (which)
    {
      case 0: coef_type[0] = ZERO; return;
      case 1: coef_type[1] = ITEM_NOT_PRESENT; return;
      case 2: throw _S"Coefficient of derivative cannot be 0 in Neumann BCs.";
    }
  if (specval == "1" || specval == "1." || specval == "1.0")
    switch (which)
    {
      // case 0 --> pass through
      case 1:
      case 2: coef_type[which] = NO_COEF; return;
    }
  try
  {
    string::size_type p;
    std::stod(specval, &p);
    if (p != specval.length()) throw _S"Incorrect constant in Neumann BC.";
    coef[which] = "c_NeumannBC_" + fd::to_string(count_++) + "=" + specval;
    coef_type[which] = CONST_VALUE;
  }
  catch (const std::exception&)
  {
    coef[which] = specval;
    coef_type[which] = FUNC;
  }
}

CustomBC& CustomBC::operator=(const Expression& expr)
{
  expression = std::shared_ptr<const Expression>(&expr);
  return *this;
}

CodeFragment
CustomBC::generate_code_fragment(const BoundaryDiscretizationData &bdd) const
{
  return expression->generate_code_boundary(bdd);
}

} // namespace fd
