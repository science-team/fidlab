#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Nonlinear PDE-1d with Dirichlet BCs, No. 4
 *
 *  Features
 *  + Custom BCs.
 *  + Nonlocal BCs.
 *  + Custom initial estimate setting.
 *  + Custom Discretizer.
 *  + Nonlocal (localized) Operator/Discretizer.
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  PDE:
 *  $-u^{\prime\prime}+e^u vu^\prime v^\prime+uv^\prime+uv+v^2=f(x)$
 *  $-uv^{\prime\prime}+(v^2+u)v^\prime+e^v u^2=g(x)$
 *
 *  BCs:
 *  b0: $0$
 *  b1: $1$
 *
 *  Exact solution:
 *  $u(x)=x^2$
 *  $v(x)=x+1$
 *
 *  Boundary discretization method: does not apply (INWARD_DIFF3)
 *  Linear solver: PETSc specified from command line.
 */
)";

namespace fd1 {
std::string code_func_call(const std::string& f, const std::string& i);
}

class PtEvalDiscretizer : public Discretizer
{
public:
  std::string point;
  PtEvalDiscretizer() : point("0") {}
  PtEvalDiscretizer(const std::string& pt) : point(pt) {}

  DECL_DISCRETIZER_DUP();
  virtual CodeFragment discretize(const DiscretizationData& d) const override;
  virtual CodeFragment discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& d) const override;

  SET_OPERATOR_NAME("ptev-operator");
};

// TODO: API replace code_elem_func("e", i, "$c$") --> element_function(i)

CodeFragment PtEvalDiscretizer::discretize(const DiscretizationData &d) const
{
  const std::string &i = point;
  const std::string code_y = "u["   + d.pde->code_elem_func(i) + "]";
  const std::string code_j = "du["  + d.pde->code_elem_func(i) + "]";
  return CodeFragment(code_y, code_j);
}

CodeFragment PtEvalDiscretizer::discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = discretize({bdd.pde, bdd.i, "", ""});
  cf.bid = bdd.bid;
  return cf;
}

// TODO: API replace code_func_call("$f$", point) --> function_call(point)

CodeFragment PtEvalDiscretizer::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const std::string code_y = d.pde->code_func_call("$f$", point, "", "");
  const std::string code_j = "0";
  return CodeFragment(code_y, code_j);
}

IMPLEMENT_DISCRETIZER_DUP(PtEvalDiscretizer);

int main()
{
  try
  {
    // Create localized non-local operator
    PtEvalDiscretizer pteval_discretizer("0");
    BasicOperator PtEval(pteval_discretizer);

    // Create mesh
    Mesh mesh(50);
    Boundary b0(0.), b1(1.);
    mesh.set_boundaries({b0, b1});

    CustomBC bc0u(b0), bc1u(b1), bc0v(b0), bc1v(b1);

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f"), g("g");
    FieldFunction fBC0u("fBC0u", 2), fBC0v("fBC0v", 1);
    Function xs("exact_solution");
    FieldFunction G("G", 2);
//    Operator L("L");

    // Set PDE BCs
    bc0u = v*Dx[u] + fBC0u(u, v);
    bc1u = u - PtEval[v]; // Nonlocal
    bc0v = v + fBC0v(u) - 2;
    bc1v = -Dx[v] + v - 1;

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE1d";
    pde.set_boundary_discretization_method(PDE::/*SYM_DIFF2*/INWARD_DIFF3);

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_BCs({bc1u, bc0u}, u);
    pde.set_BCs({bc1v, bc0v}, v);
    pde.set_functions({f, g, xs});
    pde.set_functions({G, fBC0u, fBC0v});
    pde.set_initial_estimate("c == 0 ? x : 2");
//    pde.set_initial_estimate("c == 0 ? x : (x < .25 ? 1 : 2)");
//    pde.set_initial_estimate("c == 0 ? 1.1*x*x : 1.1+x");
    pde.use_field_names = true;

    xs.set_implementation(R"(
	{
		if (c == 0) return x*x;
		return x+1;
	})");
    f.set_implementation("-2+exp(x*x)*(x+1)*2*x+x*x*(x+2)+(x+1)*(x+1)");
    g.set_implementation("(x+1)*(x+1)+x*x*(1+exp(x+1)*x*x)");

    G.set_implementation({"exp(u)*v",
      /* G_u */ "G(u,v)",
      /* G_v */ "exp(u)"});

    fBC0u.set_implementation({"u*exp(v)",
      /* fBC0u_u */ "exp(v)",
      /* fBC0u_v */ "fBC0u(u,v)"});
    fBC0v.set_implementation({"exp(u)",
      /* fBC0v_u */ "exp(u)"});

    // Part 4: set PDE equations
    pde("u") = -Dxx[u] + G(u,v)*Dx[u]*Dx[v] + u*Dx[v] + u*v + v*v - f;
    pde("v") = -u*Dxx[v] + (v*v+u)*Dx[v] + G(v,u)*u - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
    pde.set_nonlinear_solver("newton");

    // Part 6: output
    pde.executable = "gsl1d_nl_4";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl1d_nl_4");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
