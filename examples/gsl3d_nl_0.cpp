#include <iostream>
#include <fidlab/pde3d.hpp>

using namespace fd;
using namespace fd3;

const char *project_info =
R"(/**
 *  Project: Nonlinear Laplace PDE with mixed Neumann && Dirichlet BCs, No. 0
 * 
 *  PDE:
 *  $u_{xx}+u_{yy}-uu_x=-(2+\cos(x+y))\sin(x+y)$
 * 
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+u=\sin(x+y)-\cos(x+y)$
 *  b3: $u=u_exact$
 *  b2: $u=u_exact$
 *  b4: $u=u_exact$
 * 
 *  Exact solution:
 *  $u=\sin(x+y)$
 * 
 *  Boundary discretization method: all
 *  Noninear solver: GSL newton
 */)";

int main()
{
  try
  {
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
    zBoundary b6(3, {  4,  6, -.5,  .3});

    Mesh mesh({1, 6, -1, 1, 2, 3}, 39, 15, 13);
    mesh.set_boundaries({b0, b1, b6});

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f"), xs("exact_solution"), ie("initial_estimate");
    Function g("g"), g0("g0"), g1("g1"), g6("g6");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4);
//    pde.set_initial_estimate(".7*exact_solution(x, y, z)");

//    NeumannBC bcg(mesh.get_global_boundary(), "", "1", "g");
    DirichletBC bcg(mesh.get_global_boundary(), "g");
    NeumannBC bc0(b0, "", "-2", "g0");
    DirichletBC bc1(b1, "g1"), bc6(b6, "g6");

    pde.set_field(u);
    pde.set_functions({f, g, g0, g1, g6, xs});

    xs.set_implementation("sin(x+y+z)");
    f.set_implementation(" {return -3*sin(x+y+z)-sin(x+y+z)*cos(x+y+z);}");
//    g.set_implementation("cos(x+y+z)+sin(x+y+z)");
    g.set_implementation("sin(x+y+z)");
    g0.set_implementation("cos(x+y+z)-2*sin(x+y+z)");
    g1.set_implementation("sin(x+y+z)");
    g6.set_implementation("sin(x+y+z)");

    // Part 4: set PDE equations
    pde = Dxx[u] + Dyy[u] + Dzz[u] - u*Dx[u] - f;
    pde.set_BCs({bcg, bc0, bc1, bc6});

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_nonlinear_solver("newton");

    // Part 6: output
    pde.executable = "gsl3d_nl_0";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl3d_nl_0");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    exit(1);
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    exit(1);
  }

  return 0;
}
