#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(/**
 *  Project: Nonlinear Laplace PDE with Neumann BCs, No. 10(12)
 *
 *  Similar to petsc2d-nl-5.
 *  Additional features:
 *  + Multiple solutions.
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  PDE:
 *  $u_{xx}+u_{yy}-uu_x=-(2+\cos(x+y))\sin(x+y)$
 *
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+u=\sin(x+y)-\cos(x+y)$
 *  b2: $-\frac{\partial u}{\partial y}+u=\sin(x+y)-\cos(x+y)$
 *  b3: $-\frac{\partial u}{\partial y}+u=\sin(x+y)-\cos(x+y)$
 *  b4: $-\frac{\partial u}{\partial y}+u=\sin(x+y)-\cos(x+y)$
 *
 *  Exact solution:
 *  $u=\sin(x+y)$
 *
 *  Boundary discretization method: tested with INWARD_DIFF3, SYM_DIFF2
 *  Nonlinear solver: PETSC
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.), b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.), b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b3, b2, b4, b1});

    NeumannBC bc1(b1, "-1", "1", "nbc_c1"), bc3(b3, "-1", "1", "nbc_c1"),
              bc2(b2, "-1", "1", "nbc_c1"), bc4(b4, "-1", "1", "nbc_c1");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f");
    Function uex("u_exact"), g("nbc_c1"), xs("exact_solution");
    FieldFunction F("NL", 2, false, false);
    Operator Laplace("Laplace");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";
    pde.set_boundary_discretization_method(PDE::/*INWARD_DIFF3*/SYM_DIFF2);

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc2, bc3, bc4}, u);
    pde.set_functions({f, g, uex, xs});
    pde.set_function(F);

    f.set_implementation("-(2+cos(x+y))*sin(x+y)");
    g.set_implementation("sin(x+y)-cos(x+y)");
    uex.set_implementation("sin(x+y)");
    xs.set_implementation("u_exact(x,y)");

    F.set_implementation({
      /* function of variables u, v          */ "u*v",
      /* partial derivative wrt 1st variable */ "v",
      /* partial derivative wrt 2nd variable */ "u"
    });

    Laplace(v) = Dxx[v] + Dyy[v];

    // Part 4: set PDE equations
    pde = Laplace[u] - F(u, Dx[u]) - f;

    // With this estimate the algorithm converges to another solution.
    pde.set_initial_estimate("x+y");
    // This estimate gives the solution of the default estimate (0).
//    pde.set_initial_estimate(".9*u_exact(x,y)");

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc2d_nl_12";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc2d_nl_12");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
