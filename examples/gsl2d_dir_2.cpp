#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info = R"(/**
 *  @brief Project: Laplace 2-equation system with Dirichlet BCs, No. 2
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $u=u_exact, v=v_exact$
 *  b3: $u=u_exact, v=v_exact$
 *  b2: $u=u_exact, v=v_exact$
 *  b4: $u=u_exact, v=v_exact$
 *
 *  PDE:
 *  \begin{align*}
 *  & 2(u_{xx}+u_{yy})-(v_{xx}+v_{yy})=2(4(x^2+y^2+1)e^{x^2+y^2})+2\sin(x+y) \\
 *  & u_{xx}+u_{yy}+v_{xx}+v_{yy}=4(x^2+y^2+1)e^{x^2+y^2}-2\sin(x+y)
 *  \end{align*}
 *
 *  Exact solution:
 *  $u=e^{x^2+y^2}$
 *  $v=\sin(x+y)$
 *
 *  Boundary discretization method: does not apply.
 *  Linear solver: GSL LU
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.);
    xBoundary b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.);
    yBoundary b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b3, b2, b4, b1});

    DirichletBC bcu1(b1, "bcu"), bcu2(b2, "bcu"), bcu3(b3, "bcu"), bcu4(b4, "bcu");
    DirichletBC bcv1(b1, "bcv"), bcv2(b2, "bcv"), bcv3(b3, "bcv"), bcv4(b4, "bcv");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f"), g("g"), bcu("bcu"), bcv("bcv");
    Function xs("exact_solution"), uex("u_exact"), vex("v_exact");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_BCs({bcu1, bcu2, bcu3, bcu4}, u);
    pde.set_BCs({bcv1, bcv2, bcv3, bcv4}, v);
    pde.set_functions({f, g, bcu, bcv, uex, vex, xs});
    f.set_implementation("4*(x*x+y*y+1)*exp(x*x+y*y)");
    g.set_implementation("-2*sin(x+y)");
    bcu.set_implementation("u_exact(x,y)");
    bcv.set_implementation("v_exact(x,y)");
    uex.set_implementation("exp(x*x+y*y)");
    vex.set_implementation("sin(x+y)");
    xs.set_implementation("c == 0 ? u_exact(x,y) : v_exact(x,y)");

    // Part 4: set PDE equations
    pde("D1") = 2*(Dxx[u] + Dyy[u]) - (Dxx[v] + Dyy[v]) - (2*f - g);
    pde("D2") = Dxx[u] + Dyy[u] + Dxx[v] + Dyy[v] - f - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");

    // Part 6: output
    pde.executable = "gsl2d_dir_2";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/gsl2d_dir_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
