#include <fidlab/pde.hpp>
#include "DxfDx1d.hpp"

using namespace fd1;
using std::string;

IMPLEMENT_DISCRETIZER_DUP(DiscretizerDxfDx);

CodeFragment DiscretizerDxfDx::discretize(const DiscretizationData& d) const
{
  const string &i = d.i;
  const string ip = i + "+1";
  const string im = i + "-1";
  const string ep = d.pde->code_elem_func(ip);
  const string e0 = d.pde->code_elem_func(i );
  const string em = d.pde->code_elem_func(im);
  const string fp = fname + "(X("+i+")+hx/2)";
  const string fm = fname + "(X("+i+")-hx/2)";
  string code_y = "("+fp+"*(u[" + ep + "]-u[" + e0 + "])-"
                     +fm+"*(u[" + e0 + "]-u[" + em + "]))/(hx*hx)";
  string code_j = "("+fp+"*(du[" + ep + "]-du[" + e0 + "])-"
                     +fm+"*(du[" + e0 + "]-du[" + em + "]))/(hx*hx)";
  return CodeFragment(code_y, code_j, CodeFragment::BRACKETS_IF_NOT_LAST,
      CodeFragment::BRACKETS_IF_NOT_LAST);
}
