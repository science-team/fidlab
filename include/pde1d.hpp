#ifndef PDE1D_HPP_INCLUDED
#define PDE1D_HPP_INCLUDED

#include "mesh1d.hpp"
#include "codefrag.h"
#include "expressions.hpp"
#include "pde.hpp"

// =============================================================================
// PDE
// =============================================================================

namespace fd1
{

class PDE : public fd::PDECommon
{
  using CodeFragment = fd::CodeFragment;
  using BoundaryCondition = fd::BoundaryCondition;

  Mesh *mesh_;

public:

  PDE(Mesh *mesh, const std::string& id = std::string("PDE"));
  PDE(Mesh& mesh, const std::string& id = std::string("PDE"));
  virtual ~PDE();

  virtual int nx() const noexcept override {return mesh_->nx;}

  //! Get the sid of the supplied Boundary.
  /*! \sa fd::PDECommon::get_sid(Boundary *). */
  virtual int get_sid(fd::Boundary *b) const override {return mesh_->get_sid(b);}
  //! Get the sid of the supplied Boundary.
  /*! \sa fd::PDECommon::get_sid(int). */
  virtual int get_sid(int bid) const override {return mesh_->get_sid(bid);}
  //! Get the bid of the supplied Boundary.
  /*! \sa fd::PDECommon::get_bid(Boundary *). */
  /*! \sa fd::Mesh::get_bid(Boundary *). */
  virtual int get_bid(fd::Boundary *b) const override {return mesh_->get_bid(b);}

  // ---------------------------------------------------------------------------
  // Equation definition
  // ---------------------------------------------------------------------------
  // The accepting (final) node of an expression
  inline PDE &operator=(const fd::Expression& expression)
  { fd::PDECommon::set_equation(0, expression); return *this; }

  // ---------------------------------------------------------------------------
  // Equation replacement
  // ---------------------------------------------------------------------------
  inline void replace_equation(const fd::Field &u, int i,
                               const std::string& rhs, const std::string& jr)
  { PDECommon::replace_equation(u, i, 0, 0, rhs, jr); }
  inline void replace_equation(const fd::Field &u, int i, double rhs = 0)
  { PDECommon::replace_equation(u, i, 0, 0, rhs); }
  inline void replace_equation(int i, const std::string& r, const std::string& j)
  { PDECommon::replace_equation(i, 0, 0, r, j); }
  inline void replace_equation(int i, double rhs = 0)
  { PDECommon::replace_equation(i, 0, 0, rhs); }

  // ---------------------------------------------------------------------------
  // Customization and options
  // ---------------------------------------------------------------------------
  /** Get boundary discretization method. */
  int get_boundary_discretization_method() const noexcept
  { return boundary_discretization_method_; }
  /** Set boundary discretization method. */
  void set_boundary_discretization_method(int bdm)
  { fd::PDECommon::set_boundary_discretization_method(bdm); }

protected:

  //! Get the supported sids of PDE's Mesh.
  virtual std::vector<int> supported_sids() const noexcept override
  { return std::vector<int>{Mesh::R, Mesh::L}; }
  //! Get the Boundary identifiers (bids) on side \c sid.
  virtual std::vector<int> get_bids_on_side(int sid) const override;

  // ---------------------------------------------------------------------------
  // BC utilities
  // ---------------------------------------------------------------------------
  //! Get BC for component \c cid on boundary with bid \c bid.
  virtual
  BoundaryCondition *get_BC_for_component(int component, int bid) const override
  { return mesh_->get_BC_for_component(bc_, component, bid); }
  /**
   * Finalize BCs before code generation.
   */
  virtual void finalize_BCs() override
  {
    boundary_discretization_method_ == SYM_DIFF2? finalize_BCs_extdom() :
                                                  fd::PDECommon::finalize_BCs();
  }

  // ---------------------------------------------------------------------------
  // Code generation
  // ---------------------------------------------------------------------------
  //! Generate code for the functions on the lhs of the equations to be solved.
  virtual void generate_code_function_body(int what) override;
  // --------------------- Code Generation for GSL -----------------------------
  // -------------------- Code Generation for PETSC ----------------------------
  //! Generate code for the body of the functions.
  /*! Nonlinear PETSc specific. */
  virtual void generate_code_function_body_petsc_nonlinear(int what) override;
  //! Generate constructors in header.
  virtual void generate_header_constructors() override;
  // ----------------------- Formatting Utilities ------------------------------
  //! Generate code for additional user specified includes.
  // void code_extra_headers(int which, int placement, bool endl_at_end = true);
  //! Format constructor comment.
  virtual std::string format_constructor_comment() const override;
  //! Format node.
  virtual std::string format_solution_output_node(int c) const override;
  //! Format coordinate arguments.
  virtual std::string format_solution_output_coord(int c) const override;
  //! Format coordinate arguments.
  virtual std::string format_coord() const override;
};

} // namespace fd1

#endif // PDE1D_HPP_INCLUDED
