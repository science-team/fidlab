#ifndef BC2D_HPP_INCLUDED
#define BC2D_HPP_INCLUDED

#include <cmath>
#include "basedefs.h"
#include "codefrag.h"
#include "bc.hpp"

// =============================================================================
// Boundary -- 2d classes
// =============================================================================

namespace fd2
{

//! A flat boundary whose normal is a vector in the x direction.
class xBoundary : public fd::Boundary
{
	using Boundary = fd::Boundary;

private:
	double y0_, y1_;

public:
	xBoundary() : Boundary(1), y0_(0), y1_(1) {type = Boundary::X;}
	xBoundary(double x, double y0, double y1) :
		Boundary(x), y0_(y0), y1_(y1)	{check(); type = Boundary::X;}
	inline double y0	() const noexcept {return y0_;}
	inline double y1	() const noexcept {return y1_;}

	virtual int dimension() const noexcept override {return 2;}
	virtual void print() const override;

protected:
	//! Check if \c y0_, \c y1_ are distinguishable
	void check() const;
};

//! A boundary whose normal is a vector in the y direction.
class yBoundary : public fd::Boundary
{
	using Boundary = fd::Boundary;

private:
	double x0_, x1_;

public:
	yBoundary() : Boundary(1), x0_(0), x1_(1) {type = Boundary::Y;}
	yBoundary(double y, double x0, double x1) :
			Boundary(y), x0_(x0), x1_(x1) {check(); type = Boundary::Y;}
	inline double x0	() const noexcept {return x0_;}
	inline double x1	() const noexcept {return x1_;}

	virtual int dimension() const noexcept override {return 2;}
	virtual void print() const override;

protected:
	//! Check if \c x0_, \c x1_ are distinguishable
	void check() const;
};

} // namespace fd2

#endif // BC2D_HPP_INCLUDED
