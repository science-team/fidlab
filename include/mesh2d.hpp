#ifndef MESH2D_HPP_INCLUDED
#define MESH2D_HPP_INCLUDED

#include <memory>
#include "mesh.hpp"
#include "bc2d.hpp"

// ===================================================================
// Mesh
// ===================================================================

namespace fd2
{

/** \brief Two-dimensional mesh representation. */
class Mesh : public fd::MeshCommon
{
  using Boundary = fd::Boundary;
  using BoundaryCondition = fd::BoundaryCondition;

//       3       [1]       2
//         +-------------+
//         |             |
//     [2] |             | [0]
//         |             |
//         +-------------+
//       0       [3]       1

public:

  /** Number of knots in each direction */
  int nx, ny;
  /** Solution domain */
  double a, b, c, d;
  /** Mesh spacing */
  double hx, hy;

  /** Mesh constructor. */
  Mesh(int Nx, int Ny) : nx(Nx), ny(Ny) {}
  /** Add boundary to mesh. */
  Mesh &set_boundary(Boundary &b);
  /** Add boundary to mesh. */
  inline Mesh &add_boundary(Boundary &b) {return set_boundary(b);}
  /** Add boundaries to mesh. */
  void set_boundaries(std::initializer_list<Boundary::Ref> l);

  /**
   *  Get boundary ids (\c bid) by side.
   * 
   *  The returned vectors have the same order and orientation with
   *  the original vector of boundaries.
   *  \sa get_bid.
   */
  std::vector<std::vector<int>> get_bids_by_side() const;
  /**
   *  Get boundary ids (\c bid) on side \c sid.
   * 
   *  The returned vectors have the same order and orientation with
   *  the original vector of boundaries.
   *  \param sid Side identifier.
   *  \sa get_bid.
   */
  std::vector<int> get_bids_on_side(int sid) const;

  /** True when mesh has one boundary per side. */
  inline bool simple() const {return boundaries_.size() == 4;}

  /**
   *  Finalize \c Mesh.
   * 
   *  The Mesh is automatically finalized after calling \a set_boundaries.
   *  Usually, you should not have to call this method directly.
   *  \sa set_boundaries.
   */
  void finalize();

  /**
   *  Get the side id (\c sid) of the boundary.
   * 
   *  A side id (\c sid) is one Mesh::R, B etc.
   */
  int get_sid(Boundary *) const;
  /**
   *  Get the side id (\c sid) of the boundary.
   * 
   *  \sa get_bid(Boundary *).
   */
  int get_sid(int bid) const;
  /**
   *  Get extremity of boundary (0 or 1).
   *
   *  Applies to both X and Y boundaries.
   */
  int extremity(int bid) const;
  /**
   *  Get boundary type.
   * 
   *  \param bid Boundary id.
   */
  inline int get_btype(int bid) const {return boundaries_[bid]->type;}
  /**
   *  Get boundary extremities.
   * 
   *  \param bid Boundary id.
   */
  std::array<double,2> get_boundary_extremities(int bid) const;

protected:

  // TODO rename it 'bool order_check_boundaries();'
  /** New version sets solution domain (a, b, c, d) */
  bool order_boundaries();
  /** Get extremity of boundary (0 or 1). Applies to both X and Y boundaries. */
  int get_extremity(const Boundary *p) const;

};


class RectangularMesh : public Mesh
{
  xBoundary xb_[2];
  yBoundary yb_[2];

public:

  RectangularMesh(int nx, int ny) : RectangularMesh(nx, ny, 0, 1, 0, 1) {}
  RectangularMesh(int n, double b, double d) : RectangularMesh(n,n, 0,b,0,d) {}
  RectangularMesh(int nx, int ny, double b, double d) :
      RectangularMesh(nx, ny, 0, b, 0, d) {}
  RectangularMesh(int nx, int ny, double a, double b, double c, double d);
};

} // namespace fd2

#endif // MESH2D_HPP_INCLUDED
