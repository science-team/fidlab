#ifndef FIELD_HPP_INCLUDED
#define FIELD_HPP_INCLUDED

#include <map>
#include "basedefs.h"
#include "idgen.hpp"

namespace fd
{

class Expression; class ExpressionItem; class Function;

// =============================================================================
// Field, VectorField
// =============================================================================

//! Representation of an unknown function of the PDE system.
class Field
{
	static IdGenerator idgen_;
	static std::map<int, std::string> global_field_table_;

public:

	//! Field component identifier.
	/*! For internal use. Do not modify directly. */
	int id;
	//! Field component name.
	std::string name;
	int i_start, i_end, j_start, j_end, k_start, k_end;

	Field() : name("") {init(); register_field();}
	Field(const std::string& cname) : name(cname) {init(); register_field();}
	Field(const Field& r) : id(r.id), name(r.name) {init(r);}
	bool operator==(const Field& r) const {return id == r.id;}

#if !HAVE_STANDALONE_FIELDS
	operator Expression&() const;
#endif // HAVE_STANDALONE_FIELDS
	Expression *create_expression() const;

	std::string get_name() const;
	std::string format_name(bool include_id = true) const;

	static std::string find_global_id(int id);

  // ---------------------------------------------------------------------------
	// Parse binary expressions
  // ---------------------------------------------------------------------------
	Expression& operator+(double a) const;
	Expression& operator+(const Field& b) const;
	Expression& operator+(const Function& b) const;
	Expression& operator+(const Expression& b) const;
	Expression& operator-(double a) const;
	Expression& operator-(const Field& b) const;
	Expression& operator-(const Function& b) const;
	Expression& operator-(const Expression& b) const;
	Expression& operator*(double a) const;
	Expression& operator*(const Field& b) const;
	Expression& operator*(const Function& b) const;
	Expression& operator*(const Expression& b) const;
	Expression& operator/(double a) const;
	Expression& operator/(const Field& b) const;
	Expression& operator/(const Function& b) const;
	Expression& operator/(const Expression& b) const;

  // ---------------------------------------------------------------------------
	// Parse unary expressions
  // ---------------------------------------------------------------------------
	Expression& operator-() const;
	Expression& operator+() const;

protected:

	Expression& generic_operator(const char op) const;

	Expression& generic_operator(double a, const char op) const;
	Expression& generic_operator(const Field& b, const char op) const;
	Expression& generic_operator(const ExpressionItem& b, const char op) const;
	Expression& generic_operator(const Expression& b, const char op) const;

private:

	inline void init0() {i_start = i_end = j_start = j_end = k_start = k_end =-2;}
	void init() {id = idgen_.get_id(); init0();}
	void init(const Field& r)
	{ i_start = r.i_start; i_end = r.i_end; j_start = r.j_start; j_end = r.j_end;
		k_start = r.k_start; k_end = r.k_end; }

	void register_field() const;

};

#if HAVE_STANDALONE_FIELDS
Expression& operator+(double a, const Field &e);
Expression& operator-(double a, const Field &e);
Expression& operator*(double a, const Field &e);
Expression& operator/(double a, const Field &e);
#endif // HAVE_STANDALONE_FIELDS

//! The set of unknown functions (fields) of the PDE system.
/*! \em VectorField is collectivelly the set of all unknown functions
    (the fields). For example, if our PDE system involves the unknown
    functions \c u and \c v, the discretized field will contain their
    values at node points:
    \f[ u_0,\cdots,u_{f-1},v_0,\cdots,v_{f-1} \f] */
class VectorField : public std::vector<Field>
{
public:
  //! VectorField constructor.
	VectorField() {}

	//! Declare new component.
	//!
	//! Allow multiple components with same name. In the generated
	//! code they will be given an individual index (subscript).
	//! \arg cname the name of the component.
	Field new_component(const std::string& cname);

	//! Check if \c cname is a component of the field.
	//! \arg cname the name of the component to check.
	bool is_component(const std::string& cname) const
	{
		return std::find_if(begin(), end(), [cname](const Field& fc) {
			return fc.name == cname;}) != end();
	}

	//! Set field component.
	void set_component(const Field &fc);

	//! Get zero-bazed index of field component.
	int get_component_index(const Field &fc) const;

	//! Get zero-based index of field component.
	int get_component_index(int cid) const;

	//! Get component id for the field component stored at \c index.
	int get_component(int index) const;

	//! Check if component belongs to this field.
	bool find_component(const Field &fc) const;

	//! Get zero-bazed index of component which is multiply defined
	//! (inserted more than once with the same name).
	int get_component_subindex(const Field &fc) const;
};

} // namespace fd

#endif // FIELD_HPP_INCLUDED
