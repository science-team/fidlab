#ifndef PDE2D_HPP_INCLUDED
#define PDE2D_HPP_INCLUDED

// #include <fstream>
#include <array>
// #include <functional>
// #include <map>
#include "mesh2d.hpp"
#include "codefrag.h"
#include "expressions.hpp"
#include "pde.hpp"

// =============================================================================
// PDE
// =============================================================================

namespace fd2
{

class PDE : public fd::PDECommon
{
  using CodeFragment = fd::CodeFragment;
  using BoundaryCondition = fd::BoundaryCondition;

  struct VertexSides {int primary, secondary;};

  struct BCSelection
  {
    int c, vertex, sid;
  };

  Mesh *mesh_;

  const struct
  {
    int vertex;
    PDE::VertexSides sides;
  } default_bc_selections_[4]{
      {Mesh::BL, {Mesh::L, Mesh::B}},
      {Mesh::BR, {Mesh::R, Mesh::B}},
      {Mesh::TR, {Mesh::R, Mesh::T}},
      {Mesh::TL, {Mesh::L, Mesh::T}}
  };
  std::vector<BCSelection> bc_selections_;

public:

  PDE(Mesh *mesh, const std::string& id = std::string("PDE"));
  PDE(Mesh &mesh, const std::string& id = std::string("PDE"));
  virtual ~PDE();

  virtual int nx() const noexcept override {return mesh_->nx;}
  virtual int ny() const noexcept override {return mesh_->ny;}

  //! Get the sid of the supplied Boundary.
  /*! \sa fd::PDECommon::get_sid(Boundary *). */
  virtual int get_sid(fd::Boundary *b) const override {return mesh_->get_sid(b);}
  //! Get the sid of the supplied Boundary.
  /*! \sa fd::PDECommon::get_sid(int). */
  virtual int get_sid(int bid) const override {return mesh_->get_sid(bid);}
  //! Get the bid of the supplied Boundary.
  /*! \sa fd::PDECommon::get_bid(Boundary *). */
  /*! \sa fd2::Mesh::get_bid(Boundary *). */
  virtual int get_bid(fd::Boundary *b) const override {return mesh_->get_bid(b);}
  //! Get type of boundary.
  virtual int btype(int bid) const override {return mesh_->get_btype(bid);}

  // ---------------------------------------------------------------------------
  // Equation definition
  // ---------------------------------------------------------------------------
  // The accepting (final) node of an expression
  inline PDE &operator=(const fd::Expression& expression)
  { fd::PDECommon::set_equation(0, expression); return *this; }

  // ---------------------------------------------------------------------------
  // Equation replacement
  // ---------------------------------------------------------------------------
  inline void replace_equation(const fd::Field &u, int i, int j,
                               const std::string& rhs, const std::string& jr)
  { PDECommon::replace_equation(u, i, j, 0, rhs, jr); }
  inline void replace_equation(const fd::Field &u, int i, int j, double rhs = 0)
  { PDECommon::replace_equation(u, i, j, 0, rhs); }
  inline void replace_equation(int i, int j,
                               const std::string& rhs, const std::string& jrhs)
  { PDECommon::replace_equation(i, j, 0, rhs, jrhs); }
  inline void replace_equation(int i, int j, double rhs = 0)
  { PDECommon::replace_equation(i, j, 0, rhs); }

  // ---------------------------------------------------------------------------
  // Customization and options
  // ---------------------------------------------------------------------------
  //! Select which BC to use on a vertex - single component version.
  void select_bc_for_vertex(int vertex, int bid);
  //! Select which BC to use on a vertex - multidimensional field version.
  void select_bc_for_vertex(const fd::Field& u, int vertex, int bid);
  //! Get vertex BC selection table.
  std::vector<std::array<VertexSides,4>> get_bc_selection_table() const;
  /** Get boundary discretization method. */
  int get_boundary_discretization_method() const noexcept
  { return boundary_discretization_method_; }
  /** Set boundary discretization method. */
  void set_boundary_discretization_method(int bdm)
  { fd::PDECommon::set_boundary_discretization_method(bdm); }

  // ---------------------------------------------------------------------------
  // Output code
  // ---------------------------------------------------------------------------

protected:

  // ---------------------------------------------------------------------------
  // Utilities
  // ---------------------------------------------------------------------------
  static VertexSides get_vertex_bids(const VertexSides&, int vertex,
                                     const std::vector<std::vector<int>>&);
  //! Check if the supplied boundary id belongs to vertex.
  bool check_vertex(int vertex, int bid) const noexcept;
  //! Get the supported sids of PDE's Mesh.
  virtual std::vector<int> supported_sids() const noexcept override
  { return std::vector<int>{Mesh::R, Mesh::T, Mesh::L, Mesh::B}; }
  //! Get the Boundary identifiers (bids) on side \c sid.
  virtual std::vector<int> get_bids_on_side(int sid) const override
  { return mesh_->get_bids_on_side(sid); }

  // ---------------------------------------------------------------------------
  // BC utilities
  // ---------------------------------------------------------------------------
  //! Get BC for component \c cid on boundary with bid \c bid.
  virtual
  BoundaryCondition *get_BC_for_component(int component, int bid) const override
  { return mesh_->get_BC_for_component(bc_, component, bid); }
  /**
   * Finalize BCs before code generation.
   */
  virtual void finalize_BCs() override
  {
    boundary_discretization_method_ == SYM_DIFF2? finalize_BCs_extdom() :
                                                  fd::PDECommon::finalize_BCs();
  }

  // ---------------------------------------------------------------------------
  // Customization and options
  // ---------------------------------------------------------------------------
  void select_bc_for_vertex(int field_index, int vertex, int bid);
  int get_bc_selection_index(int field_index, int vertex) const;

  // ---------------------------------------------------------------------------
  // Code generation
  // ---------------------------------------------------------------------------
  //! Generate additional code for PDE class interface.
  /*! The default 2-d implementation generates additional boundary information
      which is used by the code generator.
      Override in derived classes when additional code has to be inserted. */
  virtual void generate_header_extra() override;
  //! Generate code for the functions on the lhs of the equations to be solved.
  virtual void generate_code_function_body(int what) override;
  // --------------------- Code Generation for GSL -----------------------------
  // -------------------- Code Generation for PETSC ----------------------------
  //! Generate code for the body of the functions.
  /*! Nonlinear PETSc specific code generator. */
  virtual void generate_code_function_body_petsc_nonlinear(int what) override;
  //! Generate code for Neumann BCs when the SYM_DIFF2 switch is active.
  /*! Used internally by \sa generate_code_function_body_petsc_nonlinear(int) */
  void generate_code_function_body_petsc_nonlinear_x(int what);
  //! Generate constructors in header.
  virtual void generate_header_constructors() override;
  // ----------------------- Formatting Utilities ------------------------------
  //! Generate code for additional user specified includes.
  // void code_extra_headers(int which, int placement, bool endl_at_end = true);
  //! Format constructor comment.
  virtual std::string format_constructor_comment() const override;
  //! Format node.
  virtual std::string format_solution_output_node(int c) const override;
  //! Format coordinate arguments.
  virtual std::string format_solution_output_coord(int c) const override;
  //! Format coordinate arguments.
  virtual std::string format_coord() const override;
};

} // namespace fd2

#endif // PDE2D_HPP_INCLUDED
