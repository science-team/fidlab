#ifndef PDE3D_HPP_INCLUDED
#define PDE3D_HPP_INCLUDED

// #include <fstream>
#include <array>
// #include <functional>
// #include <map>
#include "mesh3d.hpp"
#include "codefrag.h"
#include "expressions.hpp"
#include "pde.hpp"

// =============================================================================
// PDE
// =============================================================================

namespace fd3
{

class PDE : public fd::PDECommon
{
  using CodeFragment = fd::CodeFragment;
  using BoundaryCondition = fd::BoundaryCondition;

/* TODO remove
  struct EdgeFaces {int primary, secondary;};
  struct VertexFaces {int primary, secondary, tertiary;};

  struct BCSelection {
    int c, vertex, sid; //!< \c vertex is a vertex or edge
  };

  const struct {
    int vertex;
    PDE::VertexFaces faces;
  } default_vertex_bc_selections_[8]{
      {Mesh::DBL, {Mesh::L, Mesh::B, Mesh::D}},
      {Mesh::DBR, {Mesh::R, Mesh::B, Mesh::D}},
      {Mesh::DTR, {Mesh::R, Mesh::T, Mesh::D}},
      {Mesh::DTL, {Mesh::L, Mesh::T, Mesh::D}},
      {Mesh::UBL, {Mesh::L, Mesh::B, Mesh::U}},
      {Mesh::UBR, {Mesh::R, Mesh::B, Mesh::U}},
      {Mesh::UTR, {Mesh::R, Mesh::T, Mesh::U}},
      {Mesh::UTL, {Mesh::L, Mesh::T, Mesh::U}}
  };
  std::vector<BCSelection> vertex_bc_selections_;

  const struct {
    int edge;
    PDE::EdgeFaces faces;
  } default_edge_bc_selections_[12]{
      {Mesh::BL, {Mesh::L, Mesh::B}},
      {Mesh::BR, {Mesh::R, Mesh::B}},
      {Mesh::TR, {Mesh::R, Mesh::T}},
      {Mesh::TL, {Mesh::L, Mesh::T}},
      {Mesh::UB, {Mesh::B, Mesh::U}},
      {Mesh::UR, {Mesh::R, Mesh::U}},
      {Mesh::UT, {Mesh::T, Mesh::U}},
      {Mesh::UL, {Mesh::L, Mesh::U}},
      {Mesh::DB, {Mesh::B, Mesh::D}},
      {Mesh::DR, {Mesh::R, Mesh::D}},
      {Mesh::DT, {Mesh::T, Mesh::D}},
      {Mesh::DL, {Mesh::L, Mesh::D}}
  };
  std::vector<BCSelection> edge_bc_selections_;*/

protected:

  struct BoundarySideInfo
  {
    int bid;
    int eid;
    fd::Axis axis;
    fd::Interval interval;

    bool operator==(const BoundarySideInfo &r) const noexcept
    { return bid == r.bid && eid == r.eid; }
  };

  Mesh *mesh_;
  std::vector<BoundarySideInfo> boundary_sides_;

public:

  PDE(Mesh &mesh, const std::string& id = std::string("PDE"));
  PDE(Mesh *mesh, const std::string& id = std::string("PDE")); // DEPRECATED
  virtual ~PDE();

  virtual int nx() const noexcept override {return mesh_->nx;}
  virtual int ny() const noexcept override {return mesh_->ny;}
  virtual int nz() const noexcept override {return mesh_->nz;}

  /** Get the sid of the supplied Boundary.
   *
   * \sa fd::PDECommon::get_sid(Boundary *). */
  virtual int get_sid(fd::Boundary *b) const override {return mesh_->get_sid(b);}
  /** Get the sid of the supplied Boundary.
   *
   * \sa fd::PDECommon::get_sid(int). */
  virtual int get_sid(int bid) const override {return mesh_->get_sid(bid);}
  /** Get the bid of the supplied Boundary.
   *
   * \sa fd::PDECommon::get_bid(Boundary *).
   * \sa fd2::Mesh::get_bid(Boundary *). */
  virtual int get_bid(fd::Boundary *b) const override {return mesh_->get_bid(b);}
  /** Get type of boundary. */
  virtual int btype(int bid) const override {return mesh_->get_btype(bid);}
  /**
   *  Get the boundary identifier (bid) of the global boundary.
   *
   *  This method must be overriden in all derived classes. 
   */
  virtual int get_global_bid() const override {return mesh_->get_global_bid();}
  /**
   *  Get the supported sids of PDE's Mesh.
   */
  virtual std::vector<int> supported_sids() const noexcept override
  { return std::vector<int>{Mesh::R,Mesh::T,Mesh::L,Mesh::B,Mesh::U,Mesh::D}; }
  /**
   *  Get the Boundary identifiers (bids) on side \c sid.
   */
  virtual std::vector<int> get_bids_on_side(int sid) const override
  { return mesh_->get_bids_on_side(sid); }

protected:

  /**
   *  Get the bids of the 3 most important boundaries containing the vertex.
   */
  std::array<int,3> get_vertex_bids(int vid) const;
  /**
   *  Get the bids of the 3 most important boundaries containing the vertex.
   * 
   *  This method assumes that \c start_from_bid is one of the boundaries
   *  comprising the vertex. It can be used only from a code generator.
   */
  std::array<int,3> get_vertex_bids(int vertex, int start_from_bid) const;
  /**
   *  Check if the supplied boundary id belongs to vertex.
   */
  bool check_vertex(int vertex, int bid) const noexcept;
  /** Get point of vertex \c vid. */
  fd::Point vertex_point(int vid) const noexcept;
  /** Get edge SIDs. */
  static const int *get_edge_fids(int eid) noexcept;
  /** Get vertex SIDs. */
  static const int *get_vertex_fids(int vid) noexcept;
  /**
   * Fill in a \c VertexParts structure.
   *
   * This method is similar to \sa static const int *get_vertex_fids(int vid).
   * The returned structure can be subsequently used in
   * \sa boundary_vertex_extremity(const fd::VertexParts&). 
   */
  fd::VertexParts get_vertex_parts(int vid, int bid) const;
  /** Get boundary side information. */
  std::array<BoundarySideInfo,2> get_boundary_side_info(int bid) const;

public:

  // ---------------------------------------------------------------------------
  // Equation definition
  // ---------------------------------------------------------------------------
  // The accepting (final) node of an expression
  inline PDE &operator=(const fd::Expression& expression)
  { fd::PDECommon::set_equation(0, expression); return *this; }

  // ---------------------------------------------------------------------------
  // Customization and options
  // ---------------------------------------------------------------------------
  /** Get boundary discretization method. */
  int get_boundary_discretization_method() const noexcept
  { return boundary_discretization_method_; }
  /** Set boundary discretization method. */
  void set_boundary_discretization_method(int bdm);

  // ---------------------------------------------------------------------------
  // Output code
  // ---------------------------------------------------------------------------

protected:

  // ---------------------------------------------------------------------------
  // Utilities
  // ---------------------------------------------------------------------------

  // ---------------------------------------------------------------------------
  // BC utilities
  // ---------------------------------------------------------------------------
  /** Get BC for component \c cid on boundary with bid \c bid. */
  virtual
  BoundaryCondition *get_BC_for_component(int component, int bid) const override
  { return mesh_->get_BC_for_component(bc_, component, bid); }

  // ---------------------------------------------------------------------------
  // Customization and options
  // ---------------------------------------------------------------------------

  // ---------------------------------------------------------------------------
  // Code generation
  // ---------------------------------------------------------------------------
  /**
   * Initialize the code generation process.
   *
   * It is used by \sa generate_code().
   * \sa fd::PDECommon::initialize_code_generator()
   */
  virtual void initialize_code_generator();
  /**
   * Finalize the code generation process.
   *
   * It is used by \sa generate_code().
   * \sa fd::PDECommon::terminate_code_generator()
   */
  virtual void terminate_code_generator();
  /**
   * Generate additional code for PDE class instance initializer.
   *
   * This method provides additional initialization (vertices, faces etc) for
   * the 3d implementation.
   * Override in derived classes to modify the provided behavior, or when even
   * more code is necessary.
   */
  virtual void generate_header_init_extra() override;
  /**
   * Generate additional code for PDE class interface.
   *
   * The default 2-d implementation generates additional boundary information
   * which is used by the code generator.
   * Override in derived classes when additional code has to be inserted.
   */
  virtual void generate_header_extra() override;
  /**
   * Generate code for additional definitions inside the PDE class.
   *
   * This override generates definitions of nested classes used subsequently
   * in \sa generate_header_extra().
   */
  virtual void generate_header_extra_idefs() override;
  /**
   * Generate additional code for the PDE class.
   *
   * This override generates implementation code for the PDE methods
   * set_bside_info() and set_brect_info().
   */
  virtual void generate_code_class_extra() override;
  /**
   * Generate Βoundary Side Info (bsi)
   * 
   * Part of \sa generate_code_class_extra()
   */
  void generate_code_class_set_bsi();
  /**
   * Generate Βoundary Rectangle Info (bri)
   * 
   * Part of \sa generate_code_class_extra()
   */
  void generate_code_class_set_bri();
  /**
   * Generate the final discretized system of equations to be solved. 
   * 
   * The discretized system of equations to be solved is a set of equations
   * having the form f(x) = 0, where f and x are multivariate. This method
   * generates code for these functions and/or their Jacobians according to
   * the parameter \c what.
   * 
   * \param what select the kind of code to be generated; one of the
   *             \c GENERATE_* options.
   */
  virtual void generate_code_function_body(int what) override;
  /**
   * Generate function code for the global boundary. 
   * 
   * This method is called from
   * \sa generate_code_function_body(int,std::function<void(int,int,int,int)>).
   */
  void generate_code_function_body_global_boundary(int what, int ntabs,
                               std::function<void(int,int)> codegen);
  /**
   * Generate code for faces
   *
   * This method is called from \sa generate_code_function_body.
   */
  virtual void generate_code_face(int what, int bid, int ntabs);
  /**
   * Generate code for edges
   *
   * This method is called from \sa generate_code_function_body.
   */
  virtual void generate_code_edge(int what, int eid, int bid, int fid2, int ntabs);
  /**
   * Generate code for vertices
   *
   * This method is called from \sa generate_code_function_body.
   */
  virtual void generate_code_vertex(int what, int vid, int bid, int ntabs);
  /**
   * Generate code for virtual points.
   *
   * Called internally by \sa generate_code_function_body.
   * Default implementation does nothing.
   * \param what \sa generate_code_function_body
   */
  virtual void generate_code_virtual_points(int what) {}
  // --------------------- Code Generation for GSL -----------------------------
  // -------------------- Code Generation for PETSC ----------------------------
  /**
   * Generate code for the body of the functions.
   *
   * Nonlinear PETSc specific code generator.
   */
  virtual void generate_code_function_body_petsc_nonlinear(int what) override;
  /** Generate constructors in header. */
  virtual void generate_header_constructors() override;
  // ----------------------- Formatting Utilities ------------------------------
  /* * Generate code for additional user specified includes. */
  // void code_extra_headers(int which, int placement, bool endl_at_end = true);
  /** Format constructor comment. */
  virtual std::string format_constructor_comment() const override;
  /** Format node. */
  virtual std::string format_solution_output_node(int c) const override;
  /** Format coordinate arguments. */
  virtual std::string format_solution_output_coord(int c) const override;
  /** Format coordinate arguments. */
  virtual std::string format_coord() const override;
};

} // namespace fd3

#endif // PDE3D_HPP_INCLUDED
