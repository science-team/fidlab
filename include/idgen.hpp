#ifndef IDGEN_HPP_INCLUDED
#define IDGEN_HPP_INCLUDED

#include <vector>

class IdGenerator
{
  const int _startFrom;
  int _firstAvailable;
  const bool _reuse;
  std::vector<int> _repository;

public:
  IdGenerator(int from = 1, bool reuse = true) :
  	_startFrom(from),
  	_firstAvailable(from),
  	_reuse(reuse) {}
  int get_id()
  {
    if (_repository.size() == 0) return _firstAvailable++;
    int r = _repository.back();
    _repository.pop_back();
    return r;
  }
  void free(int id) {if (reuse() && in_use(id)) _repository.push_back(id);}
  bool in_use(int id) const {return !in_repository(id) && in_bounds(id);}
  bool reuse() const {return _reuse;}

private:
  bool in_bounds(int id) const
  { return id >= _startFrom && id < _firstAvailable; }
  bool in_repository(int val) const
  {
    for (int i : _repository)
      if (i == val) return true;
    return false;
  }
};

#endif // IDGEN_HPP_INCLUDED
