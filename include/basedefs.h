#ifndef BASEDEFS_H_INCLUDED
#define BASEDEFS_H_INCLUDED

#include <vector>
#include <string>
#include <algorithm>
#include <array>
#if __cplusplus < 201103L
# include <cstdio>
#endif // __cplusplus
#include <functional>

namespace fd {

#if !defined(ASSERT)
# if defined(__DEBUG__)
#   define ASSERT(b) if(!(b)) throw \
      std::string("Assertion failed in file " __FILE__ " line ") \
      + std::to_string(__LINE__) + ": " #b
# else
#   define ASSERT(b)
# endif
#endif

#if !defined(ASSERT_MSG)
# if defined(__DEBUG__)
#   define ASSERT_MSG(b,msg) if (!(b)) throw std::string(msg)
# else
#   define ASSERT_MSG(b,msg)
# endif
#endif

#if !defined(ASSERT_NOT_NULL)
# if defined(__DEBUG__)
#   define ASSERT_NOT_NULL(b) \
      if ((b) == 0)	throw \
        std::string("Null expression in file " __FILE__ " line " \
        + std::to_string(__LINE__) + ": " #b
# else
#   define ASSERT_NOT_NULL(b)
# endif
#endif

#define THROW(s) \
  throw std::string(__PRETTY_FUNCTION__)+": "+(s);

#define THROW_IF(b, s) \
  if (b) throw std::string(__PRETTY_FUNCTION__)+": "+(s);

#define THROW_IF_NOT(b, s) \
  if (!(b)) throw std::string(__PRETTY_FUNCTION__)+": "+(s);

#if !defined(DEBUG_LOG)
# if defined(__DEBUG__)
#   define DEBUG_LOG(...) printf(__VA_ARGS__)
# else
#   define DEBUG_LOG(...)
# endif
#endif

#define _S (std::string)
#define EMPTY std::string("")

using IntPair = std::array<int, 2>;
using IntTriple = std::array<int, 3>;

inline IntPair pair(const int a[2]) {return std::array<int,2>{a[0],a[1]};}
inline IntTriple triple(const int a[3]) {return std::array<int,3>{a[0],a[1],a[2]};}

template<typename T, int N>
using aref1 = T (&)[N];
template<typename T, int M, int N>
using aref2 = T (&)[M][N];

template<typename T, int N>
aref1<T,N> arrcpy(T (&a)[N], const T (&b)[N])
{
  for (int i = 0; i < N; ++i)
    a[i] = b[i];
  return a;
}

template<typename T, int M, int N>
aref2<T,M,N> arrcpy(T (&a)[M][N], const T (&b)[M][N])
{
  for (int i = 0; i < M; ++i)
    for (int j = 0; j < N; ++j)
      a[i][j] = b[i][j];
  return a;
}

template<typename T>
int index(const std::vector<T>& v, const T& r)
{
  for (int i = 0; i < (int)v.size(); ++i)
    if (v[i] == r) return i;
  return -1;
}

template<typename T>
int rindex(const std::vector<T>& v, const T& r)
{
  for (int i = (int)v.size() - 1; i > -1 ; --i)
    if (v[i] == r) return i;
  return -1;
}

template<typename T>
bool find(const std::vector<T>& v, const T& r)
{ return std::find(v.begin(), v.end(), r) != v.end(); }

template<typename T>
std::string to_string(T val)
{
# if __cplusplus < 201103L
  return "?error fd::to_string?";
# else
  return std::to_string((T)val);
# endif // __cplusplus
}

# if __cplusplus < 201103L
template<> std::string to_string<int>(int val);
template<> std::string to_string<long>(long val);
template<> std::string to_string<unsigned>(unsigned val);
template<> std::string to_string<unsigned long>(unsigned long val);
# endif // __cplusplus
template<> std::string to_string<double>(double val);

template<typename T>
std::string to_string_empty_if(bool condition, T val)
{ return condition ? std::string("") : to_string(val); }

void replace_all(std::string& s, const std::string& from, const std::string& to);

std::string strip_front(const std::string&, std::function<bool(const char)>);
std::string strip_back(const std::string&, std::function<bool(const char)>);
std::string strip_all(const std::string&, std::function<bool(const char)>);

std::string strip_space_front(const std::string&);
std::string strip_space_back(const std::string&);
std::string strip_space(const std::string&);

int parse_int(const std::string& num, const std::string& prefix);
bool regex_match_funcname(const std::string& fname);

bool file_exists(const std::string& name);
void backup_old_file(const std::string& filename);

bool is_zero(double);

void log(const std::string& msg);

} // namespace fd

#endif // BASEDEFS_H_INCLUDED
