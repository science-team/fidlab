#ifndef GLOBAL_CONFIG_H_INCLUDED
#define GLOBAL_CONFIG_H_INCLUDED

#include <string>

namespace fd {

enum ErrorLevel {NOT_AN_ERROR, NOTE, WARNING, EXCEPTION};

struct GlobalConfig
{
  //! Parameter used in formatting function implementations.
  /*! If the function's implementation requires formatting, the formatter
   *! keeps it in the line of the function's declaration IF its length does
   *! not exceed this parameter. To turn off this kind of formatting, set
   *! \c max_length_inline_impl to a small number, eg 0 or 1.
   */
  int max_length_inline_impl;

  //! Space resolution (accuracy) in the 3 space directions.
  double space_resolution[3];

  //! Area resolution (accuracy).
  /*! When it is negative the system selects the most appropriate
   *! value depending on the values of \c space_resolution. */
  double area_resolution;

  //! Maximum number of backup files for each particular source.
  int max_backup_files;

  //! Indicates how to handle missing function implementation.
  ErrorLevel missing_function_impl;

  //! Indicates how to handle missing field function implementation.
  ErrorLevel missing_field_function_impl;

  //! Numerical differentiation accuracy.
  double num_diff_h;

  //! Numerical differentiation zero.
  double num_diff_zero;

  //! Format function implementation.
  /*! If the first non-blank character is an opening brace or the start of a
   *! comment, the supplied implementation is copied verbatim to the output
   *! header file. Otherwise, the formatting routine strips any leading and
   *! trailing space, adds the \c return keyword and a terminating semicolon,
   *! and encloses the code in braces.
   *! This method is used internally by the system.
   */
  void format_function_implementation(std::string& imp) const;
};

GlobalConfig& global_config() noexcept;

} // namespace fd

#endif // GLOBAL_CONFIG_H_INCLUDED
