#ifndef PDE_HPP_INCLUDED
#define PDE_HPP_INCLUDED

#include <fstream>
// #include <array>
#include <tuple>
#include <functional>
#include <map>
#include "bc.hpp"
#include "codefrag.h"
#include "expressions.hpp"
#include "basedefs.h"

//============================================================================//
//:::::::::::::::::::::::::::::::  PDECommon  :::::::::::::::::::::::::::::::://
//============================================================================//

namespace fd
{

/**
 * Information passed to makefile generators.
 */
struct MakefileGenerationInfo
{
  std::ofstream *pof;
  std::string implem;
  std::string mainfn;
  std::string pgm_name;
  std::string objects;
};

/**
 * The parts of a vertex.
 * 
 * The primary \c bid and the faces, in 3d, edges in 2d, of a vertex.
 * In 2d the tertiary fid, \c fids[2], is always -1.
 * Never fill-in this structure manually; instead, use the method
 * \sa get_vertex_parts(int, int).
 */
struct VertexParts
{
  int bid;
  int fids[3];
};

/**
 * PDECommon implements the features common to all PDE classes.
 */
class PDECommon
{

  struct EquationReplacement
  {
    int index, i, j, k;
    double value;
    std::string rhs, jac;
  private:
    PDECommon *pde_;
    bool initialized_;
  public:
    EquationReplacement(int fi, int i, int j, int k, double val,
                        const std::string& rv, const std::string& jv,
                        PDECommon *);
    bool equation_is_identical_to(const EquationReplacement& r) const noexcept
    { return index == r.index && i == r.i && j == r.j && k == r.k; }
    inline const std::string& rhs_value() noexcept {init(); return rhs;}
    inline const std::string& rhs_jac  () noexcept {init(); return jac;}
  private:
    void init();
  };

protected:

  //--------------------------------------------------------------------------//
  //::::::::::::::::::::::::::::  Protected data  :::::::::::::::::::::::::::://
  //--------------------------------------------------------------------------//
  //! Space dimension.
  int dimension_{0};

  //! The unknown field(s).
  VectorField field_;
  /**
   *  Vector of BoundaryConditions (pointers NOT owned by PDECommon).
   *
   *  The BoundaryCondition pointers contained in \c bc_ are owned by
   *  \c PDECommon and have a valid (> -1) component id. However, the
   *  originally supplied \c BoundaryCondition 's are NOT owned. Instead,
   *  They are copied by the \c set_BC() method.
   *  \sa set_BC
   *  \sa set_BCs
   */
  std::vector<BoundaryCondition *> bc_;
  //! Functions used by the PDE.
  std::vector<Function *> functions_;
  //! Field functions used by the PDE.
  std::vector<FieldFunction *> field_functions_;
  //! Cut-off functions used by the PDE.
  std::vector<CutoffFunction *> cutoff_functions_;
  //! Generated code for each equation.
  std::vector<CodeFragment> equation_codes_;
  //! Equation name(s).
  std::vector<std::string> equation_names_;
  //! Equation replacements.
  std::vector<EquationReplacement> replacements_;

  bool decl_finished_{false};
  bool defs_finished_{false};
  bool BCs_finished_{false};
  bool eqs_finished_{false};
  bool finalized_{false};

  //! Names of node coordinates.
  std::string i_{"i"}, j_{"j"}, k_{"k"};

  //===============[ MODULE 0: Input/Output -- PROTECTED DATA ]===============//
  //! Name of source file to place main function.
  /*! Default is <name>_main.cpp. Set to "*" to have the main function
      output in the class's implementation file. */
  std::string program_{""};
  //! Name of class's implementation file.
  std::string code_file_;
  //! Name of class's interface (include) file.
  std::string header_file_;
  std::ofstream code_;
  std::ofstream code_h_;

  //! Message collector.
  std::vector<std::string> log_;

  //! Initial estimate implementation
  /*! Implementation of the function that will be used as the
   *  initial estimate for the solution. */
  std::string init_estimate_imp_;
  //! Library selection -- GSL
  int library_{GSL};
  //! Matrix method selection -- GSL
  int matrix_method_{LU};
  //! Nonlinear solver selection -- GSL
  int nonlinear_solver_{DNEWTON};
  //! Boundary discretization method.
  int boundary_discretization_method_{INWARD_DIFF3};

  //! Libraries -- GSL
  /*! Possible values: "gsl", "Petsc" */
  std::map<int, const std::string> libs_{
                        {GSL,   "gsl"},
                        {PETSC, "Petsc"}
  };
  //! Matrix methods -- GSL
  /*! Possible values: "LU", "QR", "QRP", "householder", "GMRES" */
  std::map<int, const std::string> matrix_methods_{
                        {LU,  "LU"},
                        {QR,  "QR"},
                        {QRP, "QRP"},
                        {HOUSEHOLDER, "householder"},
                        {GMRES, "GMRES"}
  };
  /**
   *  Nonlinear solvers -- GSL
   *
   *  Possible values: "hybrids", "hybrid", "dnewton", "broyden", "hybridsj",
   *  "hybridj", "newton", "gnewton"
   */
  std::map<int, const std::string> nonlinear_solvers_{
                        {HYBRIDS, "hybrids"},
                        {HYBRID, "hybrid"},
                        {DNEWTON, "dnewton"},
                        {BROYDEN, "broyden"},
                        {HYBRIDSJ, "hybridsj"},
                        {HYBRIDJ, "hybridj"},
                        {NEWTON, "newton"},
                        {GNEWTON, "gnewton"}
  };
  //! Reserved function names
  std::vector<std::string> reserved_function_names_{
                        "F", "J", ""
  };

  //===========[ MODULE 1: Makefile generation -- PROTECTED DATA ]============//
  std::vector<std::string>         extra_includes_;
  std::string                      extra_make_defs_;

  //======================[ MODULES 2: PROTECTED DATA ]=======================//
  std::map<const std::string, int> extra_headers_def_;
  std::map<const std::string, int> extra_headers_imp_;
  std::map<const std::string, int> extra_headers_main_;

  /** Proxy object used to set equation's expression. */
  struct Ref
  {
    PDECommon *pde;
    std::string eqname;
    const int eqnum{-1};
    BoundaryCondition *bc{nullptr};
    Ref(PDECommon *self, const std::string& s) : pde(self), eqname(s) {}
    Ref(PDECommon *self, int e) : pde(self), eqnum(e) {}
    Ref(PDECommon *self, BoundaryCondition *b) : pde(self), bc(b) {}
    PDECommon& operator=(const Expression& expr);
  };

  /**
   * Code generation flags.
   *
   * Apply only to Neumann BCs, on boundary extremities and when
   * \c SYM_DIFF2 is in effect.
   *
   * When all BCs on a boundary are Neumann, the PDE code on that boundary is
   * required to define a solvable system.
   */
  enum {ORDINARY_BC_CODE = 0x01, /**< Std Neumann BC code of primary boundary */
        EXTRA_BC_CODE = 0x02,    /**< Additional PDE code possibly required */
        SECONDARY_BC_CODE = 0x04,/**< Dirichlet/custom BC of secondary boundary*/
        TERTIARY_BC_CODE = 0x08, /**< Dirichlet/custom BC of tertiary boundary*/
        ALL_BC_CODES_2D = 0x07,  /**< Mask for 2d flags */
        ALL_BC_CODES = 0x0f      /**< Mask for 3d flags */
  };
  enum {GENERATE_CLASS_CODE, GENERATE_FUNCTION_CODE, GENERATE_JACOBIAN_CODE,
        GENERATE_BOTH};
  enum {NODE_NUMBERS = 0, DOMAIN_EXTREMITIES = 1, MESH_SPACING = 2};

public:

  //! Numerical platform
  enum {GSL, PETSC};
  //! Matrix method, GSL
  enum {LU, QR, QRP, HOUSEHOLDER, GMRES};
  //! Nonlinear solver, GSL
  enum {HYBRIDS,  HYBRID,  DNEWTON, BROYDEN, /* --> multiroot::fsolver */
        HYBRIDSJ, HYBRIDJ, NEWTON,  GNEWTON  /* --> multiroot::fdfsolver */};
  //! Generated code file identifier
  enum {HEADER_FILE, IMP_FILE, MAIN_FILE};
  //! Placement
  enum {PLACEMENT_TOP, PLACEMENT_BEFORE_LIB,
        PLACEMENT_BEFORE_HDR = PLACEMENT_BEFORE_LIB, PLACEMENT_LAST};
  //! Boundary discretization methods
  enum {SYM_DIFF2, BOUNDARY_EXTENSION = SYM_DIFF2,
        INWARD_DIFF3, INWARD_DIFF4, INWARD_DIFF5};

  // ---------------------------------------------------------------------------
  // Public data
  // ---------------------------------------------------------------------------
  //! PDE identifier.
  std::string name;

  //! Name of executable to be produced.
  std::string executable;

  //! Project information/documentation.
  /*! Default generator copies verbatim this information to the file
   *  containing the main function. */
  std::string project_info;

  //! Do not generate code for the Jacobian matrix (differential).
  bool no_jacobian_code{false};

  //! Generate code to make field names accessible.
  /*! The field names are accessed through the function \c field_name().
      This function is available only for multidimentional fields. */
  bool use_field_names{false};

  //! Compiler to use for the compilation.
  /*! Default is c++. It must support the comand-line interface of c++. */
  std::string compiler;

  //================[ MODULE 1: Makefile generation -- DATA ]=================//

  //! Automatically detect system configuration.
  /*! This flag affects makefile generation. */
  bool auto_system_config = true;
  //! Assume gsl library is installed in system directories.
  /*! Affects makefile generation. Effective when #auto_system_config
   *  is \c true. */
  bool system_installed_gsl = true;
  //! Assume gsl++ library is installed in system directories.
  /*! For makefile generation it is effective when #auto_system_config
   *  is \c true. */
  bool system_installed_gslpp = true;
  //! Assume petsc++ library is installed in system directories.
  /*! For makefile generation it is effective when #auto_system_config
   *  is \c true. */
  bool system_installed_petscpp = true;

protected:

  PDECommon(const std::string& pde_id = std::string("PDE")) : name(pde_id) {}

public:

  virtual ~PDECommon();

  inline int dimension()  const noexcept {return dimension_;}
  inline int field_dim () const {return (int)field_.size();}
  inline int components() const {return field_dim();}
  inline int equations () const {return (int)equation_codes_.size();}
  //! Return mesh size (number of nodes) in the x-direction.
  virtual int nx() const noexcept {return 0;}
  //! Return mesh size (number of nodes) in the y-direction.
  virtual int ny() const noexcept {return 0;}
  //! Return mesh size (number of nodes) in the z-direction.
  virtual int nz() const noexcept {return 0;}

  // ---------------------------------------------------------------------------
  // Equation definition
  // ---------------------------------------------------------------------------
  // The accepting (final) node of an expression
  inline PDECommon &operator=(const Expression& expression)
  { return set_equation(0, expression); }

  //! Return proxy object Ref to set equation's expression.
  /*! \sa Ref */
  Ref operator()(const std::string& equation_name)
  { return Ref(this, equation_name); }

  //! Return proxy object Ref to set equation's expression.
  /*! \sa Ref */
  Ref operator()(int equation_no)
  { return Ref(this, equation_no); }

#if 0 // To be enabled later
  Ref operator()(const BoundaryCondition& bc)
  { return Ref(this, const_cast<BoundaryCondition *>(&bc)); }
#endif // 0

  //! Set equation's expression.
  /*! Alias of \sa set_differential_expression() */
  inline PDECommon &set_equation(const std::string& s, const Expression& expr)
  { return set_differential_expression(s, expr); }

  //! Set equation's expression.
  /*! Alias of \sa set_differential_expression() */
  inline PDECommon &set_equation(int n, const Expression& expr)
  {
    if (n < 0) throw _S"Incorrect equation number.";
    return set_differential_expression(fd::to_string(n), expr);
  }

  //! Set equation's expression.
  /*! Equations have the form expression = 0. This method sets the equation's
   *  lhs. */
  PDECommon &set_differential_expression(const std::string&, const Expression&);
  //! Get the n-th equation's name.
  const std::string& get_equation_name(int n) const;
  //! Get the name of the PDE or system.
  /*! When \c name is empty a default name is synthesized. */
  const std::string get_pde_system_name() const;

  // ---------------------------------------------------------------------------
  // Equation replacement
  // ---------------------------------------------------------------------------
  void replace_equation(const Field &u, int i, int j, int k,
                        const std::string& rhs, const std::string& jr);
  void replace_equation(const Field &u, int i, int j, int k, double rhs = 0);
  void replace_equation(int i, int j, int k,
                        const std::string& rhs, const std::string& jrhs);
  void replace_equation(int i, int j, int k, double rhs = 0);
  //! Check if equation replacements are present.
  bool replace_equations() const {return !replacements_.empty();}

  // ---------------------------------------------------------------------------
  // PDE properties
  // ---------------------------------------------------------------------------
  bool linear_BCs() const;
  bool linear_eqs() const;
  bool linear() const {return linear_eqs() && linear_BCs();}

  // ---------------------------------------------------------------------------
  // Field component manipulation
  // ---------------------------------------------------------------------------
  //! Set field component.
  void set_field(const Field &fc);

  //! Set field components.
  void set_fields(std::initializer_list<const Field> l);

  //! Get zero-based index of field component.
  int get_component_index(const Field &fc) const;

  //! Get zero-based index of field component.
  int get_component_index(int cid) const;

  //! Get component id for the field component stored at \c index.
  int get_component(int index) const;

  //! Check if component field belongs to this PDE.
  bool find_component(const Field &fc) const;

  //! Get zero-based index of component which is multiply defined
  //! (inserted more than once with the same name).
  int get_component_subindex(const Field &fc) const;

  // ---------------------------------------------------------------------------
  // BC definition
  // ---------------------------------------------------------------------------
  void set_BC(const BoundaryCondition &bc);
  void set_BC(const BoundaryCondition &bc, const Field& u);
  void set_BCs(std::initializer_list<const BoundaryCondition::Ref> l);
  void set_BCs(std::initializer_list<const BoundaryCondition::Ref> l,
               const Field& u);

  // ---------------------------------------------------------------------------
  // BC utilities
  // ---------------------------------------------------------------------------
  //! Get vector of boundary conditions.
  inline const std::vector<BoundaryCondition *>& BCs() const {return bc_;}
  //! Check if BCs for component \c cid are unique (at most one per boundary).
  bool unique_BCs(int cid, bool matching = false) const;
  //! Check if BCs for component \c cid are unique (exactly one per boundary).
  bool matching_BCs(int cid) const
  { return unique_BCs(cid, true); }

  // ---------------------------------------------------------------------------
  // Function definition
  // ---------------------------------------------------------------------------
  void set_function(Function& f, const std::string& imp = EMPTY);
  void set_function(FieldFunction& f, const std::vector<std::string>& imp =
                                                    std::vector<std::string>());
  void set_function(CutoffFunction& f, const std::string& imp = EMPTY);
  void set_functions(std::initializer_list<Function::Ref> l);
  void set_functions(std::initializer_list<FieldFunction::Ref> l);
  void set_functions(std::initializer_list<CutoffFunction::Ref> l);

  Function& get_function(const std::string& func_name) const;
  FieldFunction& get_field_function(const std::string& func_name) const;
  CutoffFunction& get_cutoff_function(const std::string& func_name) const;

protected:

  // ---------------------------------------------------------------------------
  // Mesh properties
  // ---------------------------------------------------------------------------

  /**
   *  Get the sid of the supplied Boundary.
   *
   *  This method must be overriden in all derived classes.
   */
  virtual int get_sid(Boundary *) const {return -1;}
  /**
   *  Get the sid of the supplied Boundary.
   *
   *  This method must be overriden in all derived classes.
   */
  virtual int get_sid(int bid) const {return -1;}
  /**
   *  Get the bid of the supplied Boundary.
   *
   *  This method must be overriden in all derived classes.
   */
  virtual int get_bid(Boundary *) const {return -1;}
public:
  /**
   *  Get type of boundary.
   *
   *  This method must be overriden in all derived classes.
   */
  virtual int btype(int bid) const {return -1;}
protected:
  /**
   *  Get the Boundary identifiers (bids) on side \c sid.
   *
   *  This method must be overriden in all derived classes. 
   */
  virtual std::vector<int> get_bids_on_side(int sid) const
  { return std::vector<int>{}; }
  /**
   *  Get the supported sids of PDE's Mesh.
   *
   *  This method must be overriden in all derived classes. 
   */
  virtual std::vector<int> supported_sids() const noexcept
  { return std::vector<int>{}; }
  /**
   *  Get the boundary identifier (bid) of the global boundary.
   *
   *  This method must be overriden in all derived classes. 
   */
  virtual int get_global_bid() const {return -1;}

  // ---------------------------------------------------------------------------
  // Utilities
  // ---------------------------------------------------------------------------
  void finalize();
  void check() const;
  //! Generic checker for missing implementation.
  void check_function_implementation(bool empty, const std::string& name);
  //! Check for missing function implementations.
  void check_function_implementations();
  //! Check for missing field function implementations.
  void check_field_function_implementations();
  //! Backup if file already exists
  void backup_old_file(const std::string& filename);
  //! Indicates that a boundary has fields with Neumann and non-Neumann BCs.
  bool mixed_BCs_on_boundary() const;
  //! Check if PDE has Neumann BCs.
  bool has_neumann_BCs() const;
  //! Check if PDE has BCs of a certain type.
  bool has_BCs_type(int type) const;
  bool boundary_has_all_BCs_type(const Boundary *b, int type) const;
  //! Check if component has Neumann BC on boundary.
  /*! \param component_index The index (not identifier!) of the component.
   *  \param bid The boundary identifier. */
  bool component_has_neumann_BC_on(int component_index, int bid) const;
  //! Check if any component has Neumann BC on boundary.
  /*! \param bid The boundary identifier. */
  bool has_neumann_BC_on(int bid) const;
  //! Check if function is already defined as a field function.
  bool find_fname_in_field_functions(const std::string& fname) const;
  //! Check if function is already defined as a cutoff function.
  bool find_fname_in_cutoff_functions(const std::string& fname) const;
  //! Check if function is already defined as a scalar function.
  bool find_fname_in_functions(const std::string& fname) const;
  //! PDE has default field function implementations.
  bool has_default_field_function_implementations() const;
  //! The exact_solution function was declared.
  bool has_exact_solution() const;
  // ---------------------------- Helper functions -----------------------------
  static inline bool ordinary_bc_code(int flags) noexcept
  { return (flags & ORDINARY_BC_CODE) == ORDINARY_BC_CODE; }
  static inline bool extra_bc_code(int flags) noexcept
  { return (flags & EXTRA_BC_CODE) == EXTRA_BC_CODE; }
  static inline bool secondary_bc_code(int flags) noexcept
  { return (flags & SECONDARY_BC_CODE) == SECONDARY_BC_CODE; }
  static inline bool tertiary_bc_code(int flags) noexcept
  { return (flags & TERTIARY_BC_CODE) == TERTIARY_BC_CODE; }
#ifdef __DEBUG__
  static inline bool check_bc_code(int flags) noexcept
  { return (flags | ALL_BC_CODES) == ALL_BC_CODES; }
#endif // __DEBUG__

public:

  // ---------------------------------------------------------------------------
  // BC definition
  // ---------------------------------------------------------------------------
  void set_Dirichlet_BC (const BoundaryCondition& bc, const Field& c);
  void set_Neumann_BC   (const BoundaryCondition& bc, const Field& c);
  void set_custom_BC    (const BoundaryCondition& bc, const Field& c);
  bool find_BC          (const BoundaryCondition& bc, const Field& c) const;

  // ---------------------------------------------------------------------------
  // Code generation
  // ---------------------------------------------------------------------------
  //! Code generator.
  void generate_code();
  //! Default makefile generator.
  /*! Override to redefine makefile generation. */
  virtual void generate_makefile();

  // ---------------------------------------------------------------------------
  // Formatting and coding utilities
  // ---------------------------------------------------------------------------
  //! Format function call at (i,j,k).
  /*! Returns f(i,j,k) with the correct number of arguments according to
   *  space dimension.
   *  \sa format_function_call() */
  inline std::string code_func_call(const std::string &f, const std::string &i,
      const std::string &j, const std::string &k) const
  { return format_function_call(f, i, j, k, ""); }
  //! Format function call for element field \c c at (\c i,\c j,\c k).
  /*! This method should be normally called only from a
   *  Discretizer::discretize() override.
   *  \sa Discretizer::discretize() */
  std::string code_elem_func(const std::string &f, const std::string &i,
      const std::string &j, const std::string &k, const std::string &c) const;
  //! Format function call for element field \c c at (\c i,\c j,\c k).
  /*! This is a 3-d helper for \sa code_elem_func(const std::string &f,
   *  const std::string &i, const std::string&j, const std::string &k,
   *  const std::string &c) */
  inline std::string code_elem_func(const std::string& i,
      const std::string& j, const std::string& k) const
  { return code_elem_func("e", i, j, k, "$c$"); }
  /**
   *  Format function call for element field \c c at (\c i,\c j,\c k).
   *
   *  This is a 2-d helper for code_elem_func(const std::string &f,
   *  const std::string &i, const std::string&j, const std::string &k,
   *  const std::string &c)
   *  \sa code_elem_func(const std::string &f, const std::string &i,
   *        const std::string&j, const std::string &k, const std::string &c)
   */
  inline std::string code_elem_func(const std::string& i,
                                    const std::string& j) const
  { return code_elem_func(i, j, ""); }
  /**
   *  Format function call for element field \c c at (\c i,\c j,\c k).
   *
   *  This is a 1-d helper for code_elem_func(const std::string &f,
   *  const std::string &i, const std::string&j, const std::string &k,
   *  const std::string &c)
   *  \sa code_elem_func(const std::string &f, const std::string &i,
   *        const std::string&j, const std::string &k, const std::string &c)
   */
  inline std::string code_elem_func(const std::string& i) const
  { return code_elem_func(i, "", ""); }
  /**
   *  Return code for field index of element (i,j,k).
   *
   *  Return a string representing the field index of element (i,j,k).
   *  It can be used for any component of the field.
   *  To be used in custom \c Discretizer and \c BasicOperator
   *  implementations. See supplied examples for usage.
   */
  std::string code_field_index(const std::string& i, const std::string& j,
      const std::string& k) const;
  /**
   *  Format node function call.
   *
   *  Rerurns f(i,j,k,c), or f(i,j,k) when \c c is empty, matching number
   *  of arguments with space dimension.
   */
  std::string format_node_function_call(const std::string &f, const std::string &i,
      const std::string &j, const std::string &k, const std::string &c) const;
  //! Alternative for \sa format_node_function_call().
  inline std::string format_elem_lhs(const std::string& f, const std::string& i,
      const std::string& j, const std::string& k, const std::string& c) const
  { return format_node_function_call(f, i, j, k, c); }
  /**
   *  Helper for \c format_elem_lhs using the default node \c i_, \c j_, \c k_.
   *
   *  \sa format_elem_lhs(const std::string& f, const std::string& i,
   *  const std::string& j, const std::string& k, const std::string& c)
   */
  inline std::string format_elem_lhs(const std::string& c = EMPTY) const
  { return format_elem_lhs("e", i_, j_, k_, c); }
  std::string format_space_args(const std::string& i, const std::string& j,
                                const std::string& k) const;
  /**
   *  Format function call for node element (i,j,k) of field \c c.
   *
   *  When \c prepend is true, it returns f(c,X(i),Y(j),Z(k)), and
   *  f(X(i),Y(j),Z(k),c) when it is false. In both cases it returns
   *  f(X(i),Y(j),Z(k)) if \c c is the empty string. Only those coordinates
   *  appropriate to the dimensionality of the PDE are present.
   */
  std::string format_function_call(const std::string &f, const std::string &i,
      const std::string &j, const std::string &k, const std::string &c,
      bool prep = true) const;
  //! Format string for the \c sid of the supplied Boundary.
  std::string format_sid(Boundary *b) const;

protected:

  //! Format various mesh parameters.
  virtual std::string format_mesh_params(int which) const;
  //! Format declaration of function with space arguments in header generator.
  std::string format_space_function_declaration(const std::string &f, bool mf) const;
  //! Format node function declaration in header generator.
  std::string format_function_declaration(const std::string &f) const;
  //! Format number of mesh nodes.
  std::string format_num_nodes() const;

public:

  // ---------------------------------------------------------------------------
  // Method selection
  // ---------------------------------------------------------------------------

  /**
   *  Select library.
   * 
   *  The currently supported libraries are GLS and PETSc.
   */
  void set_library(const std::string& library);
  /**
   *  Select matrix method.
   * 
   *  This setting is in effect only when GSL is selected. For PETSc the
   *  matrix method is selected from the command line.
   */
  void set_matrix_method(const std::string& matrix_method);
  /**
   *  Select nonlinear solver.
   * 
   *  This setting is in effect only when GSL is selected. For PETSc the
   *  nonlinear solver is selected from the command line.
   */
  void set_nonlinear_solver(const std::string& nonlinear_solver);

  // ---------------------------------------------------------------------------
  // Customization and options
  // ---------------------------------------------------------------------------

  /**
   *  Set initial estimate for the solution.
   *
   *  It is used by nonlinear problems only.
   */
  void set_initial_estimate(const std::string& impl);
  //! Check if an initial estimate for the solution is available.
  bool has_initial_estimate() const;

  // ---------------------------------------------------------------------------
  // Code output
  // ---------------------------------------------------------------------------
  void set_output(const std::string& stem, const std::string& pgm = EMPTY);

  // ---------------------------------------------------------------------------
  // Control output
  // ---------------------------------------------------------------------------
  void set_headers (const std::string& srcfile, const std::string& placement,
  	                const std::string& header_list);
  void set_extra_includes(const std::string& s);
  void set_extra_make_defs(const std::string& s) {extra_make_defs_ = s;}

  // ---------------------------------------------------------------------------
  // Message logging
  // ---------------------------------------------------------------------------
  void log_msg(const std::string& msg) {log_.push_back(msg);}

protected:

  //==========================[ General Utilities ]===========================//

  /** Map source code file to header dictionary (\c map). */
  std::map<const std::string, int>& get_dict_for_source(int which);
  /**
   *  Set boundary discretization method for non-extended domain problems.
   * 
   *  Use this method only from overriden classes to make it public.
   */
  void set_boundary_discretization_method(int bdm);

  // ---------------------------------------------------------------------------
  // Equation replacement
  // ---------------------------------------------------------------------------
  void replace_equation(int index, int i, int j, int k, double rhs_value,
  	    const std::string& rhs_text, const std::string& rhs_jac);

  // ---------------------------------------------------------------------------
  // BC utilities
  // ---------------------------------------------------------------------------
  /**
   *  Get BC for component \c cid on boundary with bid \c bid.
   *
   *  This method must be overriden in all derived classes.
   */
  virtual BoundaryCondition *get_BC_for_component(int component, int bid) const
  { return nullptr; }
  /**
   *  Get code fragment for the indicated custom BC.
   *
   *  For non-global boundaries the \c sid parameter is the \c sid associated
   *  with the BC, i.e.
   *  \code{.cpp}
   *  sid = get_sid(bc.boundary());
   *  \endcode
   *  For global boundaries the \c sid is the \c sid of the face or side for
   *  which we we'd like to have the code fragment generated.
   * 
   *  This method calls \sa fd::CustomBC::generate_code_fragment()
   */
  CodeFragment get_code_fragment(CustomBC &bc, int sid);

public:

  /**
   *  Finalize BCs before code generation.
   *
   *  This method must be overriden in derived classes that implement alternative
   *  boundary behavior, as for example, those using an extended domain mesh.
   *  It is used by \sa generate_code(). */
  virtual void finalize_BCs(); // TODO protected??

protected:

  /**
   *  Initialize the code generation process.
   *
   *  This method must be overriden in derived classes in need of special
   *  initialization of the code generation process.
   *  It is used by \sa generate_code(). */
  virtual void initialize_code_generator() {}
  /**
   *  Finalize the code generation process.
   *
   *  This method must be overriden in derived classes in need of special
   *  initialization of the code generation process to perform the required
   *  cleanup.
   *  It is used by \sa generate_code(). */
  virtual void terminate_code_generator() {}
  /**
   *  Finalize BCs for PDEs using an extended domain mesh.
   *
   *  This method is not virtual. It was designed as a helper method to be called
   *  from within a \sa finalize_BCs() override. */
  void finalize_BCs_extdom();

  // ---------------------------------------------------------------------------
  // Code generation
  // ---------------------------------------------------------------------------

  //::::::::::::::::::::::::::  Header  Generation  :::::::::::::::::::::::::://
  /**
   *  Generate header.
   *
   *  Automatically called by \c generate_code()
   */
  virtual void generate_header();
  /**
   *  Generate header includes.
   *
   *  Called by \c generate_header() to code include files.
   */
  void generate_header_includes();
  /** Genetate header for include (interface) file. */
  virtual void generate_header_header(const std::string& incl_macro);
  /** Generate declarations for node functions in PDE class. */
  virtual void generate_header_node_functions();
  /** Generate declarations for various size functions in PDE class. */
  virtual void generate_header_size_functions();
  /**
   *  Generate constructors in header.
   *
   *  Override in all derived classes. */
  virtual void generate_header_constructors() {}
  /*  Generate headers for system to be solved - global version.
   *  Override in derived classes. The PETSc and GSL libraries have their own
   *  default implementations.
   *  \sa generate_header_funcs_gsl()
   *  \sa generate_header_funcs_petsc() */
  virtual void generate_header_funcs() {}
  /**
   *  Generate code for comparison with exact solution - global interface.
   *
   *  Override in derived classes. The PETSc and GSL libraries have their own
   *  default implementations.
   *  \sa generate_header_compare_solution_gsl()
   *  \sa generate_header_compare_solution_petsc() */
  virtual void generate_header_compare_solution() {}
  /**
   *  Generate additional code for PDE class instance initializer.
   *
   *  Override in derived classes when additional code is needed. */
  virtual void generate_header_init_extra() {}
  /**
   *  Generate additional code for PDE class interface.
   *
   *  Override in derived classes when additional code is needed. */
  virtual void generate_header_extra() {}
  /**
   *  Generate code for additional definitions inside the PDE class.
   *
   *  Override in derived classes when additional definitions inside the class
   *  are needed, for example, nested classes, enumerations etc. */
  virtual void generate_header_extra_idefs() {}
  /** Format include macro. */
  virtual std::string format_include_macro() const;
  /* Format mesh start/end indices. */
  virtual std::string format_start_end_indices() const;
  /* Format function mapping nodes to field indices. */
  std::string format_mesh_coordinates_to_field_index() const;
  /* Format function mapping nodes to field indices (1-field version) */
  std::string format_mesh_coordinates_to_field_index_1f() const;
  /* Format function mapping nodes to field indices (multifield version) */
  std::string format_mesh_coordinates_to_field_index_mf() const;
  /** Format function mapping field indices to nodes. */
  std::string format_field_index_to_mesh_coordinates_1f() const;
  /* Format function mapping field indices to nodes (1-field version) */
  std::string format_field_index_to_mesh_coordinates() const;
  /* Format function mapping field indices to nodes (multifield version) */
  std::string format_field_index_to_mesh_coordinates_mf() const;

  //:::::::::::::::::::::::::::  Code Generation  :::::::::::::::::::::::::::://

  /**
   *  Generate code for function, Jacobian or both.
   *
   *  Internally used by \c generate_code().
   *  \param what Target of code generation. One of \c GENERATE_FUNCTION_CODE,
   *  \c GENERATE_JACOBIAN_CODE, \c GENERATE_BOTH.
   */
  void generate_code(int what);
  /**
   *  Generate code for the functions on the lhs of the equations to be solved.
   *
   *  Override in all derived classes.
   */
  virtual void generate_code_function_body(int what) {}
  using elem_fmt_t = std::string (const std::string &i, const std::string &j,
                                  const std::string &k, const std::string &c);
  using ElementFormatter = std::function<elem_fmt_t>;
  /**
   *  Load code on \c CodeFragment argument.
   *
   *  It is used internally only for boundary points, excluding
   *  boundary extremities.
   *
   *  \param[in] bid Boundary identifier.
   *  \param[in] c Component identifier (index).
   *  \param[out] cfb Standard BC code.
   * 
   *  For non-global boundaries, this method is a shortcut for
   *  \code{.cpp}
   *  get_code_for_boundary(bid, get_sid(bid), c, cfb);
   *  \endcode
   *  Do not use this method for \em global \em boundaries.
   */
  void get_code_for_boundary(int bid, int c, CodeFragment& cfb);
  /**
   *  Load code on \c CodeFragment argument.
   *
   *  It is used internally only for boundary points, excluding
   *  boundary extremities.
   *
   *  \param[in] bid Boundary identifier.
   *  \param[in] sid Side (face in 3d) identifier of the boundary.
   *  \param[in] c Component identifier (index).
   *  \param[out] cfb Standard BC code.
   *
   *  This method can be used for global and non-global boundaries.
   */
  void get_code_for_boundary(int bid, int sid, int c, CodeFragment& cfb);
  /**
   *  Load code on \c CodeFragment arguments.
   *
   *  It is used internally only for boundary points, excluding
   *  boundary extremities. Applies to Neumann BCs with the \c PDE::SYM_DIFF2
   *  discretization method. Similar to \c get_code_for_boundary.
   *
   *  \param[in] bid Boundary identifier.
   *  \param[in] c Component identifier (index).
   *  \param[out] cfb Standard BC code.
   *  \param[out] cfx Additional code, obtained from an equation of the PDE
   *                  system. This is required by the boundary discretization
   *                  method \c PDE::SYM_DIFF2 for Neumann BCs.
   */
  void get_code_for_boundary_SYM_DIFF2(int bid, int c, CodeFragment& cfb,
                                                       CodeFragment& cfx);
  /**
   *  Load code on \c CodeFragment arguments for Neumann BCs.
   * 
   *  Simply calls \sa get_code_for_neumann(BoundaryCondition*,
   *  const BoundaryDiscretizationData&).
   */
  void get_code_for_neumann(BoundaryCondition *, int bid, CodeFragment&,
                                                          CodeFragment&);
  /**
   *  Load code on \c CodeFragment arguments for Neumann BCs.
   * 
   *  Used by \c get_code_for_boundary() to handle Neumann BCs.
   */
  void get_code_for_neumann(BoundaryCondition *,
      const BoundaryDiscretizationData&, CodeFragment&, CodeFragment&);
  /** DEPRECATED */
  void generate_code_for_OLD(int c, int bid, int bid2, int what, int ntabs);
  /** DEPRECATED */
  void generate_code_for_OLD(int c, int bid, int bid2, int what, int ntabs,
                         int flags, ElementFormatter f);
  /** DEPRECATED */
  void generate_code_SYM_DIFF2_for_OLD(int c, int bid, int bid2, int what,
                         int ntabs, int bc_code_flags, ElementFormatter ef);
  /**
   *  Generate code for boundary points.
   *
   *  Helper method, calls \ref generate_code_for() with
   *  \c flags=ALL_BC_CODES and the default \c ElementFormatter.
   */
  void generate_code_for(int c, int bid, int what, int ntabs);
  /**
   *  Generate code for boundary points.
   *
   *  It is used internally only for boundary points, excluding
   *  boundary extremities.
   *  \param[in] c Component identifier (index).
   *  \param[in] bid Boundary identifier.
   *  \param[in] what Target of code generation (function, jacobian or both).
   *  \param[in] ntabs Number of tabs to be used for code indentation.
   *  \param[in] flags Applies to Neumann BCs with SYM_DIFF2 discretization.
   *  \param[in] f Element formatting function.
   */
  void generate_code_for(int c, int bid, int what, int ntabs, ElementFormatter f);
  /**
   *  Generate code for global boundary points.
   */
  void generate_code_for_global_boundary(int c, int sid, int what, int ntabs);
  /**
   *  Generate code for global boundary points.
   */
  void generate_code_for_global_boundary(int c, int sid, int what, int ntabs,
                         ElementFormatter ef);
  /**
   *  Generate code for boundary interior.
   * 
   *  Similar to \sa generate_code_for(int,int,int,int,ElementFormatter) but
   *  for extended domains (SYM_DIFF2) option.
   */
  void generate_code_SYM_DIFF2_for(int c, int bid, int what, int ntabs,
                         int flags, ElementFormatter f);
  /**
   *  Similar to \c generate_code_for() but for the SYM_DIFF2 option.
   *
   *  To be called only from within \c generate_code_for().
   */
  void generate_code_SYM_DIFF2_for(int c, int bid, int bid2,
                         int what, int ntabs, int flags, ElementFormatter f);
  /**
   *  Generate code for boundary extremities.
   *
   *  This method calls \sa generate_code_for(int,int,int,int,int,int,
   *  ElementFormatter) with the default \c ElementFormatter.
   */
  void generate_code_for(int c, int bid, int bid2, int what, int ntabs);
  /**
   *  Generate code for boundary extremities.
   *
   *  The boundary extremities are:
   *  \li corners in 2d.
   *  \li edges in 3d.
   *
   *  This method cannot be used to generate code for vertices (in 3d).
   *  For this purpose use \sa generate_code_for(int,const array<int,3>&b,int,
   *  int,int,ElementFormatter).
   *  Not for extended domain (SYM_DIFF2) discretizations.
   */
  void generate_code_for(int c, int bid, int bid2, int what, int ntabs,
                         ElementFormatter f);
  /**
   *  Generate code for boundary vertices in 3d and 2d.
   *
   *  -- NEW 3D IMPLEMENTATION.
   *
   *  This method calls \sa generate_code_for(int,const fd::VertexParts&,
   *  int,int,ElementFormatter) with the default \c ElementFormatter.
   */
  void generate_code_for(int c, const fd::VertexParts& vp, int what,
                         int ntabs);
  /**
   *  Generate code for boundary vertices in 3d and 2d.
   *
   *  -- NEW 3D IMPLEMENTATION.
   *
   *  For SYM_DIFF2 discretizations use \sa generate_code_SYM_DIFF2_for().
   */
  void generate_code_for(int c, const fd::VertexParts& vp, int what,
                         int ntabs, ElementFormatter ef);
  /**
   *  Similar to \c generate_code_for() but for the SYM_DIFF2 option
   *
   *  -- NEW 3D IMPLEMENTATION. */
  int generate_code_SYM_DIFF2_for(int c, const std::array<int,3>& bids,
                         int what, int ntabs, int flags, ElementFormatter ef);
  /**
   *  Similar to \c generate_code_for() but for the SYM_DIFF2 option
   *
   *  -- NEW 3D IMPLEMENTATION. */
  int generate_code_SYM_DIFF2_for(int c, const std::array<int,3> &bid,
                         int what, int ntabs);
  /**
   *  Generate code for the equations to be replaced.
   *
   *  This method must be used \em before the main node loop.
   *  For 1-dimensional fields the findex argument is ignored. For
   *  multidimensional fields it is the field component (usually \c "c" ).
   *  Note that currently the multidimensional version of this method is
   *  never used.
   *
   *  @param findex Field index.
   *  @param eqn Equation number. When it is empty the default value is used:
   *         \code{.cpp} format_elem_lhs("e", i_, j_, k_, field) \endcode
   *  @param check_simple Check if PDE system is simple, and return false if
   *         it is not, otherwise proceed to the replacement. Default is true.
   *  @return true if the replacement was done, otherwise it returns false.  
   */
  bool generate_code_function_body_replace_eqs(int what, int ntabs,
  	                     const std::string& findex = EMPTY,
  	                     const std::string& eqn = EMPTY,
                         bool check_simple = true);
  /**
   *  Generate code for the equations to be replaced - non-simple version.
   *
   *  This method is more generally applicable. It must be used \em after
   *  the main node loop.
   *
   *  TODO consider renaming as generate_code_function_body_replace_eqs.
   */
  bool generate_code_function_body_replace_eqs_ns(int what);
  /**
   *  Code generator for external modules.
   *
   *  Default implementation does nothing.
   *  Override when implementing an external module to define its
   *  code generation method.
   */
  virtual void code_generate_method() {}
  /**
   *  Generate header for code file.
   *
   *  Override when necessary to modify the default behavior.
   */
  virtual void generate_code_header();
  /**
   *  Generate includes for code file.
   *
   *  Override when necessary to modify the default behavior.
   */
  virtual void generate_code_includes();
  /**
   *  Generate additional code for the PDE class.
   *
   *  Override when necessary to modify the default behavior which does nothing.
   */
  virtual void generate_code_class_extra() {}

  //:::::::::::::::::::::::  Code Generation for GSL  :::::::::::::::::::::::://

  //! Generate headers for system to be solved - gsl version.
  void generate_header_funcs_gsl();
  //! Generate code for comparison with exact solution -- gsl interface.
  void generate_header_compare_solution_gsl();
  /**
   *  Format iteration loop for solution comparison function.
   * 
   *  Used internally by header code generator.
   *  \sa generate_header_compare_solution_gsl()
   */
  std::string format_header_compare_solution_iter_gsl() const;
  /**
   *  Generate code for function, Jacobian or both -- gsl interface.
   * 
   *  Internally used by \sa generate_code(int).
   */
  void generate_code_gsl(int what);
  //! Generate PDE class code
  void generate_code_class_gsl();

  //::::::::::::::::::::::  Code Generation for PETSC  ::::::::::::::::::::::://

  //! Generate auxiliary classes for petsc.
  void generate_header_classes_petsc();
  //! Generate headers for system to be solved - petsc version.
  void generate_header_funcs_petsc();
  //! Generate headers for linear system to be solved - petsc interface.
  void generate_header_funcs_petsc_linear();
  //! Generate headers for nonlinear system to be solved - petsc interface.
  void generate_header_funcs_petsc_nonlinear();
  //! Generate code for comparison with exact solution - petsc interface.
  void generate_header_compare_solution_petsc();
  /**
   *  Generate code for function, Jacobian or both -- petsc interface.
   * 
   *  Internally used by \c generate_code(int).
   */
  void generate_code_petsc(int what);
  //! Generate code for function, Jacobian or both -- linear petsc interface.
  void generate_code_petsc_linear(int what);
  //! Generate code for function, Jacobian or both -- nonlinear petsc interface.
  void generate_code_petsc_nonlinear(int what);
  /**
   *  Generate function body code.
   * 
   *  Nonlinear PETSc specific code generator.
   *  Override this method in all derived classes supporting PETSc.
   */
  virtual void generate_code_function_body_petsc_nonlinear(int what) {}
  //! Generate code for multiprocessor SNES C++ interface.
  void generate_code_petsc_snes_mp();
  //! Generate PDE class code
  void generate_code_class_petsc();

  // ---------------------------------------------------------------------------
  // Output files
  // ---------------------------------------------------------------------------
  std::string get_implem_filename() const;
  std::string get_header_filename() const;
  std::string get_main_filename() const;

  void switch_to_main();

  //===============[ MODULE 1: Makefile generation -- METHODS ]===============//

  /**
   *  Generate header for makefiles.
   *
   *  Override to change the default makefile header generation method.
   */
  virtual void generate_makefile_header (MakefileGenerationInfo& mgi);
  /**
   *  Default makefile generator for the gsl platform.
   *
   *  Override to redefine makefile generation behavior for gsl.
   */
  virtual void generate_makefile_gsl    (MakefileGenerationInfo& mgi);
  /**
   *  Default makefile generator for the petsc platform.
   *
   *  Override to redefine makefile generation behavior for petsc.
   */
  virtual void generate_makefile_petsc  (MakefileGenerationInfo& mgi);
  /**
   *  Makefile generator for external modules.
   *
   *  Default implementation does nothing.
   *  Override when implementing an external module to define its
   *  makefile generation method.
   */
  virtual void makefile_generate_method (MakefileGenerationInfo& mgi) {}

  //============[ MODULE 2: Main file generation -- METHODS ]=================//

  /**
   *  Default main file generator.
   *
   *  Override to redefine main file generation.
   *  Automatically called by \sa generate_code()
   */
  virtual void generate_main();
  /**
   *  Default header for main file.
   *
   *  Override to redefine generated header.
   */
  virtual void generate_main_header();
  //! Default project information for main file.
  virtual void generate_main_project_info();
  //! Default include files generator for main file.
  virtual void generate_main_includes();
  //! Generate file with main function for GSL.
  virtual void generate_main_gsl();
  /**
   *  Generate main file for GSL -- linear problems.
   *
   *  Method is taken from \c matrix_method_.
   */
  virtual void generate_main_gsl_linear();
  //! Generate main file for GSL -- nonlinear problems.
  virtual void generate_main_gsl_nonlinear();
  //! Generate file with main function for PETSc.
  virtual void generate_main_petsc();
  //! Generate main file for PETSc -- linear problems.
  virtual void generate_main_petsc_linear();
  //! Generate main file for PETSc -- nonlinear problems.
  virtual void generate_main_petsc_nonlinear();
  //! Generate code for solution output.
  virtual void generate_solution_output();
  /**
   *  Main file generator for external modules.
   *
   *  Default implementation does nothing.
   *  Override when implementing an external module to define its
   *  main file generation method.
   */
  virtual void main_generate_method () {}
  //! Output solution field
  virtual void output_solution_field();
  //! Output solution norms
  virtual void output_solution_norms();
  // -------------------- Code Generation Utilities ----------------------------
  //! Resolve field references.
  void link(CodeFragment& cf) const;
  /**
   *  Resolve field references.
   *
   *  Called from link(CodeFragment&).
   */
  void link(std::string& code) const;
  //! Perform consistency checks.
  void perform_consistency_checks();
  /**
   *  Encode boundary vertex extremity.
   *
   *  \li 2D boundaries: The returned index is for use in
   *      \sa CodeFragment::text_bx_y[4] and \sa CodeFragment::text_bx_j[4].
   *      Only the first two entries are used.
   *  \li 3D boundaries: The returned index is for use in
   *      \sa CodeFragment::text_vx_y[4] and \sa CodeFragment::text_vx_j[4].
   */
  int boundary_vertex_extremity(const std::array<int,3>& sid) const;
  /**
   *  Encode boundary vertex extremity.
   *
   *  \li 2D boundaries: The returned index is for use in
   *      \sa CodeFragment::text_bx_y[4] and \sa CodeFragment::text_bx_j[4].
   *      Only the first two entries are used.
   *  \li 3D boundaries: The returned index is for use in
   *      \sa CodeFragment::text_vx_y[4] and \sa CodeFragment::text_vx_j[4].
   * 
   *  The \c VertexParts argument must have been obtained from
   *  \sa get_vertex_parts(int, int).
   */
  static int boundary_vertex_extremity(const fd::VertexParts& vp);
#if 0
  //! Encode boundary edge extremity.
  /*! \li 2D boundaries: The same as \sa boundary_vertex_extremity().
   *  \li 3D boundaries: The returned index is for use in
   *      \sa CodeFragment::text_bx_y[4] and \sa CodeFragment::text_bx_y[4]. */
  int boundary_edge_extremity(const std::array<int,2>& bid) const;
#endif // 0
  /**
   *  Encode boundary edge extremity.
   *
   *  \sa boundary_vertex_extremity(const std::array<int,3>& bid)
   */
  int boundary_edge_extremity(int bid, int sid2) const;
  //! Get extended node coordinates.
  std::tuple<std::string, std::string, std::string>
  extended_node_coordinates(int fid) const;

  //==========================( Utility functions )===========================//

  //-------------------------------[ MODULE 1 ]-------------------------------//
  static std::string strip_path  (const std::string& fname);
  static std::string get_path    (const std::string& fname);
  static std::string strip_ext   (const std::string& fname);

  //-------------------------------[ MODULE 2 ]-------------------------------//
  //! Generate code for additional user specified includes.
  void code_extra_headers(int which, int placement, bool endl_at_end = true);
  //! Format constructor comment.
  virtual std::string format_constructor_comment() const {return "";}
  //! Format stream for solution output.
  virtual std::string format_solution_output_stream() const;
  //! Format iteration for solution output.
  virtual std::string format_solution_output_iter() const;
  //! Format node.
  virtual std::string format_solution_output_node(int c) const {return "";}
  //! Format coordinate arguments.
  virtual std::string format_solution_output_coord(int c) const {return "";}
  //! Format coordinate arguments.
  virtual std::string format_coord() const {return "";}

private:

  int get_gsl_config() const noexcept;
  int get_petsc_config() const noexcept;
};

} // namespace fd

#endif // PDE_HPP_INCLUDED
