#ifndef DISCRETIZER_HPP_INCLUDED
#define DISCRETIZER_HPP_INCLUDED

#include <cstdio>
#include "bc.hpp"
#include "codefrag.h"

#ifndef SET_OPERATOR_NAME

#define SET_OPERATOR_NAME(s) \
    virtual const char *name() const {return s;}
#define DECL_DISCRETIZER_DUP() \
    virtual Discretizer *duplicate() const;
#define IMPLEMENT_DISCRETIZER_DUP(t) \
Discretizer *t::duplicate() const {t *p = new t; *p = *this; return p;}

#endif // SET_OPERATOR_NAME

namespace fd
{

class PDECommon;

struct DiscretizationData
{
  PDECommon *pde;
  const std::string &i;
  const std::string &j;
  const std::string &k;
};

struct BoundaryDiscretizationData
{
  PDECommon *pde;
  int  bid;
  int  dm;
  const std::string &i;
  const std::string &j;
  const std::string &k;
  int  sid;
};

//============================================================================//
//:::::::::::::::::::::::::::::::  Discretizer  :::::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Base discretizer class.
 * 
 *  For internal use only. You can implement new Discretizer classes,
 *  but not construct Discretizer instances directly.
 */
class Discretizer
{
protected:

  int type_; // 0 BO, 1 Function, 2 Constant, 3 BC
  IntTriple order_;

public:

  enum {BASIC_OPERATOR = 0, FUNCTION = 1, CONST_FIELD = 2,
        BC, FIELD_FUNCTION, OPERATOR, CUTOFF_FUNCTION};

  Discretizer() : type_(BASIC_OPERATOR), order_{0,0,0} {}
  virtual ~Discretizer() {}

  DECL_DISCRETIZER_DUP();

  /**
   *  @brief Discretize operator at interior points
   *  
   *  Basic discretizer simply calls \sa core_discretize, which by default
   *  throws an exception.
   * 
   *  \em Core discretizers (DiscretizerDx etc) override \sa core_discretize
   *  instead of this method in order to provide consistency between core and
   *  user defined discretizers.
   *
   *  User defined discretizers usually override the \sa discretize method.
   */
  virtual CodeFragment discretize(
              const DiscretizationData& dd) const;
  /**
   *  @brief Discretize operator on boundary
   * 
   *  Basic boundary discretizer simply calls \sa core_discretize_on_boundary,
   *  which by default throws an exception.
   * 
   *  \em Core discretizers (DiscretizerDx etc) override
   *  \sa core_discretize_on_boundary instead of this method in order
   *  to provide consistency between core and user defined discretizers.
   *
   *  User defined discretizers usually override the \sa discretize_on_boundary
   *  method.
   */
  virtual CodeFragment discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const;
  /**
   *  Discretize function.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform scalar field discretization
   *  (numerical calculation of indicated derivative).
   *  Basic scalar function discretizer throws an exception.
   */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const;
  /**
   *  Discretize function on boundary.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform scalar field discretization
   *  (numerical calculation of indicated derivative) on boundaries.
   *  Basic scalar function discretizer on boundaries calls
   *  \sa discretize_scalar_field.
   */
  virtual CodeFragment discretize_on_boundary_scalar_field(
              const BoundaryDiscretizationData& bdd) const;
  /**
   *  @brief Get boundary discretization nodes.
   *
   *  @param pd_type partial derivative axis.
   *  @param dm discretization method, 3, 4, 5.
   *  @param bdd boundary discretization data.
   *
   *  It can be used when developing new discretizers.
   *  Internally, it is used by the discretize_on_boundary_bdmX()
   *  methods to compute the required nodes.
   *
   *  TODO static!
   */
  std::array<std::string,7> get_boundary_discretization_nodes(int pd_type,
              int dm, const BoundaryDiscretizationData &bdd) const;
  /**
   *  Type of discretizer.
   */
  inline int type() const noexcept {return type_;}
  /**
   *  Check if partial orders are non-negative.
   */
  inline bool check_order() const noexcept
  { return !(order_[0] < 0 || order_[1] < 0 || order_[2] < 0); }
  /**
   *  Order of operator.
   */
  inline IntTriple order() const noexcept {return order_;}
  /**
   *  Total order of operator.
   */
  inline int total_order() const noexcept
  { return order_[0] + order_[1] + order_[2]; }
  /**
   *  Partial differential operator type.
   * 
   *  The partial orders must be non-negative.
   */
  inline int pdo_type() const noexcept
  { return order_[0] + order_[1]*100 + order_[2]*100*100; }
  /**
   *  Convert partial differential operator type to order of operator.
   */
  static IntTriple order_from_pdo_type(int pdotype) noexcept;

  SET_OPERATOR_NAME("null");

protected:
  //==========================================================================//
  //::::::::::::::::::::::::::  Core Discretizers  ::::::::::::::::::::::::::://
  //==========================================================================//

  /**
   *  @brief Discretize operator.
   *  
   *  Basic \em core discretizer throws an exception.
   *
   *  Override the core discretizer \em only when you want to change the
   *  behavior of the \em core discretizers like \c DiscretizerDx etc.
   *  Instead, override the method \sa discretize(const DiscretizationData&)
   *  when you develop a new discretizer.
   */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const;
  /**
   *  @brief Discretize operator on boundary.
   * 
   *  Basic \em core boundary discretizer throws an exception.
   *
   *  Override the core discretizer \em only when you want to change the
   *  behavior of the \em core discretizers like \c DiscretizerDx etc.
   *  Instead, override the method \sa discretize_on_boundary(const
   *  BoundaryDiscretizationData&) when you develop a new discretizer.
   */
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const;
  
  //==========================================================================//
  //::::::::::::::::::::  Utilities for Derived Classes  ::::::::::::::::::::://
  //==========================================================================//
  
  /**
   *  True if standard discretization can be applied on boundary.
   * 
   *  The boundary to be checked is \code bdd.sid \endcode.
   *  @param \c bdd Boundary discretization data.
   *  @param \c diff_type Partial differentiation axis.
   */
  bool apply_std_discretization(
              const BoundaryDiscretizationData &bdd, int diff_type) const;
  /**
   *  Discretize \f$\frac{\partial u}{\partial x}\f$ etc on boundary extremities.
   * 
   *  This method is used \em only for boundaries with \c CUSTOM BCs. Empty
   *  code at boundary extremities (text_bx_y[], text_bx_j[]) indicates that
   *  the ordinary code (text_y, text_j) must be used instead.
   *  @param \c bdd Boundary discretization data.
   *  @param \c diff_type Partial differentiation axis.
   */
  void discretize_on_boundary_extremities(int diff_type,
              const BoundaryDiscretizationData &bdd, CodeFragment *cf) const;
  /**
   *  Discretize \f$\frac{\partial u}{\partial x}\f$ etc on boundary vertices.
   * 
   *  3D only. This method is used \em for boundaries with \c CUSTOM BCs. Empty
   *  code at boundary vertices (text_vx_y[], text_vx_j[]) indicates that
   *  the ordinary code (text_y, text_j) must be used instead.
   *  @param \c bdd Boundary discretization data.
   *  @param \c diff_type Partial differentiation axis.
   */
  void discretize_on_boundary_vertices(int diff_type,
              const BoundaryDiscretizationData &bdd, CodeFragment *cf) const;
  /**
   *  Discretize operator on boundary.
   * 
   *  Default implementation for the operators \f$\frac{\partial}{\partial x}\f,
   *  \f$\frac{\partial}{\partial y}\f, \f$\frac{\partial}{\partial z}\f.
   *  It is called by \sa DiscretizerDx::discretize_on_boundary(),
   *  \sa DiscretizerDy::discretize_on_boundary() and
   *  \sa DiscretizerDz::discretize_on_boundary().
   */
  CodeFragment discretize_on_boundary(int diff_type,
              const BoundaryDiscretizationData &bdd) const;
  /**
   *  @brief Apply standard discretization to discretize operator on boundary.
   *
   *  It applies the \sa discretize() method on boundaries. */
  CodeFragment discretize_on_boundary_bdm2(
              const BoundaryDiscretizationData &bdd) const;
  /**
   *  Boundary discretization with the inward 3-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm3(int diff_type,
              const BoundaryDiscretizationData &bdd) const;
  /**
   *  Boundary discretization with the inward 4-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm4(int diff_type,
              const BoundaryDiscretizationData &bdd) const;
  /**
   *  Boundary discretization with the inward 5-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm5(int diff_type,
              const BoundaryDiscretizationData &bdd) const;
};

/**
 *  @brief Discretizer for operator \f$\frac{\partial}{\partial x}\f$.
 */
class DiscretizerDx : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDx() {order_[0] = 1;}

  DECL_DISCRETIZER_DUP();
  /** 
   *  Discretize function \f$\frac{\partial f}{\partial x}\f$.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative).
   */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;

  SET_OPERATOR_NAME("Dx");

protected:
  /**
   *  Discretize expression \f$\frac{\partial u}{\partial x}\f$.
   * 
   *  This method is used for \em internal (non-boundary) points only.
   */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize expression \f$\frac{\partial u}{\partial x}\f$ on a boundary.
   * 
   *  This method is used \em only for boundaries with a \c CUSTOM BC.
   */
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
#ifndef USE_UNIFIED_DISCRETIZER
  /**
   *  Boundary discretization with the inward 3-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm3(
              const BoundaryDiscretizationData& bdd) const;
  /**
   *  Boundary discretization with the inward 4-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm4(
              const BoundaryDiscretizationData& bdd) const;
  /** 
   *  Boundary discretization with the inward 5-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm5(
              const BoundaryDiscretizationData &bdd) const;
#endif // !USE_UNIFIED_DISCRETIZER
  /**
   *  Discretize \f$\frac{\partial u}{\partial x}\f$ on boundary's extremities.
   * 
   *  This method is used \em only with \c CUSTOM BCs.
   *  Empty code at boundary extremities (text_bx_y[], text_bx_j[]) indicates
   *  that the ordinary code (text_y, text_j) must be used instead. */
  void discretize_on_boundary_extremities(
              const BoundaryDiscretizationData& bdd, CodeFragment *cf) const
  { Discretizer::discretize_on_boundary_extremities(Boundary::X, bdd, cf); }
  /** Deprecated. Previous implementation. */
  void discretize_at_boundary_extremities_OLD(
              const BoundaryDiscretizationData &bdd, CodeFragment *cf) const;
};

/**
 *  @brief Discretizer for operator \f$\frac{\partial}{\partial y}\f$.
 */
class DiscretizerDy : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDy() {order_[1] = 1;}

  DECL_DISCRETIZER_DUP();
  /**
   *  @brief Discretize function \f$\frac{\partial f}{\partial y}\f$.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative).
   */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;

  SET_OPERATOR_NAME("Dy");

protected:
  /**
   *  @brief Discretize expression \f$\frac{\partial u}{\partial y}\f$.
   * 
   *  This method is used for \em internal (non-boundary) points only.
   */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize expression \f$\frac{\partial u}{\partial y}\f$ on a boundary.
   * 
   *  This method is used \em only for boundaries with a \c CUSTOM BC.
   */
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
#ifndef USE_UNIFIED_DISCRETIZER
  /**
   *  Boundary discretization with the inward 3-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm3(
              const BoundaryDiscretizationData& bdd) const;
  /**
   *  Boundary discretization with the inward 4-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm4(
              const BoundaryDiscretizationData& bdd) const;
  /**
   *  Boundary discretization with the inward 5-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm5(
              const BoundaryDiscretizationData &bdd) const;
#endif // !USE_UNIFIED_DISCRETIZER
  /**
   *  Discretize \f$\frac{\partial u}{\partial y}\f$ on boundary's extremities.
   * 
   *  This method is used \em only with \c CUSTOM BCs.
   *  Empty code at boundary extremities (text_bx_y[], text_bx_j[]) indicates
   *  that the ordinary code (text_y, text_j) must be used instead. */
  void discretize_on_boundary_extremities(
              const BoundaryDiscretizationData& bdd, CodeFragment *cf) const
  { Discretizer::discretize_on_boundary_extremities(Boundary::Y, bdd, cf); }
  /** Deprecated. Previous implementation. */
  void discretize_at_boundary_extremities_OLD(
              const BoundaryDiscretizationData& bdd, CodeFragment *) const;
};

/**
 *  @brief Discretizer for operator \f$\frac{\partial}{\partial z}\f$.
 */
class DiscretizerDz : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDz() {order_[2] = 1;}

  DECL_DISCRETIZER_DUP();
  /** 
   *  @brief Discretize function \f$\frac{\partial f}{\partial z}\f$.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative).
   */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;

  SET_OPERATOR_NAME("Dz");

protected:
  /**
   *  @brief Discretize expression \f$\frac{\partial u}{\partial z}\f$.
   * 
   *  This method is used for \em internal (non-boundary) points only.
   */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize expression \f$\frac{\partial u}{\partial z}\f$ on a boundary.
   * 
   *  This method is used \em only for boundaries with a \c CUSTOM BC.
   */
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
#ifndef USE_UNIFIED_DISCRETIZER
  /**
   *  Boundary discretization with the inward 3-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm3(
              const BoundaryDiscretizationData& bdd) const;
  /**
   *  Boundary discretization with the inward 4-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm4(
              const BoundaryDiscretizationData& bdd) const;
  /** 
   *  Boundary discretization with the inward 5-point differentiation method.
   * 
   *  This method is used \em only with \c CUSTOM BCs. */
  CodeFragment discretize_on_boundary_bdm5(
              const BoundaryDiscretizationData &bdd) const;
#endif // !USE_UNIFIED_DISCRETIZER
  /**
   *  Discretize \f$\frac{\partial u}{\partial z}\f$ on boundary's extremities.
   * 
   *  This method is used \em only with \c CUSTOM BCs.
   *  Empty code at boundary extremities (text_bx_y[], text_bx_j[]) indicates
   *  that the ordinary code (text_y, text_j) must be used instead. */
  void discretize_on_boundary_extremities(
              const BoundaryDiscretizationData& bdd, CodeFragment *cf) const
  { Discretizer::discretize_on_boundary_extremities(Boundary::Z, bdd, cf); }
};

/**
 *  @brief Discretizer for operator \f$\frac{\partial^2}{\partial x^2}\f$. */
class DiscretizerDxx : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDxx() {order_[0] = 2;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("Dxx");

protected:
  /**
   *  Discretize expression \f$\frac{\partial^2 u}{\partial x^2}\f$.
   * 
   *  This method is used for \em internal (non-boundary) points. */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize function \f$\frac{\partial^2 f}{\partial x^2}\f$.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative). */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;
};

class DiscretizerDyy : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDyy() {order_[1] = 2;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("Dyy");

protected:
  /**
   *  Discretize expression \f$\frac{\partial^2 u}{\partial y^2}\f$.
   *
   *  This method is used for \em internal (non-boundary) points. */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize function \f$\frac{\partial^2 f}{\partial y^2}\f$.
   *
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative). */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;
};

/**
 *  @brief Discretizer for operator \f$\frac{\partial^2}{\partial z^2}\f$. */
class DiscretizerDzz : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDzz() {order_[2] = 2;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("Dzz");

protected:
  /**
   *  Discretize expression \f$\frac{\partial^2 u}{\partial z^2}\f$.
   * 
   *  This method is used for \em internal (non-boundary) points. */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   * Discretize function \f$\frac{\partial^2 f}{\partial z^2}\f$.
   * 
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative). */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;
};

/**
 *  Discretizer for operator \f$\frac{\partial^2}{\partial x\partial y}\f$. */
class DiscretizerDxy : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDxy() {order_[0] = 1; order_[1] = 1;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("Dxy");

protected:
  /**
   *  Discretize expression \f$\frac{\partial^2 u}{\partial x\partial y}\f$.
   *
   *  This method is used for \em internal (non-boundary) points. */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize function \f$\frac{\partial^2 f}{\partial x\partial y}\f$.
   *
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative). */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;
};

/**
 *  Discretizer for operator \f$\frac{\partial^2}{\partial x\partial z}\f$. */
class DiscretizerDxz : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDxz() {order_[0] = 1; order_[2] = 1;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("Dxz");

protected:
  /**
   *  Discretize expression \f$\frac{\partial^2 u}{\partial x\partial z}\f$.
   *
   *  This method is used for \em internal (non-boundary) points.
   */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize function \f$\frac{\partial^2 f}{\partial x\partial z}\f$.
   *
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative).
   */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;
};

/**
 *  Discretizer for operator \f$\frac{\partial^2}{\partial y\partial z}\f$.
 */
class DiscretizerDyz : public Discretizer
{
public:
  /**
   *  Constructor. */
  DiscretizerDyz() {order_[1] = 1; order_[2] = 1;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("Dyz");

protected:
  /**
   *  Discretize expression \f$\frac{\partial^2 u}{\partial y\partial z}\f$.
   *
   *  This method is used for \em internal (non-boundary) points.
   */
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  /**
   *  Discretize function \f$\frac{\partial^2 f}{\partial y\partial z}\f$.
   *
   *  This method should not be used directly. It is called by the
   *  \c FunctionDiscretizer to perform function discretization
   *  (numerical calculation of indicated derivative).
   */
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;
};

class BasicOperator;

class FunctionDiscretizer : public Discretizer
{
  /**
   *  Differential operator to be applied to the function.
   * 
   *  If null, no differentiation is done by the discretization. This field
   *  is not owned by the class (someone else must delete it).
   */
  BasicOperator *_basic_operator;

public:

  /** Function name. */
  std::string fname;

  /** Constructor. */
  FunctionDiscretizer(const std::string &name = std::string("")) :
      _basic_operator(nullptr), fname(name)
  { type_ = FUNCTION; }

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("function");

  bool numerical_differentiation() const {return _basic_operator != nullptr;}
  void associate_operator(BasicOperator *o) {_basic_operator = o;}

protected:
  virtual CodeFragment discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
};

class ConstantDiscretizer : public Discretizer
{
  double _c;

public:

  ConstantDiscretizer(double c) : _c(c)
  { type_ = CONST_FIELD; }

  inline double value() const {return _c;}

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("const");

protected:
  virtual CodeFragment discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;

private:
  ConstantDiscretizer() : ConstantDiscretizer(0.) {}
};

/**
 *  Identity (unit) operator discretizer.
 * 
 *  This class can be used to convert a field into an expression:
 *  \f$I[u]\equiv u\f$.
 */
class DiscretizerId : public Discretizer
{
public:
  DiscretizerId() {}

  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& dd) const override;

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("unit-operator");

protected:
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
};

class FieldFunctionDiscretizer : public Discretizer
{
  /** Number of field arguments. */
  int argc_;
  /** Indicates that field function has also space coordinate arguments. */
  bool space_args_;

  /** Private constructor. */
  FieldFunctionDiscretizer() :
      argc_(1), space_args_(false), fname("")
  { type_ = FIELD_FUNCTION; }

public:

  std::string fname;

  /** Constructor. */
  FieldFunctionDiscretizer(const std::string &s, int argc, bool space = false) :
      argc_(argc), space_args_(space), fname(s)
  { type_ = FIELD_FUNCTION; }

  int argc() const {return argc_;}
  bool has_space_args() const {return space_args_;}
  std::string code_field_function_call(const std::string& funcname,
              const DiscretizationData& dd) const;
  std::string code_call_with_expressions(const std::string& funcname,
              const DiscretizationData& dd) const;

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("field-function");

  virtual CodeFragment discretize_with_expressions(
              const DiscretizationData& dd) const;
  virtual CodeFragment discretize_on_boundary_with_expressions(
              const BoundaryDiscretizationData &bdd) const;

protected:
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
};

class CutoffDiscretizer : public Discretizer
{
  /** Private constructor. */
  CutoffDiscretizer() : fname("") {type_ = CUTOFF_FUNCTION;}

public:

  /** Cutoff function name */
  std::string fname;

  /** Constructor. */
  CutoffDiscretizer(const std::string &name) : fname(name)
  { type_ = CUTOFF_FUNCTION; }

  std::string code_cutoff_call(const std::string &var,
              const DiscretizationData& dd) const;
  std::string code_call_with_expressions(const std::string &arg,
              const DiscretizationData& dd) const;

  DECL_DISCRETIZER_DUP();
  SET_OPERATOR_NAME("cutoff-function");

  virtual CodeFragment discretize_with_expressions(
              const DiscretizationData& dd) const;
  virtual CodeFragment discretize_on_boundary_with_expressions(
              const BoundaryDiscretizationData &bdd) const;

protected:
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
};

class OperatorDiscretizer : public Discretizer
{
public:

  /** Constructor. */
  OperatorDiscretizer() {type_ = OPERATOR;}

  DECL_DISCRETIZER_DUP();
  virtual CodeFragment discretize(
              const DiscretizationData& dd) const override
  { return CodeFragment(); }
  virtual CodeFragment discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override
  { return CodeFragment(); }

  SET_OPERATOR_NAME("operator");
};

class DirichletBC;

/**
 * \brief Discretizer for Dirichlet BCs. */
class DirichletBCDiscretizer : public Discretizer
{
  DirichletBC *_bc;

public:
  /** Constructor. */
  DirichletBCDiscretizer(DirichletBC *bc) : _bc(bc)
  { type_ = Discretizer::BC; }
  bool is_function() const;
  SET_OPERATOR_NAME("BC-Dirichlet");

protected:
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
};

class NeumannBC;

/**
 * \brief Discretizer for Neumann BCs. */
class NeumannBCDiscretizer : public Discretizer
{
  NeumannBC *_bc;

public:
  /** Constructor. */
  NeumannBCDiscretizer(NeumannBC *bc) : _bc(bc) {type_ = Discretizer::BC;}
  SET_OPERATOR_NAME("BC-Neumann");

protected:
  virtual CodeFragment core_discretize(
              const DiscretizationData& dd) const override;
  virtual CodeFragment core_discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
};

/*
std::string code_elem_func(const std::string& f, const std::string& i,
                           const std::string& j, const std::string& c);
std::string code_field_index(const std::string& i, const std::string& j);
std::string code_func_call(const std::string& f, const std::string& i,
                           const std::string& j);
*/

} // namespace fd

#endif // DISCRETIZER_HPP_INCLUDED
