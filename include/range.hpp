#ifndef _RANGE_HPP_INCLUDED
#define _RANGE_HPP_INCLUDED

namespace fd {

template <typename T>
class Range
{

private:

  class iter
  {
  private:
    T x_;

  public:
    iter(T x) : x_(x) { }

    void operator++() {x_++;}

    const T& operator*() const {return x_;}

    bool operator!=(const iter &other) const
    { return x_ != other.x_; }
  };

  T begin_, end_;

public:

  Range(T a, T b) : begin_(a), end_(b) {}
  Range(T n) : begin_(0), end_(n) {}

  iter begin() const {return iter(begin_);}
  iter end  () const {return iter(end_  );}

  const T front() const {return begin_;}
  const T back () const {return end_-1;}
};

} // namespace fd

#endif // _RANGE_HPP_INCLUDED
